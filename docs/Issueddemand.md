# Veit.Abra.Model.Issueddemand
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Issueddemandrow&gt;**](Issueddemandrow.md) | Řádky; kolekce BO Poptávka vydaná - řádek [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**FirmID** | **string** | Firma dodavatele; ID objektu Firma [persistentní položka] | [optional] 
**FirmOfficeID** | **string** | Provozovna firmy dodavatele; ID objektu Provozovna [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba ve firmě dodavatele; ID objektu Osoba [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**CurrRate** | **double?** | Kurz měny [persistentní položka] | [optional] 
**RefCurrRate** | **double?** | Vztažný kurz měny [persistentní položka] | [optional] 
**RequestedAnswerDateDATE** | **DateTimeOffset?** | Požadovaný termín dodání odpovědi [persistentní položka] | [optional] 
**ValidToDateDATE** | **DateTimeOffset?** | Dokdy je nabídka dodavatele platná [persistentní položka] | [optional] 
**TransportationTypeID** | **string** | Způsob dopravy; ID objektu Způsob dopravy [persistentní položka] | [optional] 
**OtherCosts** | **double?** | Odhad ostatních nákladů [persistentní položka] | [optional] 
**LocalOtherCosts** | **double?** | Odhad ostatních nákladů v lokální měně [persistentní položka] | [optional] 
**CommunicationTypeID** | **string** | Způsob komunikace; ID objektu Způsob komunikace [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**RefCurrencyID** | **string** | Ref.měna; ID objektu Měna | [optional] 
**LocalRefCurrencyID** | **string** | Lok.ref.měna; ID objektu Měna | [optional] 
**AddressID** | **string** | Vlastní adresa; ID objektu Adresa [persistentní položka] | [optional] 
**PricePrecision** | **int?** | Zobrazení desetinných míst pro zadávací částky [persistentní položka] | [optional] 
**DemandFailureReasonID** | **string** | Důvod neúspěchu; ID objektu Důvod neúspěchu oslovených dodavatelů [persistentní položka] | [optional] 
**DemandFailureComment** | **string** | Komentář důvodů neúspěchu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

