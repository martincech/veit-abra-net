# Veit.Abra.Model.Calcfield
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CalcChartID** | **string** | Výpočtové schéma; ID objektu Výpočtové schéma [persistentní položka] | [optional] 
**CalcFieldCode** | **int?** | Součet [persistentní položka] | [optional] 
**System** | **bool?** | Syst. [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platí od [persistentní položka] | [optional] 
**Supress** | **bool?** | Potlačit [persistentní položka] | [optional] 
**FieldCode** | **int?** | Sčítanec [persistentní položka] | [optional] 
**CLSID** | **string** | Třída sčítance [persistentní položka] | [optional] 
**Condition** | **string** | Podmínka [persistentní položka] | [optional] 
**Correction** | **string** | Korekce [persistentní položka] | [optional] 
**UserCondition** | **string** | Uživ. podmínka | [optional] 
**UserCorrection** | **string** | Uživ. korekce | [optional] 
**SupressExpression** | **int?** | Použít výraz | [optional] 
**FieldName** | **string** | Název položky-sčítanec | [optional] 
**FieldLabel** | **string** | Popis položky-sčítanec | [optional] 
**SupressExpressionText** | **string** | Použít výraz | [optional] 
**CalcFieldName** | **string** | Název položky-součet | [optional] 
**CalcFieldLabel** | **string** | Popis položky-součet | [optional] 
**CLSIDName** | **string** | Třída sčítance | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

