# Veit.Abra.Model.Assettype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód druhu [persistentní položka] | [optional] 
**Name** | **string** | Druh majetku [persistentní položka] | [optional] 
**DepreciationType** | **int?** | Typ [persistentní položka] | [optional] 
**DepreciationTypeAsText** | **string** | Typ majetku | [optional] 
**AccPresetDefINID** | **string** | Předkontace pro zařazení maj.; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**AccPresetDefDeprecID** | **string** | Předkontace pro odpisy; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**AccPresetDefOUTID** | **string** | Předkontace pro vyřazení; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**AccPresetValueChangeID** | **string** | Předkontace pro změnu ceny; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**TangibleType** | **int?** | Typ-hmotný [persistentní položka] | [optional] 
**TangibleTypeAsText** | **string** | Typ-hmotný | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

