# Veit.Abra.Model.Logstoreposition
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Logstorecontent&gt;**](Logstorecontent.md) | Řádky; kolekce BO Obsah skladové pozice [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**BarCode** | **string** | Čárový kód [persistentní položka] | [optional] 
**Width** | **double?** | Šířka [persistentní položka] | [optional] 
**Height** | **double?** | Výška [persistentní položka] | [optional] 
**Depth** | **double?** | Hloubka [persistentní položka] | [optional] 
**MaxWeight** | **double?** | Nosnost [persistentní položka] | [optional] 
**SizeUnit** | **int?** | Jednotka rozměrů [persistentní položka] | [optional] 
**Frozen** | **bool?** | Mimo provoz [persistentní položka] | [optional] 
**Accessibility** | **int?** | Dostupnost [persistentní položka] | [optional] 
**PositionType** | **int?** | Typ pozice [persistentní položka] | [optional] 
**UsedWeight** | **double?** | Hmotnost obsahu | [optional] 
**UsedSpace** | **double?** | Objem obsahu | [optional] 
**BasicFreeWeight** | **double?** | Volná nosnost [persistentní položka] | [optional] 
**BasicFreeSpace** | **double?** | Volný objem [persistentní položka] | [optional] 
**ReservedForDocType** | **string** | Typ vychystávaného dokladu [persistentní položka] | [optional] 
**ReservedForDocID** | **string** | Vazba na vychystávaný doklad; ID objektu Dokument [persistentní položka] | [optional] 
**LogStorePositionDemands** | [**List&lt;Logstorepositiondemand&gt;**](Logstorepositiondemand.md) | Přednostní naskladnění; kolekce BO Přednostní naskladnění [nepersistentní položka] | [optional] 
**MaxSpace** | **double?** | Prostor | [optional] 
**SizeUnitDesc** | **string** | Jednotka rozměrů - označení | [optional] 
**CapacityUnitDesc** | **string** | Jednotka objemu - označení | [optional] 
**PositionTypeDesc** | **string** | Typ pozice - popis | [optional] 
**InventoryStatus** | **int?** | Blokace inventurou [persistentní položka] | [optional] 
**LastInventoryDATE** | **DateTimeOffset?** | Poslední inventarizace [persistentní položka] | [optional] 
**DateOfEndInventoryDATE** | **DateTimeOffset?** | Konec inventury [persistentní položka] | [optional] 
**BasicUsedWeight** | **double?** | Použitá nosnost [persistentní položka] | [optional] 
**BasicUsedSpace** | **double?** | Použitý objem [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

