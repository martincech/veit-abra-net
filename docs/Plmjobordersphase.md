# Veit.Abra.Model.Plmjobordersphase
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Výrobní příkaz [persistentní položka] | [optional] 
**PhaseID** | **string** | Etapa; ID objektu Etapa [persistentní položka] | [optional] 
**ReleasedAtDATE** | **DateTimeOffset?** | Uvolněna [persistentní položka] | [optional] 
**ReleasedByID** | **string** | Uvolnil; ID objektu Uživatel [persistentní položka] | [optional] 
**FinishedAtDATE** | **DateTimeOffset?** | Ukončena [persistentní položka] | [optional] 
**FinishedByID** | **string** | Ukončil; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

