# Veit.Abra.Model.Holiday
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Holidayrow&gt;**](Holidayrow.md) | Čerpání; kolekce BO Čerpaná dovolená [nepersistentní položka] | [optional] 
**GoOn** | **bool?** | Vytv. v dalším roce [persistentní položka] | [optional] 
**ClaimShorten** | **double?** | Krác. nár. [persistentní položka] | [optional] 
**WorkingRelationID** | **string** | Pracovní poměr; ID objektu Pracovní poměr [persistentní položka] | [optional] 
**HolidayID** | **string** | Druh dov.; ID objektu Druh dovolené [persistentní položka] | [optional] 
**CalendarYear** | **int?** | Kalen. rok [persistentní položka] | [optional] 
**Regular** | **bool?** | Řádná [persistentní položka] | [optional] 
**Claim** | **double?** | Nárok [persistentní položka] | [optional] 
**RestTransfer** | **double?** | Přenos. dní [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platí od [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Platí do [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Priority** | **int?** | Priorita [persistentní položka] | [optional] 
**Payable** | **bool?** | Proplácet [persistentní položka] | [optional] 
**DrawDown** | **double?** | Cel. čerpáno [persistentní položka] | [optional] 
**PreviousRest** | **double?** | Stará dov. [persistentní položka] | [optional] 
**ClaimShortenCorr** | **double?** | Oprava krácení [persistentní položka] | [optional] 
**ClaimShortenComp** | **double?** | Vypočtené krácení [persistentní položka] | [optional] 
**TransferWholeRest** | **bool?** | Přenášet vše [persistentní položka] | [optional] 
**ValidFromMonthDay** | **int?** | Platí od | [optional] 
**ValidToMonthDay** | **int?** | Platí do | [optional] 
**ValidFromMonth** | **int?** | Měsíc platnosti od | [optional] 
**ValidFromDay** | **int?** | Den platnosti od | [optional] 
**ValidFromDate** | **string** | Datum platnosti od | [optional] 
**ValidToMonth** | **int?** | Měsíc platnosti do | [optional] 
**ValidToDay** | **int?** | Den platnosti do | [optional] 
**ValidToDate** | **string** | Datum platnosti do | [optional] 
**EmployeeID** | **string** | Zaměstnanec; ID objektu Zaměstnanec | [optional] 
**EmployeeName** | **string** | Příjmení a jméno | [optional] 
**PERSPersonalNumber** | **string** | os. číslo | [optional] 
**ClaimType** | **int?** | Jednotka nároku [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

