# Veit.Abra.Model.Issueddepositusage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DepositDocumentID** | **string** | Zálohový list; ID objektu Zálohový list vydaný [persistentní položka] | [optional] 
**PaymentDateDATE** | **DateTimeOffset?** | Datum zúčtování [persistentní položka] | [optional] 
**AccDateDATE** | **DateTimeOffset?** | Datum účtování [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**Amount** | **double?** | Celková částka [persistentní položka] | [optional] 
**LocalAmount** | **double?** | Celk. částka lok. [persistentní položka] | [optional] 
**PAmount** | **double?** | Platba [persistentní položka] | [optional] 
**PDocumentID** | **string** | Placený doklad; ID objektu Dokument [persistentní položka] | [optional] 
**PDocumentType** | **string** | Typ plac.dokl. [persistentní položka] | [optional] 
**PDisKind** | **int?** | Rozdělení platby [persistentní položka] | [optional] 
**AccPresetDefID** | **string** | Předkontace; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna | [optional] 
**AccDocQueueID** | **string** | Účetní řada; ID objektu Účetní řada dokladů [persistentní položka] | [optional] 
**SourceGroupIdenticalID** | **string** | Souv. zdrojových skupin; ID objektu Souvislost zdrojových skupin [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

