# Veit.Abra.Model.Serviceassemblycoopreceiving
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Montážní list [persistentní položka] | [optional] 
**ReturnDateDATE** | **DateTimeOffset?** | Datum příjmu [persistentní položka] | [optional] 
**OutgoingTransferID** | **string** | Převodka výdej z koop.; ID objektu Převodka výdej [persistentní položka] | [optional] 
**BillOfDeliveryID** | **string** | Dodací list; ID objektu Dodací list [persistentní položka] | [optional] 
**ReceiptCardID** | **string** | Příjemka; ID objektu Příjemka [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

