# Veit.Abra.Model.Dataprotectiondefinition
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**IsActive** | **bool?** | Aktivní [persistentní položka] | [optional] 
**PurposeID** | **string** | Důvod ochrany dat; ID objektu Důvod ochrany dat [persistentní položka] | [optional] 
**SourceID** | **string** | Zdroj povolení ke zpracování dat; ID objektu Zdroj povolení ke zpracování dat [persistentní položka] | [optional] 
**SolverID** | **string** | Řešitel; ID objektu Uživatel [persistentní položka] | [optional] 
**LocationID** | **string** | Umístění povolení ke zpracování dat; ID objektu Umístění povolení ke zpracování dat [persistentní položka] | [optional] 
**DataControllerType** | **int?** | Typ správce | [optional] 
**DataControllerFirmID** | **string** | Správce - firma; ID objektu Firma [persistentní položka] | [optional] 
**DataControllerPersonID** | **string** | Správce - osoba; ID objektu Osoba [persistentní položka] | [optional] 
**DataProtectionDriverID** | **string** | Ovladač; ID objektu Ovladač ochrany dat [persistentní položka] | [optional] 
**ProtectedFields** | [**List&lt;Dataprotectionprotectedfield&gt;**](Dataprotectionprotectedfield.md) | Chráněné položky; kolekce BO Chráněná položka [nepersistentní položka] | [optional] 
**AccessRights** | [**List&lt;Dataprotectionaccessright&gt;**](Dataprotectionaccessright.md) | Oprávnění k chráněným položkám; kolekce BO Oprávnění k chráněným položkám [nepersistentní položka] | [optional] 
**DataProcessors** | [**List&lt;Dataprotectiondataprocessor&gt;**](Dataprotectiondataprocessor.md) | Zpracovatel dat; kolekce BO Zpracovatel dat [nepersistentní položka] | [optional] 
**DataCategory** | **int?** | Kategorie dat [persistentní položka] | [optional] 
**PatternID** | **string** | Vzor definice ochrany dat [persistentní položka] | [optional] 
**UniqueID** | **string** | Jedinečný identifikátor [persistentní položka] | [optional] 
**ProcessSubjectRights** | **bool?** | Zpracovávat práva subjektů [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

