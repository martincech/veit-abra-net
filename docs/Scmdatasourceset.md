# Veit.Abra.Model.Scmdatasourceset
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Scmdatasourcesetrow&gt;**](Scmdatasourcesetrow.md) | Řádky; kolekce BO Řádek sady datových zdrojů [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**System** | **bool?** | Systémová [persistentní položka] | [optional] 
**SettingID** | **string** | Nastavení; ID objektu Nastavení SCM [persistentní položka] | [optional] 
**ColumnsEvolution** | [**List&lt;Scmdatasourcesetcolumnevolution&gt;**](Scmdatasourcesetcolumnevolution.md) | Sloupce vývoje; kolekce BO Řádek definice sloupců vývoje [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

