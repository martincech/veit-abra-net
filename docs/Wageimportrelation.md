# Veit.Abra.Model.Wageimportrelation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Definice převodů dat [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**DestinationField** | **int?** | Do položky [persistentní položka] | [optional] 
**SourceField** | **string** | Z položky [persistentní položka] | [optional] 
**WithExpression** | **bool?** | Výrazem [persistentní položka] | [optional] 
**Expression** | **string** | Výraz [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

