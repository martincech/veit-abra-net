# Veit.Abra.Model.Serviceassemblyform
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ServiceDocumentID** | **string** | Servisní list; ID objektu Servisní list [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**Cooperation** | **bool?** | Kooperace [persistentní položka] | [optional] 
**CooperationFirmID** | **string** | Kooperace - firma; ID objektu Firma [persistentní položka] | [optional] 
**CooperationFirmOfficeID** | **string** | Kooperace - provozovna; ID objektu Provozovna [persistentní položka] | [optional] 
**CooperationPersonID** | **string** | Kooperace - osoba; ID objektu Osoba [persistentní položka] | [optional] 
**CooperationSurcharge** | **double?** | Kooperace - marže [persistentní položka] | [optional] 
**ServiceWorkSpaceID** | **string** | Dílna/Pracoviště; ID objektu Servisní dílna/pracoviště [persistentní položka] | [optional] 
**AssemblyState** | **int?** | Stav [persistentní položka] | [optional] 
**StartDateDATE** | **DateTimeOffset?** | Datum zahájení [persistentní položka] | [optional] 
**EndDateDATE** | **DateTimeOffset?** | Datum ukončení [persistentní položka] | [optional] 
**ResponsibleRoleID** | **string** | Role odpovědné osoby; ID objektu Role [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**CooperationState** | **int?** | Stav kooperace [persistentní položka] | [optional] 
**CooperationOutDateDATE** | **DateTimeOffset?** | Datum výdeje [persistentní položka] | [optional] 
**CooperationBackPlanDateDATE** | **DateTimeOffset?** | Plán.datum návratu [persistentní položka] | [optional] 
**CooperationBackDateDATE** | **DateTimeOffset?** | Datum návratu [persistentní položka] | [optional] 
**CoopTransferOutID** | **string** | Převodka do kooperace; ID objektu Převodka výdej [persistentní položka] | [optional] 
**CoopTransferBackID** | **string** | Převodka z kooperace; ID objektu Skladový doklad [persistentní položka] | [optional] 
**AllowIntermediation** | **bool?** | Umožnit přefakturaci [persistentní položka] | [optional] 
**Rows** | [**List&lt;Serviceassemblyformrow&gt;**](Serviceassemblyformrow.md) | Řádky; kolekce BO Řádek montážního listu [nepersistentní položka] | [optional] 
**AssemblyStateAsText** | **string** | Stav | [optional] 
**TotalPriceWithoutVAT** | **double?** | Celk.cena bez DPH | [optional] 
**TotalPriceWithVAT** | **double?** | Celk.cena s DPH | [optional] 
**MaterialOnStockState** | **double?** | Pokrytí skladem | [optional] 
**MaterialOutStockingState** | **double?** | Vyskladněno | [optional] 
**CooperationReturn** | [**List&lt;Serviceassemblycoopreceiving&gt;**](Serviceassemblycoopreceiving.md) | Návraty z kooperace; kolekce BO Vrácení zpět z koop. pro ML [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

