# Veit.Abra.Model.Wikifile
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**BlobData** | **byte[]** | Data [persistentní položka] | [optional] 
**SpaceID** | **string** | Prostor; ID objektu Wiki prostor [persistentní položka] | [optional] 
**Path** | **string** | Cesta [persistentní položka] | [optional] 
**IsImage** | **bool?** | Obrázek [persistentní položka] | [optional] 
**FileSize** | **int?** | Velikost souboru [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

