# Veit.Abra.Model.Fastsearchdefinition
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**BOCLSID** | **string** | Třída [persistentní položka] | [optional] 
**TableName** | **string** | Tabulka [persistentní položka] | [optional] 
**Indexes** | [**List&lt;Fastsearchdefinitionindex&gt;**](Fastsearchdefinitionindex.md) | Indexy; kolekce BO Fulltextové hledání - definice položky [nepersistentní položka] | [optional] 
**DBTableName** | **string** | Db. tabulka | [optional] 
**BOName** | **string** | Název třídy | [optional] 
**SiteName** | **string** | Název agendy | [optional] 
**UTF** | **bool?** | Unicode [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

