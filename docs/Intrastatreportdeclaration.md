# Veit.Abra.Model.Intrastatreportdeclaration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Podání INTRASTAT [persistentní položka] | [optional] 
**DeclarationID** | **string** | Číslo pobočky [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**ReferencePeriod** | **string** | Vykazované období [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum a čas vygenerování [persistentní položka] | [optional] 
**FlowCode** | **int?** | Směr transakce [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**ItemsCount** | **int?** | Počet položek | [optional] 
**DeletingItemsCount** | **int?** | Počet mazacích položek | [optional] 
**FlowCodeText** | **string** | Směr transakce - text | [optional] 
**IsNegative** | **bool?** | Negativní | [optional] 
**IntrastatReportItems** | [**List&lt;Annualclearingcountoff&gt;**](Annualclearingcountoff.md) | Položky deklarace; kolekce BO Odpočet k roč.zúčtování [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

