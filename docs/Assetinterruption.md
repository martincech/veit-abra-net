# Veit.Abra.Model.Assetinterruption
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Karta majetku [persistentní položka] | [optional] 
**PeriodID** | **string** | Období přer.; ID objektu Období [persistentní položka] | [optional] 
**AccInterruptToo** | **bool?** | I účetní? [persistentní položka] | [optional] 
**AccInterruptTooAsText** | **string** | I účetní? | [optional] 
**CreatedDateDATE** | **DateTimeOffset?** | Datum [persistentní položka] | [optional] 
**CreatedByID** | **string** | Kdo zadal; ID objektu Uživatel [persistentní položka] | [optional] 
**TaxWithAccounted** | **bool?** | Přerušit i zaúčtované [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

