# Veit.Abra.Model.Period
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Code** | **string** | Zkratka [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**DateFromDATE** | **DateTimeOffset?** | Od [persistentní položka] | [optional] 
**DateToDATE** | **DateTimeOffset?** | Do [persistentní položka] | [optional] 
**Closing** | **bool?** | Závěrka [persistentní položka] | [optional] 
**Beginnings** | **bool?** | Počátky [persistentní položka] | [optional] 
**SequenceNumber** | **int?** | Pořadí [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

