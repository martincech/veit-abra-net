# Veit.Abra.Model.Documentcontent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**RevidedID** | **string** | ID revidovaného objektu; ID objektu Obsah dokumentu | [optional] 
**RevisionDescription** | **string** | Popis revize | [optional] 
**RevisionDateDATE** | **DateTimeOffset?** | Datum revize | [optional] 
**RevisionAuthorID** | **string** | Autor revize; ID objektu Uživatel | [optional] 
**Revision** | **int?** | Číslo revize | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Dokument [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectionTimeDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreationTimeDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**DataID** | [**Documentdata**](Documentdata.md) |  | [optional] 
**FileName** | **string** | Název souboru [persistentní položka] | [optional] 
**Specification** | **int?** | Specifikace [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**Compressed** | **bool?** | Komprimován | [optional] 
**ExternalFile** | **bool?** | Externí soubor [persistentní položka] | [optional] 
**PathAndFileName** | **string** | Cesta k externímu souboru [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

