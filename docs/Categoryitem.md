# Veit.Abra.Model.Categoryitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ItemType** | **int?** | Typ [persistentní položka] | [optional] 
**UserFieldDef2ID** | **string** | Uživatelská položka; ID objektu Definovatelná položka [persistentní položka] | [optional] 
**CategoryItemGroupID** | **string** | Skupina; ID objektu Skupina kategorizačních údajů [persistentní položka] | [optional] 
**Expression** | **string** | Výraz [persistentní položka] | [optional] 
**FieldName** | **string** | Jméno položky [persistentní položka] | [optional] 
**FieldLabel** | **string** | Popis položky | [optional] 
**FieldHint** | **string** | Popiska položky | [optional] 
**ItemTypeTXT** | **string** | Typ | [optional] 
**DataTypeTXT** | **string** | Datový typ | [optional] 
**FieldCode** | **int?** | Číslo položky | [optional] 
**SQLQuery** | **string** | Definice [persistentní položka] | [optional] 
**DataType** | **int?** | Datový typ [persistentní položka] | [optional] 
**DataSize** | **int?** | Velikost [persistentní položka] | [optional] 
**DecimalPlaces** | **int?** | Počet des.míst [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

