# Veit.Abra.Model.Plmjoinputitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**PLMMIPLMaterialDistribution** | [**List&lt;Plmmiplmaterialdistribution&gt;**](Plmmiplmaterialdistribution.md) | Výdejky; kolekce BO VP - výdej materiálu [nepersistentní položka] | [optional] 
**PLMMIPLRMaterialDistribution** | [**List&lt;Plmmiplrmaterialdistribution&gt;**](Plmmiplrmaterialdistribution.md) | Vratky; kolekce BO VP - vrácení materiálu [nepersistentní položka] | [optional] 
**PLMJOInputItemReservations** | [**List&lt;Plmjoinputitemreservation&gt;**](Plmjoinputitemreservation.md) | Rezervace; kolekce BO VP - rezervace [nepersistentní položka] | [optional] 
**Replaceable** | **bool?** | Aut. náhrada [persistentní položka] | [optional] 
**RealStoreCardID** | **string** | Skutečně použitý materiál; ID objektu Skladová karta [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství v ev. jednotkách [persistentní položka] | [optional] 
**Qunit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství | [optional] 
**AllowMix** | **bool?** | Mix [persistentní položka] | [optional] 
**ReservedQuantity** | **double?** | Rezervováno | [optional] 
**ReservedUnitQuantity** | **double?** | Rezervováno v ev.jedn. | [optional] 
**DistributedQuantity** | **double?** | Vydáno v ev.jedn. | [optional] 
**DistributedUnitQuantity** | **double?** | Vydáno | [optional] 
**PhaseID** | **string** | Etapa; ID objektu Etapa [persistentní položka] | [optional] 
**Description** | **string** | Označení [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**RecordsSN** | **bool?** | Evidovat SČ [persistentní položka] | [optional] 
**SupposedStoreID** | **string** | Předpokládaný sklad; ID objektu Sklad [persistentní položka] | [optional] 
**WastePercentage** | **double?** | Ztráty [%] [persistentní položka] | [optional] 
**QuantityRounding** | **int?** | Zaokrouhlení množství [persistentní položka] | [optional] 
**DemandStatus** | **int?** | Stav poptávky [persistentní položka] | [optional] 
**StoreQuantityWithReplacements** | **double?** | Stav skladu | [optional] 
**ConfirmationDifferences** | [**List&lt;Plmjoinputitemconfdiff&gt;**](Plmjoinputitemconfdiff.md) | Potvrzení rozdílů; kolekce BO VP - potvrzení rozdílu materiálu při ocenění [nepersistentní položka] | [optional] 
**OwnerID** | **string** | Vlastník; ID objektu Uzel stromu výrobního příkazu [persistentní položka] | [optional] 
**DoNotMultiply** | **bool?** | Konst. mn. [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

