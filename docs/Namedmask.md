# Veit.Abra.Model.Namedmask
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Mask** | **string** | Maska [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**MaskType** | **int?** | Typ masky [persistentní položka] | [optional] 
**IsBasicMask** | **bool?** | Základní [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

