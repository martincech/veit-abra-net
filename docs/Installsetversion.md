# Veit.Abra.Model.Installsetversion
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Instalační sada [persistentní položka] | [optional] 
**Version** | **int?** | Číslo verze [persistentní položka] | [optional] 
**VersionAuthor** | **string** | Autor verze [persistentní položka] | [optional] 
**VersionDateDATE** | **DateTimeOffset?** | Datum verze [persistentní položka] | [optional] 
**VersionDescription** | **string** | Popis [persistentní položka] | [optional] 
**VersionItems** | [**List&lt;Installsetversion&gt;**](Installsetversion.md) | Položka instalační sady ve verzi; kolekce BO Verze instalační sady [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

