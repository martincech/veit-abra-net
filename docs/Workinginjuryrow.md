# Veit.Abra.Model.Workinginjuryrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Pracovní úraz [persistentní položka] | [optional] 
**WagePeriodID** | **string** | Mzdové období; ID objektu Mzdové období [persistentní položka] | [optional] 
**SSicknessComp** | **double?** | Náhrada po dobu prac. neschop. [persistentní položka] | [optional] 
**SAfterSicknessComp** | **double?** | Náhrada po prac. neschopnosti [persistentní položka] | [optional] 
**STherapyCostComp** | **double?** | Náhrada nákladů léčení [persistentní položka] | [optional] 
**SDamagesComp** | **double?** | Náhrada věcné škody [persistentní položka] | [optional] 
**SCureCostComp** | **double?** | Náhrada nákladů léčení [persistentní položka] | [optional] 
**SFuneralComp** | **double?** | Náhrada pohřbu [persistentní položka] | [optional] 
**SResidualFeedComp** | **double?** | Náhrada na výživu pozůstalých [persistentní položka] | [optional] 
**SResidualOneCaseComp** | **double?** | Jednorázové odškod. pozůst. [persistentní položka] | [optional] 
**STotalComp** | **double?** | Celková částka [persistentní položka] | [optional] 
**SResidualDamagesComp** | **double?** | Náhrada věcné škody dědicům [persistentní položka] | [optional] 
**SPainAndSocialProblemsComp** | **double?** | Náhr.za bolest a ztíž.uplat. [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

