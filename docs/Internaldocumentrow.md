# Veit.Abra.Model.Internaldocumentrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Interní doklad [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**TAmount** | **double?** | Celkem [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Celkem lokálně [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**DebitAccountID** | **string** | Účet MD; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**DebitDivisionID** | **string** | Středisko MD; ID objektu Středisko [persistentní položka] | [optional] 
**DebitBusOrderID** | **string** | Zakázka MD; ID objektu Zakázka [persistentní položka] | [optional] 
**DebitBusTransactionID** | **string** | O.případ MD; ID objektu Obchodní případ [persistentní položka] | [optional] 
**DebitBusProjectID** | **string** | Projekt MD; ID objektu Projekt [persistentní položka] | [optional] 
**CreditAccountID** | **string** | Účet D; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**CreditDivisionID** | **string** | Středisko D; ID objektu Středisko [persistentní položka] | [optional] 
**CreditBusOrderID** | **string** | Zakázka D; ID objektu Zakázka [persistentní položka] | [optional] 
**CreditBusTransactionID** | **string** | O.případ D; ID objektu Obchodní případ [persistentní položka] | [optional] 
**CreditBusProjectID** | **string** | Projekt D; ID objektu Projekt [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

