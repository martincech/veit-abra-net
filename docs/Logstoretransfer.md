# Veit.Abra.Model.Logstoretransfer
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Logstoretransferrow&gt;**](Logstoretransferrow.md) | Řádky; kolekce BO Přesun mezi pozicemi - řádek [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**DocumentType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**StoreDocumentType** | **string** | Typ skl.dokladu [persistentní položka] | [optional] 
**StoreDocumentID** | **string** | Skl.doklad; ID objektu Dokument [persistentní položka] | [optional] 
**StoreGatewayID** | **string** | Místo naskladnění/vyskladnění; ID objektu Nasklad. a vysklad. místo [persistentní položka] | [optional] 
**Executed** | **bool?** | Provedeno [persistentní položka] | [optional] 
**NoStoreDocument** | **bool?** | Jen polohovací [persistentní položka] | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 
**ExternalNumber** | **string** | Externí číslo [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**StoreManID** | **string** | Skladník; ID objektu Osoba [persistentní položka] | [optional] 
**ReservedForDocType** | **string** | Typ svázaného dokladu [persistentní položka] | [optional] 
**ReservedForDocID** | **string** | Vazba na vychystávaný doklad; ID objektu Dokument [persistentní položka] | [optional] 
**MasterDocCLSID** | **string** | Třída nadřízeného dokladu [persistentní položka] | [optional] 
**MasterDocumentID** | **string** | Nadřízený doklad; ID objektu Dokument [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

