# Veit.Abra.Model.Unreliablefirmlogfirmbankaccount
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Kontrola nespolehlivosti plátce DPH - Firma [persistentní položka] | [optional] 
**BankAccount** | **string** | Číslo bankovního účtu [persistentní položka] | [optional] 
**PublishedDateDATE** | **DateTimeOffset?** | Datum zveřejnění [persistentní položka] | [optional] 
**PublishedEndDateDATE** | **DateTimeOffset?** | Datum konce zveřejnění [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

