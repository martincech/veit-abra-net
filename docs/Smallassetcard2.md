# Veit.Abra.Model.Smallassetcard2
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Karta drobného majetku [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**InventoryNr** | **string** | Inventární číslo [persistentní položka] | [optional] 
**ResponsibleID** | **string** | Odpovědná osoba; ID objektu Odpovědná osoba [persistentní položka] | [optional] 
**Quantity** | **int?** | Počet [persistentní položka] | [optional] 
**EAN** | **string** | EAN [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

