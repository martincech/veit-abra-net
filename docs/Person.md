# Veit.Abra.Model.Person
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**LastName** | **string** | Příjmení [persistentní položka] | [optional] 
**FirstName** | **string** | Jméno [persistentní položka] | [optional] 
**Title** | **string** | Titul [persistentní položka] | [optional] 
**Grade** | **string** | Popis pozice [persistentní položka] | [optional] 
**BirthNumber** | **string** | Rodné číslo [persistentní položka] | [optional] 
**IDCardNumber** | **string** | Číslo OP/pasu [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**Comment** | **string** | Poznámka | [optional] 
**AddressID** | [**Address**](Address.md) |  | [optional] 
**FullName** | **string** | Jméno + Příjmení | [optional] 
**IsEmployee** | **bool?** | Zaměstnanec/Pracovník [persistentní položka] | [optional] 
**IsOwned** | **bool?** | Vlastněná [persistentní položka] | [optional] 
**PersonalNumber** | **string** | Osob.číslo [persistentní položka] | [optional] 
**PictureID** | [**Picture**](Picture.md) |  | [optional] 
**Suffix** | **string** | Titul za jménem [persistentní položka] | [optional] 
**LastAndFirstName** | **string** | Příjmení + Jméno | [optional] 
**DateOfBirthDATE** | **DateTimeOffset?** | Datum narození [persistentní položka] | [optional] 
**InsolvencyCheckResult** | **int?** | Stav insolvence | [optional] 
**InsolvencyCheckResultAsText** | **string** | Stav insolvence textově | [optional] 
**InsolvencyLastCheckDateTimeDATE** | **DateTimeOffset?** | Datum a čas posledního ověření insolvence | [optional] 
**InsolvencyLastCheckDateTime** | **DateTimeOffset?** | Datum a čas posledního ověření insolvence | [optional] 
**GDPRValiditySuspended** | **bool?** | Omezení zpracování (GDPR) [persistentní položka] | [optional] 
**SalutationTitle** | **string** | Oslovení [persistentní položka] | [optional] 
**SalutationName** | **string** | Jméno v 5. pádu [persistentní položka] | [optional] 
**CommercialsAgreement** | **int?** | Souhlas s reklamou [persistentní položka] | [optional] 
**CommercialsAgreementDesc** | **string** | Souhlas s reklamou - popis | [optional] 
**Pictures** | [**List&lt;Personpicture&gt;**](Personpicture.md) | Obrázky; kolekce BO Obrázek k osobě [nepersistentní položka] | [optional] 
**X_PURCHASE** | **bool?** | Nákupčí [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

