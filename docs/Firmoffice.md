# Veit.Abra.Model.Firmoffice
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Firma [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**AddressID** | [**Address**](Address.md) |  | [optional] 
**MassCorrespondence** | **bool?** | Hromadná korespondence [persistentní položka] | [optional] 
**Name** | **string** | Název provozovny [persistentní položka] | [optional] 
**SynchronizeAddress** | **bool?** | Synchronizovat [persistentní položka] | [optional] 
**Credit** | **double?** | Kredit [persistentní položka] | [optional] 
**CheckCredit** | **bool?** | Sledovat kredit [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**DealerCategoryID** | **string** | Dealerská třída; ID objektu Dealerská třída [persistentní položka] | [optional] 
**OfficeUniqueID** | **string** | Interní identifikátor provozovny [persistentní položka] | [optional] 
**LastChildID** | **string** | Potomek; ID objektu Provozovna | [optional] 
**OfficeIdentNumber** | **string** | IČP [persistentní položka] | [optional] 
**ElectronicAddressID** | [**Address**](Address.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

