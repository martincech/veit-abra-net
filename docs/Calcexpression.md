# Veit.Abra.Model.Calcexpression
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Výpočtové schéma [persistentní položka] | [optional] 
**FieldCode** | **int?** | Kód [persistentní položka] | [optional] 
**TypeOfSum** | **int?** | Součet [persistentní položka] | [optional] 
**Expression** | **string** | Výraz [persistentní položka] | [optional] 
**Condition** | **string** | Podmínka [persistentní položka] | [optional] 
**Rounding** | **int?** | Zaokrouhlení [persistentní položka] | [optional] 
**StdActionCode** | **int?** | Std. akce [persistentní položka] | [optional] 
**StdActionExecute** | **int?** | Kde provést [persistentní položka] | [optional] 
**CheckExpression** | **string** | Kontrola [persistentní položka] | [optional] 
**ErrorSeverity** | **int?** | Typ chyby [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platí od [persistentní položka] | [optional] 
**UserExpression** | **string** | Uživ. výraz | [optional] 
**UserCondition** | **string** | Uživ. podmínka | [optional] 
**UserExplanation** | **string** | Vysvětlení uživ. výrazu | [optional] 
**UserCheckExpression** | **string** | Uživ. kontrola | [optional] 
**SupressExpression** | **int?** | Použít výraz | [optional] 
**FieldName** | **string** | Název položky | [optional] 
**FieldLabel** | **string** | Popis položky | [optional] 
**TypeOfSumText** | **string** | Součet | [optional] 
**StdActionExecuteText** | **string** | Kde provést | [optional] 
**ErrorSeverityText** | **string** | Typ chyby | [optional] 
**SupressExpressionText** | **string** | Použít výraz | [optional] 
**StdActionName** | **string** | Std. akce | [optional] 
**RoundingText** | **string** | Zaokrouhlení | [optional] 
**ErrorMessage** | **string** | Zpráva [persistentní položka] | [optional] 
**StdActionParams** | **string** | Parametry std. akce [persistentní položka] | [optional] 
**System** | **bool?** | Syst. definice [persistentní položka] | [optional] 
**Explanation** | **string** | Vysvětlení [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

