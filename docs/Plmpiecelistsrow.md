# Veit.Abra.Model.Plmpiecelistsrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**RevidedID** | **string** | ID revidovaného objektu; ID objektu Kusovník - řádek | [optional] 
**RevisionDescription** | **string** | Popis revize | [optional] 
**RevisionDateDATE** | **DateTimeOffset?** | Datum revize | [optional] 
**RevisionAuthorID** | **string** | Autor revize; ID objektu Uživatel | [optional] 
**Revision** | **int?** | Číslo revize | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Kusovník [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pozice [persistentní položka] | [optional] 
**Replaceable** | **bool?** | Aut. náhrada [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta pozice; ID objektu Skladová karta [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství v ev.jedn. [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**Qunit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství | [optional] 
**AllowMix** | **bool?** | MIX [persistentní položka] | [optional] 
**Issue** | **int?** | Výdej [persistentní položka] | [optional] 
**PhaseID** | **string** | Etapa; ID objektu Etapa [persistentní položka] | [optional] 
**Description** | **string** | Označení [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**CostingPrice** | **double?** | Cena pro kalkulaci [persistentní položka] | [optional] 
**CostingMethod** | **int?** | Způsob výpočtu ceny pro kalkulaci [persistentní položka] | [optional] 
**RecordsSN** | **bool?** | Evidovat SČ [persistentní položka] | [optional] 
**SupposedStoreID** | **string** | Předpokládaný sklad; ID objektu Sklad [persistentní položka] | [optional] 
**WastePercentage** | **double?** | Ztráty [%] [persistentní položka] | [optional] 
**QuantityRounding** | **int?** | Zaokrouhlení množství [persistentní položka] | [optional] 
**DoNotMultiply** | **bool?** | Konst. mn. [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

