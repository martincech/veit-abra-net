# Veit.Abra.Model.Crmactivityforoverview
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ActQueueID** | **string** | Řada aktivit; ID objektu Řada aktivit [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**SourceID** | **string** | Odkaz na aktivitu; ID objektu Aktivita [persistentní položka] | [optional] 
**ActivityTypeID** | **string** | Typ aktivity; ID objektu Typ aktivit [persistentní položka] | [optional] 
**ActivityProcessID** | **string** | Proces aktivity; ID objektu Proces aktivit [persistentní položka] | [optional] 
**Status** | **int?** | Stav [persistentní položka] | [optional] 
**Priority** | **int?** | Priorita [persistentní položka] | [optional] 
**IsPrivate** | **bool?** | Neveřejná [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**SolverRoleID** | **string** | Role řešitele; ID objektu Role [persistentní položka] | [optional] 
**SolverUserID** | **string** | Řešitel; ID objektu Uživatel [persistentní položka] | [optional] 
**SheduledStartDATE** | **DateTimeOffset?** | Čas zahájení [persistentní položka] | [optional] 
**SheduledEndDATE** | **DateTimeOffset?** | Čas dokončení [persistentní položka] | [optional] 
**SheduledDurationDATE** | **DateTimeOffset?** | Doba trvání | [optional] 
**RealStartDATE** | **DateTimeOffset?** | Skutečný čas zahájení [persistentní položka] | [optional] 
**RealEndDATE** | **DateTimeOffset?** | Skutečný čas dokončení [persistentní položka] | [optional] 
**RealDurationDATE** | **DateTimeOffset?** | Skutečná doba trvání | [optional] 
**Subject** | **string** | Předmět [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**Answer** | **string** | Odpověď [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | O.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**ProductID** | **string** | Produkt; ID objektu Produkt [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Čas vytvoření [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Čas poslední opravy [persistentní položka] | [optional] 
**ActivityAreaID** | **string** | Oblast aktivit; ID objektu Oblast aktivity [persistentní položka] | [optional] 
**ProcessIsRequired** | **bool?** | Proces je povinný [persistentní položka] | [optional] 
**BlockActivityArea** | **bool?** | Nepovolit editaci oblasti aktivity | [optional] 
**BlockActivityType** | **bool?** | Nepovolit editaci typu aktivity | [optional] 
**ResolvedByID** | **string** | Vyřešil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**FirmOfficeID** | **string** | Provozovna; ID objektu Provozovna [persistentní položka] | [optional] 
**ScheduledWorkingTimeInHours** | **double?** | Prac.čas plánovaný | [optional] 
**RealWorkingTimeInHours** | **double?** | Prac.čas skutečný | [optional] 
**ScheduledOverTime** | **double?** | Plánovaný přesčas [persistentní položka] | [optional] 
**RealOverTime** | **double?** | Skutečný přesčas [persistentní položka] | [optional] 
**ScheduledTotalWorkingTime** | **double?** | Celk.prac.čas plánovaný | [optional] 
**RealTotalWorkingTime** | **double?** | Celk.prac.čas skutečný | [optional] 
**PictureID** | [**Picture**](Picture.md) |  | [optional] 
**FinancialVolume** | **double?** | Finanční objem [persistentní položka] | [optional] 
**NextContactDATE** | **DateTimeOffset?** | Datum příštího kontaktu [persistentní položka] | [optional] 
**PipeLineStatusID** | **string** | Pravděpodobnost úspěchu; ID objektu Stav v pipe-line [persistentní položka] | [optional] 
**ProgressID** | **string** | Krok procesu; ID objektu Krok procesu [persistentní položka] | [optional] 
**TradeDateDATE** | **DateTimeOffset?** | Plánované datum uzavř. obchodu [persistentní položka] | [optional] 
**Pictures** | [**List&lt;Crmactivitypicture&gt;**](Crmactivitypicture.md) | Obrázky; kolekce BO Obrázek k CRM aktivitě [nepersistentní položka] | [optional] 
**Participants** | [**List&lt;Crmactivityparticipants&gt;**](Crmactivityparticipants.md) | Účastníci; kolekce BO Účastník na CRM Aktivitě [nepersistentní položka] | [optional] 
**TimeRecords** | [**List&lt;Crmactivitytimerecord&gt;**](Crmactivitytimerecord.md) | Evidence času; kolekce BO Evidence času k CRM Aktivitě [nepersistentní položka] | [optional] 
**Contacts** | [**List&lt;Crmactivitycontact&gt;**](Crmactivitycontact.md) | Kontakty; kolekce BO Adresáti [nepersistentní položka] | [optional] 
**ContactNote** | **string** | Poznámka kontaktu [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

