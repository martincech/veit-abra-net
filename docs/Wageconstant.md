# Veit.Abra.Model.Wageconstant
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Wageconstantvalue&gt;**](Wageconstantvalue.md) | Hodnoty; kolekce BO Hodnota globální proměnné [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**System** | **bool?** | Orig. [persistentní položka] | [optional] 
**FieldDataType** | **int?** | Datový typ [persistentní položka] | [optional] 
**EditMethod** | **int?** | Editace [persistentní položka] | [optional] 
**Enumeration** | **string** | Výčet [persistentní položka] | [optional] 
**FieldDataTypeText** | **string** | Datový typ | [optional] 
**CodeWithPrefix** | **string** | Prefix, kód | [optional] 
**Prefix** | **string** | Prefix | [optional] 
**AllowChangeHistory** | **bool?** | Lze měnit historii [persistentní položka] | [optional] 
**ForWizard** | **bool?** | Nabízet v průvodci [persistentní položka] | [optional] 
**IsLocalAmount** | **bool?** | Je v lokální měně [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

