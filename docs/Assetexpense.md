# Veit.Abra.Model.Assetexpense
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**AssetCardID** | **string** | Karta majetku; ID objektu Karta majetku [persistentní položka] | [optional] 
**DocType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**DocID** | **string** | Doklad; ID objektu Dokument [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dokladu [persistentní položka] | [optional] 
**DateDATE** | **DateTimeOffset?** | Datum připojení [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

