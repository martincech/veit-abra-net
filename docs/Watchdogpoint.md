# Veit.Abra.Model.Watchdogpoint
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Notpositionedrowbusinessobject&gt;**](Notpositionedrowbusinessobject.md) | Řádky; kolekce BO Řádek bez pořadí [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**System** | **bool?** | Syst. [persistentní položka] | [optional] 
**InternalName** | **string** | Interní název [persistentní položka] | [optional] 
**ScriptPackage** | **string** | Balíček skriptů [persistentní položka] | [optional] 
**LibraryName** | **string** | Knihovna [persistentní položka] | [optional] 
**AgendaRows** | [**List&lt;Watchdogpointagenda&gt;**](Watchdogpointagenda.md) | Agendy; kolekce BO Agenda kontrolního bodu [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

