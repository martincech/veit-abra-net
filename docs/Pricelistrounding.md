# Veit.Abra.Model.Pricelistrounding
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Ceník [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**AmountTo** | **double?** | Hraniční částka [persistentní položka] | [optional] 
**PriceRounding** | **int?** | Zp. zaokrouhlení [persistentní položka] | [optional] 
**ConstantToAdd** | **double?** | Konstanta k přičtení [persistentní položka] | [optional] 
**RoundingType** | **int?** | Typ zaokrouhlení | [optional] 
**RoundingBase** | **double?** | Zaokrouhlit na | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

