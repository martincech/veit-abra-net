# Veit.Abra.Model.Payreminderpar
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Payreminderparrow&gt;**](Payreminderparrow.md) | Řádky; kolekce BO Parametry automatických upomínek - upomínky [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Popis [persistentní položka] | [optional] 
**EmailAccountID** | **string** | E-mail; ID objektu E-mailový účet [persistentní položka] | [optional] 
**MinAmount** | **double?** | Min. částka [persistentní položka] | [optional] 
**NumOfDaysRemand** | **int?** | Povolený odklad [persistentní položka] | [optional] 
**InvoiceReports** | [**List&lt;Payreminderparinvoicereport&gt;**](Payreminderparinvoicereport.md) | Kolekce reportů faktur vydaných; kolekce BO Parametry automatických upomínek - reporty faktur vydaných [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

