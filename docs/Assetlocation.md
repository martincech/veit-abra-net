# Veit.Abra.Model.Assetlocation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Místo [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**ParentID** | **string** | Nadřízené místo (ID); ID objektu Umístění majetku [persistentní položka] | [optional] 
**DisplayParent** | **string** | Nadřízené místo | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

