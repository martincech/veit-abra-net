# Veit.Abra.Model.Assetdiscard
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**AccPresetDefID** | **string** | Předkontace; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**AccDateDATE** | **DateTimeOffset?** | Datum účt. [persistentní položka] | [optional] 
**AccDocQueueID** | **string** | Účetní řada; ID objektu Účetní řada dokladů [persistentní položka] | [optional] 
**AccountingType** | **int?** | Jak účtovat | [optional] 
**IsAccounted** | **bool?** | Účtováno | [optional] 
**Dirty** | **bool?** | Zakázané přepočítání | [optional] 
**AssetCardType** | **int?** | Typ karty | [optional] 
**AssetCardID** | **string** | Karta majetku; ID objektu Karta majetku [persistentní položka] | [optional] 
**TaxBasePriceAmount** | **double?** | Daň.vstup.cena [persistentní položka] | [optional] 
**AccBasePriceAmount** | **double?** | Účet.vstup.cena [persistentní položka] | [optional] 
**TaxRemainderPriceAmount** | **double?** | Daň.zůst.cena [persistentní položka] | [optional] 
**AccRemainderPriceAmount** | **double?** | Účet.zůst.cena [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**LastTaxDepreciationPerc** | **double?** | %daň.odpisu při vyř. [persistentní položka] | [optional] 
**IsFinal** | **bool?** | úplné [persistentní položka] | [optional] 
**IsFinalAsText** | **string** | úplné | [optional] 
**Components** | [**List&lt;Assetdiscardcomponent&gt;**](Assetdiscardcomponent.md) | Prvky dokladu vyřazení majetku; kolekce BO Prvek na dokladu vyřazení majetku [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

