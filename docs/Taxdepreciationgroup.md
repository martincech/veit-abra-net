# Veit.Abra.Model.Taxdepreciationgroup
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název odpisové skupiny [persistentní položka] | [optional] 
**FirstYearCoef** | **double?** | Koef.první [persistentní položka] | [optional] 
**OtherYearsCoef** | **double?** | Koef.další [persistentní položka] | [optional] 
**OverValuedCoef** | **double?** | Koef.zvýš. [persistentní položka] | [optional] 
**YearsCount** | **int?** | Počet let [persistentní položka] | [optional] 
**AlgorithmType** | **int?** | Algoritmus [persistentní položka] | [optional] 
**AlgorithmTypeAsText** | **string** | Algoritmus textově | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platí od data [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Platí do data [persistentní položka] | [optional] 
**ConstantRate** | **double?** | Konst.odp. | [optional] 
**WayOfDefConst** | **int?** | Typ def. [persistentní položka] | [optional] 
**Numerator** | **double?** | Čitatel [persistentní položka] | [optional] 
**Denominator** | **double?** | Jmenovatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

