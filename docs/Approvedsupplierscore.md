# Veit.Abra.Model.Approvedsupplierscore
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hodnocený dodavatel [persistentní položka] | [optional] 
**EvaluationCriterionID** | **string** | Hodnotící kritérium; ID objektu Hodnotící kritérium [persistentní položka] | [optional] 
**CreatedDateDATE** | **DateTimeOffset?** | Datum známkování [persistentní položka] | [optional] 
**Frequency** | **int?** | Časový typ hodnocení [persistentní položka] | [optional] 
**OrderNumber** | **int?** | Pořadové číslo období [persistentní položka] | [optional] 
**InYear** | **int?** | Rok [persistentní položka] | [optional] 
**EvaluatedFromDateDATE** | **DateTimeOffset?** | Spodní datum vyhodnocovaného období [persistentní položka] | [optional] 
**EvaluatedToDateDATE** | **DateTimeOffset?** | Horní datum vyhodnocovaného období [persistentní položka] | [optional] 
**CalculatedScore** | **int?** | Vypočtený počet bodů [persistentní položka] | [optional] 
**Score** | **int?** | Počet bodů [persistentní položka] | [optional] 
**Takeover** | **bool?** | Převzato [persistentní položka] | [optional] 
**PeriodText** | **string** | Období | [optional] 
**Satisfactory** | **bool?** | Vyhovuje | [optional] 
**PercentScore** | **double?** | Procentní hodnocení | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

