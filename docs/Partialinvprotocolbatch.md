# Veit.Abra.Model.Partialinvprotocolbatch
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Dílčí inventární protokol - řádek [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**RealQuantity** | **double?** | Zjištěné množství [persistentní položka] | [optional] 
**MIPBatchID** | **string** | Šarže/sér. č.; ID objektu Hlavní inventární protokol - sér.č./šarže [persistentní položka] | [optional] 
**UnitDocumentedQuantity** | **double?** | Evidenční množství | [optional] 
**UnitRealQuantity** | **double?** | Zjištěné množství | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

