# Veit.Abra.Model.Vatindex
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Tariff** | **double?** | Sazba [persistentní položka] | [optional] 
**Income** | **bool?** | Výstup [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**AnalyticalAccount** | **string** | Analytika [persistentní položka] | [optional] 
**VATRateID** | **string** | %DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**AllowanceVATIndexID** | **string** | Odpočet DPH; ID objektu DPH index [persistentní položka] | [optional] 
**ReverseVATIndexID** | **string** | Inverzní index; ID objektu DPH index [persistentní položka] | [optional] 
**IsCommon** | **bool?** | Bez ohledu [persistentní položka] | [optional] 
**IsAllowance** | **bool?** | Je odpočet [persistentní položka] | [optional] 
**IsReverse** | **bool?** | Je inverzní [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**VATIndexType** | **int?** | Typ DPH Indexu [persistentní položka] | [optional] 
**LegalNotice** | **string** | Upozornění [persistentní položka] | [optional] 
**UsingNotice** | **string** | Použití [persistentní položka] | [optional] 
**ForCustomsDeclaration** | **bool?** | Pro celní prohlášení [persistentní položka] | [optional] 
**ForDomesticReverseCharge** | **bool?** | Režim Přenesení daňové povinnosti [persistentní položka] | [optional] 
**ForInsolventVATCorrection** | **bool?** | Oprava daně u pohledávek za dlužníky v insolvenci [persistentní položka] | [optional] 
**OutOfVAT** | **bool?** | Mimo evidenci DPH [persistentní položka] | [optional] 
**ESLIndicatorType** | **int?** | Typ kódu plnění ESL [persistentní položka] | [optional] 
**ESLIndicatorTypeAsText** | **string** | Typ kódu plnění ESL | [optional] 
**AccrualWithVAT** | **bool?** | Pro časové rozlišení použít částku včetně daně [persistentní položka] | [optional] 
**SaldoRateType** | **int?** | Typ saldokontní sazby [persistentní položka] | [optional] 
**SaldoRateTypeAsText** | **string** | Typ saldokontní sazby | [optional] 
**ForVATReturnMethod** | **int?** | Pro metodu DPH přiznání [persistentní položka] | [optional] 
**ForVATReturnMethodAsText** | **string** | Pro metodu DPH přiznání | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

