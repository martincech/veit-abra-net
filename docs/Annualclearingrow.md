# Veit.Abra.Model.Annualclearingrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Roční zúčtování [persistentní položka] | [optional] 
**CalcCharts** | [**List&lt;Annualclearingchart&gt;**](Annualclearingchart.md) | Výpočtová schémata; kolekce BO Vzorec roč.zúčt. [nepersistentní položka] | [optional] 
**RowType** | **int?** | Typ [persistentní položka] | [optional] 
**WagePeriodFromID** | **string** | Období od; ID objektu Mzdové období [persistentní položka] | [optional] 
**WagePeriodToID** | **string** | Období do; ID objektu Mzdové období [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**Comments** | **string** | Poznámka [persistentní položka] | [optional] 
**TotalIncome** | **double?** | Celk.příjem [persistentní položka] | [optional] 
**TotalInsurance** | **double?** | Celk.pojistné [persistentní položka] | [optional] 
**TotalCountOffs** | **double?** | Celk.odpočty [persistentní položka] | [optional] 
**TaxBase** | **double?** | Základ [persistentní položka] | [optional] 
**PaidTax** | **double?** | Zaplac.daň [persistentní položka] | [optional] 
**SpecialTaxBase** | **double?** | Základ srážk.d. [persistentní položka] | [optional] 
**SpecialTax** | **double?** | Srážk.daň [persistentní položka] | [optional] 
**TotalSickBenefit** | **double?** | Celk.nemocenská [persistentní položka] | [optional] 
**TotalTaxBonus** | **double?** | Daňový bonus [persistentní položka] | [optional] 
**TotalTaxAdvantage** | **double?** | Daňové zvýhodnění [persistentní položka] | [optional] 
**TotalTaxDiscount** | **double?** | Sleva na dani [persistentní položka] | [optional] 
**EmployeePremium** | **double?** | Zaměstnanecká prémie [persistentní položka] | [optional] 
**HealthInsurance** | **double?** | Zdravotní poj. [persistentní položka] | [optional] 
**SocialInsurance** | **double?** | Sociální poj. [persistentní položka] | [optional] 
**AgreementIncome** | **double?** | Příjmy z dohod [persistentní položka] | [optional] 
**AnnualClearingCountOffs** | [**List&lt;Annualclearingcountoff&gt;**](Annualclearingcountoff.md) | Kolekce odpočtů; kolekce BO Odpočet k roč.zúčtování [nepersistentní položka] | [optional] 
**RowTypeAsText** | **string** | Typ řádku | [optional] 
**OAPensSavingBaseCut** | **double?** | SDS uplatněné do základu daně [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

