# Veit.Abra.Model.Trmcartype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Category** | **int?** | Skupina typů [persistentní položka] | [optional] 
**SeviceWeight** | **double?** | Pohotovostní hmotnost [persistentní položka] | [optional] 
**TotalWeight** | **double?** | Celková hmotnost [persistentní položka] | [optional] 
**CargoWeight** | **double?** | Užitková hmotnost [persistentní položka] | [optional] 
**WeightOnAxle** | **double?** | Max. hmotnost na nápravu [persistentní položka] | [optional] 
**FuelNormSummer** | **double?** | Letní norma [persistentní položka] | [optional] 
**FuelNormWinter** | **double?** | Zimní norma [persistentní položka] | [optional] 
**FuelNormInter** | **double?** | Střední norma [persistentní položka] | [optional] 
**FuelNormCollS** | **double?** | Letní norma souprav [persistentní položka] | [optional] 
**FuelNormCollW** | **double?** | Zimní norma souprav [persistentní položka] | [optional] 
**FuelNormCollInter** | **double?** | Střední norma souprav [persistentní položka] | [optional] 
**FuelNormManip** | **double?** | Manipulační norma [persistentní položka] | [optional] 
**PassTotal** | **int?** | Počet míst [persistentní položka] | [optional] 
**PassSitting** | **int?** | Počet míst k sezení [persistentní položka] | [optional] 
**TRMFuelTypeID** | **string** | Druh pohonu; ID objektu Druh PHM [persistentní položka] | [optional] 
**AxleCount** | **int?** | Počet náprav [persistentní položka] | [optional] 
**TyreCount** | **int?** | Počet pneu [persistentní položka] | [optional] 
**VehicleLength** | **int?** | Délka vozidla [persistentní položka] | [optional] 
**VehicleWidth** | **int?** | Šířka vozidla [persistentní položka] | [optional] 
**VehicleHeight** | **int?** | Výška vozidla [persistentní položka] | [optional] 
**CylinderCapacity** | **int?** | Obsah válců [persistentní položka] | [optional] 
**CargoVolume** | **double?** | Ložný prostor [persistentní položka] | [optional] 
**RoadTaxRate** | **double?** | Silniční daň [persistentní položka] | [optional] 
**TRMPublicTransTariffID** | **string** | Tarifní systém; ID objektu Tarif OD [persistentní položka] | [optional] 
**TankCapacity** | **double?** | Objem nádrže [persistentní položka] | [optional] 
**CategoryAsText** | **string** | Skupina typů | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

