# Veit.Abra.Model.Assetcomponent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Karta majetku [persistentní položka] | [optional] 
**Code** | **string** | Kód CPA [persistentní položka] | [optional] 
**InventorySubNr** | **string** | Inventární subčíslo [persistentní položka] | [optional] 
**EAN** | **string** | EAN [persistentní položka] | [optional] 
**Name** | **string** | Název prvku [persistentní položka] | [optional] 
**PutInDateDATE** | **DateTimeOffset?** | Zařazen [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**Comment** | **string** | Poznámka | [optional] 
**PurchasePrice** | **double?** | Nákupní cena [persistentní položka] | [optional] 
**TaxBasePriceInitial** | **double?** | Vých.daň.vstup.c. [persistentní položka] | [optional] 
**TaxBasePriceEdit** | **double?** | Editovaná daň.vst.cena prvku | [optional] 
**AccBasePriceInitial** | **double?** | Vých.účet.vstup.c. [persistentní položka] | [optional] 
**AccBasePriceEdit** | **double?** | Editovaná úč.vst.cena prvku | [optional] 
**TaxRemainderPriceInitial** | **double?** | Vých.daň.zůst.c. [persistentní položka] | [optional] 
**AccRemainderPriceInitial** | **double?** | Vých.účet.zůst.c. [persistentní položka] | [optional] 
**TaxBasePriceActual** | **double?** | Akt.daň.vstup.c. [persistentní položka] | [optional] 
**AccBasePriceActual** | **double?** | Akt.účet.vstup.c. [persistentní položka] | [optional] 
**TaxRemainderPriceActual** | **double?** | Akt.daň.zůst.c. [persistentní položka] | [optional] 
**AccRemainderPriceActual** | **double?** | Akt.účet.zůst.c. [persistentní položka] | [optional] 
**TakenOutDateDATE** | **DateTimeOffset?** | Vyjmut [persistentní položka] | [optional] 
**TakenOutReason** | **string** | Důvod [persistentní položka] | [optional] 
**TakenOutByID** | **string** | Vyjmul; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**ComponentInitiallyTakenOut** | **bool?** |  | [optional] 
**ComponentActuallyTakenOut** | **bool?** |  | [optional] 
**AssetPeriods** | [**List&lt;Assetcomponentperiod&gt;**](Assetcomponentperiod.md) | Období změny ceny; kolekce BO Období zvýšení ceny prvku [nepersistentní položka] | [optional] 
**PostponedTaxDeprec** | **double?** | Odložený odpis [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

