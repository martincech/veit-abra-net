# Veit.Abra.Model.Plmjoborderad
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Výrobní příkaz [persistentní položka] | [optional] 
**AdditionalCostsID** | **string** | Vedlejší pořizovací náklady; ID objektu Příjemka - Vedlejší pořizovací náklady [persistentní položka] | [optional] 
**PriceAmount** | **double?** | Pevná cena [persistentní položka] | [optional] 
**ConsumableAmount** | **double?** | Spotřební materiál [persistentní položka] | [optional] 
**MaterialExpense** | **double?** | Materiálová režie [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

