# Veit.Abra.Model.Employeecountcategory
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název kategorie [persistentní položka] | [optional] 
**Minimum** | **int?** | Minimální počet [persistentní položka] | [optional] 
**Maximum** | **int?** | Maximální počet [persistentní položka] | [optional] 
**MeanValue** | **int?** | Střední hodnota [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

