# Veit.Abra.Model.Systemsupportentry
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Kind** | **int?** | Typ záznamu [persistentní položka] | [optional] 
**EntryDateDATE** | **DateTimeOffset?** | Datum zadání [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**ResponseEMail** | **string** | E-mail pro odpověď [persistentní položka] | [optional] 
**ShortDescription** | **string** | Krátký popis [persistentní položka] | [optional] 
**ProblemDescription** | **string** | Popis problému [persistentní položka] | [optional] 
**ImplementationData** | **string** | Implementační data [persistentní položka] | [optional] 
**ProblemData** | **string** | Data [persistentní položka] | [optional] 
**SolutionDateDATE** | **DateTimeOffset?** | Datum řešení [persistentní položka] | [optional] 
**Solver** | **string** | Řešitel [persistentní položka] | [optional] 
**SolutionDescription** | **string** | Popis řešení [persistentní položka] | [optional] 
**CrossReference** | **string** | Odkaz [persistentní položka] | [optional] 
**Status** | **int?** | Stav [persistentní položka] | [optional] 
**EntryNumber** | **int?** | Číslo záznamu [persistentní položka] | [optional] 
**ProblemSendDateDATE** | **DateTimeOffset?** | Datum odeslání problému [persistentní položka] | [optional] 
**SolutionSendDateDATE** | **DateTimeOffset?** | Datum odeslání řešení [persistentní položka] | [optional] 
**MachineName** | **string** | Stanice [persistentní položka] | [optional] 
**NotifyOnStart** | **bool?** | Hlásit při startu [persistentní položka] | [optional] 
**NotifyOnSite** | **string** | Hlásit hnízdu [persistentní položka] | [optional] 
**EntrySite** | **string** | Hnízdo záznamu [persistentní položka] | [optional] 
**PictureID** | [**Picture**](Picture.md) |  | [optional] 
**VendorEntry** | **bool?** | Záznam prodejce [persistentní položka] | [optional] 
**Pictures** | [**List&lt;Systemsupportentrypicture&gt;**](Systemsupportentrypicture.md) | Obrazovky; kolekce BO Obrázek k servisní knížce [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

