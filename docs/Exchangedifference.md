# Veit.Abra.Model.Exchangedifference
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**AccPresetDefID** | **string** | Předkontace; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**FirmOfficeID** | **string** | Provozovna; ID objektu Provozovna [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**AccDateDATE** | **DateTimeOffset?** | Datum účt. [persistentní položka] | [optional] 
**AccDocQueueID** | **string** | Účetní řada; ID objektu Účetní řada dokladů [persistentní položka] | [optional] 
**AccountingType** | **int?** | Jak účtovat | [optional] 
**IsAccounted** | **bool?** | Účtováno | [optional] 
**Dirty** | **bool?** | Zakázané přepočítání | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | O.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**Profit** | **bool?** | Je zisk [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna | [optional] 
**CountryID** | **string** | Země; ID objektu Země | [optional] 
**RefCurrencyID** | **string** | Ref.měna; ID objektu Měna | [optional] 
**LocalRefCurrencyID** | **string** | Lok.ref.měna; ID objektu Měna | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**PDocumentID** | **string** | Korig.doklad; ID objektu Dokument [persistentní položka] | [optional] 
**PDocumentType** | **string** | Typ korig.dokl. [persistentní položka] | [optional] 
**VATDepositUsage** | **bool?** | Příznak, zda souvisí se zúčtováním DZL [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

