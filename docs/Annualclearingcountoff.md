# Veit.Abra.Model.Annualclearingcountoff
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Řádek ročního zúčtování [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**MonthCount** | **int?** | Poč.měsíců [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**YearAmount** | **double?** | Roční částka | [optional] 
**CountOffTypeID** | **string** | Typ odpočtu; ID objektu Typ odpočtu/slevy [persistentní položka] | [optional] 
**FMName** | **string** | Rodinný příslušník [persistentní položka] | [optional] 
**WagePeriodFromID** | **string** | Období od; ID objektu Mzdové období [persistentní položka] | [optional] 
**WagePeriodToID** | **string** | Období do; ID objektu Mzdové období [persistentní položka] | [optional] 
**Correction** | **double?** | Oprava [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

