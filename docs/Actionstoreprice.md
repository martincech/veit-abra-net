# Veit.Abra.Model.Actionstoreprice
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skl. karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**PriceRows** | [**List&lt;Actionstorepricerow&gt;**](Actionstorepricerow.md) | Definice cen; kolekce BO Akční ceníková cena [nepersistentní položka] | [optional] 
**PriceListID** | **string** | Ceník; ID objektu Akční ceník [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

