# Veit.Abra.Model.Storecardcategory
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ToAccountReceiptCard** | **bool?** | Účtovat příjmy [persistentní položka] | [optional] 
**ToAccountBillOfDelivery** | **bool?** | Účtovat výdeje [persistentní položka] | [optional] 
**ToAccountInventory** | **bool?** | Účtovat inventury [persistentní položka] | [optional] 
**ToAccountTransfer** | **bool?** | Účtovat převody [persistentní položka] | [optional] 
**AnalyticalAccount** | **string** | Analytika [persistentní položka] | [optional] 
**ToIntrastat** | **bool?** | Do INTRASTAT [persistentní položka] | [optional] 
**ToESL** | **bool?** | Do ESL [persistentní položka] | [optional] 
**IntrastatTransport** | **bool?** | Doprava do Intrastatu [persistentní položka] | [optional] 
**CostPriceSourceType** | **int?** | Výchozí zdroj nákladové ceny [persistentní položka] | [optional] 
**PLMPrRequestDocQueueID** | **string** | Řada dokladů Požadavky na výrobu použitá při tvorbě nabídky vydané; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PLMPrRequestDocQueueID2** | **string** | Řada dokladů Požadavky na výrobu použitá při tvorbě objednávky přijaté; ID objektu Řada dokladů [persistentní položka] | [optional] 
**SplitIntrastat** | **bool?** | Rozpočítávat částku INTRASTAT do ost. položek [persistentní položka] | [optional] 
**IntrastatStatus** | **int?** |  | [optional] 
**IntrastatTransportStatus** | **int?** |  | [optional] 
**ESLIndicatorID** | **string** | Rozlišení typu plnění(ESL); ID objektu Rozlišení typu plnění(ESL) [persistentní položka] | [optional] 
**ROIgnoreCreateRes** | **bool?** | Ignorovat automatické vytváření rezervací [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

