# Veit.Abra.Model.Ordersgeneration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Quantity** | **double?** | Počet [persistentní položka] | [optional] 
**RequestedQuantity** | **double?** | Požadovaný počet [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Počet | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**RequestedDeliveryDATE** | **DateTimeOffset?** | Požadované datum dodání [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**ReservationDateDATE** | **DateTimeOffset?** | Termín objednání | [optional] 
**IsDemand** | **bool?** | Poptáno [persistentní položka] | [optional] 
**XNoteInter** | **string** | Poznámka (interní) [persistentní položka] | [optional] 
**XGeneratedToStoreCardID** | **string** | Generováno pro výrobek; ID objektu Skladová karta [persistentní položka] | [optional] 
**XCreatedAtDate** | **DateTimeOffset?** | Datum vytvoření požadavku [persistentní položka] | [optional] 
**XDepartDate** | **DateTimeOffset?** | Datum odeslání [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

