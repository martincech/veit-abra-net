# Veit.Abra.Model.Serviceoperationrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Servisní operace [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**ItemType** | **int?** | Typ [persistentní položka] | [optional] 
**TextDescription** | **string** | Textový popis [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**QUnit** | **string** | Jed. [persistentní položka] | [optional] 
**UnitRate** | **double?** | Poměr [persistentní položka] | [optional] 
**ServiceWorkCategoryID** | **string** | Odbornost; ID objektu Servisní odbornost [persistentní položka] | [optional] 
**WorkHoursPlanned** | **double?** | Hodin [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 
**VATRateID** | **string** | %DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**SecurityRoleID** | **string** | Role; ID objektu Role [persistentní položka] | [optional] 
**Amount** | **double?** | Částka s DPH [persistentní položka] | [optional] 
**AmountWithoutVAT** | **double?** | Částka bez DPH [persistentní položka] | [optional] 
**Variants** | [**List&lt;Serviceoperationvariants&gt;**](Serviceoperationvariants.md) | Varianty; kolekce BO Servis.oper. - varianty modelů [nepersistentní položka] | [optional] 
**UnitQuantity** | **double?** | Počet | [optional] 
**StoreID** | **string** | Sklad | [optional] 
**ItemTypeAsText** | **string** | Typ | [optional] 
**CommonDescription** | **string** | Položka | [optional] 
**TotalAmountWithoutVAT** | **double?** | Celkem bez DPH | [optional] 
**TotalAmountWithVAT** | **double?** | Celkem s DPH | [optional] 
**VariantStoreCardID** | **string** | Varianta skladové karty; ID objektu Skladová karta | [optional] 
**VariantQUnit** | **string** | Varianta jednotky | [optional] 
**VariantQuantity** | **double?** | Varianta množství | [optional] 
**VariantWorkHoursPlanned** | **double?** | Varianta plánovaných hodin | [optional] 
**VariantAmount** | **double?** | Varianta množství | [optional] 
**VariantAmountWithoutVAT** | **double?** | Varianta ceny bez daně | [optional] 
**VariantVATRateID** | **string** | Varianta ceny s daní; ID objektu DPH sazba | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

