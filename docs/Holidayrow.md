# Veit.Abra.Model.Holidayrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Nárok na dovolenou [persistentní položka] | [optional] 
**WagePeriodID** | **string** | Mzdové období; ID objektu Mzdové období [persistentní položka] | [optional] 
**DrawDown** | **double?** | Čerpáno [persistentní položka] | [optional] 
**Paid** | **double?** | Proplaceno [persistentní položka] | [optional] 
**WagePeriodCode** | **string** | Kód mzdového období | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

