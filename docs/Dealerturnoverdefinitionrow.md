# Veit.Abra.Model.Dealerturnoverdefinitionrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Tabulka obratů dealerských skupin [persistentní položka] | [optional] 
**AmountLimit** | **double?** | Min.částka [persistentní položka] | [optional] 
**DealerCategoryID** | **string** | Deal.třída; ID objektu Dealerská třída [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

