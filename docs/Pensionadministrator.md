# Veit.Abra.Model.Pensionadministrator
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**AddressID** | [**Address**](Address.md) |  | [optional] 
**BankAccount** | **string** | Bankovní účet [persistentní položka] | [optional] 
**VarSymbol** | **string** | Variabilní symbol [persistentní položka] | [optional] 
**ConstSymbolID** | **string** | Konst.symbol; ID objektu Konstantní symbol [persistentní položka] | [optional] 
**SpecSymbol** | **string** | Spec.symbol [persistentní položka] | [optional] 
**AccountID** | **string** | Anal.účet; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**VarSymbolMask** | **string** | Maska var. symb. [persistentní položka] | [optional] 
**SpecSymbolMask** | **string** | Maska pro generováni specifického symbolu [persistentní položka] | [optional] 
**FirmOrgIdentNumber** | **string** | IČO zdravotní pojišťovny | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

