# Veit.Abra.Model.Postoffice
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**PostCode** | **string** | PSČ [persistentní položka] | [optional] 
**PostName** | **string** | Název pošty [persistentní položka] | [optional] 
**RegionCode** | **string** | Okres [persistentní položka] | [optional] 
**RegionName** | **string** | Název okresu [persistentní položka] | [optional] 
**DistrictID** | **string** | Okres; ID objektu Okres [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

