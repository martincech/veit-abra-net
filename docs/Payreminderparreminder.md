# Veit.Abra.Model.Payreminderparreminder
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Parametry automatických upomínek - upomínky [persistentní položka] | [optional] 
**OrderNum** | **int?** | Pořadí [persistentní položka] | [optional] 
**ReportID** | **string** | Report; ID objektu Definice reportu [persistentní položka] | [optional] 
**ConditionExpr** | **string** | Podmínka [persistentní položka] | [optional] 
**EmailSubject** | **string** | Předmět [persistentní položka] | [optional] 
**EmailText** | **string** | Text [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

