# Veit.Abra.Model.Crmcampaignaudience
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Kampaň [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**FirmOfficeID** | **string** | Provozovna; ID objektu Provozovna [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**Email** | **string** | E-mail [persistentní položka] | [optional] 
**TargetAddressType** | **int?** | Typ adresy [persistentní položka] | [optional] 
**PersonSpecification** | **string** | Specifikace osoby [persistentní položka] | [optional] 
**AudienceState** | **int?** | Stav [persistentní položka] | [optional] 
**AddressID** | **string** | Adresa; ID objektu Adresa | [optional] 
**ContactTimeDATE** | **DateTimeOffset?** | Termín kontaktu [persistentní položka] | [optional] 
**CampaignVariableInstances** | [**List&lt;Crmcampaignvariableinstance&gt;**](Crmcampaignvariableinstance.md) | Hodnoty proměnných; kolekce BO Hodnota proměnné kampaně [nepersistentní položka] | [optional] 
**CampaignFeedBackID** | **string** | Zpětná vazba; ID objektu Zpětná vazba kampaně [persistentní položka] | [optional] 
**DocumentID** | **string** | Založený doklad; ID objektu Dokument [persistentní položka] | [optional] 
**DocumentType** | **string** | Typ založeného dokladu [persistentní položka] | [optional] 
**EmailSentSendStateStr** | **string** | Stav odeslané pošty | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

