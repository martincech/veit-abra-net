# Veit.Abra.Model.Intangibleassettype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**CommmonMonthCount** | **int?** | Běžná doba odp. [persistentní položka] | [optional] 
**ValorisationMonthCount** | **int?** | Doba odp. pro TZ [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platí od [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Platí do [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

