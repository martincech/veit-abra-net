# Veit.Abra.Model.Firmbankaccount
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Firma [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**BankAccount** | **string** | Účet [persistentní položka] | [optional] 
**SpecSymbol** | **string** | Specifický symbol [persistentní položka] | [optional] 
**AddressID** | [**Address**](Address.md) |  | [optional] 
**SwiftCode** | **string** | SWIFT [persistentní položka] | [optional] 
**BankCountryID** | **string** | Země banky; ID objektu Země [persistentní položka] | [optional] 
**PublishedStatus** | **int?** | Status účtu | [optional] 
**PublishedStatusAsText** | **string** | Status účtu - text | [optional] 
**PublishedDateDATE** | **DateTimeOffset?** | Datum zveřejnění | [optional] 
**PublishedEndDateDATE** | **DateTimeOffset?** | Datum konce zveřejnění | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

