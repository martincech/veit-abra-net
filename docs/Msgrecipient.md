# Veit.Abra.Model.Msgrecipient
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Vzkaz [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**RecipientType** | **int?** | Typ [persistentní položka] | [optional] 
**SecurityUserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**SecurityRoleID** | **string** | Role; ID objektu Role [persistentní položka] | [optional] 
**SecurityGroupID** | **string** | Skupina rolí; ID objektu Skupina rolí [persistentní položka] | [optional] 
**Email** | **string** | Adresát(i) [persistentní položka] | [optional] 
**EmailAccountID** | **string** | Vlastní e-mail. účet; ID objektu E-mailový účet [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

