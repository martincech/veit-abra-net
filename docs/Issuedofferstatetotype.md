# Veit.Abra.Model.Issuedofferstatetotype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Typ nabídky [persistentní položka] | [optional] 
**IssuedOfferStateID** | **string** | Stav; ID objektu Stav nabídky [persistentní položka] | [optional] 
**OrderNr** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**ActivityProcessID** | **string** | Proces aktivit | [optional] 
**DefaultDuration** | **int?** | Maximální doba k dokončení [persistentní položka] | [optional] 
**DefaultRoleID** | **string** | Výchozí role řešitele; ID objektu Role [persistentní položka] | [optional] 
**DefaultUserID** | **string** | Výchozí řešitel; ID objektu Uživatel [persistentní položka] | [optional] 
**IsGenerateToPlan** | **bool?** | Generovat do plánu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

