# Veit.Abra.Model.Wlworker
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**DateOfChange** | **DateTimeOffset?** | Datum změny | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**SuperiorID** | **string** | Nadřízený pracovník; ID objektu Docházka - pracovníci [persistentní položka] | [optional] 
**WorkingRelationID** | **string** | Pracovní poměr; ID objektu Pracovní poměr [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platí od [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Platí do [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obchodní případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**ExternalID** | **string** | Externí identifikátor [persistentní položka] | [optional] 
**PersonName** | **string** | Jméno pracovníka | [optional] 
**SuperiorName** | **string** | Jméno nadříz. pracovníka | [optional] 
**PersonalNumber** | **string** | Číslo pracovníka | [optional] 
**SecurityUserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

