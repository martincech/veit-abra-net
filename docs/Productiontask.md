# Veit.Abra.Model.Productiontask
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Productiontaskrow&gt;**](Productiontaskrow.md) | Řádky; kolekce BO Výrobní úloha - řádek [nepersistentní položka] | [optional] 
**StoreCardID** | **string** | Skl. karta výrobku; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad výrobku; ID objektu Sklad [persistentní položka] | [optional] 
**PlannedQuantity** | **double?** | Plánováno [persistentní položka] | [optional] 
**DeadLineDATE** | **DateTimeOffset?** | Termín [persistentní položka] | [optional] 
**Quantity** | **double?** | Přijato výrobků [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**Ordnumber** | **int?** | Číslo dokladu [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**OwnerType** | **int?** | Typ vlastníka [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

