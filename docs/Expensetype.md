# Veit.Abra.Model.Expensetype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**AnalyticalAccount** | **string** | Analytika [persistentní položka] | [optional] 
**Category** | **int?** | Kategorie [persistentní položka] | [optional] 
**BookColumn** | **int?** | Sloupec deníku [persistentní položka] | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 
**EETKind** | **int?** | EET rozlišení [persistentní položka] | [optional] 
**ParentID** | **string** | Nadřízený; ID objektu Typ výdaje [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

