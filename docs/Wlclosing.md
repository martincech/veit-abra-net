# Veit.Abra.Model.Wlclosing
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Wlclosingrow&gt;**](Wlclosingrow.md) | Řádky; kolekce BO Docházka - řádek uzávěrky [nepersistentní položka] | [optional] 
**ClosedFromDATE** | **DateTimeOffset?** | Uzavřeno od [persistentní položka] | [optional] 
**ClosedToDATE** | **DateTimeOffset?** | Uzavřeno do [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 
**WagePeriodID** | **string** | Mzdové období; ID objektu Mzdové období [persistentní položka] | [optional] 
**WageOperationImportID** | **string** | Import do výkonů; ID objektu Import do výkonů [persistentní položka] | [optional] 
**AbsenceImportID** | **string** | Import do nepřítomností; ID objektu Import do Nepřítomností [persistentní položka] | [optional] 
**SickBenefitImportID** | **string** | Import do nemocenských dávek; ID objektu Import do Nemocenských dávek [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

