# Veit.Abra.Model.Viescheckvatresponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**VATIdentNumber** | **string** | DIČ [persistentní položka] | [optional] 
**VATIdentNumberStatus** | **int?** | Stav DIČ [persistentní položka] | [optional] 
**VATIdentNumberStatusAsText** | **string** | Stav DIČ - text | [optional] 
**RequestDateDATE** | **DateTimeOffset?** | Datum požadavku na ověření [persistentní položka] | [optional] 
**ResponseDateTimeDATE** | **DateTimeOffset?** | Datum a čas odpovědi [persistentní položka] | [optional] 
**RequestIdentifier** | **string** | Identifikátor požadavku na ověření DIČ [persistentní položka] | [optional] 
**TraderName** | **string** | Název subjektu v systému VIES [persistentní položka] | [optional] 
**TraderCompanyType** | **string** | Typ subjektu [persistentní položka] | [optional] 
**TraderAddress** | **string** | Kompletní adresa subjektu v systému VIES [persistentní položka] | [optional] 
**TraderStreet** | **string** | Ulice subjektu [persistentní položka] | [optional] 
**TraderPostcode** | **string** | PSČ subjektu [persistentní položka] | [optional] 
**TraderCity** | **string** | Město subjektu [persistentní položka] | [optional] 
**TraderNameMatch** | **int?** | Shoda názvu subjektu [persistentní položka] | [optional] 
**TraderCompanyTypeMatch** | **int?** | Shoda typu subjektu [persistentní položka] | [optional] 
**TraderStreetMatch** | **int?** | Shoda ulice subjektu [persistentní položka] | [optional] 
**TraderPostcodeMatch** | **int?** | Shoda PSČ subjektu [persistentní položka] | [optional] 
**TraderCityMatch** | **int?** | Shoda města subjektu [persistentní položka] | [optional] 
**CheckMessage** | **string** | Zpráva z provedeného ověření [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

