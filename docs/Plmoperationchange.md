# Veit.Abra.Model.Plmoperationchange
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Pracovní lístek [persistentní položka] | [optional] 
**Transfer2ID** | **string** | Řádek přenosu do mezd [persistentní položka] | [optional] 
**DeltaTotalTime** | **double?** | Změna doby trvání [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

