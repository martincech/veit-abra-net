# Veit.Abra.Model.Docconfirmationrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Schvalování dokladu [persistentní položka] | [optional] 
**ConfirmationRound** | **int?** | Kolo schvalování [persistentní položka] | [optional] 
**RequestCreatedDateDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**ConfirmatorRoleID** | **string** | Role schvalovatele; ID objektu Role [persistentní položka] | [optional] 
**ConfirmatorUserID** | **string** | Schvalovatel; ID objektu Uživatel [persistentní položka] | [optional] 
**ConfirmatorOrder** | **int?** | Pořadí [persistentní položka] | [optional] 
**ConfirmedByID** | **string** | Schválil; ID objektu Uživatel [persistentní položka] | [optional] 
**ConfirmationState** | **int?** | Stav schválení [persistentní položka] | [optional] 
**LastStateChangeDateDATE** | **DateTimeOffset?** | Posl. změna [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**Canceled** | **bool?** | Storno [persistentní položka] | [optional] 
**CanceledByID** | **string** | Stornoval; ID objektu Uživatel [persistentní položka] | [optional] 
**CanceledDateDATE** | **DateTimeOffset?** | Datum storna [persistentní položka] | [optional] 
**ConfirmationStateDesc** | **string** | Popis stavu schválení | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

