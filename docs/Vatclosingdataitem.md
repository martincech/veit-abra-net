# Veit.Abra.Model.Vatclosingdataitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Uzávěrka DPH [persistentní položka] | [optional] 
**RowNumber** | **string** | Řádek [persistentní položka] | [optional] 
**CorrectionValue** | **double?** | Oprava hodnoty [persistentní položka] | [optional] 
**CalculatedValue** | **double?** | Vypočtená hodnota [persistentní položka] | [optional] 
**RowValue** | **double?** | Hodnota [persistentní položka] | [optional] 
**OldRowValue** | **double?** | Původní hodnota [persistentní položka] | [optional] 
**DefinitionType** | **int?** | Typ řádku definice [persistentní položka] | [optional] 
**RowValueKind** | **int?** | Typ hodnoty řádku definice [persistentní položka] | [optional] 
**VATDefinitionRowID** | **string** | Definice DPH; ID objektu Definice pro DPH přiznání - řádek [persistentní položka] | [optional] 
**DefinitionTypeAsText** | **string** | Typ řádku definice - text | [optional] 
**RowValueKindAsText** | **string** | Typ řádku definice - text | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

