# Veit.Abra.Model.Plmreqcostingoutputitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**PLMReqCostingRoutines** | [**List&lt;Plmreqcostingroutine&gt;**](Plmreqcostingroutine.md) | Technologický postup; kolekce BO Požadavek na výrobu - kalkulace technologický postup [nepersistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 
**QUnit** | **string** | Měrná jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah jednotky [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství | [optional] 
**RoutineTypeID** | **string** | Typ technologického postupu; ID objektu Typ technologického postupu [persistentní položka] | [optional] 
**PLQuantity** | **double?** | Množství z kusovníku [persistentní položka] | [optional] 
**PLQUnit** | **string** | Jednotka z kusovníku [persistentní položka] | [optional] 
**PLUnitRate** | **double?** | Vztah jednotky z kusovníku [persistentní položka] | [optional] 
**RoutineQuantity** | **double?** | Množství z tech. postupu [persistentní položka] | [optional] 
**RoutineQUnit** | **string** | Jednotka z tech. postupu [persistentní položka] | [optional] 
**RoutineUnitRate** | **double?** | Vztah jednotky z tech. postupu [persistentní položka] | [optional] 
**PLUnitQuantity** | **double?** | Množství z kusovníku | [optional] 
**RoutineUnitQuantity** | **double?** | Množství z tech. postupu | [optional] 
**MaterialAmount** | **double?** | Materiál - částka [persistentní položka] | [optional] 
**SemiFinProdAmount** | **double?** | Polotovary - částka [persistentní položka] | [optional] 
**ConsumablesAmount** | **double?** | Spotř. materiál - částka [persistentní položka] | [optional] 
**TotalTime** | **double?** | Celkový čas v sekundách [persistentní položka] | [optional] 
**Salary** | **double?** | Mzda [persistentní položka] | [optional] 
**OverheadCosts** | **double?** | Výrobní režie [persistentní položka] | [optional] 
**GeneralExpense** | **double?** | Správní režie [persistentní položka] | [optional] 
**TotalOverheads** | **double?** | Čas. režie | [optional] 
**MaterialAmountCompl** | **bool?** | Cena za materiál kompletní [persistentní položka] | [optional] 
**MaterialExpense** | **double?** | Materiálová režie [persistentní položka] | [optional] 
**OwnerID** | **string** | Vlastník; ID objektu Uzel stromu kalkulace požadavku na výrobu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

