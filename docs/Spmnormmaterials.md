# Veit.Abra.Model.Spmnormmaterials
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Norma kompletace [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**Quantity** | **double?** | Počet v jedn.se vzt.1 [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Počet | [optional] 
**Weight** | **double?** | Hmotnost | [optional] 
**UnitWeight** | **double?** | Hmotnost jednotky | [optional] 
**WeightUnit** | **int?** | Hmotnostní jednotka | [optional] 
**WeightInGrams** | **double?** | Hmotnost v gramech | [optional] 
**WastePercentage** | **double?** | Ztráty [persistentní položka] | [optional] 
**Replaceable** | **int?** | Nahrazování [persistentní položka] | [optional] 
**Decomposition** | **bool?** | Rozpad [persistentní položka] | [optional] 
**UseOperatingStore** | **bool?** | Z provozního skladu [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah | [optional] 
**CalculatedPrice** | **double?** | Cena [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**TransferQUnit** | **string** | Jednotka pro převod [persistentní položka] | [optional] 
**TransferUnitRate** | **double?** | Vztah přev.j. | [optional] 
**XTisknout** | **bool?** | Tisknout [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

