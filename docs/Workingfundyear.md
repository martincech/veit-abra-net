# Veit.Abra.Model.Workingfundyear
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Fondy pracovní doby pro týdenní úvazek [persistentní položka] | [optional] 
**CalendarYear** | **int?** | Rok [persistentní položka] | [optional] 
**FirstQuarterFund** | **double?** | 1.čtvrtletí [persistentní položka] | [optional] 
**SecondQuarterFund** | **double?** | 2.čtvrtletí [persistentní položka] | [optional] 
**ThirdQuarterFund** | **double?** | 3.čtvrtletí [persistentní položka] | [optional] 
**FourthQuarterFund** | **double?** | 4.čtvrtletí [persistentní položka] | [optional] 
**YearFund** | **double?** | Roční fond [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

