# Veit.Abra.Model.Firmassortmentdiscount
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Firma [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**StoreAssortmentGroupID** | **string** | Sortimentní skupina; ID objektu Sortimentní skupina [persistentní položka] | [optional] 
**PriceID** | **string** | Typ ceny; ID objektu Definice ceny [persistentní položka] | [optional] 
**Discount** | **double?** | Sleva [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

