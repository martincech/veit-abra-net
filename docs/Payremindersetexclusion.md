# Veit.Abra.Model.Payremindersetexclusion
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**LinkSourcePK** | **string** | OID zdroje [persistentní položka] | [optional] 
**LinkSourceType** | **string** | Typ zdroje [persistentní položka] | [optional] 
**FirstSetDateDATE** | **DateTimeOffset?** | Datum prvního nastavení [persistentní položka] | [optional] 
**ExclDateToDATE** | **DateTimeOffset?** | Vyloučeno z upomínání do [persistentní položka] | [optional] 
**ExclReason** | **string** | Důvod vyloučení [persistentní položka] | [optional] 
**EnforceStateID** | **string** | Stav vymáhání; ID objektu Stav vymáhání pohledávky [persistentní položka] | [optional] 
**HandedOver** | **bool?** | Předáno vedení [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

