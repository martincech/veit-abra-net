# Veit.Abra.Model.Absence
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**WorkingRelationID** | **string** | Pracovní poměr; ID objektu Pracovní poměr [persistentní položka] | [optional] 
**AbsenceID** | **string** | Druh nepřítomnosti; ID objektu Druh nepřítomnosti [persistentní položka] | [optional] 
**AbsenceBaseType** | **int?** | Základní typ [persistentní položka] | [optional] 
**AbsentFromDATE** | **DateTimeOffset?** | Počátek nepřítomnosti [persistentní položka] | [optional] 
**AbsentToDATE** | **DateTimeOffset?** | Konec nepřítomnosti [persistentní položka] | [optional] 
**FirstDay** | **double?** | Nepřítomnost v prvém dni [persistentní položka] | [optional] 
**GiveCalendarDays** | **bool?** | Počítat kalendářní dny [persistentní položka] | [optional] 
**NoEvidenceState** | **bool?** | Mimo evidenční stav [persistentní položka] | [optional] 
**NoCalculation** | **bool?** | Nepočítat mzdu [persistentní položka] | [optional] 
**ExTimeType** | **int?** | Dlouhodobá nepřítomnost [persistentní položka] | [optional] 
**NoInsurance** | **bool?** | Bez pojištění [persistentní položka] | [optional] 
**PaidFreeType** | **int?** | Kat. plac. volna [persistentní položka] | [optional] 
**ImportSourceID** | **string** | Zdroj importu; ID objektu Import do Nepřítomností [persistentní položka] | [optional] 
**EmployeeID** | **string** | Zaměstnanec; ID objektu Zaměstnanec | [optional] 
**AbsenceBaseTypeText** | **string** | Druh nepřítomnosti | [optional] 
**ExTimeTypeText** | **string** | Dlouhodobá nepřítomnost | [optional] 
**EmployeeName** | **string** | Příjmení a jméno | [optional] 
**PERSPersonalNumber** | **string** | Osobní číslo | [optional] 
**PaidFreeTypeText** | **string** | Kat. plac. volna | [optional] 
**LastDay** | **double?** | Nepřítomnost v posledním dni [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

