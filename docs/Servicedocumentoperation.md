# Veit.Abra.Model.Servicedocumentoperation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Servisní list [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**OperationText** | **string** | Operace textově [persistentní položka] | [optional] 
**DocumentDefectID** | **string** | Závada; ID objektu Závada k serv.listu [persistentní položka] | [optional] 
**ServiceOperationID** | **string** | Operace; ID objektu Servisní operace [persistentní položka] | [optional] 
**Quantity** | **double?** | Počet [persistentní položka] | [optional] 
**WorkOperationTime** | **double?** | Pracovní čas | [optional] 
**DefectCode** | **string** | K závadě | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

