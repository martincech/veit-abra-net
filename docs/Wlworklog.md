# Veit.Abra.Model.Wlworklog
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**WorkerID** | **string** | Pracovník; ID objektu Docházka - pracovníci [persistentní položka] | [optional] 
**EntryTypeID** | **string** | Typ záznamu; ID objektu Docházka - Druh činnosti [persistentní položka] | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 
**BaseType** | **int?** | Základní typ [persistentní položka] | [optional] 
**EntryValue** | **double?** | Hodnota záznamu [persistentní položka] | [optional] 
**BeginDateDATE** | **DateTimeOffset?** | Datum a čas počátku [persistentní položka] | [optional] 
**EndDateDATE** | **DateTimeOffset?** | Datum a čas konce [persistentní položka] | [optional] 
**LengthDateDATE** | **DateTimeOffset?** | Datum a čas konce - délka | [optional] 
**ClosedDateDATE** | **DateTimeOffset?** | Datum uzavření [persistentní položka] | [optional] 
**ClosingWPeriodID** | **string** | Mzdové období uzavření; ID objektu Mzdové období [persistentní položka] | [optional] 
**EntryStatus** | **int?** | Status záznamu [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obchodní případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**ClosingID** | **string** | Uzávěrka; ID objektu Docházka - uzávěrka [persistentní položka] | [optional] 
**FeastHours** | **double?** | Hodiny ve svátek [persistentní položka] | [optional] 
**NightHours** | **double?** | Hodiny v noci [persistentní položka] | [optional] 
**EnvHours** | **double?** | Hodiny ve st. prostředí [persistentní položka] | [optional] 
**WEndHours** | **double?** | Hodiny o víkendu [persistentní položka] | [optional] 
**DefTypeAsText** | **string** | Status záznamu | [optional] 
**DurationInDays** | **double?** | Čas ve dnech | [optional] 
**NetDurationInDays** | **double?** | Čistý čas ve dnech | [optional] 
**DurationInHours** | **double?** | Čas v hodinách | [optional] 
**NetDurationInHours** | **double?** | Čistý čas v hodinách | [optional] 
**NetLengthDate** | **DateTimeOffset?** | Čistý čas bez nepřítomností | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

