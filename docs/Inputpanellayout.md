# Veit.Abra.Model.Inputpanellayout
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Inputpaneldefinition&gt;**](Inputpaneldefinition.md) | Řádky; kolekce BO Variantní formuláře - definice [nepersistentní položka] | [optional] 
**SiteClassID** | **string** | Identifikace agendy [persistentní položka] | [optional] 
**PanelName** | **string** | Název panelu [persistentní položka] | [optional] 
**LayoutOrder** | **int?** | Pořadí vyhodnocení výrazu varianty [persistentní položka] | [optional] 
**LayoutName** | **string** | Název varianty [persistentní položka] | [optional] 
**LayoutExpression** | **string** | Výraz [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

