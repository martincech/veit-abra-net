# Veit.Abra.Model.Accdocqueue
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Zkratka [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**LastNumbers** | [**List&lt;Accdocqueueperiod&gt;**](Accdocqueueperiod.md) | Čísla; kolekce BO Účetní řada dokladů - období [nepersistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**AutoFillHole** | **bool?** | Zaplňovat mezery [persistentní položka] | [optional] 
**DocumentType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**AccountWhere** | **bool?** | Účtovat [persistentní položka] | [optional] 
**SummaryAccounted** | **bool?** | Souhrnně [persistentní položka] | [optional] 
**ReverseAccounting** | **bool?** | Obráceně [persistentní položka] | [optional] 
**ReverseDepositAccounting** | **bool?** | Zálohy obráceně [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

