# Veit.Abra.Model.Assetinv
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Assetinvlist&gt;**](Assetinvlist.md) | Řádky; kolekce BO Inventarizační seznam majetku [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**CommitteeID** | **string** | Ústřední inventarizační komise; ID objektu Inventarizační komise [persistentní položka] | [optional] 
**InventoryState** | **int?** | Stav inventury [persistentní položka] | [optional] 
**InventoryType** | **int?** | Typ inventury [persistentní položka] | [optional] 
**RestrictionKind** | **int?** | Podmínka výběru majetku [persistentní položka] | [optional] 
**StartDateDATE** | **DateTimeOffset?** | Datum zahájení [persistentní položka] | [optional] 
**EndDateDATE** | **DateTimeOffset?** | Datum ukončení [persistentní položka] | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 
**InventoryTypeAsText** | **string** | Typ inventury | [optional] 
**InventoryStateAsText** | **string** | Stav inventury | [optional] 
**RestrictionKindAsText** | **string** | Podmínka výběru majetku | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

