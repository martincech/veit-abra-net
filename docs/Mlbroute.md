# Veit.Abra.Model.Mlbroute
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Mlbrouterow&gt;**](Mlbrouterow.md) | Řádky; kolekce BO Vzor jízdy - průjezdná místa [nepersistentní položka] | [optional] 
**ThroughPlaces** | **string** | Průjezdná místa | [optional] 
**DistanceCounted** | **double?** | Spočtená vzdálenost | [optional] 
**StartPlaceID** | **string** | Výchozí místo; ID objektu Místa [persistentní položka] | [optional] 
**EndPlaceID** | **string** | Cílové místo; ID objektu Místa [persistentní položka] | [optional] 
**AndBack** | **bool?** | Zpět [persistentní položka] | [optional] 
**Distance** | **double?** | Vzdálenost [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**FirmOfficeID** | **string** | Provozovna; ID objektu Provozovna [persistentní položka] | [optional] 
**PurposeID** | **string** | Účel jízdy; ID objektu Účel jízdy [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**DistanceDelta** | **double?** | Odchylka [persistentní položka] | [optional] 
**DurationDATE** | **DateTimeOffset?** | Doba jízdy [persistentní položka] | [optional] 
**WaitingTimeMinDATE** | **DateTimeOffset?** | Doba čekání min [persistentní položka] | [optional] 
**WaitingTimeMaxDATE** | **DateTimeOffset?** | Doba čekání max [persistentní položka] | [optional] 
**CreatedByID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

