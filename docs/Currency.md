# Veit.Abra.Model.Currency
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Currencyrow&gt;**](Currencyrow.md) | Historie; kolekce BO Měna - historie [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Symbol** | **string** | Zkratka [persistentní položka] | [optional] 
**Rounding** | **int?** | Zaokrouhlení [persistentní položka] | [optional] 
**Values** | [**List&lt;Currencyvalue&gt;**](Currencyvalue.md) | Hodnoty; kolekce BO Měna - hodnota [nepersistentní položka] | [optional] 
**BankCode** | **string** | Bank.kód [persistentní položka] | [optional] 
**DocRounding** | **int?** | Zaokrouhlení dokladů [persistentní položka] | [optional] 
**DocCashRounding** | **int?** | Zaokrouhlení pokl. dokladů [persistentní položka] | [optional] 
**DocVATRounding** | **int?** | Zaokrouhlení DPH dokladů [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

