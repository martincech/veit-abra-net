# Veit.Abra.Model.Intrastatreportitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Deklarace INTRASTAT [persistentní položka] | [optional] 
**ItemNumber** | **int?** | Číslo položky [persistentní položka] | [optional] 
**CommoditiesID** | **string** | Kód kombinované nomenklatury; ID objektu Kombinovaná nomenklatura [persistentní položka] | [optional] 
**MsConsDestID** | **string** | Země přij./odesl.; ID objektu Země [persistentní položka] | [optional] 
**CountryOfOriginID** | **string** | Země původu; ID objektu Země [persistentní položka] | [optional] 
**NetMass** | **double?** | Hmotnost [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství MJ [persistentní položka] | [optional] 
**InvoicedAmount** | **double?** | Fakturovaná hodnota [persistentní položka] | [optional] 
**TransactionTypeID** | **string** | Povaha transakce; ID objektu Povaha transakce [persistentní položka] | [optional] 
**RegionID** | **string** | Kraj přij./odesl.; ID objektu Kraj [persistentní položka] | [optional] 
**TransportationID** | **string** | Druh dopravy; ID objektu Druh dopravy INTRASTAT [persistentní položka] | [optional] 
**DeliveryTermID** | **string** | Dodací podmínky; ID objektu Dodací podmínky [persistentní položka] | [optional] 
**ExtraTypeID** | **string** | Kód zvláštního pohybu; ID objektu Zvláštní pohyb [persistentní položka] | [optional] 
**FunctionCode** | **int?** | Funkční kód [persistentní položka] | [optional] 
**PreviousDeclarationID** | **string** | Opravovaná deklarace; ID objektu Položka deklarace [persistentní položka] | [optional] 
**FunctionCodeText** | **string** | Funkční kód - text | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

