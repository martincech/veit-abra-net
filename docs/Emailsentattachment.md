# Veit.Abra.Model.Emailsentattachment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Odeslaný e-mail [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**FileName** | **string** | Název souboru [persistentní položka] | [optional] 
**FileSize** | **int?** | Velikost (B) [persistentní položka] | [optional] 
**ContentID** | [**Emailsentattachcontent**](Emailsentattachcontent.md) |  | [optional] 
**ContentType** | **int?** | Typ [persistentní položka] | [optional] 
**DocumentID** | **string** | Dokument; ID objektu Dokument [persistentní položka] | [optional] 
**ContentTypeStr** | **string** | Typ | [optional] 
**CID** | **string** | CID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

