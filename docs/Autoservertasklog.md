# Veit.Abra.Model.Autoservertasklog
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Začátek [persistentní položka] | [optional] 
**InstanceID** | **string** | Identifikace automat.serveru [persistentní položka] | [optional] 
**SchedulerItemID** | **string** | Naplánovaná úloha [persistentní položka] | [optional] 
**QueueItemID** | **string** | Úloha ve frontě [persistentní položka] | [optional] 
**Description** | **string** | Popis úlohy [persistentní položka] | [optional] 
**TaskCLSID** | **string** | Typ úlohy [persistentní položka] | [optional] 
**TaskParameters** | **string** | Parametry úlohy [persistentní položka] | [optional] 
**SPID** | **int?** | PID [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

