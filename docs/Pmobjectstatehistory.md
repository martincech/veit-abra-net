# Veit.Abra.Model.Pmobjectstatehistory
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ObjectID** | **string** | Objekt [persistentní položka] | [optional] 
**ObjectIDObj** | **string** | Objekt; ID objektu Obecný objekt | [optional] 
**ObjectCLSID** | **string** | Třída objektu [persistentní položka] | [optional] 
**StateID** | **string** | Stav; ID objektu Procesní stav [persistentní položka] | [optional] 
**FromDateDATE** | **DateTimeOffset?** | Platnost od [persistentní položka] | [optional] 
**ToDateDATE** | **DateTimeOffset?** | Platnost do [persistentní položka] | [optional] 
**ResponsibleUserID** | **string** | Zodpovědná osoba; ID objektu Uživatel [persistentní položka] | [optional] 
**ResponsibleRoleID** | **string** | Zodpovědná role; ID objektu Uživatel [persistentní položka] | [optional] 
**ChangedByID** | **string** | Změnu provedl; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

