# Veit.Abra.Model.Datadocumentbusinessobject
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Rowbusinessobject&gt;**](Rowbusinessobject.md) | Řádky; kolekce BO Řádek [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

