# Veit.Abra.Model.Scmproviderulesetrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Pravidlo pro zajištění zdrojů [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Condition** | **string** | Podmínka [persistentní položka] | [optional] 
**BuilderCLSID** | **string** | Typ dokumentu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada dokladů; ID objektu Řada dokladů [persistentní položka] | [optional] 
**FirmExpr** | **string** | Výraz firmy [persistentní položka] | [optional] 
**FirmOfficeExpr** | **string** | Výraz provozovny [persistentní položka] | [optional] 
**DivisionExpr** | **string** | Výraz střediska [persistentní položka] | [optional] 
**BusOrderExpr** | **string** | Výraz zakázky [persistentní položka] | [optional] 
**BusTransactionExpr** | **string** | Výraz obch. případu [persistentní položka] | [optional] 
**BusProjectExpr** | **string** | Výraz projektu [persistentní položka] | [optional] 
**FunctionID** | **string** | Funkce pro zajištění; ID objektu Funkce zajištění zdrojů [persistentní položka] | [optional] 
**ContinueEvaluation** | **bool?** | Pokračovat [persistentní položka] | [optional] 
**StoreExpr** | **string** | Výraz skladu [persistentní položka] | [optional] 
**QuantityExpr** | **string** | Výraz pro množství [persistentní položka] | [optional] 
**QUnitExpr** | **string** | Výraz pro jednotku [persistentní položka] | [optional] 
**DateExpr** | **string** | Výraz pro datum [persistentní položka] | [optional] 
**SourceStoreExpr** | **string** | Výraz zdrojového skladu [persistentní položka] | [optional] 
**TargetDocQueueID** | **string** | Cílová řada dokladů; ID objektu Řada dokladů [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

