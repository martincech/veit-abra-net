# Veit.Abra.Model.Bustransactionfreal
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**BusObjectID** | **string** | Nadřízený objekt; ID objektu Obchodní případ [persistentní položka] | [optional] 
**EvaluationDateDATE** | **DateTimeOffset?** | Datum vyhodnocení [persistentní položka] | [optional] 
**RealCosts** | **double?** | Reálné náklady [persistentní položka] | [optional] 
**RealRevenues** | **double?** | Reálné výnosy [persistentní položka] | [optional] 
**RealCostsWithSubTree** | **double?** | Reálné náklady vč.podřízených [persistentní položka] | [optional] 
**RealRevenuesWithSubTree** | **double?** | Reálné výnosy vč.podřízených [persistentní položka] | [optional] 
**RealCostsFromSources** | **double?** | Reálné náklady zdrojů [persistentní položka] | [optional] 
**RealCostsFromSourcesWSubTree** | **double?** | Reálné náklady zdrojů vč.podřízených [persistentní položka] | [optional] 
**CalculatedByID** | **string** | Bilanci spočítal; ID objektu Uživatel [persistentní položka] | [optional] 
**CalculatedWhenDATE** | **DateTimeOffset?** | Datum posl.výpočtu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

