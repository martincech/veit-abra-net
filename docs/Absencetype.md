# Veit.Abra.Model.Absencetype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**AbsenceBaseType** | **int?** | Typ [persistentní položka] | [optional] 
**GiveCalendarDays** | **bool?** | Kal. dny [persistentní položka] | [optional] 
**NoEvidenceState** | **bool?** | Mimo evid. [persistentní položka] | [optional] 
**NoCalculation** | **bool?** | Bez mzdy [persistentní položka] | [optional] 
**ExTimeType** | **int?** | Dlouhodobá nepřít. [persistentní položka] | [optional] 
**PaidFreeType** | **int?** | Kat. plac. volna [persistentní položka] | [optional] 
**NoInsurance** | **bool?** | Bez pojištění [persistentní položka] | [optional] 
**AbsenceBaseTypeText** | **string** | Typ | [optional] 
**ExTimeTypeText** | **string** | Dlouhodobá nepřít. | [optional] 
**PaidFreeTypeText** | **string** | Kat. plac. volna | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

