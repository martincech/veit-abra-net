# Veit.Abra.Model.Wageoperation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Mzdový list dílčí [persistentní položka] | [optional] 
**OperTypeID** | **string** | Typ výkonu; ID objektu Druhy výkonů [persistentní položka] | [optional] 
**Charge** | **double?** | Sazba [persistentní položka] | [optional] 
**CountOfUnit** | **double?** | Počet [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**OperDateDATE** | **DateTimeOffset?** | Datum [persistentní položka] | [optional] 
**ImportSourceID** | **string** | Zdroj importu; ID objektu Import do výkonů [persistentní položka] | [optional] 
**ImportDateDATE** | **DateTimeOffset?** | Datum importu [persistentní položka] | [optional] 
**OperUnit** | **string** | Jednotka | [optional] 
**TotalAmount** | **double?** | Celková částka | [optional] 
**ExternalCreator** | **string** | Zdroj importu | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

