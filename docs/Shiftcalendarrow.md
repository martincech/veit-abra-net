# Veit.Abra.Model.Shiftcalendarrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Pracovní kalendář [persistentní položka] | [optional] 
**ShiftDateDATE** | **DateTimeOffset?** | Datum [persistentní položka] | [optional] 
**ShiftGenCode** | **string** | Kód [persistentní položka] | [optional] 
**ShiftBaseType** | **int?** | Typ [persistentní položka] | [optional] 
**InitialHour** | **double?** | Hodina od [persistentní položka] | [optional] 
**HoursShift** | **double?** | Délka [persistentní položka] | [optional] 
**HoursCount** | **double?** | Hodiny [persistentní položka] | [optional] 
**HoursNight** | **double?** | V noci [persistentní položka] | [optional] 
**HoursEnvir** | **double?** | V prostředí [persistentní položka] | [optional] 
**HoursHoliday** | **double?** | Svátek [persistentní položka] | [optional] 
**HoursPausesTotal** | **double?** | Přestávky celkem | [optional] 
**ShiftBaseTypeText** | **string** | Základní typ | [optional] 
**OriginDate** | **DateTimeOffset?** | Datum | [optional] 
**Pauses** | [**List&lt;Shiftcalendarpause&gt;**](Shiftcalendarpause.md) | Přestávky; kolekce BO Přestávka v plánované směně [nepersistentní položka] | [optional] 
**InitialHourStr** | **string** | Hodina od | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

