# Veit.Abra.Model.Payment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocumentID** | **string** | Plateb.dokl.; ID objektu Dokument [persistentní položka] | [optional] 
**DocumentType** | **string** | Typ plateb.dokl. [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**PDocumentID** | **string** | Plac.dokl.; ID objektu Dokument [persistentní položka] | [optional] 
**PDocumentType** | **string** | Typ plac.dokl. [persistentní položka] | [optional] 
**RowID** | **string** | Řádek plac.dokl.; ID objektu Řádek [persistentní položka] | [optional] 
**LocalAmount** | **double?** | Částka (lok.) [persistentní položka] | [optional] 
**VATDateDATE** | **DateTimeOffset?** | Datum plnění/odpočtu [persistentní položka] | [optional] 
**TAmountWithoutVAT** | **double?** | Bez daně [persistentní položka] | [optional] 
**LocalTAmountWithoutVAT** | **double?** | Bez daně lokálně [persistentní položka] | [optional] 
**TAmount** | **double?** | Celkem [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Celkem lokálně [persistentní položka] | [optional] 
**LocalAmountWithoutVATEET** | **double?** | Částka bez daně (lok.) využitá do tržby EET [persistentní položka] | [optional] 
**LocalAmountEET** | **double?** | Částka (lok.) [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

