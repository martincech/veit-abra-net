# Veit.Abra.Model.Vatclosingdocument
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Uzávěrka; ID objektu Uzávěrka DPH [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**DocumentType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**DocumentID** | **string** | ID dokladu; ID objektu Dokument [persistentní položka] | [optional] 
**IsInVATDeclarationReport** | **bool?** | Je v DPH přiznání [persistentní položka] | [optional] 
**IsInVATCheckReport** | **bool?** | Je v KV/KH DPH [persistentní položka] | [optional] 
**DocumentStatus** | **int?** | Stav dokumentu [persistentní položka] | [optional] 
**DocumentStatusAsText** | **string** | Stav dokumentu | [optional] 
**VATDateDATE** | **DateTimeOffset?** | Datum plnění | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

