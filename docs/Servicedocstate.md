# Veit.Abra.Model.Servicedocstate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název stavu [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**IsDefault** | **bool?** | Výchozí [persistentní položka] | [optional] 
**IsFinished** | **bool?** | Ukončený [persistentní položka] | [optional] 
**IsClosed** | **bool?** | Uzavřený [persistentní položka] | [optional] 
**IsInvoiced** | **bool?** | Fakturovaný [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

