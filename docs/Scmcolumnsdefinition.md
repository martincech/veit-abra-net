# Veit.Abra.Model.Scmcolumnsdefinition
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Scmcolumnsdefinitionrow&gt;**](Scmcolumnsdefinitionrow.md) | Řádky; kolekce BO Řádek definice sloupců [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**DataSourceSetID** | **string** | Sada datových zdrojů; ID objektu Sada datových zdrojů [persistentní položka] | [optional] 
**System** | **bool?** | Systémová [persistentní položka] | [optional] 
**SettingID** | **string** | Nastavení; ID objektu Nastavení SCM [persistentní položka] | [optional] 
**Groups** | [**List&lt;Scmcolumnsdefinitiongroup&gt;**](Scmcolumnsdefinitiongroup.md) | Skupiny; kolekce BO Skupina definice sloupců [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

