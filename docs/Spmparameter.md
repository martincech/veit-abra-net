# Veit.Abra.Model.Spmparameter
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Spmparameterrows&gt;**](Spmparameterrows.md) | Řádky; kolekce BO Řádek nastavení kompletace [nepersistentní položka] | [optional] 
**Site** | **string** | Hnízdo [persistentní položka] | [optional] 
**PreferredPriceOrigin** | **int?** | Preferované ceny pro kalkulace [persistentní položka] | [optional] 
**AutomaticallyCreateTransfers** | **bool?** | Při výdeji automaticky vytvářet převodky [persistentní položka] | [optional] 
**StoreID** | **string** | Kalkulační sklad materiálu; ID objektu Sklad [persistentní položka] | [optional] 
**HideALTabMaterialsSummared** | **bool?** | Skrýt v KL záložku Souhrn materiálu [persistentní položka] | [optional] 
**HideALTabMaterials** | **bool?** | Skrýt v KL záložku Výskyt materiálu [persistentní položka] | [optional] 
**UseMDForCUCosts** | **bool?** | Čerpat náklady přednostně z VMV [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

