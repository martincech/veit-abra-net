# Veit.Abra.Model.Installsetversionrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Verze instalační sady [persistentní položka] | [optional] 
**ISClassID** | **string** | Třída [persistentní položka] | [optional] 
**Identifier** | **string** | Identifikátor [persistentní položka] | [optional] 
**ISClassIDText** | **string** | Typ položky | [optional] 
**KindName** | **string** | Zařazení | [optional] 
**Name** | **string** | Název | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

