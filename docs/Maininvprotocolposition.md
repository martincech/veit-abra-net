# Veit.Abra.Model.Maininvprotocolposition
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Hlavní inventární protokol; ID objektu Hlavní inventární protokol [persistentní položka] | [optional] 
**StorePositionID** | **string** | Skladová pozice; ID objektu Skladová pozice [persistentní položka] | [optional] 
**Completed** | **bool?** | Kompletní [persistentní položka] | [optional] 
**Closed** | **bool?** | Uzavřena [persistentní položka] | [optional] 
**OrderFlow** | **int?** | OrderFlow [persistentní položka] | [optional] 
**InventoryBeginDATE** | **DateTimeOffset?** | Vložení do inventury [persistentní položka] | [optional] 
**ReadyToClose** | **bool?** | K uzavření | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

