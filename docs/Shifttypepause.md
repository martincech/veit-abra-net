# Veit.Abra.Model.Shifttypepause
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Druh pracovní směny [persistentní položka] | [optional] 
**RelativeInitialHour** | **double?** | Počáteční hodina přestávky [persistentní položka] | [optional] 
**InitialHour** | **double?** | Hodina od | [optional] 
**InitialHourStr** | **string** | Hodina od | [optional] 
**HoursPause** | **double?** | Délka trvání přestávky (v hodinách) [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

