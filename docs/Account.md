# Veit.Abra.Model.Account
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Účet [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**AccountType** | **string** | Typ [persistentní položka] | [optional] 
**Taxable** | **bool?** | Daňový [persistentní položka] | [optional] 
**Short** | **bool?** | Krátkodobý [persistentní položka] | [optional] 
**AccountTypeName** | **string** | Typ účtu | [optional] 
**IsIncomplete** | **bool?** | Neúplný [persistentní položka] | [optional] 
**Transferable** | **bool?** | Přenášet koncové stavy [persistentní položka] | [optional] 
**IsCostForProjectControl** | **bool?** | Náklad proj.ř. [persistentní položka] | [optional] 
**IsRevenueForProjectControl** | **bool?** | Výnos proj.ř. [persistentní položka] | [optional] 
**TypeOfActivity** | **int?** | Ekonomická činnost [persistentní položka] | [optional] 
**ParentID** | **string** | Nadřízený účet; ID objektu Středisko [persistentní položka] | [optional] 
**ParentCode** | **string** | Kód nadřízeného účtu | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

