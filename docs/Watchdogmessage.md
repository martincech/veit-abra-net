# Veit.Abra.Model.Watchdogmessage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**WatchDogPointID** | **string** | Kontrolní bod; ID objektu Kontrolní bod [persistentní položka] | [optional] 
**FieldValues** | **byte[]** | Data [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**ObjectID** | **string** | Identifikace objektu [persistentní položka] | [optional] 
**ObjectCLSID** | **string** | Třída objektu [persistentní položka] | [optional] 
**FieldNames** | **string** | Sloupce [persistentní položka] | [optional] 
**Status** | **int?** | Stav [persistentní položka] | [optional] 
**SentAtDATE** | **DateTimeOffset?** | Odesláno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

