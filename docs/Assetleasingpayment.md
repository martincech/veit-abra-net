# Veit.Abra.Model.Assetleasingpayment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavičkový objekt [persistentní položka] | [optional] 
**PaymentDateDATE** | **DateTimeOffset?** | Splatno [persistentní položka] | [optional] 
**ToPayTotalAmount** | **double?** | Částka k úhradě [persistentní položka] | [optional] 
**ToPayVAT** | **double?** | DPH k úhradě [persistentní položka] | [optional] 
**IsGenerated** | **bool?** | Předpis platby námi generovaný [persistentní položka] | [optional] 
**ToPayDocType** | **string** | Druh dokladu předpisu leasingové splátky [persistentní položka] | [optional] 
**ToPayDocID** | **string** | Odkaz na doklad, který je předpisem leasingové splátky.; ID objektu Dokument [persistentní položka] | [optional] 
**HireAmount** | **double?** | Nájemné bez daně [persistentní položka] | [optional] 
**HireInsuaranceAmount** | **double?** | Pojištění [persistentní položka] | [optional] 
**HireAmountVAT** | **double?** | Nájemné DPH [persistentní položka] | [optional] 
**HireDocID** | **string** | Odkaz na vygenerovaný interní doklad na nájemné; ID objektu Ostatní výdaj [persistentní položka] | [optional] 
**PaymentOrderID** | **string** | Odkaz na vygenerovanou žádost o platební příkaz; ID objektu Platební příkaz [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

