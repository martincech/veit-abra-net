# Veit.Abra.Model.Docconfirmation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Docconfirmationrow&gt;**](Docconfirmationrow.md) | Řádky; kolekce BO Schvalování dokladu - řádek [nepersistentní položka] | [optional] 
**DocType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**DocumentID** | **string** | ID schvalovaného dokladu; ID objektu Dokument [persistentní položka] | [optional] 
**StartedByID** | **string** | Schválení vyžádal; ID objektu Uživatel [persistentní položka] | [optional] 
**StartedDateDATE** | **DateTimeOffset?** | Schvalování započato [persistentní položka] | [optional] 
**ConfirmationState** | **int?** | Aktuální stav schvalování dokladu [persistentní položka] | [optional] 
**ActualConfirmationRound** | **int?** | Kolo [persistentní položka] | [optional] 
**FinishedDateDATE** | **DateTimeOffset?** | Schvalování dokončeno [persistentní položka] | [optional] 
**ConfirmationStateDesc** | **string** | Aktuální stav schvalování dokladu | [optional] 
**FinishedByID** | **string** | Schvalování ukončil; ID objektu Uživatel [persistentní položka] | [optional] 
**ExistsConfirmationDefs** | **bool?** | Existuje schvalovací scénář [persistentní položka] | [optional] 
**IsConfirmationRequired** | **bool?** | Je vyžadováno schválení [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

