# Veit.Abra.Model.Relation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**REL_DEF** | **int?** | ID Relační definice [persistentní položka] | [optional] 
**LEFTSIDE_ID** | **string** | Levá strana [persistentní položka] | [optional] 
**RIGHTSIDE_ID** | **string** | Pravá strana [persistentní položka] | [optional] 
**NUMVALUE** | **double?** | Hodnota vazby [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

