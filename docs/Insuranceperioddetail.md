# Veit.Abra.Model.Insuranceperioddetail
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavičkový objekt [persistentní položka] | [optional] 
**WagePeriodID** | **string** | Mzdové období; ID objektu Mzdové období [persistentní položka] | [optional] 
**InsuranceBase** | **double?** | Vyměřovací základ [persistentní položka] | [optional] 
**InsBaseCorrection** | **double?** | Vym.základ-opr. [persistentní položka] | [optional] 
**ExclDaysInsBase** | **double?** | VZ při vyl.době [persistentní položka] | [optional] 
**ExclDaysInsBaseCorrection** | **double?** | VZ při vyl.době-opr [persistentní položka] | [optional] 
**ExclDays** | **int?** | Vyloučená doba [persistentní položka] | [optional] 
**ExclDaysCorrection** | **int?** | Vyloučená doba-opr. [persistentní položka] | [optional] 
**InsuranceBaseTotal** | **double?** | Celkový vym.základ | [optional] 
**ExclDaysInsBaseTotal** | **double?** | Celkový vym.základ v době vyloučené doby | [optional] 
**ExclDaysTotal** | **int?** | Celkové vyloučená doba | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

