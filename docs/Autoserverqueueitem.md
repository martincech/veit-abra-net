# Veit.Abra.Model.Autoserverqueueitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**TaskCLSID** | **string** | Typ úlohy [persistentní položka] | [optional] 
**TaskParameters** | **string** | Parametry úlohy [persistentní položka] | [optional] 
**TaskDisplayName** | **string** | Název úlohy | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**RunAsID** | **string** | Spustit jako; ID objektu Uživatel [persistentní položka] | [optional] 
**RunAsPassword** | **string** | Heslo | [optional] 
**Subject** | **string** | Předmět [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**StateKind** | **int?** | Stav úlohy [persistentní položka] | [optional] 
**StateKindStr** | **string** | Stav úlohy (popis) | [optional] 
**SchedulerItemID** | **string** | Odkaz na úlohu [persistentní položka] | [optional] 
**Recipients** | [**List&lt;Autoserverqueueitemrecipient&gt;**](Autoserverqueueitemrecipient.md) | Adresáti; kolekce BO Úloha ve frontě automatizačního serveru - adresát [nepersistentní položka] | [optional] 
**SPID** | **int?** | PID [persistentní položka] | [optional] 
**SchedulerCode** | **string** | Kód úlohy | [optional] 
**SchedulerName** | **string** | Název úlohy | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

