# Veit.Abra.Model.Firmcategorymetadata
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Firma [persistentní položka] | [optional] 
**CategoryItemID** | **string** | Kategorizační údaj; ID objektu Kategorizační údaj [persistentní položka] | [optional] 
**CategoryItemGroupID** | **string** | Skupina kat. údajů; ID objektu Skupina kategorizačních údajů | [optional] 
**StringValue** | **string** | Hodnota [persistentní položka] | [optional] 
**CategoryUpdateMode** | **int?** | Způsob akt. [persistentní položka] | [optional] 
**ChangeDateDATE** | **DateTimeOffset?** | Datum akt. [persistentní položka] | [optional] 
**StringData** | **string** | Hodnota | [optional] 
**RelatedIDData** | **string** | Hodnota | [optional] 
**IntegerData** | **int?** | Hodnota | [optional] 
**DoubleData** | **double?** | Hodnota | [optional] 
**BooleanData** | **bool?** | Hodnota | [optional] 
**DateData** | **DateTimeOffset?** | Hodnota | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

