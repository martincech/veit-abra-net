# Veit.Abra.Model.Storecarddrcarticle
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavičkový objekt [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**DRCVATMode** | **bool?** | Režim Přenesení daňové povinnosti [persistentní položka] | [optional] 
**DRCArticleID** | **string** | Typ plnění - přenesení DP; ID objektu Kód typu plnění [persistentní položka] | [optional] 
**DRCArticleUnitRate** | **double?** | Vztah jednotky typu plnění [persistentní položka] | [optional] 
**DRCArticleUnitRateRef** | **double?** | Ref.vztah jednotky typu plnění [persistentní položka] | [optional] 
**DRCArticleQUnit** | **string** | Vykazovaná jednotka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

