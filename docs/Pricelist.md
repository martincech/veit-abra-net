# Veit.Abra.Model.Pricelist
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Pricelistvalidity&gt;**](Pricelistvalidity.md) | Řádky; kolekce BO Datové platnosti ceníku [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**Comment** | **string** | Poznámka | [optional] 
**ManagedByID** | **string** | Správce ceníku; ID objektu Uživatel [persistentní položka] | [optional] 
**CreationDateDATE** | **DateTimeOffset?** | Datum vytvoření ceníku [persistentní položka] | [optional] 
**DealerDiscountExcluded** | **bool?** | Neuplatňovat dealerské slevy [persistentní položka] | [optional] 
**IndividualDiscountExcluded** | **bool?** | Neuplatňovat individuální slevy [persistentní položka] | [optional] 
**FinancialDiscountExcluded** | **bool?** | Neuplatňovat finanční slevy [persistentní položka] | [optional] 
**QuantityDiscountExcluded** | **bool?** | Neuplatňovat množstevní slevy [persistentní položka] | [optional] 
**DocumentDiscountExcluded** | **bool?** | Vyloučeno z dodatečné slevy [persistentní položka] | [optional] 
**PriceListRoundings** | [**List&lt;Pricelistrounding&gt;**](Pricelistrounding.md) | Zaokrouhlování ceníku; kolekce BO Zaokrouhlování ceníku [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

