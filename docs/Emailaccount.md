# Veit.Abra.Model.Emailaccount
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**AccountName** | **string** | Název účtu [persistentní položka] | [optional] 
**AccountAddress** | **string** | E-mailová adresa [persistentní položka] | [optional] 
**Login** | **string** | Přihlašovací jméno [persistentní položka] | [optional] 
**SecPassword** | **string** | Zakódované heslo [persistentní položka] | [optional] 
**Password** | **string** | Heslo | [optional] 
**Server** | **string** | POP3 server [persistentní položka] | [optional] 
**Port** | **int?** | Port [persistentní položka] | [optional] 
**LeaveMsgOnServer** | **bool?** | Nemazat zprávy na serveru [persistentní položka] | [optional] 
**EmailSMTPAccountID** | **string** | Účet odchozího serveru SMTP; ID objektu E-mailový účet odchozího serveru SMTP [persistentní položka] | [optional] 
**ReceivedMailDocQueueID** | **string** | Řada dokladů doručených e-mailů; ID objektu Řada dokladů [persistentní položka] | [optional] 
**SendMailDocQueueID** | **string** | Řada dokladů odeslaných e-mailů; ID objektu Řada dokladů [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch. případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**DefaultText** | **string** | Výchozí text [persistentní položka] | [optional] 
**MaxSizeLimit** | **int?** | Max. velikost přílohy (MB) [persistentní položka] | [optional] 
**OwnerID** | **string** | Vlastník účtu; ID objektu Uživatel [persistentní položka] | [optional] 
**ConnectionSecurity** | **int?** | Zabezpečení [persistentní položka] | [optional] 
**AccountType** | **int?** | Typ účtu [persistentní položka] | [optional] 
**PersonalDeliveryDocQueueID** | **string** | Řada dokladů datových zpráv do vl. rukou; ID objektu Řada dokladů [persistentní položka] | [optional] 
**UseCredentialsToReceive** | **bool?** | Automaticky použít přihlašovací údaje pro příjem datových zpráv. [persistentní položka] | [optional] 
**UseCredentialsToSending** | **bool?** | Automaticky použít přihlašovací údaje pro odeslání datových zpráv. [persistentní položka] | [optional] 
**AccountTypeName** | **string** | Typ účtu - název | [optional] 
**TestDataBox** | **bool?** | Testovací účet datových schránek [persistentní položka] | [optional] 
**DataBoxType** | **int?** | Typ datové schránky [persistentní položka] | [optional] 
**EffectiveOVM** | **bool?** | Možno odesílat zprávy jako OVM [persistentní položka] | [optional] 
**DefaultTextSavedAs** | **int?** | Výchozí text uložit jako [persistentní položka] | [optional] 
**ReplyTo** | **string** | E-mailová adresa pro odpověď [persistentní položka] | [optional] 
**ReportEmailDelivery** | **bool?** | Oznámit doručení e-mailu [persistentní položka] | [optional] 
**ReportEmail** | **bool?** | Oznámit přečtení e-mailu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

