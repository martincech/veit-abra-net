# Veit.Abra.Model.Partialinvprotocol
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**DateOfBeginDATE** | **DateTimeOffset?** | Datum zahájení inventury [persistentní položka] | [optional] 
**DateOfEndDATE** | **DateTimeOffset?** | Datum ukončení inventury [persistentní položka] | [optional] 
**MainProtocolID** | **string** | Hlavní inventární protokol; ID objektu Hlavní inventární protokol [persistentní položka] | [optional] 
**Closed** | **bool?** | Uzavřen [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**ReaderID** | **string** | Čtečka [persistentní položka] | [optional] 
**Correction** | **bool?** | Korekční doklad [persistentní položka] | [optional] 
**UserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**AddRows** | **bool?** | Povoleno přidávat řádky [persistentní položka] | [optional] 
**PrefillBatchesAndSNr** | **bool?** | Předvyplňovat ke skladovým kartám nenulové šarže/sér. čísla [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

