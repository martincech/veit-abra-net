# Veit.Abra.Model.Demandsheetrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Poptávkový list [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**RowType** | **int?** | Typ řádku [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**Quantity** | **double?** | Počet v jedn.se vzt.1 [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Počet | [optional] 
**DemandAnswersCount** | **int?** | Osloveno | [optional] 
**DemandAnswersAnsweredCount** | **int?** | Odpovědělo | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**RequestedDeliveryDateDATE** | **DateTimeOffset?** | Požadovaný termín dodání [persistentní položka] | [optional] 
**LocalPriceLimit** | **double?** | Limitní cena [persistentní položka] | [optional] 
**DemandingDocType** | **string** | Typ zdrojového dokladu [persistentní položka] | [optional] 
**DemandingDocRowID** | **string** | Zdrojový řádek [persistentní položka] | [optional] 
**DemandingDocDisplayName** | **string** | Zdrojový doklad | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**WinnerFirmID** | **string** | Vítězný dodavatel/firma; ID objektu Firma | [optional] 
**DemandAnswers** | [**List&lt;Demandanswer&gt;**](Demandanswer.md) | Oslovení dodavatelé; kolekce BO Oslovený dodavatel pro poptávku [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

