# Veit.Abra.Model.Accpresetdefinition
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Accpresetdefinitionrow&gt;**](Accpresetdefinitionrow.md) | Řádky; kolekce BO Účetní předkontace - řádek [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**DocumentType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Basic** | **bool?** | Základní [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**SplitAmountDefs** | [**List&lt;Accpresetdefinitionsplitamountrow&gt;**](Accpresetdefinitionsplitamountrow.md) | Rozúčtování částek; kolekce BO Účetní předkontace - řádek rozúčtování částek [nepersistentní položka] | [optional] 
**ComputedDocQueue** | **string** | Hodnota pro interní potřebu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

