# Veit.Abra.Model.Assetinvlist
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Inventura majetku [persistentní položka] | [optional] 
**CommitteeID** | **string** | Inventarizační komise; ID objektu Inventarizační komise [persistentní položka] | [optional] 
**RestrictionRows** | [**List&lt;Assetinvlistrestriction&gt;**](Assetinvlistrestriction.md) | Omezení; kolekce BO Omezení seznamu inventury majetku [nepersistentní položka] | [optional] 
**IncludedAssetTypes** | **int?** | Omezení | [optional] 
**IncludedAssetTypeAsText** | **string** | Omezení | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

