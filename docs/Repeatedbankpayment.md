# Veit.Abra.Model.Repeatedbankpayment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**RepetitionCount** | **int?** | Opakování [persistentní položka] | [optional] 
**RepetitionKind** | **int?** | Způsob opakování [persistentní položka] | [optional] 
**RepeatToDateDATE** | **DateTimeOffset?** | Do data [persistentní položka] | [optional] 
**LastGenDateDATE** | **DateTimeOffset?** | Generováno [persistentní položka] | [optional] 
**RepetitionPeriod** | **int?** | Jak často [persistentní položka] | [optional] 
**RepetitionDay** | **int?** | Den [persistentní položka] | [optional] 
**RepetitionMonth** | **int?** | Měsíc [persistentní položka] | [optional] 
**WeekendCorrection** | **int?** | Korekce [persistentní položka] | [optional] 
**RepetitionDaysInWeek** | **string** | Dny v týdnu [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**ForeignIssue** | **int?** | Způsob úhrady dod.popl [persistentní položka] | [optional] 
**Urgent** | **int?** | Urgence [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**TargetBankAccount** | **string** | Bankovní účet [persistentní položka] | [optional] 
**TargetBankName** | **string** | Název banky [persistentní položka] | [optional] 
**VarSymbol** | **string** | Var. symbol [persistentní položka] | [optional] 
**ConstSymbolID** | **string** | Konst. symbol; ID objektu Konstantní symbol [persistentní položka] | [optional] 
**SpecSymbol** | **string** | Spec. symbol [persistentní položka] | [optional] 
**SwiftCode** | **string** | SWIFT [persistentní položka] | [optional] 
**BankAccountID** | **string** | Vlastní účet; ID objektu Bankovní účet [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**IsActive** | **bool?** | Aktivní | [optional] 
**TargetBankStreet** | **string** | Ulice adresy cílové banky [persistentní položka] | [optional] 
**TargetBankCity** | **string** | Město adresy cílové banky [persistentní položka] | [optional] 
**TargetBankPostCode** | **string** | PSČ/ZIP adresy cílové banky [persistentní položka] | [optional] 
**TargetBankCountry** | **string** | Země adresy cílové banky [persistentní položka] | [optional] 
**TargetBankCountryID** | **string** | Země cílové banky; ID objektu Země [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

