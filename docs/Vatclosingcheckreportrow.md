# Veit.Abra.Model.Vatclosingcheckreportrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Uzávěrka DPH [persistentní položka] | [optional] 
**SectionID** | **string** | Oddíl [persistentní položka] | [optional] 
**DocumentType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**DocumentID** | **string** | ID dokladu; ID objektu Dokument [persistentní položka] | [optional] 
**DocNumber** | **string** | Číslo dokladu [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**VATIdentNumber** | **string** | DIČ [persistentní položka] | [optional] 
**VATDateDATE** | **DateTimeOffset?** | Datum DPH [persistentní položka] | [optional] 
**SourceDocType** | **string** | Typ zdrojového dokladu [persistentní položka] | [optional] 
**SourceID** | **string** | Zdrojový doklad; ID objektu Dokument [persistentní položka] | [optional] 
**SourceDocNumber** | **string** | Číslo zdrojového dokladu [persistentní položka] | [optional] 
**VATRate** | **double?** | DPH sazba [persistentní položka] | [optional] 
**BaseAmount** | **double?** | Základ [persistentní položka] | [optional] 
**VATAmount** | **double?** | DPH [persistentní položka] | [optional] 
**AllowanceAmount** | **double?** | Odpočet [persistentní položka] | [optional] 
**DRCCode** | **string** | DRC kód [persistentní položka] | [optional] 
**DRCQuantity** | **double?** | DRC množství [persistentní položka] | [optional] 
**DRCQUnit** | **string** | DRC jednotka [persistentní položka] | [optional] 
**DRC_ID** | **string** | DRC ID; ID objektu Kód typu plnění [persistentní položka] | [optional] 
**DefinitionRowID** | **string** | ID řádku definice DPH; ID objektu Definice pro DPH přiznání - řádek [persistentní položka] | [optional] 
**TaxLineNumber** | **string** | Číslo řádku přiznání DPH [persistentní položka] | [optional] 
**VATReportMode** | **string** | Příznak KV/KH DPH [persistentní položka] | [optional] 
**SortDocNumber** | **string** | Číslo dokladu pro třídění [persistentní položka] | [optional] 
**CorrectionCode** | **string** | Kód korekce [persistentní položka] | [optional] 
**BaseAmountLow** | **double?** | Základ v nižší sazbě [persistentní položka] | [optional] 
**VATAmountLow** | **double?** | DPH v nižší sazbě [persistentní položka] | [optional] 
**TurnoverAmount** | **double?** | Obrat [persistentní položka] | [optional] 
**VATRateID** | **string** | ID DPH sazby; ID objektu DPH sazba [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

