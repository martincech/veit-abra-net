# Veit.Abra.Model.Paymentorder
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Paymentorderrow&gt;**](Paymentorderrow.md) | Řádky; kolekce BO Platební příkaz - řádek [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**DueDateDATE** | **DateTimeOffset?** | Datum spl. [persistentní položka] | [optional] 
**SentDateDATE** | **DateTimeOffset?** | Datum odesl. [persistentní položka] | [optional] 
**BankAccountID** | **string** | Vlastní účet; ID objektu Bankovní účet [persistentní položka] | [optional] 
**TotalAmount** | **double?** | Celková částka | [optional] 
**RowsCurrency** | **string** | Měna řádků | [optional] 
**Sent** | **bool?** | Odesláno [persistentní položka] | [optional] 
**HomeBankUsed** | **bool?** | Homebanking použit [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

