# Veit.Abra.Model.Sourcegroupidentical
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**SourceID** | **string** | Zdr. skupina zdroje; ID objektu Zdrojová skupina [persistentní položka] | [optional] 
**TargetID** | **string** | Zdr. skupina cíle; ID objektu Zdrojová skupina [persistentní položka] | [optional] 
**IsUser** | **bool?** | Uživatelský záznam [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

