# Veit.Abra.Model.Restrictiondefinition
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**UniqueID** | **string** | Jednoznačná identifikace definice omezení [persistentní položka] | [optional] 
**Kind** | **int?** | Druh [persistentní položka] | [optional] 
**ProgramPoint** | **string** | Místo v programu (program point) [persistentní položka] | [optional] 
**ProgramPointText** | **string** | Místo v programu (program point) | [optional] 
**UserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**DSQLValues** | **byte[]** | Obsah definice omezení [persistentní položka] | [optional] 
**ColDefName** | **string** | Název def.sloupců [persistentní položka] | [optional] 
**ColDefIsGlobal** | **bool?** | Def.sloupců je globální [persistentní položka] | [optional] 
**VisibilityKind** | **int?** | Viditelnost [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**ConditionsProps** | **string** | Vlastnosti podmínek [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**LastChangeDateDATE** | **DateTimeOffset?** | Datum poslední změny [persistentní položka] | [optional] 
**RestrictionSharings** | [**List&lt;Restrictionsharing&gt;**](Restrictionsharing.md) | Kolekce sdílení definice; kolekce BO Sdílení definic omezení [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

