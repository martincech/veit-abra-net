# Veit.Abra.Model.Filequeuerule
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**GoOn** | **bool?** | Pokračovat [persistentní položka] | [optional] 
**RuleActions** | [**List&lt;Filequeueruleaction&gt;**](Filequeueruleaction.md) | Akce; kolekce BO Akce pravidla pro zpracování front souborů [nepersistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**FileNameMask** | **string** | Maska pro název souboru [persistentní položka] | [optional] 
**FileNameMaskIsRegEx** | **bool?** | Maska názvu souborů je regulární výraz [persistentní položka] | [optional] 
**FileQueueID** | **string** | Fronta souborů; ID objektu Fronta souborů [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

