# Veit.Abra.Model.Wagecompensation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Mzdový list dílčí [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**OperType** | **string** | Část. výkon [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**RelativePart** | **bool?** | Poměrná část [persistentní položka] | [optional] 
**InInsurance** | **bool?** | Pojištění [persistentní položka] | [optional] 
**TotalAmount** | **double?** | Celková částka | [optional] 
**InsuranceBase** | **double?** | Základ pojištění | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

