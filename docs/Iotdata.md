# Veit.Abra.Model.Iotdata
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**UNID** | **string** | Identifikátor [persistentní položka] | [optional] 
**SensorID** | **string** | Senzor; ID objektu Senzor IoT [persistentní položka] | [optional] 
**SensorQuantity** | **string** | Veličina [persistentní položka] | [optional] 
**RecordedAtDATE** | **DateTimeOffset?** | Zaznamenáno [persistentní položka] | [optional] 
**ValueDataType** | **int?** | Typ dat [persistentní položka] | [optional] 
**NumericValue** | **double?** | Číselná hodnota [persistentní položka] | [optional] 
**ShortStringValue** | **string** | Textová hodnota [persistentní položka] | [optional] 
**DateTimeValueDATE** | **DateTimeOffset?** | Časová hodnota [persistentní položka] | [optional] 
**BinaryBlobValue** | **byte[]** | Binární data [persistentní položka] | [optional] 
**TextBlobValue** | **string** | Dlouhá textová hodnota [persistentní položka] | [optional] 
**BooleanValue** | **bool?** | Logická hodnota [persistentní položka] | [optional] 
**ObjectID** | **string** | Identifikátor objektu [persistentní položka] | [optional] 
**ObjectIDObj** | **string** | Objekt; ID objektu Obecný objekt | [optional] 
**ObjectClass** | **string** | Třída objektu [persistentní položka] | [optional] 
**ValueAsText** | **string** | Data | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

