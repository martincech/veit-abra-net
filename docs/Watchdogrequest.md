# Veit.Abra.Model.Watchdogrequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**WatchDogPointID** | **string** | Kontrolní bod; ID objektu Kontrolní bod [persistentní položka] | [optional] 
**ObjectCLSID** | **string** | Třída objektu [persistentní položka] | [optional] 
**ObjectClsidDisplayName** | **string** | Třída objektu | [optional] 
**ObjectID** | **string** | Identifikace objektu [persistentní položka] | [optional] 
**Condition** | **string** | Výraz [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**TriggeredAtDATE** | **DateTimeOffset?** | Použito [persistentní položka] | [optional] 
**Recipients** | [**List&lt;Watchdogrequestrecipient&gt;**](Watchdogrequestrecipient.md) | Adresáti; kolekce BO Definice automatického vzkazu - adresát [nepersistentní položka] | [optional] 
**MessageToSent** | **string** | Zpráva [persistentní položka] | [optional] 
**SiteCLSID** | **string** | Agenda [persistentní položka] | [optional] 
**SiteClsidDisplayName** | **string** | Agenda | [optional] 
**IsCondition** | **bool?** | Podmínka [persistentní položka] | [optional] 
**Subject** | **string** | Předmět [persistentní položka] | [optional] 
**ObjectDisplayName** | **string** | Název business objektu | [optional] 
**FieldNames** | **string** | Sloupce [persistentní položka] | [optional] 
**ConditionExtension** | **byte[]** | Rozšíření podmínky [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

