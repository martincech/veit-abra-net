# Veit.Abra.Model.Shifttype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ShiftGenCode** | **string** | Kód [persistentní položka] | [optional] 
**WeekDays** | **string** | Dny v týdnu [persistentní položka] | [optional] 
**ShiftBaseType** | **int?** | Základní typ [persistentní položka] | [optional] 
**InHolidays** | **bool?** | Ve svátek [persistentní položka] | [optional] 
**InitialHour** | **double?** | Hodina od [persistentní položka] | [optional] 
**InitialHourDATE** | **DateTimeOffset?** | Hodina od (ve std. formátu času) | [optional] 
**HoursShift** | **double?** | Délka [persistentní položka] | [optional] 
**HoursCount** | **double?** | Hodiny [persistentní položka] | [optional] 
**HoursNight** | **double?** | V noci [persistentní položka] | [optional] 
**HoursEnvir** | **double?** | V prostředí [persistentní položka] | [optional] 
**HoursPausesTotal** | **double?** | Přestávky celkem | [optional] 
**WeekDayMo** | **bool?** | po | [optional] 
**WeekDayTu** | **bool?** | út | [optional] 
**WeekDayWe** | **bool?** | st | [optional] 
**WeekDayTh** | **bool?** | čt | [optional] 
**WeekDayFr** | **bool?** | pá | [optional] 
**WeekDaySa** | **bool?** | so | [optional] 
**WeekDaySu** | **bool?** | ne | [optional] 
**ShiftBaseTypeText** | **string** | Základní typ | [optional] 
**CalendarColor** | **int?** | Barva [persistentní položka] | [optional] 
**CRPGroup** | **string** | Skupina [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Pauses** | [**List&lt;Shifttypepause&gt;**](Shifttypepause.md) | Přestávky; kolekce BO Přestávka v druhu pracovní směny [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

