# Veit.Abra.Model.Vatsummarydefinition
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Vatsummarydefinitionrow&gt;**](Vatsummarydefinitionrow.md) | Řádky; kolekce BO Definice pro DPH přiznání - řádek [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**PrefillExpressions** | [**List&lt;Vatsummarydefinitionprefillexpression&gt;**](Vatsummarydefinitionprefillexpression.md) | Předvyplnění; kolekce BO Definice pro DPH přiznání - předvyplnění [nepersistentní položka] | [optional] 
**ClosingKind** | **int?** | Typ přiznání [persistentní položka] | [optional] 
**ClosingKindAsText** | **string** | Typ přiznání | [optional] 
**NotForVATClosing** | **bool?** | Ne pro uzávěrku DPH [persistentní položka] | [optional] 
**VATReturnMethod** | **int?** | Metoda DPH přiznání [persistentní položka] | [optional] 
**VATReturnMethodAsText** | **string** | Metoda DPH přiznání | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platí od [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

