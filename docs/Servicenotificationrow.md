# Veit.Abra.Model.Servicenotificationrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Notifikace stavu serv.listu [persistentní položka] | [optional] 
**EmailAddress** | **string** | E-mailová adresa [persistentní položka] | [optional] 
**ServiceNotificationTextID** | **string** | Text notifikace; ID objektu Text notifikace stavu serv.listu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

