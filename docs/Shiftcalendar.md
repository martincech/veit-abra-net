# Veit.Abra.Model.Shiftcalendar
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Shiftcalendarrow&gt;**](Shiftcalendarrow.md) | Kalendář; kolekce BO Plánovaná směna [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Shifts** | [**List&lt;Shiftcalendarshift&gt;**](Shiftcalendarshift.md) | Směny; kolekce BO Pořadí směny [nepersistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Duty** | **double?** | Úvazek [persistentní položka] | [optional] 
**BeginingDATE** | **DateTimeOffset?** | První den [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

