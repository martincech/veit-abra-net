# Veit.Abra.Model.Assetinvlistcard
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Assetinvlistcardcomp&gt;**](Assetinvlistcardcomp.md) | Řádky; kolekce BO Inventura majetku - prvek karty majetku [nepersistentní položka] | [optional] 
**ParentID** | **string** | Odkaz na seznam; ID objektu Inventarizační seznam majetku [persistentní položka] | [optional] 
**AssetInvID** | **string** | Odkaz na inventuru; ID objektu Inventura majetku [persistentní položka] | [optional] 
**AssetCardType** | **int?** | Typ majetku [persistentní položka] | [optional] 
**AssetCardID** | **string** | Karta majetku; ID objektu Karta majetku [persistentní položka] | [optional] 
**IsCollection** | **int?** | Je kolekce [persistentní položka] | [optional] 
**SmallSubCardID** | **string** | Karta DrM; ID objektu Položka drobného majetku [persistentní položka] | [optional] 
**InventoryNr** | **string** | Inventární číslo [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**EAN** | **string** | EAN [persistentní položka] | [optional] 
**ResponsibleID** | **string** | Odpovědná osoba; ID objektu Odpovědná osoba [persistentní položka] | [optional] 
**AssetLocationID** | **string** | Umístění; ID objektu Umístění majetku [persistentní položka] | [optional] 
**EvidenceDivisionID** | **string** | Evidenční středisko; ID objektu Středisko [persistentní položka] | [optional] 
**ExpensesDivisionID** | **string** | Nákladové středisko; ID objektu Středisko [persistentní položka] | [optional] 
**Quantity** | **int?** | Počet [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

