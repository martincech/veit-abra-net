# Veit.Abra.Model.Wagetaxtable
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Wagetaxtablerow&gt;**](Wagetaxtablerow.md) | Řádky; kolekce BO Daňová tabulka - řádek [nepersistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platnost od [persistentní položka] | [optional] 
**TableType** | **int?** | Typ tabulky [persistentní položka] | [optional] 
**System** | **bool?** | Systémová [persistentní položka] | [optional] 
**TableTypeAsText** | **string** | Typ tabulky - text | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

