# Veit.Abra.Model.Servicedocumentdefect
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Servisní list [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**DefectText** | **string** | Závada textově [persistentní položka] | [optional] 
**ServiceDefectID** | **string** | Závada; ID objektu Servisní závada [persistentní položka] | [optional] 
**EstimatedPriceWithoutVAT** | **double?** | Odhadovaná cena bez daně [persistentní položka] | [optional] 
**EstimatedPrice** | **double?** | Odhadovaná cena s daní [persistentní položka] | [optional] 
**Charged** | **bool?** | Placená [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

