# Veit.Abra.Model.Storesubbatch
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**StoreBatchID** | **string** | Šarže; ID objektu Šarže/sériové číslo [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**Quantity** | **double?** | Počet [persistentní položka] | [optional] 
**MainUnitCode** | **string** | Hlavní jednotka | [optional] 
**MainUnitQuantity** | **double?** | Množství v hl. jednotce | [optional] 
**LocationID** | **string** | Místo; ID objektu Skladové místo [persistentní položka] | [optional] 
**GuaranteeEndDateDATE** | **DateTimeOffset?** | Datum záruky [persistentní položka] | [optional] 
**GuaranteeEndDateAsText** | **string** | Datum záruky | [optional] 
**BookedQuantity** | **double?** | Množství ve výdeji [persistentní položka] | [optional] 
**AcceptedQuantity** | **double?** | Přijímané množství [persistentní položka] | [optional] 
**AvailableQuantity** | **double?** | Dostupné množství | [optional] 
**MainUnitBookedQuantity** | **double?** | Blokované množství v hl. jednotce | [optional] 
**MainUnitAcceptedQuantity** | **double?** | Množství v příjmu v hl. jednotce | [optional] 
**MainUnitAvailableQuantity** | **double?** | Dostupné množství v hl. jednotce | [optional] 
**CanValidateOutOfStock** | **bool?** | Kontrola záporného množství | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

