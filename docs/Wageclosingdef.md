# Veit.Abra.Model.Wageclosingdef
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Periodicity** | **int?** | Periodicita [persistentní položka] | [optional] 
**CalcChartID** | **string** | Výpočtové schéma; ID objektu Výpočtové schéma [persistentní položka] | [optional] 
**ToAccounting** | **bool?** | Účtovat částku [persistentní položka] | [optional] 
**AccPresetDefID** | **string** | Předkontace; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**SplitByOperations** | **bool?** | Rozdělit [persistentní položka] | [optional] 
**OperationCodes** | **string** | Výkony [persistentní položka] | [optional] 
**ToPaymentOrder** | **bool?** | Platební příkaz [persistentní položka] | [optional] 
**MinusSign** | **bool?** | Záporná částka [persistentní položka] | [optional] 
**PaymentDefID** | **string** | Definice platby; ID objektu Definice plateb [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada dokladů uzávěrky; ID objektu Řada dokladů [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**Rounding** | **int?** | Zaokrouhlení [persistentní položka] | [optional] 
**CorrectionType** | **int?** | Typ korekce [persistentní položka] | [optional] 
**CorrectionExpr** | **string** | Korekce [persistentní položka] | [optional] 
**CorrectionTypeAsText** | **string** | Typ korekce | [optional] 
**OnlyEvalExpression** | **bool?** | Jen vyhodnotit výraz [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

