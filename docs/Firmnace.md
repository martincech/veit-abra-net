# Veit.Abra.Model.Firmnace
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Firma [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**NaceID** | **string** | NACE kód; ID objektu NACE kód [persistentní položka] | [optional] 
**NACEUpdateMode** | **int?** | Způsob akt. [persistentní položka] | [optional] 
**ChangeDateDATE** | **DateTimeOffset?** | Datum akt. [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

