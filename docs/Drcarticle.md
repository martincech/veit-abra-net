# Veit.Abra.Model.Drcarticle
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód typu [persistentní položka] | [optional] 
**Name** | **string** | Popis [persistentní položka] | [optional] 
**QUnit** | **string** | Vykazovaná jednotka [persistentní položka] | [optional] 
**OutOfUse** | **bool?** | Nepoužívaný [persistentní položka] | [optional] 
**LimitAmount** | **double?** | Limit [persistentní položka] | [optional] 
**LimitGroup** | **string** | Skupina pro limit [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**VATCheckReportKind** | **int?** | Vykázat v KV/KH DPH [persistentní položka] | [optional] 
**VATCheckReportKindAsText** | **string** | Vykázat v KV/KH DPH | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

