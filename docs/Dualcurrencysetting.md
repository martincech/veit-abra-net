# Veit.Abra.Model.Dualcurrencysetting
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DateFromDATE** | **DateTimeOffset?** | Platný od [persistentní položka] | [optional] 
**DisplayDual** | **bool?** | Zobrazovat duálně [persistentní položka] | [optional] 
**LocalCurrencyID** | **string** | Lokální měna; ID objektu Měna [persistentní položka] | [optional] 
**DualCurrencyID** | **string** | Duální měna; ID objektu Měna [persistentní položka] | [optional] 
**CurrRate** | **double?** | Fixní kurz [persistentní položka] | [optional] 
**RefCurrRate** | **double?** | Vztažný kurz [persistentní položka] | [optional] 
**Rounding** | **int?** | Zaokrouhlení [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

