# Veit.Abra.Model.Autoservertaskendlog
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**FinishedAtDATE** | **DateTimeOffset?** | Čas dokončení [persistentní položka] | [optional] 
**TaskLogID** | **string** | Začátek logu [persistentní položka] | [optional] 
**ResultKind** | **int?** | Výsledek [persistentní položka] | [optional] 
**Note** | **string** | Popis [persistentní položka] | [optional] 
**ResultKindStr** | **string** | Výsledek (textově) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

