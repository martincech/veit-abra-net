# Veit.Abra.Model.Wageperiod
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DateFromDATE** | **DateTimeOffset?** | Od [persistentní položka] | [optional] 
**DateToDATE** | **DateTimeOffset?** | Do [persistentní položka] | [optional] 
**WPeriodYear** | **int?** | Rok | [optional] 
**WPeriodMonth** | **int?** | Měsíc | [optional] 
**PeriodID** | **string** | Fisk.období; ID objektu Období [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**WPeriodClosed** | **bool?** | Uzav. [persistentní položka] | [optional] 
**WPeriodInitialized** | **bool?** | Init. [persistentní položka] | [optional] 
**Terminator** | **bool?** | Term. [persistentní položka] | [optional] 
**FirstWPeriod** | **bool?** | První [persistentní položka] | [optional] 
**WPeriodIsActual** | **bool?** | Aktivní | [optional] 
**StateText** | **string** | Stav | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

