# Veit.Abra.Model.Serviceoperationvariants
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Řádek servisní operace [persistentní položka] | [optional] 
**ServiceObjectTypeID** | **string** | Model; ID objektu Model servisovaného předmětu [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**QUnit** | **string** | Jed. [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**WorkHoursPlanned** | **double?** | Plán.hodin [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 
**VATRateID** | **string** | Sazba DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**TotalAmountWithoutVAT** | **double?** | Celkem bez DPH | [optional] 
**TotalAmountWithVAT** | **double?** | Celkem s DPH | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

