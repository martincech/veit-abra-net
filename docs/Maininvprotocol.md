# Veit.Abra.Model.Maininvprotocol
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**DateOfEndDATE** | **DateTimeOffset?** | Rozhodné datum inventury [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**Closed** | **bool?** | Ukončen [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**StartingLogistic** | **bool?** | Zahájení polohování [persistentní položka] | [optional] 
**InventoryType** | **int?** | Typ inventury [persistentní položka] | [optional] 
**InventoryTypeDesc** | **string** | Popis typu inventury | [optional] 
**AddRowsFromPIP** | **bool?** | Přidávat řádky z DIP [persistentní položka] | [optional] 
**StartedAtDATE** | **DateTimeOffset?** | Zahájení inventury [persistentní položka] | [optional] 
**Started** | **bool?** | Inventura zahájena | [optional] 
**CheckDiffContents** | **bool?** | Kontrola rozdílu počtu na dílčích skladových kartách vůči počtu na pozicích [persistentní položka] | [optional] 
**CheckSDocsInPreparation** | **bool?** | Kontrola na skladové pohyby ve stavu \&quot;V přípravě\&quot; [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

