# Veit.Abra.Model.Penaltyinvoicerow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Sankční faktura [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**TAmount** | **double?** | Celkem [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Celkem lokálně [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | O.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**IncomeTypeID** | **string** | Typ příjmu; ID objektu Typ příjmu [persistentní položka] | [optional] 
**DatePenaltyFromDATE** | **DateTimeOffset?** | Poč. dat. sankce [persistentní položka] | [optional] 
**DatePenaltyToDATE** | **DateTimeOffset?** | Konc. dat. sankce [persistentní položka] | [optional] 
**AmountInvoice** | **double?** | Částka pro výp. sankce [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**PenaltyPercent** | **double?** | Úrok z prodlení [%] [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**NumberPenaltyDays** | **int?** | Počet dní | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

