# Veit.Abra.Model.Useremailhistory
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DateDATE** | **DateTimeOffset?** | Datum použití [persistentní položka] | [optional] 
**UserID** | **string** | Osoba; ID objektu Uživatel [persistentní položka] | [optional] 
**Email** | **string** | E-mail [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

