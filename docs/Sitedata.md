# Veit.Abra.Model.Sitedata
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Site** | **string** | Dat.hnízdo [persistentní položka] | [optional] 
**AddressID** | [**Address**](Address.md) |  | [optional] 
**GeneratedOIDInfix** | **string** | OID infix [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

