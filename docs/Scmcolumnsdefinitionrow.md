# Veit.Abra.Model.Scmcolumnsdefinitionrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Definice sloupců bilance [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Caption** | **string** | Popisek [persistentní položka] | [optional] 
**IsCalculated** | **bool?** | Počítaný sloupec [persistentní položka] | [optional] 
**DataSourceID** | **string** | Datový zdroj; ID objektu Datový zdroj [persistentní položka] | [optional] 
**FieldName** | **string** | Sloupec [persistentní položka] | [optional] 
**DataType** | **int?** | Datový typ [persistentní položka] | [optional] 
**Kind** | **int?** | Typ sloupce [persistentní položka] | [optional] 
**DisplayWidth** | **int?** | Zobrazovací velikost [persistentní položka] | [optional] 
**AggregateFunction** | **int?** | Agregační funkce [persistentní položka] | [optional] 
**IncludeToResult** | **bool?** | Započítat [persistentní položka] | [optional] 
**Expression** | **string** | Výraz [persistentní položka] | [optional] 
**ShowCondition** | **string** | Zobrazovací podmínka [persistentní položka] | [optional] 
**Conditions** | **byte[]** | Podmínky [persistentní položka] | [optional] 
**System** | **bool?** | Systémový [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

