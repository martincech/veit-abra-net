# Veit.Abra.Model.Reservation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**RevidedID** | **string** | ID revidovaného objektu; ID objektu Skladová rezervace | [optional] 
**RevisionDescription** | **string** | Popis revize | [optional] 
**RevisionDateDATE** | **DateTimeOffset?** | Datum revize | [optional] 
**RevisionAuthorID** | **string** | Autor revize; ID objektu Uživatel | [optional] 
**Revision** | **int?** | Číslo revize | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**StoreCardID** | **string** | Sklad. karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**Priority** | **int?** | Priorita [persistentní položka] | [optional] 
**DateToDATE** | **DateTimeOffset?** | Rezervovat do [persistentní položka] | [optional] 
**DateFromDATE** | **DateTimeOffset?** | Rezervovat od [persistentní položka] | [optional] 
**Reserved** | **double?** | Množství [persistentní položka] | [optional] 
**Supplied** | **double?** | Vydodané množ. [persistentní položka] | [optional] 
**OrderDateDATE** | **DateTimeOffset?** | Čas objednání [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**UnitReserved** | **double?** | Rezervováno | [optional] 
**UnitSupplied** | **double?** | Splněno | [optional] 
**UnitActualReserved** | **double?** | Akt. rezervováno | [optional] 
**OwnerID** | **string** | Vlastník [persistentní položka] | [optional] 
**OwnerKind** | **int?** | Typ vlastníka [persistentní položka] | [optional] 
**OwnerDisplayName** | **string** | Název vlastníka | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch. případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

