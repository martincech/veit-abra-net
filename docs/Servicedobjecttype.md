# Veit.Abra.Model.Servicedobjecttype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**ParentID** | **string** | Nadřízený model; ID objektu Model servisovaného předmětu [persistentní položka] | [optional] 
**Name** | **string** | Model [persistentní položka] | [optional] 
**ServiceTypeID** | **string** | Typ servisního případu; ID objektu Typ servisního případu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

