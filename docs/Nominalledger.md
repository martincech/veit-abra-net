# Veit.Abra.Model.Nominalledger
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**AccountID** | **string** | Účet; ID objektu Účet účetního rozvrhu | [optional] 
**CreditAmount** | **double?** | Dal | [optional] 
**DebitAmount** | **double?** | Má dáti | [optional] 
**FinalAmount** | **double?** | Konečný stav | [optional] 
**BeginningCreditAmount** | **double?** | Počátek Dal | [optional] 
**BeginningDebitAmount** | **double?** | Počátek Má dáti | [optional] 
**BeginAmount** | **double?** | Počáteční stav | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

