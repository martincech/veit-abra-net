# Veit.Abra.Model.Userdefinedform
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**CLSID** | **string** | Třída [persistentní položka] | [optional] 
**System** | **bool?** | Syst. [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Data** | **byte[]** | Definice [persistentní položka] | [optional] 
**SourceCode** | **string** | Zdrojový kód | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**OwnerID** | **string** | Vlastník; ID objektu Uživatel [persistentní položka] | [optional] 
**CLSIDText** | **string** | Třída (textově) | [optional] 
**DefinitionID** | **string** | GUID definice [persistentní položka] | [optional] 
**Preferred** | **bool?** | Preferovaný [persistentní položka] | [optional] 
**SGlobal** | **bool?** | Globální | [optional] 
**VisibleFromDATE** | **DateTimeOffset?** | Viditelnost od [persistentní položka] | [optional] 
**VisibleToDATE** | **DateTimeOffset?** | Viditelnost do [persistentní položka] | [optional] 
**Hash** | **string** | Hash [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

