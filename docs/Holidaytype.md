# Veit.Abra.Model.Holidaytype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Regular** | **bool?** | Řádná [persistentní položka] | [optional] 
**Claim** | **double?** | Nárok [persistentní položka] | [optional] 
**GoOn** | **bool?** | Přenos [persistentní položka] | [optional] 
**RestTransfer** | **double?** | Zbytek [persistentní položka] | [optional] 
**ValidFromMonthDay** | **int?** | Měsíc, den od [persistentní položka] | [optional] 
**ValidFromMonth** | **int?** | Měsíc od | [optional] 
**ValidFromDay** | **int?** | Den od | [optional] 
**ValidFromDate** | **string** | Datum od | [optional] 
**ValidToMonthDay** | **int?** | Měsíc, den do [persistentní položka] | [optional] 
**ValidToMonth** | **int?** | Měsíc do | [optional] 
**ValidToDay** | **int?** | Den do | [optional] 
**ValidToDate** | **string** | Datum do | [optional] 
**Priority** | **int?** | Priorita [persistentní položka] | [optional] 
**PayAble** | **bool?** | Proplácet [persistentní položka] | [optional] 
**ClaimType** | **int?** | Jednotka nároku [persistentní položka] | [optional] 
**TransferWholeRest** | **bool?** | Přenášet vše [persistentní položka] | [optional] 
**ClaimByExpression** | **bool?** | Nárok výrazem [persistentní položka] | [optional] 
**ClaimExpression** | **string** | Výraz nároku [persistentní položka] | [optional] 
**ClaimText** | **string** | Nárok textově | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

