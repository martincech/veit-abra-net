# Veit.Abra.Model.Crmcampaign
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Crmcampaignaudience&gt;**](Crmcampaignaudience.md) | Adresáti kampaní; kolekce BO Adresát kampaně [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**CampaignName** | **string** | Název kampaně [persistentní položka] | [optional] 
**CampaignTypeID** | **string** | Typ kampaně; ID objektu Typ kampaně [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch. případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**CampaignState** | **int?** | Stav [persistentní položka] | [optional] 
**Budget** | **double?** | Rozpočet [persistentní položka] | [optional] 
**RealExpenses** | **double?** | Skutečné výdaje [persistentní položka] | [optional] 
**ScheduledStartDateDATE** | **DateTimeOffset?** | Plánovaný čas zahájení [persistentní položka] | [optional] 
**ScheduledEndDateDATE** | **DateTimeOffset?** | Plánovaný čas dokončení [persistentní položka] | [optional] 
**RealStartDateDATE** | **DateTimeOffset?** | Skutečný čas zahájení [persistentní položka] | [optional] 
**RealEndDateDATE** | **DateTimeOffset?** | Skutečný čas dokončení [persistentní položka] | [optional] 
**CampaignErrors** | **string** | Chyby [persistentní položka] | [optional] 
**CampaignCheckState** | **int?** | Stav kontroly [persistentní položka] | [optional] 
**CampaignCheckStateText** | **string** | Stav kontroly | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

