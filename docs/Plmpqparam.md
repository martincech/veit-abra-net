# Veit.Abra.Model.Plmpqparam
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada dokladů; ID objektu Řada dokladů [persistentní položka] | [optional] 
**AuthorizationRequired** | **int?** | Požadováno schválení [persistentní položka] | [optional] 
**DocQueueForJOID** | **string** | Řada dokladů VP; ID objektu Řada dokladů [persistentní položka] | [optional] 
**TariffForJOID** | **string** | Režijní sazba; ID objektu Režijní sazba [persistentní položka] | [optional] 
**EditNorm** | **bool?** | Povoleno editovat normu [persistentní položka] | [optional] 
**DefaultManEntRtnID** | **string** | Výchozí typ tech. postupu pro ruční editaci; ID objektu Typ technologického postupu [persistentní položka] | [optional] 
**NoShowDialog** | **bool?** | Nezobrazit dialog [persistentní položka] | [optional] 
**MethodOfSalaryCalculation** | **int?** | Hodinová sazba operace techn. postupu [persistentní položka] | [optional] 
**MethodOfSalaryClass** | **int?** | Tarifní třída pro ocenění výrobku [persistentní položka] | [optional] 
**DefaultSettingOfMIX** | **bool?** | Výchozí nastavení položky MIX [persistentní položka] | [optional] 
**DefaultSupposedStoreID** | **string** | Výchozí předpokládaný sklad; ID objektu Sklad [persistentní položka] | [optional] 
**InPlan** | **bool?** | Započítat do kapacitního plánu [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**ChoosingRoutine** | **int?** | Výběr TP [persistentní položka] | [optional] 
**ChoosingPL** | **int?** | Výběr kus. [persistentní položka] | [optional] 
**DefaultCanceled** | **bool?** | Výchozí - vyloučený z realizace  [persistentní položka] | [optional] 
**CalculateOneTBCForDocument** | **bool?** | Kalkulovat jeden přípravný čas [persistentní položka] | [optional] 
**EmptyPieceListSoftValidation** | **bool?** | Kontrolovat prázdný kusovník [persistentní položka] | [optional] 
**EmptyRoutineSoftValidation** | **bool?** | Kontrolovat prázdný tech. postup [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

