# Veit.Abra.Model.Bookentry
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**AccDateDATE** | **DateTimeOffset?** | Datum účtování [persistentní položka] | [optional] 
**AccDocQueueID** | **string** | Účetní řada; ID objektu Účetní řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**AmountInCurrency** | **double?** | Cizí částka [persistentní položka] | [optional] 
**Audited** | **bool?** | Audit [persistentní položka] | [optional] 
**AccGroupID** | **string** | Účetní skupina; ID objektu Účetní skupina [persistentní položka] | [optional] 
**IsRequest** | **bool?** | Žádost [persistentní položka] | [optional] 
**DebitAccountID** | **string** | Účet MD; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**DebitDivisionID** | **string** | Středisko MD; ID objektu Středisko [persistentní položka] | [optional] 
**DebitBusOrderID** | **string** | Zakázka MD; ID objektu Zakázka [persistentní položka] | [optional] 
**DebitBusTransactionID** | **string** | O.případ MD; ID objektu Obchodní případ [persistentní položka] | [optional] 
**DebitBusProjectID** | **string** | Projekt MD; ID objektu Projekt [persistentní položka] | [optional] 
**CreditAccountID** | **string** | Účet D; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**CreditDivisionID** | **string** | Středisko D; ID objektu Středisko [persistentní položka] | [optional] 
**CreditBusOrderID** | **string** | Zakázka D; ID objektu Zakázka [persistentní položka] | [optional] 
**CreditBusTransactionID** | **string** | O.případ D; ID objektu Obchodní případ [persistentní položka] | [optional] 
**CreditBusProjectID** | **string** | Projekt D; ID objektu Projekt [persistentní položka] | [optional] 
**GroupFirmID** | **string** | Firma pro skupinu; ID objektu Firma | [optional] 
**AccGroupDebitID** | **string** | Skupina MD; ID objektu Účetní skupina [persistentní položka] | [optional] 
**AccGroupCreditID** | **string** | Skupina D; ID objektu Účetní skupina [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

