# Veit.Abra.Model.Logstoreaddresscodedef
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Logstoreaddresscodedefrow&gt;**](Logstoreaddresscodedefrow.md) | Řádky; kolekce BO Segment struktury adresy skl. poz. [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Code** | **string** | Kód | [optional] 
**CodeSample** | **string** | Ukázka kódu | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

