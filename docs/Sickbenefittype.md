# Veit.Abra.Model.Sickbenefittype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Sickbenefittyperow&gt;**](Sickbenefittyperow.md) | Sazby; kolekce BO Sazba nemocenské dávky [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**SickBaseType** | **int?** | Typ [persistentní položka] | [optional] 
**OneCase** | **bool?** | Jedn. [persistentní položka] | [optional] 
**IsAbsent** | **bool?** | Nepřít. [persistentní položka] | [optional] 
**MaxDayCount** | **double?** | Max. dní [persistentní položka] | [optional] 
**AverageReduction** | **bool?** | Redukce DVZ [persistentní položka] | [optional] 
**NoExcludedDays** | **bool?** | Není vyl.doba [persistentní položka] | [optional] 
**IsUnpaid** | **bool?** | Neproplácí se [persistentní položka] | [optional] 
**MaxExcludedDays** | **double?** | Max. počet vyl. dob [persistentní položka] | [optional] 
**SBaseTypeSText** | **string** | Typ | [optional] 
**SBaseTypeLText** | **string** | Typ - popis | [optional] 
**PayWorkingDays** | **bool?** | Proplácet pracovní dny [persistentní položka] | [optional] 
**AmountExpression** | **string** | Korekce částky [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Platí do [persistentní položka] | [optional] 
**ContinuingSickBenefit** | **bool?** | Pokračující dávka [persistentní položka] | [optional] 
**SickBenefitRounding** | **int?** | Zaokrouhlení dávky [persistentní položka] | [optional] 
**DayBaseRounding** | **int?** | Zaokrouhlení DVZ [persistentní položka] | [optional] 
**Preferred** | **bool?** | Předvyplňovat [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

