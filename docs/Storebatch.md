# Veit.Abra.Model.Storebatch
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skl. karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ExpirationDateDATE** | **DateTimeOffset?** | Datum expirace [persistentní položka] | [optional] 
**SerialNumber** | **bool?** | Sér. číslo [persistentní položka] | [optional] 
**PrefixCode** | **string** | Prefix kódu | [optional] 
**SuffixCode** | **string** | Sufix kódu | [optional] 
**BodyCode** | **string** | Číslo kódu | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**Specification** | **string** | Specifikace [persistentní položka] | [optional] 
**Comment** | **string** | Poznámka | [optional] 
**SkipNewCode** | **bool?** | Negeneruj nový kód | [optional] 
**ProductionDateDATE** | **DateTimeOffset?** | Datum a čas výroby [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

