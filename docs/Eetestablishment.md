# Veit.Abra.Model.Eetestablishment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **int?** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**SettingID** | **string** | Nastavení; ID objektu Nastavení EET [persistentní položka] | [optional] 
**VATIdentNumber** | **string** | DIČ poplatníka [persistentní položka] | [optional] 
**VATIdentNumberAuthorize** | **string** | DIČ pověřujícího poplatníka [persistentní položka] | [optional] 
**ChoiceContentCashDeviceCode** | **int?** | Identifikovat pokladní zařízení [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

