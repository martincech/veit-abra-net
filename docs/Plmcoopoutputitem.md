# Veit.Abra.Model.Plmcoopoutputitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Kooperace [persistentní položka] | [optional] 
**PLMCoopRoutines** | [**List&lt;Plmcooproutine&gt;**](Plmcooproutine.md) | Operace; kolekce BO Kooperace - technologický postup [nepersistentní položka] | [optional] 
**PLMCoopSN** | [**List&lt;Plmcoopsn&gt;**](Plmcoopsn.md) | SN; kolekce BO Kooperace - sér.č./šarže [nepersistentní položka] | [optional] 
**JOOutputItemID** | **string** | Vyráběná položka; ID objektu VP - vyráběná položka [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství do kooperace v ev.jedn. [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství do kooperace | [optional] 
**ReturnedQuantity** | **double?** | Množství z kooperace v ev.jedn. [persistentní položka] | [optional] 
**ReturnedUnitQuantity** | **double?** | Množství z kooperace | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

