# Veit.Abra.Model.Plmjoroutineconfdiff
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu VP - technologický postup [persistentní položka] | [optional] 
**ConfirmedByID** | **string** | Potvrdil; ID objektu Uživatel [persistentní položka] | [optional] 
**ConfirmedAtDATE** | **DateTimeOffset?** | Potvrzeno [persistentní položka] | [optional] 
**Kind** | **int?** | Druh [persistentní položka] | [optional] 
**Difference** | **double?** | Rozdíl/Hodnota [persistentní položka] | [optional] 
**ProductReceptionRowID** | **string** | Řádek příjmu; ID objektu Příjem hotových výrobků - řádek [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

