# Veit.Abra.Model.Logstorecontent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Skladová pozice [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreBatchID** | **string** | Sériové číslo/šarže; ID objektu Šarže/sériové číslo [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Převodní vztah jedn. [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 
**QuantityReserved** | **double?** | Rezervované množství [persistentní položka] | [optional] 
**QuantityAwaited** | **double?** | Očekávané množství [persistentní položka] | [optional] 
**DateOfStorageDATE** | **DateTimeOffset?** | Datum naskladnění [persistentní položka] | [optional] 
**InvQuantity** | **double?** | Skutečné množství [persistentní položka] | [optional] 
**InvChange** | **bool?** | Změna v průběhu inventury [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství v jednotce | [optional] 
**UnitQuantityReserved** | **double?** | Rezervované množství v jednotce | [optional] 
**UnitQuantityAwaited** | **double?** | Očekávané množství v jednotce | [optional] 
**UnitEditInvQuantity** | **double?** | Skutečné množství (pro editaci) v jednotce | [optional] 
**InvUnitQuantity** | **double?** | Skutečné množství v jednotce | [optional] 
**TotalCapacity** | **double?** | Celkový objem v jednotce [persistentní položka] | [optional] 
**CapacityUnit** | **int?** | Jednotka objemu [persistentní položka] | [optional] 
**TotalWeight** | **double?** | Celková váha [persistentní položka] | [optional] 
**WeightUnit** | **int?** | Jednotka hmotnosti [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

