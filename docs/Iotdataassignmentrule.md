# Veit.Abra.Model.Iotdataassignmentrule
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**IsActive** | **bool?** | Aktivní [persistentní položka] | [optional] 
**SensorID** | **string** | Identifikace senzoru; ID objektu Senzor IoT [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí zpracování [persistentní položka] | [optional] 
**Condition** | **string** | Podmínka použití [persistentní položka] | [optional] 
**StopProcessingNextRules** | **bool?** | Zastavit zpracování dalších pravidel [persistentní položka] | [optional] 
**ObjectClassSelection** | **int?** | Způsob výběru třídy objektu [persistentní položka] | [optional] 
**ObjectClass** | **string** | Třída objektu [persistentní položka] | [optional] 
**ObjectClassExpression** | **string** | Výraz pro určení třídy objektu [persistentní položka] | [optional] 
**ObjectIDSelection** | **int?** | Způsob výběru objektu [persistentní položka] | [optional] 
**ObjectID** | **string** | Objekt [persistentní položka] | [optional] 
**ObjectIDExpression** | **string** | Výraz pro určení objektu [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

