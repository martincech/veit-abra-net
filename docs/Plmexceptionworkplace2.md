# Veit.Abra.Model.Plmexceptionworkplace2
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Výjimka pracoviště [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Datum od [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Datum do [persistentní položka] | [optional] 
**WorkPlaceID** | **string** | Pracoviště; ID objektu Pracoviště a stroj [persistentní položka] | [optional] 
**DeltaPerc** | **double?** | Procentní změna kapacity [persistentní položka] | [optional] 
**DeltaTime** | **double?** | Časová změna kapacity v hodinách [persistentní položka] | [optional] 
**ExceptionType** | **int?** | Typ výjimky [persistentní položka] | [optional] 
**TimeValidFromAsString** | **string** | Čas od | [optional] 
**TimeValidToAsString** | **string** | Čas do | [optional] 
**MachineCount** | **double?** | Počet strojů/pracovníků [persistentní položka] | [optional] 
**ShiftTypeID** | **string** | Druh směny; ID objektu Druh pracovní směny [persistentní položka] | [optional] 
**ExceptionStartDATE** | **DateTimeOffset?** | Datum (čas) od dle směny | [optional] 
**ExceptionFinishDATE** | **DateTimeOffset?** | Datum (čas) do dle směny | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

