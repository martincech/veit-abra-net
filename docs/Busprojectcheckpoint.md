# Veit.Abra.Model.Busprojectcheckpoint
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Projekt [persistentní položka] | [optional] 
**PointDateDATE** | **DateTimeOffset?** | Kontrolní bod [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ResponsibleSupplierRoleID** | **string** | Za dodavatele; ID objektu Role [persistentní položka] | [optional] 
**ResponsibleCustomerPersonID** | **string** | Za zákazníka; ID objektu Osoba [persistentní položka] | [optional] 
**CheckDescription** | **string** | Popis [persistentní položka] | [optional] 
**PlannedCosts** | **double?** | Plánované náklady [persistentní položka] | [optional] 
**PlannedRevenues** | **double?** | Plánované výnosy [persistentní položka] | [optional] 
**PlannedInvoicing** | **double?** | Plánovaná fakturace [persistentní položka] | [optional] 
**CheckDone** | **bool?** | Zkontrolováno [persistentní položka] | [optional] 
**CheckResult** | **string** | Výsledky kontrol [persistentní položka] | [optional] 
**DoCalculateBilance** | **bool?** | Spočítat bilanci | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

