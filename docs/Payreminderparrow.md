# Veit.Abra.Model.Payreminderparrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Parametry automatických upomínek [persistentní položka] | [optional] 
**DocOrder** | **int?** | Pořadí [persistentní položka] | [optional] 
**Name** | **string** | Popis [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch. případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**IncomeTypeID** | **string** | Typ příjmu; ID objektu Typ příjmu [persistentní položka] | [optional] 
**DayCountOfLastReminder** | **int?** | Počet dní [persistentní položka] | [optional] 
**IsAutomaticReminder** | **bool?** | Zpracovat [persistentní položka] | [optional] 
**EmailPrepareOnly** | **bool?** | E-mail neodesílat [persistentní položka] | [optional] 
**Reminders** | [**List&lt;Payreminderparreminder&gt;**](Payreminderparreminder.md) | Kolekce reportů upomínek a e-mailů; kolekce BO Parametry automatických upomínek - reporty upomínek a e-maily [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

