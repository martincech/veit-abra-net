# Veit.Abra.Model.Msgredirection
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**SecurityUserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**EmailAddress** | **string** | Adresát(i) [persistentní položka] | [optional] 
**ValidFromDateDATE** | **DateTimeOffset?** | Platné od data [persistentní položka] | [optional] 
**ValidToDateDATE** | **DateTimeOffset?** | Platné do data [persistentní položka] | [optional] 
**Reason** | **string** | Důvod [persistentní položka] | [optional] 
**KeepMessage** | **bool?** | Ponechat vzkaz [persistentní položka] | [optional] 
**EmailAccountID** | **string** | Vlastní e-mail. účet; ID objektu E-mailový účet [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

