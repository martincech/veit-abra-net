# Veit.Abra.Model.Emailprocessingrule
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**GoOn** | **bool?** | Pokračovat [persistentní položka] | [optional] 
**RuleActions** | [**List&lt;Emailprocessingruleaction&gt;**](Emailprocessingruleaction.md) | Akce; kolekce BO Akce pravidla pro zpracování doručené e-mail. pošty [nepersistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**EmailAccountID** | **string** | E-mailový účet; ID objektu E-mailový účet [persistentní položka] | [optional] 
**FileNameMask** | **string** | Maska pro název přílohy [persistentní položka] | [optional] 
**FileNameMaskIsRegEx** | **bool?** | Maska pro název přílohy je regulární výraz [persistentní položka] | [optional] 
**SenderMask** | **string** | Maska pro odesílatele [persistentní položka] | [optional] 
**SenderMaskIsRegEx** | **bool?** | Maska pro odesílatele je regulární výraz [persistentní položka] | [optional] 
**SendToMask** | **string** | Maska pro příjemce [persistentní položka] | [optional] 
**SendToMaskIsRegEx** | **bool?** | Maska pro příjemce je regulární výraz [persistentní položka] | [optional] 
**SubjectMask** | **string** | Maska pro předmět [persistentní položka] | [optional] 
**SubjectMaskIsRegEx** | **bool?** | Maska pro předmět je regulární výraz [persistentní položka] | [optional] 
**EmailCondition** | **string** | Podmínka [persistentní položka] | [optional] 
**IsForEmail** | **bool?** | Pro e-mail [persistentní položka] | [optional] 
**IsForAttachment** | **bool?** | Pro přílohu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

