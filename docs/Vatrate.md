# Veit.Abra.Model.Vatrate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Vatraterow&gt;**](Vatraterow.md) | Řádky; kolekce BO Platnost DPH sazby [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Tariff** | **double?** | Sazba DPH [persistentní položka] | [optional] 
**VATRateType** | **int?** | Typ sazby [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**AccountID** | **string** | Účet; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**IncomeDomesticDefVATIndexID** | **string** | Tuzemský DPH index na výstupu; ID objektu DPH index [persistentní položka] | [optional] 
**IncomeForeignDefVATIndexID** | **string** | Zahraniční DPH index na výstupu; ID objektu DPH index [persistentní položka] | [optional] 
**OutcomeDomesticDefVATIndexID** | **string** | Tuzemský DPH index na vstupu; ID objektu DPH index [persistentní položka] | [optional] 
**OutcomeForeignDefVATIndexID** | **string** | Zahraniční DPH index na vstupu; ID objektu DPH index [persistentní položka] | [optional] 
**IncomeForeignEUDefVATIndexID** | **string** | Zahraniční DPH index do EU na výstupu; ID objektu DPH index [persistentní položka] | [optional] 
**OutcomeForeignEUDefVATIndexID** | **string** | Zahraniční DPH index do EU na vstupu; ID objektu DPH index [persistentní položka] | [optional] 
**IncomeDomesticRCVATIndexID** | **string** | Tuzemský DPH index pro RC na výstupu; ID objektu DPH index [persistentní položka] | [optional] 
**OutcomeDomesticRCVATIndexID** | **string** | Tuzemský DPH index pro RC na vstupu; ID objektu DPH index [persistentní položka] | [optional] 
**OutcomeDomesticRCXVATIndexID** | **string** | Tuzemský DPH index pro RC na vstupu (mimo evidenci DPH); ID objektu DPH index [persistentní položka] | [optional] 
**OutcomeForeignRCVATIndexID** | **string** | Zahraniční DPH index pro RC mimo EU na vstupu; ID objektu DPH index [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

