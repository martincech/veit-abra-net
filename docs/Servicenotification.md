# Veit.Abra.Model.Servicenotification
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**ServicedObjectID** | **string** | Servisovaný předmět; ID objektu Servisovaný předmět [persistentní položka] | [optional] 
**FromServiceDocStateID** | **string** | Výchozí stav serv.listu; ID objektu Stav servisního listu [persistentní položka] | [optional] 
**ToServiceDocStateID** | **string** | Aktuální stav serv.listu; ID objektu Stav servisního listu [persistentní položka] | [optional] 
**IsActive** | **bool?** | Aktivní [persistentní položka] | [optional] 
**ConfirmationRequired** | **bool?** | Potvrdit [persistentní položka] | [optional] 
**SendImmediately** | **bool?** | Odeslat ihned [persistentní položka] | [optional] 
**EmailAccountID** | **string** | E-mailový účet; ID objektu E-mailový účet [persistentní položka] | [optional] 
**Rows** | [**List&lt;Servicenotificationrow&gt;**](Servicenotificationrow.md) | Řádek notifikace; kolekce BO Řádek notifikace stavu serv.listu [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

