# Veit.Abra.Model.Mlbexpense
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**MLBJourneyID** | **string** | Jízda; ID objektu Jízda [persistentní položka] | [optional] 
**TRMCarID** | **string** | Vozidlo; ID objektu Vozidlo [persistentní položka] | [optional] 
**ExpenseDateDATE** | **DateTimeOffset?** | Datum výdaje [persistentní položka] | [optional] 
**ExpenseType** | **int?** | Typ výdaje [persistentní položka] | [optional] 
**MLBPlaceID** | **string** | Místo; ID objektu Místa [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**FuelQuantity** | **double?** | Množství [persistentní položka] | [optional] 
**TachoFuel** | **double?** | Stav tachometru [persistentní položka] | [optional] 
**PaymentType** | **int?** | Typ platby [persistentní položka] | [optional] 
**BalanceIt** | **bool?** | Vyúčtovat [persistentní položka] | [optional] 
**VATRateID** | **string** | Sazba DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**ImportKey** | **string** | Identifikace importu [persistentní položka] | [optional] 
**UnitPrice** | **double?** | Jednotková cena | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

