# Veit.Abra.Model.Vatregistrationrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu DPH registrace v zemi EU [persistentní položka] | [optional] 
**DateOfChangeDATE** | **DateTimeOffset?** | Datum změny [persistentní položka] | [optional] 
**VATPayor** | **bool?** | Plátce DPH [persistentní položka] | [optional] 
**VATIdentNumber** | **string** | DIČ v dané zemi [persistentní položka] | [optional] 
**CompanyName** | **string** | Název firmy [persistentní položka] | [optional] 
**MOSSStateOfID** | **bool?** | Stát identifikace [persistentní položka] | [optional] 
**MOSSAllowed** | **bool?** | Režim MOSS [persistentní položka] | [optional] 
**VATByPayment** | **bool?** | Uplatnění DPH na základě přijetí platby [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

