# Veit.Abra.Model.Customsdeclarationrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Jednotný správní doklad [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Celkem lokálně [persistentní položka] | [optional] 
**TAmount** | **double?** | Celkem [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | O.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**VATRate** | **double?** | DPH sazba [persistentní položka] | [optional] 
**VATIndexID** | **string** | DPH Index; ID objektu DPH index [persistentní položka] | [optional] 
**CustomsAmount** | **double?** | Clo [persistentní položka] | [optional] 
**SpendingTax** | **double?** | Spot.daň [persistentní položka] | [optional] 
**ImportTax** | **double?** | Dov.přir. [persistentní položka] | [optional] 
**VATAmount** | **double?** | DPH [persistentní položka] | [optional] 
**LocalVATAmount** | **double?** | DPH v lokální měně [persistentní položka] | [optional] 
**VATBaseAmount** | **double?** | Základ cla [persistentní položka] | [optional] 
**LocalVATBaseAmount** | **double?** | Základ cla v lokální měně [persistentní položka] | [optional] 
**VATRateID** | **string** | %DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**VATBaseCorrection** | **double?** | Opr. zákl. DPH [persistentní položka] | [optional] 
**LocalVATBaseCorrection** | **double?** | Opr. zákl. DPH lok. [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**ExpenseTypeID** | **string** | Typ výdaje; ID objektu Typ výdaje [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

