# Veit.Abra.Model.Trmdriver
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**TRMDriverCategoryID** | **string** | Kategorie; ID objektu Kategorie řidiče [persistentní položka] | [optional] 
**TRMDriverClassID** | **string** | Třída; ID objektu Třída řidiče [persistentní položka] | [optional] 
**DriverName** | **string** | Řidič | [optional] 
**DriverNumber** | **string** | Osob.číslo | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

