# Veit.Abra.Model.Pmstatestransition
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**CLSID** | **string** | Identifikace třídy [persistentní položka] | [optional] 
**FromStateID** | **string** | Z uživatelského stavu; ID objektu Procesní stav [persistentní položka] | [optional] 
**ToStateID** | **string** | Na uživatelský stav; ID objektu Procesní stav [persistentní položka] | [optional] 
**ResponsibleRoleID** | **string** | Zodpovědná role po přechodu; ID objektu Role [persistentní položka] | [optional] 
**ResponsibleUserID** | **string** | Zodpovědná osoba v novém stavu; ID objektu Uživatel [persistentní položka] | [optional] 
**ResponsibleUserActual** | **bool?** | Vyplnit zodpovědnou osobu aktuálně přihlášeným uživatelem [persistentní položka] | [optional] 
**CLSIDText** | **string** | Třída objektu | [optional] 
**AllowChangeResponsibleRole** | **bool?** | Povolit změnu zodpovědné role [persistentní položka] | [optional] 
**AllowChangeResponsibleUser** | **bool?** | Povolit změnu zodpovědné osoby [persistentní položka] | [optional] 
**TransferfResponsibleRole** | **bool?** | Přenášet zadanou zodpovědnou roli při změně stavu [persistentní položka] | [optional] 
**TransferfResponsibleUser** | **bool?** | Přenášet zadanou zodpovědnou osobu při změně stavu [persistentní položka] | [optional] 
**MandatoryResponsibleRole** | **bool?** | Povinné zadání zodpovědné role [persistentní položka] | [optional] 
**MandatoryResponsibleUser** | **bool?** | Povinné zadání zodpovědné osoby [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

