# Veit.Abra.Model.Execcalcchart
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Execcalcexpression&gt;**](Execcalcexpression.md) | Položky; kolekce BO Výpočtový vzorec při výpočtu [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**System** | **bool?** | Syst. [persistentní položka] | [optional] 
**CLSID** | **string** | Třída objektu [persistentní položka] | [optional] 
**Specification** | **int?** | Specifikace [persistentní položka] | [optional] 
**CLSIDName** | **string** | Třída | [optional] 
**CalcFieldsSpecialBehaviour** | **bool?** | Způsob součtování [persistentní položka] | [optional] 
**UserCalcFieldsSpecialBehaviour** | **int?** | Způsob součtování - uživ. část | [optional] 
**SourceChartID** | **string** | Zdrojový vzorec; ID objektu Výpočtové schéma [persistentní položka] | [optional] 
**Copied** | **bool?** | Kopie [persistentní položka] | [optional] 
**Scripts** | [**List&lt;Execcalcchartscript&gt;**](Execcalcchartscript.md) | Skripty; kolekce BO Skripty výpočtového vzorce při výpočtu [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

