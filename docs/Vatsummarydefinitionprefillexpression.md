# Veit.Abra.Model.Vatsummarydefinitionprefillexpression
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Definice pro DPH přiznání [persistentní položka] | [optional] 
**FieldCode** | **int?** | FieldCode [persistentní položka] | [optional] 
**UseExpression** | **int?** | UseExpression [persistentní položka] | [optional] 
**SystemExpression** | **string** | SystemExpression [persistentní položka] | [optional] 
**UserExpression** | **string** | UserExpression [persistentní položka] | [optional] 
**FieldName** | **string** | FieldName | [optional] 
**PrefillKind** | **int?** | Předvyplnit [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

