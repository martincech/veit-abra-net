# Veit.Abra.Model.Plmfinishedproduct
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Plmfinishedproductrow&gt;**](Plmfinishedproductrow.md) | Řádky; kolekce BO Dokončený výrobek - řádek [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**AccPresetDefID** | **string** | Předkontace; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**AccDateDATE** | **DateTimeOffset?** | Datum účt. [persistentní položka] | [optional] 
**AccDocQueueID** | **string** | Účetní řada; ID objektu Účetní řada dokladů [persistentní položka] | [optional] 
**AccountingType** | **int?** | Jak účtovat | [optional] 
**IsAccounted** | **bool?** | Účtováno | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**JobOrderID** | **string** | Výrobní příkaz; ID objektu Výrobní příkaz [persistentní položka] | [optional] 
**Consumable** | **double?** | Spotřební mat. | [optional] 
**MaterialExpense** | **double?** | Materiálová režie | [optional] 
**Price** | **double?** | Pevná cena | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

