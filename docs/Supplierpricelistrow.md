# Veit.Abra.Model.Supplierpricelistrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Dodavatelský ceník [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**EAN** | **string** | EAN [persistentní položka] | [optional] 
**PurchasePrice** | **double?** | Cena [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**VATRate** | **double?** | Sazba DPH [persistentní položka] | [optional] 
**DeliveryTime** | **int?** | Dodací lhůta [persistentní položka] | [optional] 
**MinimalQuantity** | **double?** | Min. množství [persistentní položka] | [optional] 
**Note** | **string** | Popis [persistentní položka] | [optional] 
**PictureID** | **string** | Obrázek; ID objektu Data obrázku [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skl. karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreUnitID** | **string** | Skl. jednotka; ID objektu Jednotka skladové karty [persistentní položka] | [optional] 
**SupplierID** | **string** | Dodavatel; ID objektu Dodavatel [persistentní položka] | [optional] 
**DiscountedPrice** | **double?** | Cena po slevě | [optional] 
**UnitRate** | **double?** | Vztah | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

