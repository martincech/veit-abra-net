# Veit.Abra.Model.Plmjooutputitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**PLMJobOrdersRoutines** | [**List&lt;Plmjobordersroutine&gt;**](Plmjobordersroutine.md) | Postupy; kolekce BO VP - technologický postup [nepersistentní položka] | [optional] 
**PLMJobOrdersSN** | [**List&lt;Plmjoborderssn&gt;**](Plmjoborderssn.md) | Sériová čísla; kolekce BO VP - sériové číslo [nepersistentní položka] | [optional] 
**Quantity** | **double?** | Množství v ev. jednotkách [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**RoutineTypeID** | **string** | Typ; ID objektu Typ technologického postupu [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství | [optional] 
**FinishedQuantity** | **double?** | Vyrobené množství v ev.jedn. | [optional] 
**FinishedUnitQuantity** | **double?** | Vyrobené množství | [optional] 
**PLQuantity** | **double?** | Množství [persistentní položka] | [optional] 
**PLQUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**PLUnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**RoutineQuantity** | **double?** | Množství [persistentní položka] | [optional] 
**RoutineQUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**RoutineUnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**PLUnitQuantity** | **double?** | Množství | [optional] 
**RoutineUnitQuantity** | **double?** | Množství | [optional] 
**RoutineStoreCardID** | **string** | Skladová karta TP; ID objektu Skladová karta [persistentní položka] | [optional] 
**RoutineNote** | **string** | Poznámka z TP [persistentní položka] | [optional] 
**PLNote** | **string** | Poznámka z PL [persistentní položka] | [optional] 
**OwnerID** | **string** | Vlastník; ID objektu Uzel stromu výrobního příkazu [persistentní položka] | [optional] 
**RoutineTypeSource** | **int?** | Zdroj technologického postupu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

