# Veit.Abra.Model.Dataprotectionprotectedfield
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Definice ochrany dat [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**CLSID** | **string** | Třída objektu [persistentní položka] | [optional] 
**CLSIDText** | **string** | Třída (textově) | [optional] 
**FieldName** | **string** | Položka [persistentní položka] | [optional] 
**PermitValidityEffect** | **bool?** | Ovlivňuje platnost povolení ke zpracování dat [persistentní položka] | [optional] 
**ReferenceCLSID** | **string** |  [persistentní položka] | [optional] 
**ReferenceFieldName** | **string** |  [persistentní položka] | [optional] 
**UpperFieldName** | **string** |  [persistentní položka] | [optional] 
**FieldNameDisplay** | **string** | Popis položky | [optional] 
**IsReported** | **bool?** | Reportovat [persistentní položka] | [optional] 
**RealBOCLSID** | **string** | Třída BO [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

