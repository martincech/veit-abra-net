# Veit.Abra.Model.Maininvprotocolrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Maininvprotocolbatch&gt;**](Maininvprotocolbatch.md) | Řádky; kolekce BO Hlavní inventární protokol - sér.č./šarže [nepersistentní položka] | [optional] 
**ParentID** | **string** | Hlavní inventární protokol; ID objektu Hlavní inventární protokol [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**DocumentedQuantity** | **double?** | Evidenční množství [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**RealQuantity** | **double?** | Zjištěné množství [persistentní položka] | [optional] 
**Completed** | **bool?** | Kompletní [persistentní položka] | [optional] 
**MIPPositionID** | **string** | Pozice; ID objektu Hlavní inventární protokol - pozice [persistentní položka] | [optional] 
**OrderFlow** | **int?** | OrderFlow [persistentní položka] | [optional] 
**Closed** | **bool?** | Uzavřen [persistentní položka] | [optional] 
**InventoryBeginDATE** | **DateTimeOffset?** | Vložení do inventury [persistentní položka] | [optional] 
**UnitDocumentedQuantity** | **double?** | Evidenční množství | [optional] 
**UnitRealQuantity** | **double?** | Zjištěné množství | [optional] 
**ReadyToClose** | **bool?** | K uzavření | [optional] 
**FoundRealQuantityOnPIP** | **bool?** | Zadáno | [optional] 
**IncorrectBatches** | **bool?** | Nesprávné šarže [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

