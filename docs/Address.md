# Veit.Abra.Model.Address
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Recipient** | **string** | Adresát [persistentní položka] | [optional] 
**City** | **string** | Město [persistentní položka] | [optional] 
**Street** | **string** | Ulice [persistentní položka] | [optional] 
**PostCode** | **string** | PSČ [persistentní položka] | [optional] 
**ZIP** | **string** | ZIP kód [persistentní položka] | [optional] 
**Country** | **string** | Země [persistentní položka] | [optional] 
**PhoneNumber1** | **string** | Telefon 1. [persistentní položka] | [optional] 
**PhoneNumber2** | **string** | Telefon 2. [persistentní položka] | [optional] 
**FaxNumber** | **string** | FAX [persistentní položka] | [optional] 
**EMail** | **string** | E-mail [persistentní položka] | [optional] 
**Location** | **string** | Oddělení [persistentní položka] | [optional] 
**ShortAddress** | **string** | Adresa | [optional] 
**CountryCode** | **string** | Kód země [persistentní položka] | [optional] 
**OfficialStreet** | **string** | Úřední ulice | [optional] 
**OfficialCity** | **string** | Úřední město | [optional] 
**OfficialHouseNumber** | **string** | Úřední domovní číslo | [optional] 
**GPS** | **string** | GPS [persistentní položka] | [optional] 
**Databox** | **string** | Databox [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

