# Veit.Abra.Model.Workingfund
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Duty** | **double?** | Úvazek [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Years** | [**List&lt;Workingfundyear&gt;**](Workingfundyear.md) | Fondy v letech; kolekce BO Fond pracovní doby v daném roce [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

