# Veit.Abra.Model.Evaluationcriteriondatasource
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavičkový objekt [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**DSQLID** | **string** | Identifikace DynSQL příkazu [persistentní položka] | [optional] 
**DSQLName** | **string** | Název DynSQL příkazu [persistentní položka] | [optional] 
**DSQLValues** | **byte[]** | Definice omezení [persistentní položka] | [optional] 
**DSQLAggregateFunction** | **int?** | Agregační funkce aplikováná na položku v DynSQL. [persistentní položka] | [optional] 
**DSQLFieldName** | **string** | Název položky z dat DynSQL příkazu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

