# Veit.Abra.Model.Wsservice
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Wsservicerow&gt;**](Wsservicerow.md) | Řádky; kolekce BO Webová služba - operace [nepersistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**GUID** | **string** | Identifikace [persistentní položka] | [optional] 
**ABRAUserName** | **string** | Uživatelské jméno [persistentní položka] | [optional] 
**ServiceKind** | **int?** | Druh služby [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

