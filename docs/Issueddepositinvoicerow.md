# Veit.Abra.Model.Issueddepositinvoicerow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Zálohový list vydaný [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**TAmount** | **double?** | Celkem [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Celkem lokálně [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**RowType** | **int?** | Typ [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**Quantity** | **double?** | Počet v ev.jedn. [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Počet | [optional] 
**UnitPrice** | **double?** | J. cena [persistentní položka] | [optional] 
**VATRateID** | **string** | %DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**VATIndexID** | **string** | DPHIndex; ID objektu DPH index [persistentní položka] | [optional] 
**VATRate** | **double?** | %DPH [persistentní položka] | [optional] 
**TAmountWithoutVAT** | **double?** | Bez daně | [optional] 
**LocalTAmountWithoutVAT** | **double?** | Bez daně lokálně | [optional] 
**IncomeTypeID** | **string** | Typ příjmu; ID objektu Typ příjmu [persistentní položka] | [optional] 
**XOPCislo** | **string** | OP - číslo [persistentní položka] | [optional] 
**X_OP_ID** | **string** | OP - ID [persistentní položka] | [optional] 
**XOPIDRadku** | **string** | OP - ID řádku [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

