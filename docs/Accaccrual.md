# Veit.Abra.Model.Accaccrual
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Accaccrualrow&gt;**](Accaccrualrow.md) | Řádky; kolekce BO Řádek časového rozlišení [nepersistentní položka] | [optional] 
**SourceDocType** | **string** | Typ zdrojového dokladu [persistentní položka] | [optional] 
**SourceDocID** | **string** | ID zdrojového účtovaného dokladu; ID objektu Dokument [persistentní položka] | [optional] 
**SourceDocRowID** | **string** | ID řádku zdrojového účtovaného dokladu [persistentní položka] | [optional] 
**AccrualsDateFromDATE** | **DateTimeOffset?** | Datum časového rozlišení od [persistentní položka] | [optional] 
**AccrualsDateToDATE** | **DateTimeOffset?** | Datum časového rozlišení do [persistentní položka] | [optional] 
**PeriodCount** | **int?** | Počet období časového rozlišení [persistentní položka] | [optional] 
**PeriodType** | **int?** | Typ periody časového rozlišení [persistentní položka] | [optional] 
**UserChange** | **bool?** | Rozpady čas. rozlišení změněny uživatelem [persistentní položka] | [optional] 
**TotalRowLocalAmount** | **double?** | Celková částka (lok.) [persistentní položka] | [optional] 
**TotalRowAmountInCurrency** | **double?** | Celková částka [persistentní položka] | [optional] 
**SourceDocumentAllRows** | **bool?** | Platí pro celý zdr. doklad [persistentní položka] | [optional] 
**SourceDocAccDateDATE** | **DateTimeOffset?** | Datum zaúčtování [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

