# Veit.Abra.Model.Issuedoffertype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Issuedofferstatetotype&gt;**](Issuedofferstatetotype.md) | Možné stavy; kolekce BO Typ nabídky - možný stav [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**DefaultDocQueueID** | **string** | Výchozí řada dokladů; ID objektu Řada dokladů [persistentní položka] | [optional] 
**DefaultRoleID** | **string** | Výchozí zodpovědná role řešitele; ID objektu Role [persistentní položka] | [optional] 
**DefaultUserID** | **string** | Výchozí zodpovědný řešitel; ID objektu Uživatel [persistentní položka] | [optional] 
**Roles** | [**List&lt;Issuedofferroletotype&gt;**](Issuedofferroletotype.md) | Možné role řešitele nebo řešitelé; kolekce BO Typ nabídky - možná role řešitele nebo řešitel [nepersistentní položka] | [optional] 
**TotalDuration** | **DateTimeOffset?** | Celková doba trvání | [optional] 
**IsShorteningTerms** | **bool?** | Posunout termíny v případě dřívějšího splnění [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

