# Veit.Abra.Model.Crmiostateactivityprocess
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**IOStateToTypeID** | **string** | Stav nabídky; ID objektu Typ nabídky - možný stav [persistentní položka] | [optional] 
**ActivityProcessID** | **string** | Proces; ID objektu Proces aktivit [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

