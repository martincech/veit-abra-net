# Veit.Abra.Model.Vatsummarydefinitionrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Definice pro DPH přiznání [persistentní položka] | [optional] 
**VATIndexID** | **string** | DPHIndex; ID objektu DPH index [persistentní položka] | [optional] 
**BaseLineNumber** | **string** | Iden.ř.základu [persistentní položka] | [optional] 
**TaxLineNumber** | **string** | Iden.ř.daně [persistentní položka] | [optional] 
**VATReportMode** | **string** | Příznak pro výkazy DPH [persistentní položka] | [optional] 
**AllowCorrection** | **bool?** | Povolit opravu [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

