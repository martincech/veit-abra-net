# Veit.Abra.Model.Wlclosingrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Docházka - uzávěrka [persistentní položka] | [optional] 
**WorkerID** | **string** | Pracovník; ID objektu Docházka - pracovníci [persistentní položka] | [optional] 
**TransferType** | **int?** | Způsob přenosu [persistentní položka] | [optional] 
**FieldCode** | **int?** | FieldCode cílové položky [persistentní položka] | [optional] 
**WageOperationID** | **string** | Odkaz na výkon; ID objektu Výkon [persistentní položka] | [optional] 
**AbsenceID** | **string** | Odkaz na Nepřítomnost; ID objektu Nepřítomnost [persistentní položka] | [optional] 
**SickBenefitID** | **string** | Odkaz na Nemoc.dávku; ID objektu Nemocenská dávka [persistentní položka] | [optional] 
**SickBenefitRowID** | **string** | Odkaz na řádek Nemoc.dávky; ID objektu Řádek ND [persistentní položka] | [optional] 
**WageListPartialID** | **string** | Odkaz na dílčí mzdový list; ID objektu Mzdový list dílčí [persistentní položka] | [optional] 
**ClosedValue** | **double?** | Hodnota uzávěrky [persistentní položka] | [optional] 
**TotalValue** | **double?** | Hodnota před korekcí [persistentní položka] | [optional] 
**ScriptParams** | **byte[]** | Parametry pro skript [persistentní položka] | [optional] 
**ScriptIdentifyString** | **string** | Identifikace skriptu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

