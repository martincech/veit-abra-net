# Veit.Abra.Model.Assetparameter
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**TangibleAssetPriceLimit** | **double?** | Limit pořizovací ceny [persistentní položka] | [optional] 
**IntangibleAssetPriceLimit** | **double?** | Limit pořizovací ceny [persistentní položka] | [optional] 
**OverValuationPriceLimit** | **double?** | Cenový limit pro technické zh. [persistentní položka] | [optional] 
**TaxDepreciationRounding** | **int?** | Zaokrouhlování daňových odpisů [persistentní položka] | [optional] 
**AccDepreciationRounding** | **int?** | Zaokrouhlování účetních odpisů [persistentní položka] | [optional] 
**DiscardTaxDepreciationRate** | **double?** | Procento daň.odpisu v r.vyřaz. [persistentní položka] | [optional] 
**AccDepreciationBeginning** | **int?** | Účetní odpisy uplatnit [persistentní položka] | [optional] 
**PercentsInShortPeriod** | **double?** | Procento roč.odpisu ve zkr.ob. [persistentní položka] | [optional] 
**FormulaInventoryNrAssetCard** | **string** | Struktura inventárního čísla karet DM [persistentní položka] | [optional] 
**FormulaInvNrSmallAssetCard** | **string** | Struktura inventárního čísla karet DrM [persistentní položka] | [optional] 
**ResponsibleIsObligatory** | **bool?** | Povinná zodp.osoba [persistentní položka] | [optional] 
**CountShrinkedPeriod** | **int?** | Počítej zkrác.období [persistentní položka] | [optional] 
**AlwaysAccountSummary** | **bool?** | Vždy účtuj o.souhr. [persistentní položka] | [optional] 
**FirstYearDeprecIncreaseDATE** | **DateTimeOffset?** | Datum změny alg. [persistentní položka] | [optional] 
**KeepDegressDeprecLength** | **bool?** | Zachovat délku daň.odpisů po zvýšení ceny [persistentní položka] | [optional] 
**RestAccValueRate** | **double?** | Zbytková úč. hodnota majetku [persistentní položka] | [optional] 
**BookingType** | **int?** | Způsob účtování [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

