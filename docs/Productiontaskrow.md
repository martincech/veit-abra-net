# Veit.Abra.Model.Productiontaskrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Výrobní úloha [persistentní položka] | [optional] 
**MaterialID** | **string** | Materiál; ID objektu Skladová karta [persistentní položka] | [optional] 
**ProductID** | **string** | Výrobek; ID objektu Skladová karta [persistentní položka] | [optional] 
**MaterialQuantity** | **double?** | Množství [persistentní položka] | [optional] 
**ProductRowID** | **string** | Příjem v.; ID objektu Příjem hotových výrobků - řádek [persistentní položka] | [optional] 
**MaterialRowID** | **string** | Výdej m.; ID objektu Výdej materiálu do výroby - řádek [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

