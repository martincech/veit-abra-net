# Veit.Abra.Model.Supplier
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skl. karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**ExternalNumber** | **string** | Ext.kód [persistentní položka] | [optional] 
**Name** | **string** | Ext. název [persistentní položka] | [optional] 
**DeliveryTime** | **int?** | Dod.lhůta [persistentní položka] | [optional] 
**MinimalQuantity** | **double?** | Min.množství [persistentní položka] | [optional] 
**Packing** | **double?** | Balení [persistentní položka] | [optional] 
**DoDemand** | **bool?** | Poptávat dodavatele [persistentní položka] | [optional] 
**PurchasePrice** | **double?** | Nákup.cena [persistentní položka] | [optional] 
**UnitPurchasePrice** | **double?** | Jedn.nákup.cena | [optional] 
**PurchaseCurrencyID** | **string** | Nákup.měna; ID objektu Měna [persistentní položka] | [optional] 
**PurchaseCurrRate** | **double?** | Nákup.kurz [persistentní položka] | [optional] 
**PurchaseRefCurrRate** | **double?** | Nákup.vzt.kurz [persistentní položka] | [optional] 
**PurchaseCoef** | **int?** | Koeficient [persistentní položka] | [optional] 
**LocalPurchaseCoef** | **int?** | Lokální koeficient [persistentní položka] | [optional] 
**PurchaseZoneID** | **string** | Zóna; ID objektu Měna [persistentní položka] | [optional] 
**LocalPurchaseZoneID** | **string** | Lokální zóna; ID objektu Měna [persistentní položka] | [optional] 
**PurchaseDateDATE** | **DateTimeOffset?** | Datum nák. [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**RefCurrencyID** | **string** | Ref.měna; ID objektu Měna | [optional] 
**LocalRefCurrencyID** | **string** | Lok.ref.měna; ID objektu Měna | [optional] 
**IsApproved** | **bool?** | Je dodavatel schválený | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

