# Veit.Abra.Model.Archivceniky
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**CLSID** | **string** | CLSID [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**XUnitPrice** | **double?** | Jednotková cena [persistentní položka] | [optional] 
**XQuantityTo** | **int?** | Počet \&quot;do\&quot; [persistentní položka] | [optional] 
**XQuantityFrom** | **int?** | Počet \&quot;od\&quot; [persistentní položka] | [optional] 
**XStoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**XLongName** | **string** | Dlouhý název [persistentní položka] | [optional] 
**XActivityAreaID** | **string** | Oblast aktivity; ID objektu Oblast aktivity [persistentní položka] | [optional] 
**XActivityTypeID** | **string** | Typ aktivity; ID objektu Typ aktivit [persistentní položka] | [optional] 
**XActQueueID** | **string** | Řada aktivit; ID objektu Řada aktivit [persistentní položka] | [optional] 
**XSolverRoleID** | **string** | Role řešitele; ID objektu Role [persistentní položka] | [optional] 
**XSubject** | **string** | Předmět [persistentní položka] | [optional] 
**XDirDocument** | **string** | Složka pro dokumenty [persistentní položka] | [optional] 
**XNameEN** | **string** | Anglický název [persistentní položka] | [optional] 
**XNote** | **string** | Poznámka (Memo) [persistentní položka] | [optional] 
**XNoteUnicode** | **string** | Poznámka (Memo Unicode) [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

