# Veit.Abra.Model.Unreliablefirmlogfirm
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Unreliablefirmlogfirmbankaccount&gt;**](Unreliablefirmlogfirmbankaccount.md) | Řádky; kolekce BO Kontrola nespolehlivosti plátců DPH - Bankovní účet [nepersistentní položka] | [optional] 
**LogDateDATE** | **DateTimeOffset?** | Datum a čas kontroly [persistentní položka] | [optional] 
**LogHeaderID** | **string** | Vazba na komunikační protokol; ID objektu Kontrola nespolehlivosti plátců DPH - Protokol [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**VATIdentNumber** | **string** | DIČ [persistentní položka] | [optional] 
**StatusCode** | **int?** | Kód výsledku kontroly [persistentní položka] | [optional] 
**StatusFromDateDATE** | **DateTimeOffset?** | Datum, odkdy platí stav [persistentní položka] | [optional] 
**FinancialBureauCode** | **string** | Číslo Finančního úřadu [persistentní položka] | [optional] 
**StatusCodeAsText** | **string** | Kód výsledku kontroly-text | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

