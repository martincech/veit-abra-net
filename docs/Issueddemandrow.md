# Veit.Abra.Model.Issueddemandrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Poptávka vydaná [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**RowType** | **int?** | Typ řádku [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**UnitPrice** | **double?** | Nabídnutá jednotková cena v měně dokladu [persistentní položka] | [optional] 
**TotalPrice** | **double?** | Nabídnutá celková cena v měně dokladu [persistentní položka] | [optional] 
**TAmount** | **double?** | Nabídnutá celková cena v měně dokladu [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Nabídnutá celková cena v lok.měně [persistentní položka] | [optional] 
**LocalUAmount** | **double?** | Nabídnutá jednotková cena v lok.měně | [optional] 
**LocalOtherCostsRatio** | **double?** | Poměrná část ostatních výdajů [persistentní položka] | [optional] 
**RequestedDeliveryDateDATE** | **DateTimeOffset?** | Požadovaný termín dodání [persistentní položka] | [optional] 
**SupplyDateDATE** | **DateTimeOffset?** | Termín možného dodání [persistentní položka] | [optional] 
**SupplyConditions** | **string** | Dodací podmínky [persistentní položka] | [optional] 
**Quantity** | **double?** | Potvrzený počet [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Potvrzený počet | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**DemandSheetID** | **string** | Poptávkový list | [optional] 
**DemandSheetDisplayName** | **string** | Poptávkový list ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

