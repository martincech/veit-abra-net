# Veit.Abra.Model.Installsetsrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Instalační sada [persistentní položka] | [optional] 
**ISClassID** | **string** | Třída instalační položky [persistentní položka] | [optional] 
**Identifier** | **string** | Identifikátor [persistentní položka] | [optional] 
**Category** | **string** | Kategorie položky instalační sady [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**DataInstalled** | **byte[]** | Data položky [persistentní položka] | [optional] 
**DataImported** | **byte[]** | Data importovaná z jiné verze sady [persistentní položka] | [optional] 
**InstalledDataVersion** | **int?** | Číslo instalované verze [persistentní položka] | [optional] 
**InstalationDateDATE** | **DateTimeOffset?** | Datum instalace [persistentní položka] | [optional] 
**RedundantFromVersion** | **int?** | Datum odstranění [persistentní položka] | [optional] 
**Exportable** | **bool?** | Exportovatelná položka [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Typ** | **string** | Typ | [optional] 
**KindName** | **string** | Zařazení [persistentní položka] | [optional] 
**DeleteAndUninstall** | **bool?** | Řádek může být odebrán ze sady | [optional] 
**IsInstalled** | **bool?** | Instalováno | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

