# Veit.Abra.Model.Gdprrequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**FirmID** | **string** | Žadatel - firma; ID objektu Firma [persistentní položka] | [optional] 
**PersonID** | **string** | Žadatel - osoba; ID objektu Osoba [persistentní položka] | [optional] 
**SolverID** | **string** | Řešitel; ID objektu Uživatel [persistentní položka] | [optional] 
**ShortDescription** | **string** | Popis [persistentní položka] | [optional] 
**CategoryID** | **string** | Kategorie požadavku; ID objektu Kategorie požadavku GDPR [persistentní položka] | [optional] 
**Status** | **int?** | Stav [persistentní položka] | [optional] 
**StatusDescription** | **string** | Popis stavu [persistentní položka] | [optional] 
**RequesterType** | **int?** | Typ žadatele | [optional] 
**SolutionDateDATE** | **DateTimeOffset?** | Datum vyřešení [persistentní položka] | [optional] 
**StatusAsText** | **string** | Stav požadavku textově | [optional] 
**SubjectIdentification** | **string** | Identifikace subjektu [persistentní položka] | [optional] 
**AdditionalDescription** | **string** | Doplňující informace [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

