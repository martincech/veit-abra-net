# Veit.Abra.Model.Gdprprocessingpermit
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**SubjectType** | **int?** | Typ subjektu | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**ValiditySuspended** | **bool?** | Platnost pozastavena [persistentní položka] | [optional] 
**ValidFromDateDATE** | **DateTimeOffset?** | Platnost od [persistentní položka] | [optional] 
**ValidToDateDATE** | **DateTimeOffset?** | Platnost do [persistentní položka] | [optional] 
**SourceID** | **string** | Zdroj povolení ke zpracování dat; ID objektu Zdroj povolení ke zpracování dat [persistentní položka] | [optional] 
**SolverID** | **string** | Řešitel; ID objektu Uživatel [persistentní položka] | [optional] 
**LocationID** | **string** | Umístění povolení ke zpracování dat; ID objektu Umístění povolení ke zpracování dat [persistentní položka] | [optional] 
**DefinitionID** | **string** | Definice ochrany dat; ID objektu Definice ochrany dat [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**SourceRecordCLSID** | **string** | Třída zdrojového záznamu automaticky generovaného povolení [persistentní položka] | [optional] 
**SourceRecordID** | **string** | Zdrojový záznam automaticky generovaného povolení [persistentní položka] | [optional] 
**RuleID** | **string** | Pravidlo ochrany dat; ID objektu Pravidlo ochrany dat [persistentní položka] | [optional] 
**OrderFlow** | **int?** | Interní pořadí [persistentní položka] | [optional] 
**ExternalIdentification** | **string** | Externí identifikace [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

