# Veit.Abra.Model.Serviceoperation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ServiceTypeID** | **string** | Servisní typ; ID objektu Typ servisního případu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Serviceoperationrow&gt;**](Serviceoperationrow.md) | Položky servisní operace; kolekce BO Řádek servisní operace [nepersistentní položka] | [optional] 
**TotalAmountWithoutVAT** | **double?** | Celkem bez DPH | [optional] 
**TotalAmountWithVAT** | **double?** | Celkem s DPH | [optional] 
**WorkOperationTime** | **double?** | Celkový prac.čas | [optional] 
**ReturnedAmountsForModel** | **string** | Řízení výběru modelu pro zobrazení varianty | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

