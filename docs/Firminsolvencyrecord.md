# Veit.Abra.Model.Firminsolvencyrecord
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**OrgIdentNumber** | **string** | IČO [persistentní položka] | [optional] 
**CourtFileNr** | **string** | Číslo spisu [persistentní položka] | [optional] 
**ProcessStatus** | **int?** | Stav konkursu [persistentní položka] | [optional] 
**ProcessStatusAsText** | **string** | Stav konkursu - text | [optional] 
**LastCheckDateTimeDATE** | **DateTimeOffset?** | Datum a čas posledního ověření [persistentní položka] | [optional] 
**FirmName** | **string** | Název firmy v IR [persistentní položka] | [optional] 
**FirmAddress** | **string** | Adresa firmy v IR [persistentní položka] | [optional] 
**ProcessDetailURL** | **string** | URL detailu konkursu [persistentní položka] | [optional] 
**CourtNr** | **int?** | Číslo senátu [persistentní položka] | [optional] 
**LastCheckMessage** | **string** | Zpráva z provedeného ověření [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

