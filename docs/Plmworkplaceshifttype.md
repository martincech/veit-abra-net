# Veit.Abra.Model.Plmworkplaceshifttype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Pracoviště a stroj [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**ShiftTypeID** | **string** | Druhy směn; ID objektu Druh pracovní směny [persistentní položka] | [optional] 
**IncCapacityCoef** | **double?** | Koeficient navýšení [persistentní položka] | [optional] 
**IncCapacityIsMulti** | **bool?** | Způsob navýšení [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

