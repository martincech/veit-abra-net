# Veit.Abra.Model.Wikiuserstyle
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**StyleKind** | **int?** | Typ [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**StyleData** | **byte[]** | Parametry [persistentní položka] | [optional] 
**StyleText** | **string** | Styl [persistentní položka] | [optional] 
**CreatedByID** | **string** | Styl vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Styl opravil; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

