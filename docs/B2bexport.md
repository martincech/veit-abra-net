# Veit.Abra.Model.B2bexport
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Title** | **string** | Název [persistentní položka] | [optional] 
**System** | **bool?** | Systémová [persistentní položka] | [optional] 
**DataSource** | **string** | Datový zdroj [persistentní položka] | [optional] 
**ExportID** | **string** | GUID sestavy [persistentní položka] | [optional] 
**Data** | **byte[]** | Data sestavy [persistentní položka] | [optional] 
**IsForm** | **bool?** | Je formulář [persistentní položka] | [optional] 
**OwnerID** | **string** | Vlastník; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**VisibleFromDATE** | **DateTimeOffset?** | Viditelnost od [persistentní položka] | [optional] 
**VisibleToDATE** | **DateTimeOffset?** | Viditelnost do [persistentní položka] | [optional] 
**Hash** | **string** | Hash [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

