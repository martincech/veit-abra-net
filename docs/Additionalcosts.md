# Veit.Abra.Model.Additionalcosts
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**CustomsAmount** | **double?** | Clo [persistentní položka] | [optional] 
**SpendingTaxAmount** | **double?** | Proc.náklady [persistentní položka] | [optional] 
**TransportationAmount** | **double?** | Doprava [persistentní položka] | [optional] 
**OtherCostAmount** | **double?** | Ostatní náklady [persistentní položka] | [optional] 
**CustomsTariff** | **double?** | Clo - sazba [persistentní položka] | [optional] 
**OtherCostTariff** | **double?** | Ost.nákl. - sazba [persistentní položka] | [optional] 
**SpendingTaxTariff** | **double?** | Proc.náklady - sazba [persistentní položka] | [optional] 
**TransportationTariff** | **double?** | Doprava - sazba [persistentní položka] | [optional] 
**TransportationUsed** | **bool?** | Je doprava [persistentní položka] | [optional] 
**OtherCostUsed** | **bool?** | Jsou ostat.nákl. [persistentní položka] | [optional] 
**TransportationIsLocal** | **bool?** | Dopr.lokál. [persistentní položka] | [optional] 
**OtherCostIsLocal** | **bool?** | Ost.nákl.lokál. [persistentní položka] | [optional] 
**CustomsIsLocal** | **bool?** | Clo lokálně [persistentní položka] | [optional] 
**SpendingTaxIsLocal** | **bool?** | Proc.náklady lokál. [persistentní položka] | [optional] 
**IntrastatAffectKind** | **int?** | Započítat do částky pro Intrastat [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

