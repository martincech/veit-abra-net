# Veit.Abra.Model.Assetdepreciationcomponent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavičkový objekt [persistentní položka] | [optional] 
**ComponentID** | **string** | Prvek; ID objektu Prvek karty majetku [persistentní položka] | [optional] 
**TaxDepreciation** | **double?** | Daňový odpis [persistentní položka] | [optional] 
**AccDepreciation** | **double?** | Účetní odpis [persistentní položka] | [optional] 
**TaxBasePrice** | **double?** | Daň.vstup.c. [persistentní položka] | [optional] 
**AccBasePrice** | **double?** | Účet.vstup.c. [persistentní položka] | [optional] 
**TaxRemainderPrice** | **double?** | Daň.zůst.c.poč. [persistentní položka] | [optional] 
**AccRemainderPrice** | **double?** | Účet.zůst.c.poč. [persistentní položka] | [optional] 
**TaxRemainderPriceFinal** | **double?** | Daň.zůst.c.konc. | [optional] 
**AccRemainderPriceFinal** | **double?** | Účet.zůst.c.konc. | [optional] 
**DeprecCorrectionSubsidy** | **double?** | Korekce odpisu prvku [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

