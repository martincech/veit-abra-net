# Veit.Abra.Model.Logstoresettingsrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Nastavení polohovaných skladů [persistentní položka] | [optional] 
**Kind** | **int?** | Druh [persistentní položka] | [optional] 
**StrategyID** | **string** | Strategie [persistentní položka] | [optional] 
**IsAccessibilityFilter** | **bool?** | Je zapnut filtr na dostupnost [persistentní položka] | [optional] 
**AccessibilityLimit** | **int?** | Hranice dostupnosti [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

