# Veit.Abra.Model.Restrictionusage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Notpositionedrowbusinessobject&gt;**](Notpositionedrowbusinessobject.md) | Řádky; kolekce BO Řádek bez pořadí [nepersistentní položka] | [optional] 
**RestrictionDefinitionID** | **string** | Definice omezení; ID objektu Definice omezení [persistentní položka] | [optional] 
**Site** | **string** | Hnízdo [persistentní položka] | [optional] 
**ProgramPoint** | **string** | Místo v programu (program point) [persistentní položka] | [optional] 
**Kind** | **int?** | Druh [persistentní položka] | [optional] 
**UserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**MonthOfUse** | **int?** | Měsíc použití [persistentní položka] | [optional] 
**YearOfUse** | **int?** | Rok použití [persistentní položka] | [optional] 
**NumberOfUse** | **int?** | Počet použití [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

