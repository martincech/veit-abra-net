# Veit.Abra.Model.Globdatavatreg
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Globální data [persistentní položka] | [optional] 
**DateOfChangeDATE** | **DateTimeOffset?** | Datum změny [persistentní položka] | [optional] 
**VATIdentNumber** | **string** | DIČ [persistentní položka] | [optional] 
**VATByPayment** | **bool?** | Uplatnění DPH na základě přijetí platby [persistentní položka] | [optional] 
**VATReturnMethod** | **int?** | Metoda DPH přiznání [persistentní položka] | [optional] 
**SaldoVATRate1** | **double?** | 1. sazba [persistentní položka] | [optional] 
**SaldoVATRate2** | **double?** | 2. sazba [persistentní položka] | [optional] 
**VATReturnMethodAsText** | **string** | Metoda DPH přiznání | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

