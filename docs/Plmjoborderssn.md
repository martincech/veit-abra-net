# Veit.Abra.Model.Plmjoborderssn
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu VP - vyráběná položka [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Quantity** | **double?** | Počet [persistentní položka] | [optional] 
**StoreBatchID** | **string** | Sériové číslo/šarže; ID objektu Šarže/sériové číslo [persistentní položka] | [optional] 
**StoreSubBatchID** | **string** | Dílčí sériové číslo/šarže; ID objektu Dílčí šarže/sériové číslo | [optional] 
**NewBatch** | **bool?** | Nová šarže | [optional] 
**NewBatchName** | **string** | Název | [optional] 
**NewBatchExpirationDateDATE** | **DateTimeOffset?** | Datum expirace | [optional] 
**NewBatchComment** | **string** | Popis | [optional] 
**NewBatchSpecification** | **string** | Specifikace | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**MainUnitRate** | **double?** | Vztah hl. jednotky | [optional] 
**UnitQuantity** | **double?** | Počet | [optional] 
**MainUnitQuantity** | **double?** | Počet v hl. jednotce | [optional] 
**PLMJobOrdersComponents** | [**List&lt;Plmjoborderscomponent&gt;**](Plmjoborderscomponent.md) | Sestavy komponenty; kolekce BO VP - sestavy komponenty [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

