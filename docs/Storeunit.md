# Veit.Abra.Model.Storeunit
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Skladová karta [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Code** | **string** | Zkratka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**IndivisibleQuantity** | **double?** | Neděl. množství [persistentní položka] | [optional] 
**Weight** | **double?** | Hmotnost [persistentní položka] | [optional] 
**WeightUnit** | **int?** | Hm.jednotka [persistentní položka] | [optional] 
**Capacity** | **double?** | Objem [persistentní položka] | [optional] 
**CapacityUnit** | **int?** | Obj.jednotka [persistentní položka] | [optional] 
**EAN** | **string** | EAN [persistentní položka] | [optional] 
**PLU** | **int?** | PLU [persistentní položka] | [optional] 
**HasAnyContainer** | **bool?** | Je kolekce Obaly prázdná [persistentní položka] | [optional] 
**StoreContainers** | [**List&lt;Storecontainer&gt;**](Storecontainer.md) | Obaly; kolekce BO Obal k jednotce skl. karty [nepersistentní položka] | [optional] 
**StoreEANs** | [**List&lt;Storeean&gt;**](Storeean.md) | EAN kódy; kolekce BO EAN kód [nepersistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**Width** | **double?** | Šířka [persistentní položka] | [optional] 
**Height** | **double?** | Výška [persistentní položka] | [optional] 
**Depth** | **double?** | Hloubka [persistentní položka] | [optional] 
**SizeUnit** | **int?** | Jednotka rozměrů [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

