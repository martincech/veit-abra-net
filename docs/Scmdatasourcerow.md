# Veit.Abra.Model.Scmdatasourcerow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Datový zdroj [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Caption** | **string** | Popisek [persistentní položka] | [optional] 
**Kind** | **int?** | Typ sloupce [persistentní položka] | [optional] 
**DataType** | **int?** | Datový typ [persistentní položka] | [optional] 
**RollCLSID** | **string** | Číselník [persistentní položka] | [optional] 
**RollTextField** | **string** | Sloupec číselníku [persistentní položka] | [optional] 
**RollMask** | **int?** | Bezpečnostní maska [persistentní položka] | [optional] 
**RollParams** | **string** | Parametry číselníku [persistentní položka] | [optional] 
**System** | **bool?** | Systémový [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

