# Veit.Abra.Model.Fastsearchdefinitionindex
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Fulltextové hledání - definice [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**IndexExpression** | **string** | Výraz [persistentní položka] | [optional] 
**Ready** | **bool?** | Aktualizováno [persistentní položka] | [optional] 
**WordSplitKind** | **int?** | Dělení slov [persistentní položka] | [optional] 
**WordSplitKindStr** | **string** | Dělení slov | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**MinWordLength** | **int?** | Min. délka slova [persistentní položka] | [optional] 
**MaxWordLength** | **int?** | Max. délka slova [persistentní položka] | [optional] 
**WordSplitDelimiters** | **string** | Oddělovače [persistentní položka] | [optional] 
**GID** | **string** | Identifikátor inst. sady [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

