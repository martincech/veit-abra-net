# Veit.Abra.Model.Assetinvlistcardcomp
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Karta majetku - seznam inventury [persistentní položka] | [optional] 
**ComponentID** | **string** | Odkaz na prvek; ID objektu Prvek karty majetku [persistentní položka] | [optional] 
**InventorySubNr** | **string** | InventorySubNr [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**EAN** | **string** | EAN [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

