# Veit.Abra.Model.Plmworkplace
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**DateOfChange** | **DateTimeOffset?** | Datum změny | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**HourlyRate** | **double?** | Hod. sazba [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**Capacity** | **double?** | Kapacita [persistentní položka] | [optional] 
**BatchBuffer** | **int?** | Zásobník dávek [persistentní položka] | [optional] 
**BatchSize** | **double?** | Velikost dávky [persistentní položka] | [optional] 
**MachineCount** | **double?** | Počet strojů / pracovníků [persistentní položka] | [optional] 
**ShiftCalendarID** | **string** | Kalendář; ID objektu Pracovní kalendář [persistentní položka] | [optional] 
**WorkPlaceShiftTypes** | [**List&lt;Plmworkplaceshifttype&gt;**](Plmworkplaceshifttype.md) | Vazba na druhy směn; kolekce BO Druh směn na pracovišti [nepersistentní položka] | [optional] 
**CRPShow** | **bool?** | Kap. plán [persistentní položka] | [optional] 
**CRPGrain** | **int?** | Zrnitost [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

