# Veit.Abra.Model.Scmsetting
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**System** | **bool?** | Systémové [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skryté [persistentní položka] | [optional] 
**DisplayFormat** | **string** | Zobrazovací maska [persistentní položka] | [optional] 
**CrisisColor** | **int?** | Barva krize [persistentní položka] | [optional] 
**TempCrisisColor** | **int?** | Barva dočasné krize [persistentní položka] | [optional] 
**CrisisFontColor** | **int?** | Barva písma krize [persistentní položka] | [optional] 
**TempCrisisFontColor** | **int?** | Barva písma dočasné krize [persistentní položka] | [optional] 
**StoreCardsSourceID** | **string** | Zdroj skl. karet; ID objektu Datový zdroj [persistentní položka] | [optional] 
**StoresSourceID** | **string** | Zdroj skladů; ID objektu Datový zdroj [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

