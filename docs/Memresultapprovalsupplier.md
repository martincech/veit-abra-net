# Veit.Abra.Model.Memresultapprovalsupplier
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**FirmID** | **string** | Dodavatel; ID objektu Firma [persistentní položka] | [optional] 
**FirmOfficeID** | **string** | Provozovna; ID objektu Provozovna [persistentní položka] | [optional] 
**ApprovingAreaID** | **string** | Oblast hodnocení; ID objektu Oblast hodnocení [persistentní položka] | [optional] 
**IsApproved** | **bool?** | Schváleno [persistentní položka] | [optional] 
**OverallRatingScore** | **int?** | Celkem získano bodů [persistentní položka] | [optional] 
**ValidFromDateDATE** | **DateTimeOffset?** | Výsledek schválení platí od [persistentní položka] | [optional] 
**Scores** | [**List&lt;Approvedsupplierscore&gt;**](Approvedsupplierscore.md) | Získané počty bodů z hodnocení.; kolekce BO Získané počty bodů v hodnocení dodavatelů [nepersistentní položka] | [optional] 
**Approvals** | [**List&lt;Approvedsupplierapproval&gt;**](Approvedsupplierapproval.md) | Výsledek schvalování dodavatele; kolekce BO Výsledky schvalování dodavatelů [nepersistentní položka] | [optional] 
**IsValid** | **bool?** | Platné [persistentní položka] | [optional] 
**HeaderIndex** | **int?** | Unikátní pořadové číslo [persistentní položka] | [optional] 
**ApprovedSupplierID** | **string** | Identifikace hlavičky schválení dodavatele [persistentní položka] | [optional] 
**ApprovedSupplierApprovalsID** | **string** | Identifikace hlavičky schválení dodavatele [persistentní položka] | [optional] 
**ResultText** | **string** | Výsledek schvalování [persistentní položka] | [optional] 
**Note** | **string** | Textová poznámka [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

