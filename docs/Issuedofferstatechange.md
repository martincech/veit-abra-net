# Veit.Abra.Model.Issuedofferstatechange
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**IssuedOfferID** | **string** | Nabídka; ID objektu Nabídka vydaná [persistentní položka] | [optional] 
**OfferStateID** | **string** | Stav nabídky; ID objektu Stav nabídky [persistentní položka] | [optional] 
**PlanDateDATE** | **DateTimeOffset?** | Plánovaný termín [persistentní položka] | [optional] 
**RealDateDATE** | **DateTimeOffset?** | Reálný termín [persistentní položka] | [optional] 
**ActualSolverID** | **string** | Řešitel; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

