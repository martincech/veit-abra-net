# Veit.Abra.Model.Currencyrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Měna [persistentní položka] | [optional] 
**DateOfChangeDATE** | **DateTimeOffset?** | Datum [persistentní položka] | [optional] 
**Denomination** | **bool?** | Denominace [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**Coef** | **int?** | Komprimovaný koeficient [persistentní položka] | [optional] 
**FloatCoef** | **double?** | Koeficient | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

