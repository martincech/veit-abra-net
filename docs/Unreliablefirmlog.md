# Veit.Abra.Model.Unreliablefirmlog
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**LogDateDATE** | **DateTimeOffset?** | Datum kontroly [persistentní položka] | [optional] 
**StatusDateDATE** | **DateTimeOffset?** | Datum odpovědi [persistentní položka] | [optional] 
**StatusCode** | **int?** | Kód výsledku komunikace s DPH registrem [persistentní položka] | [optional] 
**StatusText** | **string** | Popis výsledku komunikace [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**InputXML** | **string** | Vstupní data [persistentní položka] | [optional] 
**OutputXML** | **string** | Výstupní data [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**DownloadKind** | **int?** | Způsob stažení údajů [persistentní položka] | [optional] 
**StatusCodeAsText** | **string** | Výsledek komunikace s DPH registrem-text | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

