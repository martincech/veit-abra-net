# Veit.Abra.Model.Average
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CalcCharts** | [**List&lt;Averagechart&gt;**](Averagechart.md) | Výpočtová schémata; kolekce BO Výp.vzorec průměru [nepersistentní položka] | [optional] 
**WorkingRelationID** | **string** | Pracovní poměr; ID objektu Pracovní poměr [persistentní položka] | [optional] 
**EmployeeID** | **string** | Zaměstnanec; ID objektu Zaměstnanec | [optional] 
**AverageKind** | **int?** | Typ průměru [persistentní položka] | [optional] 
**AverageOrigin** | **int?** | Původ [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**CalcFromDATE** | **DateTimeOffset?** | Spočten od [persistentní položka] | [optional] 
**CalcToDATE** | **DateTimeOffset?** | Spočten do [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platný od [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Platný do [persistentní položka] | [optional] 
**Numerator** | **double?** | Čitatel [persistentní položka] | [optional] 
**Denominator** | **double?** | Jmenovatel [persistentní položka] | [optional] 
**_Average** | **double?** | Průměr | [optional] 
**AverageKindAsText** | **string** | Typ průměru | [optional] 
**AverageOriginAsText** | **string** | Původ průměru | [optional] 
**CalcChartID** | **string** | Výp.schéma; ID objektu Výpočtové schéma | [optional] 
**EmployeeName** | **string** | Příjmení a jméno | [optional] 
**PERSPersonalNumber** | **string** | Os. číslo | [optional] 
**DateFrom** | **DateTimeOffset?** | Data od | [optional] 
**DateTo** | **DateTimeOffset?** | Data do | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

