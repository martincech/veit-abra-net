# Veit.Abra.Model.Wlentrytype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**BaseType** | **int?** | Základní typ [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**UnitCode** | **string** | Jednotka [persistentní položka] | [optional] 
**DisplayButton** | **bool?** | Zobrazovat tlačítko rychlého zadání [persistentní položka] | [optional] 
**BaseTypeAsText** | **string** | Základní typ textově | [optional] 
**Color** | **int?** | Barva [persistentní položka] | [optional] 
**CalculateSurchargeFeastHours** | **bool?** | Předvyplnit odpr. čas ve svátek [persistentní položka] | [optional] 
**CalculateSurchargeNightHours** | **bool?** | Předvyplnit odpr. čas v noci [persistentní položka] | [optional] 
**CalculateSurchargeEnvHours** | **bool?** | Předvyplnit odpr. čas ve šk. prostředí [persistentní položka] | [optional] 
**CalculateSurchargeWEndHours** | **bool?** | Předvyplnit odpr. čas v So/Ne [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

