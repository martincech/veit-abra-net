# Veit.Abra.Model.Wagenotice
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Wagenoticeemployee&gt;**](Wagenoticeemployee.md) | Řádky; kolekce BO Související zaměstnanec [nepersistentní položka] | [optional] 
**Repeated** | **bool?** | Opakovat [persistentní položka] | [optional] 
**NoticeText** | **string** | Popis [persistentní položka] | [optional] 
**PeriodType** | **int?** | Typ opakování [persistentní položka] | [optional] 
**PeriodLength** | **int?** | Délka [persistentní položka] | [optional] 
**DeadLineDATE** | **DateTimeOffset?** | Termín [persistentní položka] | [optional] 
**TaskClosedDateDATE** | **DateTimeOffset?** | Datum vyřešení [persistentní položka] | [optional] 
**TaskClosed** | **bool?** | Vyřešeno [persistentní položka] | [optional] 
**WageNoticeTypeID** | **string** | Druh úkolu; ID objektu Typ úkolu [persistentní položka] | [optional] 
**TaskOwnerID** | **string** | Řešitel; ID objektu Uživatel [persistentní položka] | [optional] 
**TaskOwnerRoleID** | **string** | Role řešitele; ID objektu Role [persistentní položka] | [optional] 
**FixedDate** | **bool?** | Pevné datum [persistentní položka] | [optional] 
**RepetitionDay** | **int?** | Den opakování [persistentní položka] | [optional] 
**RepetitionMonth** | **int?** | Měsíc opakování [persistentní položka] | [optional] 
**ParentID** | **string** | Zdrojový úkol; ID objektu Úkol [persistentní položka] | [optional] 
**ParentText** | **string** | Popis zdrojového úkolu | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

