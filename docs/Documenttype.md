# Veit.Abra.Model.Documenttype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**DocQueueCode** | **string** | Zkratka [persistentní položka] | [optional] 
**ToAccount** | **bool?** | Účtovat [persistentní položka] | [optional] 
**SummaryAccounted** | **bool?** | Způsob účtování [persistentní položka] | [optional] 
**AccPresetDef** | **bool?** | Předkontace [persistentní položka] | [optional] 
**ThroughBank** | **bool?** | Bankou [persistentní položka] | [optional] 
**PaymentKind** | **int?** | Typ platby [persistentní položka] | [optional] 
**ReverseAccounting** | **bool?** | Účtovat obráceně [persistentní položka] | [optional] 
**DefaultSummaryAccounted** | **bool?** | Doporučeno účtovat souhrnně [persistentní položka] | [optional] 
**AccountCode** | **string** | Účet [persistentní položka] | [optional] 
**ForceAccounting** | **bool?** | Přeúčtovávat vždy [persistentní položka] | [optional] 
**ReverseDepositAccounting** | **bool?** | Účtovat kladně zálohy [persistentní položka] | [optional] 
**CurrencyFromFirm** | **bool?** | Předvyplnění měny z firmy [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

