# Veit.Abra.Model.Assetinvlistrestriction
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Inventarizační seznam majetku [persistentní položka] | [optional] 
**IncludedAssetTypes** | **int?** | Zahrnutý majetek [persistentní položka] | [optional] 
**ResponsibleID** | **string** | Odpovědná osoba; ID objektu Odpovědná osoba [persistentní položka] | [optional] 
**AssetLocationID** | **string** | Umístění; ID objektu Umístění majetku [persistentní položka] | [optional] 
**EvidenceDivisionID** | **string** | Evidenční středisko; ID objektu Středisko [persistentní položka] | [optional] 
**ExpensesDivisionID** | **string** | Nákladové středisko; ID objektu Středisko [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

