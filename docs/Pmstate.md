# Veit.Abra.Model.Pmstate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**CLSID** | **string** | Identifikace třídy [persistentní položka] | [optional] 
**SystemState** | **int?** | Systémový stav [persistentní položka] | [optional] 
**Code** | **string** | Kód stavu [persistentní položka] | [optional] 
**CLSIDText** | **string** | Třída objektu | [optional] 
**SystemStateText** | **string** | Systémový stav | [optional] 
**Description** | **string** | Popis stavu [persistentní položka] | [optional] 
**SequenceNumber** | **int?** | Pořadí [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

