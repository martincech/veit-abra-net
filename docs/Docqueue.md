# Veit.Abra.Model.Docqueue
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Zkratka [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**LastNumbers** | [**List&lt;Docqueueperiod&gt;**](Docqueueperiod.md) | Čísla; kolekce BO Řada dokladů - období [nepersistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**AutoFillHole** | **bool?** | Zaplňovat mezery [persistentní položka] | [optional] 
**DocumentType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**ToAccount** | **bool?** | Účtovat [persistentní položka] | [optional] 
**SummaryAccounted** | **bool?** | Souhrnně [persistentní položka] | [optional] 
**ForceAccounting** | **bool?** | Přeúčtovat vždy [persistentní položka] | [optional] 
**SingleAccDocQueueID** | [**Accdocqueue**](Accdocqueue.md) |  | [optional] 
**SummaryAccDocQueueID** | [**Accdocqueue**](Accdocqueue.md) |  | [optional] 
**PrefixVar** | **int?** | Prefix řady [persistentní položka] | [optional] 
**FirstOpenPeriodID** | **string** | První otevřené období; ID objektu Období [persistentní položka] | [optional] 
**LastOpenPeriodID** | **string** | Poslední otevřené období; ID objektu Období [persistentní položka] | [optional] 
**AccountID** | **string** | Účet; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**OutOfUse** | **bool?** | Nepoužívaná řada [persistentní položka] | [optional] 
**ExpenseTypeID** | **string** | Typ výdaje; ID objektu Typ výdaje [persistentní položka] | [optional] 
**IncomeTypeID** | **string** | Typ příjmu; ID objektu Typ příjmu [persistentní položka] | [optional] 
**EditExtNumOnRows** | **bool?** | Zadávat externí číslo na řádcích [persistentní položka] | [optional] 
**CreateReservations** | **bool?** | Vytvořit rezervace [persistentní položka] | [optional] 
**PrefillCurrencyFromFirm** | **bool?** | Předvyplnit měnu dle firmy [persistentní položka] | [optional] 
**EETEstablishmentID** | **string** | Provozovna EET; ID objektu Provozovna EET [persistentní položka] | [optional] 
**OtherDocElectronicPayment** | **bool?** | Pro evidenci elektronické platby [persistentní položka] | [optional] 
**StoreClosingSelectiveValuation** | **int?** | Spouštět automaticky selektivní oceňování [persistentní položka] | [optional] 
**XFirma** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

