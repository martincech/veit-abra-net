# Veit.Abra.Model.Bustransactioninvoicingrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Obchodní případ - vyúčtování fakturace [persistentní položka] | [optional] 
**DocType** | **string** | Typ [persistentní položka] | [optional] 
**DocumentID** | **string** | Doklad; ID objektu Výstupní doklad [persistentní položka] | [optional] 
**RowID** | **string** | Řádek; ID objektu Řádek výstupního dokladu [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum [persistentní položka] | [optional] 
**DocDispName** | **string** | Číslo dokladu [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

