# Veit.Abra.Model.Crmactivityparticipants
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Aktivita [persistentní položka] | [optional] 
**ParticipantUserID** | **string** | Účastník; ID objektu Uživatel [persistentní položka] | [optional] 
**ParticipantRoleID** | **string** | Role účastníka; ID objektu Role [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CRMActivityCompetence** | **int?** | Práva [persistentní položka] | [optional] 
**VisibleInTimeSheet** | **bool?** | Zobrazovat v časovém plánu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

