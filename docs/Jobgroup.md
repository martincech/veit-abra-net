# Veit.Abra.Model.Jobgroup
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**DateOfChange** | **DateTimeOffset?** | Datum změny | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**HourClaim** | **double?** | Nárok pro hodinovou mzdu [persistentní položka] | [optional] 
**MonthClaim** | **double?** | Nárok pro měsíční mzdu [persistentní položka] | [optional] 
**System** | **bool?** | Syst. [persistentní položka] | [optional] 
**HourClaim2** | **double?** | Nárok pro hodinovou mzdu - inv. důchod [persistentní položka] | [optional] 
**MonthClaim2** | **double?** | Nárok pro měsíční mzdu - inv. důchod [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

