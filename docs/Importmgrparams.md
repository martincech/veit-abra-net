# Veit.Abra.Model.Importmgrparams
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OutputDocument** | [**Id**](Id.md) |  | [optional] 
**InputDocument** | [**Id**](Id.md) | ID importovaného dokladu, nebo pole ID importovaných dokladů (v případě importu více dokladů do jednoho) | [optional] 
**Params** | **Object** | Objekt parametrů importního manažera. Název parametru je vlastností objektu, jeho hodnota je hodnotou vlastnosti. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

