# Veit.Abra.Model.Accdepreciationgroup
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Accdepreciationgrouprow&gt;**](Accdepreciationgrouprow.md) | Řádky; kolekce BO Řádky účetní odpisové skupiny [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název/popis skupiny [persistentní položka] | [optional] 
**MonthCount** | **int?** | Poč.měsíců [persistentní položka] | [optional] 
**LinearRate** | **double?** | Sazba lineárního odpisu | [optional] 
**DepreciationType** | **int?** | Typ [persistentní položka] | [optional] 
**DepreciationTypeAsText** | **string** | Typ skupiny | [optional] 
**RateInOtherYears** | **double?** | V dalších letech [persistentní položka] | [optional] 
**ValidFromDateDATE** | **DateTimeOffset?** | Platí od data [persistentní položka] | [optional] 
**ValidToDateDATE** | **DateTimeOffset?** | Platí do data [persistentní položka] | [optional] 
**RatesTotal** | **double?** | Celkem součet sazeb | [optional] 
**RemainsTotal** | **double?** | Celkem zbývá | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

