# Veit.Abra.Model.Approvedsupplierapproval
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hodnocený dodavatel [persistentní položka] | [optional] 
**EvaluationDateDATE** | **DateTimeOffset?** | Datum vyhodnocení schvalování [persistentní položka] | [optional] 
**IsApproved** | **bool?** | Výsledek schvalování [persistentní položka] | [optional] 
**OverallRatingScore** | **int?** | Celkem bodů [persistentní položka] | [optional] 
**ValidFromDateDATE** | **DateTimeOffset?** | Datum platnosti od [persistentní položka] | [optional] 
**Note** | **string** | Textová poznámka [persistentní položka] | [optional] 
**IsValid** | **bool?** | Výsledek platný [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

