# Veit.Abra.Model.Wlwagetransferdef
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**DefType** | **int?** | Typ záznamu [persistentní položka] | [optional] 
**EntryTypeID** | **string** | Základní typ; ID objektu Docházka - Druh činnosti [persistentní položka] | [optional] 
**TransferType** | **int?** | Způsob přenosu [persistentní položka] | [optional] 
**AggregationType** | **int?** | Způsob agregace [persistentní položka] | [optional] 
**FieldCode** | **int?** | FieldCode cílové položky [persistentní položka] | [optional] 
**CorrectionExpression** | **string** | Výraz korekce [persistentní položka] | [optional] 
**WageOperTypeIDExpr** | **string** | Výraz pro zjištění ID z Druhu výkonu [persistentní položka] | [optional] 
**AbsenceTypeIDExpr** | **string** | Výraz pro zjištění ID z Druhu srážek [persistentní položka] | [optional] 
**SickBenefitTypeIDExpr** | **string** | Výraz pro zjištění ID z Druhu nem. dávky [persistentní položka] | [optional] 
**ScriptPackageID** | **string** | Balíček skriptů [persistentní položka] | [optional] 
**ScriptLibrary** | **string** | Knihovna skriptů [persistentní položka] | [optional] 
**ScriptFunction** | **string** | Funkce [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada dokladů; ID objektu Řada dokladů [persistentní položka] | [optional] 
**DisabledForClosing** | **bool?** | Neaktivní [persistentní položka] | [optional] 
**ClosingValueType** | **int?** | Typ počítané položky [persistentní položka] | [optional] 
**FieldCodeLabel** | **string** | FieldCode cílové položky | [optional] 
**FieldCodeName** | **string** | FieldCode cílové položky | [optional] 
**DefTypeAsText** | **string** | Typ záznamu | [optional] 
**AggregationTypeAsText** | **string** | Způsob agregace | [optional] 
**TransferTypeAsText** | **string** | Způsob přenosu | [optional] 
**ScriptPackageName** | **string** | Název balíčku skriptů | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

