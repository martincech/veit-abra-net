# Veit.Abra.Model.Plmtransfer
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Plmtransfersrow&gt;**](Plmtransfersrow.md) | Řádky; kolekce BO Řádek přenosu pracovních lístků do mezd [nepersistentní položka] | [optional] 
**TransferDateDATE** | **DateTimeOffset?** | Datum přenosu [persistentní položka] | [optional] 
**CreatedByID** | **string** | Přenos vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**WagePeriodID** | **string** | Mzdové období; ID objektu Mzdové období [persistentní položka] | [optional] 
**WageOperationImportID** | **string** | Import do výkonů; ID objektu Import do výkonů [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

