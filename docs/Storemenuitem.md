# Veit.Abra.Model.Storemenuitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**ParentID** | **string** | Nadřízený; ID objektu Skladové menu [persistentní položka] | [optional] 
**DisplayParent** | **string** | Nadřízený(zobr.) | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**FULLPATH** | **string** | Cesta | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

