# Veit.Abra.Model.Crmcampaigntypefeedback
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | ID nadřízeného záznamu; ID objektu Typ kampaně [persistentní položka] | [optional] 
**CampaignFeedBackID** | **string** | Zpětná vazba; ID objektu Zpětná vazba kampaně [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

