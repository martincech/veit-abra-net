# Veit.Abra.Model.Datachangessetting
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CLSID** | **string** | Třída [persistentní položka] | [optional] 
**Status** | **int?** | Typ změny [persistentní položka] | [optional] 
**IsActive** | **bool?** | Aktivní [persistentní položka] | [optional] 
**CLSIDText** | **string** | Třída (textově) | [optional] 
**LogInsert** | **bool?** | Zaznamenávat nové | [optional] 
**LogUpdate** | **bool?** | Zaznamenávat opravy | [optional] 
**LogDelete** | **bool?** | Zaznamenávat mazání | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

