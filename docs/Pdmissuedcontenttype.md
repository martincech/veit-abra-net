# Veit.Abra.Model.Pdmissuedcontenttype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ShortName** | **string** | Krátký název [persistentní položka] | [optional] 
**IncludeIntoReports** | **bool?** | Zahrnovat do podacích archů [persistentní položka] | [optional] 
**GenPostNumber** | **bool?** | Generování podacího čísla [persistentní položka] | [optional] 
**ReportNumber** | **string** | Název podacího archu [persistentní položka] | [optional] 
**PostCategory** | **string** | Kategorie pošty [persistentní položka] | [optional] 
**NumberQueue** | **int?** | Číselná řada [persistentní položka] | [optional] 
**NumberPrefix** | **string** | Prefix číselné řady [persistentní položka] | [optional] 
**NumberSuffix** | **string** | Přípona číselné řady [persistentní položka] | [optional] 
**NumberLimit** | **int?** | Limit číselné řady [persistentní položka] | [optional] 
**NumberQueueID** | **string** | Odkaz na číselnou řadu; ID objektu Typy obsahů odeslané pošty [persistentní položka] | [optional] 
**CompleteNumber** | **string** | Sestavená číselná řada | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

