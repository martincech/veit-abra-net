# Veit.Abra.Model.Partialinvprotocolrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Partialinvprotocolbatch&gt;**](Partialinvprotocolbatch.md) | Řádky; kolekce BO Dílčí inventární protokol - sér.č./šarže [nepersistentní položka] | [optional] 
**ParentID** | **string** | Dílčí inventární protokol; ID objektu Dílčí inventární protokol [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**RealQuantity** | **double?** | Zjištěné množství [persistentní položka] | [optional] 
**RealQuantityChanged** | **bool?** | Zadáno [persistentní položka] | [optional] 
**PIPPositionID** | **string** | Pozice; ID objektu Dílčí inventární protokol - pozice [persistentní položka] | [optional] 
**TimeStampDATE** | **DateTimeOffset?** | Čas přidání [persistentní položka] | [optional] 
**MIPRowID** | **string** | Hlavní inventární protokol - řádek; ID objektu Hlavní inventární protokol - řádek [persistentní položka] | [optional] 
**UnitDocumentedQuantity** | **double?** | Evidenční množství | [optional] 
**UnitRealQuantity** | **double?** | Zjištěné množství | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

