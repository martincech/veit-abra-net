# Veit.Abra.Model.Intrastatreport
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Intrastatreportdeclaration&gt;**](Intrastatreportdeclaration.md) | Řádky; kolekce BO Deklarace INTRASTAT [nepersistentní položka] | [optional] 
**IntrastatReportID** | **string** | Opravované podání; ID objektu Podání INTRASTAT [persistentní položka] | [optional] 
**Envelopeid** | **string** | Identifikátor PVS [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum a čas generování [persistentní položka] | [optional] 
**DateFromDATE** | **DateTimeOffset?** | Datum od [persistentní položka] | [optional] 
**DateToDATE** | **DateTimeOffset?** | Datum do [persistentní položka] | [optional] 
**CommunicationDocumentID** | **string** | Dokument; ID objektu Dokument [persistentní položka] | [optional] 
**Conditions** | **byte[]** | Podmínky [persistentní položka] | [optional] 
**Status** | **int?** | Status [persistentní položka] | [optional] 
**IsChanged** | **bool?** | Podklady změněny | [optional] 
**Origin** | **int?** | Původ [persistentní položka] | [optional] 
**DifferentFromPreviousReport** | **bool?** | Různý od předchozího | [optional] 
**IsOnlyDeleting** | **bool?** | Obsahuje pouze mazací deklarace | [optional] 
**ExportVersion** | **int?** | Verze exportu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

