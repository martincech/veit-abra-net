# Veit.Abra.Model.Jobclasificationisco
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ValidDateToDATE** | **DateTimeOffset?** | Datum platnosti do [persistentní položka] | [optional] 
**ValidDateFromDATE** | **DateTimeOffset?** | Datum platnosti od [persistentní položka] | [optional] 
**SuccessorCode** | **string** | Kód nástupce klasifikace zaměstnání [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

