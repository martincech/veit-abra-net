# Veit.Abra.Model.Additionalcharge
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Mzdový list dílčí [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**Percentage** | **double?** | Procento [persistentní položka] | [optional] 
**HourRate** | **double?** | Sazba [persistentní položka] | [optional] 
**HourOperType** | **string** | Hod. výkon [persistentní položka] | [optional] 
**HourCount** | **double?** | Hodin [persistentní položka] | [optional] 
**AmtOperType** | **string** | Část. výkon [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

