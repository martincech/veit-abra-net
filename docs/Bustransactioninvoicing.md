# Veit.Abra.Model.Bustransactioninvoicing
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Bustransactioninvoicingrow&gt;**](Bustransactioninvoicingrow.md) | Řádky; kolekce BO Obchodní případ - řádek vyúčtování fakturace [nepersistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreUnitID** | **string** | Jed.; ID objektu Jednotka skladové karty [persistentní položka] | [optional] 
**QuantityToInvoice** | **double?** | Množství k fak. [persistentní položka] | [optional] 
**AmountToInvoice** | **double?** | Částka k fak. [persistentní položka] | [optional] 
**QuantityCorrection** | **double?** | Množství-korekce [persistentní položka] | [optional] 
**AmountCorrection** | **double?** | Částka-korekce [persistentní položka] | [optional] 
**BusObjectID** | **string** | Obchodní případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**SourceID** | **string** | Zdroj; ID objektu Obchodní případ - zdroj [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

