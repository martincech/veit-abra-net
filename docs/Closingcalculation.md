# Veit.Abra.Model.Closingcalculation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Notpositionedrowbusinessobject&gt;**](Notpositionedrowbusinessobject.md) | Řádky; kolekce BO Řádek bez pořadí [nepersistentní položka] | [optional] 
**CalcCharts** | [**List&lt;Calcclosingchartlist&gt;**](Calcclosingchartlist.md) | Výpočtová schémata; kolekce BO Schéma pro výpočet uzávěrky [nepersistentní položka] | [optional] 
**Amount** | **double?** | Částka | [optional] 
**WagePeriodID** | **string** | Mzdové období; ID objektu Mzdové období | [optional] 
**EmployeeID** | **string** | Zaměstnanec; ID objektu Zaměstnanec | [optional] 
**WorkingRelationID** | **string** | Pracovní poměr; ID objektu Pracovní poměr | [optional] 
**DockID** | **string** | Srážka; ID objektu Srážka [persistentní položka] | [optional] 
**SupressWList** | **int?** | Zpracovávaný objekt | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

