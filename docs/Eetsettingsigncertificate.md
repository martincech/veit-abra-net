# Veit.Abra.Model.Eetsettingsigncertificate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavičkový objekt [persistentní položka] | [optional] 
**Repository** | **int?** | Úložiště certifikátu [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platný od [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Platný do [persistentní položka] | [optional] 
**CertificateHash** | **string** | Hash certifikátu [persistentní položka] | [optional] 
**CertificateData** | **byte[]** | Data certifikátu [persistentní položka] | [optional] 
**CertificatePassword** | **string** | Heslo certifikátu [persistentní položka] | [optional] 
**CertificateActive** | **bool?** | Certifikát aktivní [persistentní položka] | [optional] 
**CertificateSize** | **int?** | Velikost certifikátu [persistentní položka] | [optional] 
**StoreName** | **string** | Název úložiště [persistentní položka] | [optional] 
**SubjectName** | **string** | Vystaveno pro [persistentní položka] | [optional] 
**IssuerName** | **string** | Vystavil [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

