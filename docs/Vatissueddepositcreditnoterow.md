# Veit.Abra.Model.Vatissueddepositcreditnoterow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Dobropis daň.zál.listu vydaného [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**TAmount** | **double?** | Celkem [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Celkem lokálně [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**RowType** | **int?** | Typ [persistentní položka] | [optional] 
**VATRateID** | **string** | %DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**VATIndexID** | **string** | DPHIndex; ID objektu DPH index [persistentní položka] | [optional] 
**VATRate** | **double?** | %DPH [persistentní položka] | [optional] 
**TAmountWithoutVAT** | **double?** | Bez daně [persistentní položka] | [optional] 
**LocalTAmountWithoutVAT** | **double?** | Bez daně lokálně [persistentní položka] | [optional] 
**ToESL** | **bool?** | Do ESL [persistentní položka] | [optional] 
**ESLStatus** | **int?** | Do ESL | [optional] 
**PaymentAmount** | **double?** | Částka platby [persistentní položka] | [optional] 
**RoundingAmount** | **double?** | Zaok.v nulové sazbě [persistentní položka] | [optional] 
**LocalRoundingAmount** | **double?** | Zaok.v nulové sazbě lokálně [persistentní položka] | [optional] 
**ESLIndicatorID** | **string** | Rozlišení typu plnění(ESL); ID objektu Rozlišení typu plnění(ESL) [persistentní položka] | [optional] 
**ESLDateDATE** | **DateTimeOffset?** | Datum pro souhrnné hlášení ESL [persistentní položka] | [optional] 
**DRCArticleID** | **string** | Typ plnění; ID objektu Kód typu plnění [persistentní položka] | [optional] 
**DRCQuantity** | **double?** | Vykazované množství [persistentní položka] | [optional] 
**DRCQUnit** | **string** | Vykazovaná jednotka [persistentní položka] | [optional] 
**VATMode** | **int?** | Režim DPH [persistentní položka] | [optional] 
**MOSSServiceID** | **string** | Druh poskytnuté služby MOSS; ID objektu Druhy poskytovaných služeb MOSS [persistentní položka] | [optional] 
**RSourceID** | **string** | Ř. dobr.dokladu; ID objektu Daňový zálohový list vydaný - řádek [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

