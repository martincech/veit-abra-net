# Veit.Abra.Model.Eetsetting
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Code [persistentní položka] | [optional] 
**Name** | **string** | Name [persistentní položka] | [optional] 
**LimitResponseTime** | **int?** | Mezní doba odezvy [persistentní položka] | [optional] 
**TurnoverModes** | [**List&lt;Eetsettingturnovermode&gt;**](Eetsettingturnovermode.md) | Režimy evidence tržby; kolekce BO Nastavení EET - řádek režimu evidence tržby [nepersistentní položka] | [optional] 
**SendingModes** | [**List&lt;Eetsettingsendingmode&gt;**](Eetsettingsendingmode.md) | Módy odesílaní datových zpráv; kolekce BO Nastavení EET - řádek módu odesílaní datových zpráv [nepersistentní položka] | [optional] 
**SignCertificates** | [**List&lt;Eetsettingsendingmode&gt;**](Eetsettingsendingmode.md) | Podpisové certifikáty; kolekce BO Nastavení EET - řádek módu odesílaní datových zpráv [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

