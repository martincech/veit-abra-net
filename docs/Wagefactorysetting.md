# Veit.Abra.Model.Wagefactorysetting
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Nastavení parametrů mezd [persistentní položka] | [optional] 
**AnnClearingTaxBaseRounding** | **int?** | Zaokr.základu daně [persistentní položka] | [optional] 
**AnnClearingTaxRounding** | **int?** | Zaokr.daně [persistentní položka] | [optional] 
**PercentDocksRounding** | **int?** | Zaokr.srážek [persistentní položka] | [optional] 
**HIYearRounding** | **int?** | Způsob zaokr. vypoč. roč. ZP [persistentní položka] | [optional] 
**HIMinInsBaseRounding** | **int?** | Způsob zaokr. min. zákl. pro roční zúčt. ZP [persistentní položka] | [optional] 
**HIKMinKMaxRounding** | **int?** | Způsob zaokr. koef. RZ ZP [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

