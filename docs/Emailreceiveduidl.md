# Veit.Abra.Model.Emailreceiveduidl
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**UIDL** | **string** | Unikátní POP3 ID [persistentní položka] | [optional] 
**EmailAccountID** | **string** | Vlastní e-mail. účet; ID objektu E-mailový účet [persistentní položka] | [optional] 
**EmailReceivedOID** | **string** | Doručený e-mail [persistentní položka] | [optional] 
**IsDownloaded** | **bool?** | Staženo [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

