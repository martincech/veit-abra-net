# Veit.Abra.Model.Wageimportsabsence
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ExternalCreator** | **string** | Zdroj importu [persistentní položka] | [optional] 
**InformationalText** | **string** | Informace [persistentní položka] | [optional] 
**ImportDateDATE** | **DateTimeOffset?** | Datum importu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

