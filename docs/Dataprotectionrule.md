# Veit.Abra.Model.Dataprotectionrule
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**DefinitionID** | **string** | Definice; ID objektu Definice ochrany dat [persistentní položka] | [optional] 
**Area** | **int?** | Oblast [persistentní položka] | [optional] 
**AreaAsText** | **string** | Oblast | [optional] 
**Params** | **string** | Parametry [persistentní položka] | [optional] 
**ValidMonths** | **int?** | Platnost (měsíců) [persistentní položka] | [optional] 
**RuleType** | **int?** | Typ pravidla [persistentní položka] | [optional] 
**RuleTypeAsText** | **string** | Typ pravidla | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**UniqueID** | **string** | Jedinečný identifikátor [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

