# Veit.Abra.Model.Emailreceivedattachment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Doručený e-mail [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**FileName** | **string** | Název souboru [persistentní položka] | [optional] 
**FileSize** | **int?** | Velikost (B) [persistentní položka] | [optional] 
**DocumentID** | **string** | Zpracovaný dokument; ID objektu Dokument [persistentní položka] | [optional] 
**BlobData** | **byte[]** | Obsah | [optional] 
**MimePos** | **string** | Identifikace přílohy [persistentní položka] | [optional] 
**ContentID** | [**Emailreceivedattachcontent**](Emailreceivedattachcontent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

