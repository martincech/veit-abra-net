# Veit.Abra.Model.Reversechargedeclarationrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Doklad reverse charge [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | O.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**VATRate** | **double?** | DPH sazba [persistentní položka] | [optional] 
**VATIndexID** | **string** | DPH Index; ID objektu DPH index [persistentní položka] | [optional] 
**LocalVATAmount** | **double?** | Odvod DPH v lok.měně [persistentní položka] | [optional] 
**LocalBaseAmount** | **double?** | Základ přepočítaný v lokální měně [persistentní položka] | [optional] 
**VATRateID** | **string** | %DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**VATAmount** | **double?** | Odvod DPH [persistentní položka] | [optional] 
**BaseAmount** | **double?** | Základ přepočítaný [persistentní položka] | [optional] 
**SourceAmount** | **double?** | Základ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**DRCArticleID** | **string** | Typ plnění; ID objektu Kód typu plnění [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

