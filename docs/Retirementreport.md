# Veit.Abra.Model.Retirementreport
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**WorkingRelationID** | **string** | Pracovní poměr; ID objektu Pracovní poměr [persistentní položka] | [optional] 
**StateOfDocument** | **int?** | Stav zadání [persistentní položka] | [optional] 
**StateOfSending** | **int?** | Stav zpracování [persistentní položka] | [optional] 
**DocumentData** | **byte[]** | Data dokladu ELDP [persistentní položka] | [optional] 
**CalendarYear** | **int?** | Rok [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**Corrective** | **bool?** | Opravný [persistentní položka] | [optional] 
**PartOfYear** | **bool?** | Za část roku [persistentní položka] | [optional] 
**PartDateFromDATE** | **DateTimeOffset?** | Datum od [persistentní položka] | [optional] 
**PartDateToDATE** | **DateTimeOffset?** | Datum do [persistentní položka] | [optional] 
**StateOfDocumentText** | **string** | Stav zadání | [optional] 
**StateOfSendingText** | **string** | Stav zpracování | [optional] 
**EmployeeID** | **string** | Zaměstnanec; ID objektu Zaměstnanec | [optional] 
**RetirementReportDefID** | **string** | Definice ELDP; ID objektu Definice ELDP | [optional] 
**CommunicationDocumentID** | **string** | Dokument; ID objektu Dokument [persistentní položka] | [optional] 
**EmployeeName** | **string** | Příjmení a jméno | [optional] 
**PersonalNumber** | **string** | Osobní číslo | [optional] 
**PartMonthFrom** | **int?** | Měsíc od | [optional] 
**PartMonthTo** | **int?** | Měsíc do | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

