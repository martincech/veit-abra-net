# Veit.Abra.Model.Accaccrualrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Časové rozlišení [persistentní položka] | [optional] 
**AccDateDATE** | **DateTimeOffset?** | Datum zaúčtování řádku čas. rozl. [persistentní položka] | [optional] 
**Amount** | **double?** | Částka rozpadu časového rozlišení [persistentní položka] | [optional] 
**AmountInCurrency** | **double?** | Částka rozpadu časového rozlišení v měně dokladu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

