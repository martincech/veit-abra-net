# Veit.Abra.Model.Logstorepreference
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Logstorepreferencerow&gt;**](Logstorepreferencerow.md) | Řádky; kolekce BO Preference skladových pozic - řádek [nepersistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**PosAddressType** | **int?** | Typ adresy [persistentní položka] | [optional] 
**StorePositionID** | **string** | Skladová pozice; ID objektu Skladová pozice [persistentní položka] | [optional] 
**StorePositionAddress** | **string** | Adresa skladové pozice [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**PosAddressTypeDesc** | **string** | Typ adresy - popis | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

