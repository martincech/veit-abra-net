# Veit.Abra.Model.Wikipagestyle
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**StyleText** | **string** | Obsah [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil/a; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil/a; ID objektu Uživatel [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

