# Veit.Abra.Model.Paymentorderrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Platební příkaz [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**DueDateDATE** | **DateTimeOffset?** | Datum spl. [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**ForeignIssue** | **int?** | Způsob úhrady dod.popl. [persistentní položka] | [optional] 
**Urgent** | **int?** | Způsob provedení platby [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**TargetBankAccount** | **string** | Bankovní účet [persistentní položka] | [optional] 
**TargetBankName** | **string** | Název banky [persistentní položka] | [optional] 
**TargetBankStreet** | **string** | Ulice adresy cílové banky [persistentní položka] | [optional] 
**TargetBankCity** | **string** | Město adresy cílové banky [persistentní položka] | [optional] 
**TargetBankPostCode** | **string** | PSČ adresy cílové banky [persistentní položka] | [optional] 
**TargetBankCountry** | **string** | Země adresy cílové banky [persistentní položka] | [optional] 
**TargetBankCountryID** | **string** | Země cílové banky; ID objektu Země [persistentní položka] | [optional] 
**VarSymbol** | **string** | Var. symbol [persistentní položka] | [optional] 
**ConstSymbolID** | **string** | Konst. symbol; ID objektu Konstantní symbol [persistentní položka] | [optional] 
**SpecSymbol** | **string** | Spec.symbol [persistentní položka] | [optional] 
**SwiftCode** | **string** | SWIFT [persistentní položka] | [optional] 
**BankAccountID** | **string** | Převod z účtu; ID objektu Bankovní účet [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**BDocumentID** | **string** | Řádek bank.výpisu; ID objektu Bankovní výpis - řádek [persistentní položka] | [optional] 
**IsNotOk** | **bool?** | Není OK [persistentní položka] | [optional] 
**PaymentOrderDocuments** | [**List&lt;Paymentorderdocument&gt;**](Paymentorderdocument.md) | Dílčí platby; kolekce BO Platební příkaz - dílčí platba [nepersistentní položka] | [optional] 
**PDocumentDisplayName** | **string** | Placený doklad | [optional] 
**FakeAmount** | **double?** | Částka | [optional] 
**PDocumentDescription** | **string** | Placený doklad - popis | [optional] 
**PaymentType** | **int?** | Typ platby [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

