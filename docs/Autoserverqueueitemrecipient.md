# Veit.Abra.Model.Autoserverqueueitemrecipient
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Úloha ve frontě automatizačního serveru [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**RecipientType** | **int?** | Typ příjemce [persistentní položka] | [optional] 
**SecurityUserID** | **string** | Vzkaz - uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**SecurityRoleID** | **string** | Vzkaz - role; ID objektu Role [persistentní položka] | [optional] 
**SecurityGroupID** | **string** | Vzkaz - skupina; ID objektu Skupina rolí [persistentní položka] | [optional] 
**Email** | **string** | E-mail [persistentní položka] | [optional] 
**SentKind** | **int?** | Typ odeslání [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

