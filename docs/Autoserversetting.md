# Veit.Abra.Model.Autoserversetting
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**EmailAccountID** | **string** | E-mailový účet; ID objektu E-mailový účet [persistentní položka] | [optional] 
**DefaultUserID** | **string** | Výchozí uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**RefreshInterval** | **int?** | Interval občerstvení [persistentní položka] | [optional] 
**PoolSize** | **int?** | Počet úloh [persistentní položka] | [optional] 
**IsActive** | **bool?** | Aktivní [persistentní položka] | [optional] 
**Site** | **string** | Replikační hnízdo [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

