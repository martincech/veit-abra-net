# Veit.Abra.Model.Mlbparameter
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**NormWinterFromTo** | **string** | Období pro normu Zimní [persistentní položka] | [optional] 
**NormSpringFromTo** | **string** | Období pro normu Střední (Jaro) [persistentní položka] | [optional] 
**NormSummerFromTo** | **string** | Období pro normu Letní [persistentní položka] | [optional] 
**NormAutumnFromTo** | **string** | Období pro normu Střední (Podzim) [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

