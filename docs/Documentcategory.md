# Veit.Abra.Model.Documentcategory
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusOrderUsage** | **int?** | Způsob použití zakázky [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**BusProjectUsage** | **int?** | Způsob použití projektu [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch. případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusTransactionUsage** | **int?** | Způsob použití obch. případu [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**CompressContent** | **bool?** | Komprimovat [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**DivisionUsage** | **int?** | Způsob použití střediska [persistentní položka] | [optional] 
**DriverID** | **string** | ID ovladače [persistentní položka] | [optional] 
**DriverParams** | **byte[]** | Parametry ovladače [persistentní položka] | [optional] 
**FileNameMask** | **string** | Maska souboru [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**MaxSize** | **int?** | Max. velikost souborů [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ProcessingType** | **int?** | Způsob zpracování [persistentní položka] | [optional] 
**ProcessingTypeName** | **string** | Způsob zpracování (název) | [optional] 
**Purpose** | **int?** | Účel dokladu [persistentní položka] | [optional] 
**PurposeName** | **string** | Účel dokladu | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**MaskIsRegEx** | **bool?** | Regulární výraz [persistentní položka] | [optional] 
**Participants** | [**List&lt;Defaultdocumentparticipant&gt;**](Defaultdocumentparticipant.md) | Účastníci; kolekce BO Výchozí účastník dokumentu [nepersistentní položka] | [optional] 
**DocQueues** | [**List&lt;Documentdocqueue&gt;**](Documentdocqueue.md) | Řady dokladů; kolekce BO Řada dokladů pro dokumenty [nepersistentní položka] | [optional] 
**DefaultCompetence** | **int?** | Výchozí oprávnění [persistentní položka] | [optional] 
**AddAsParticipant** | **bool?** | Automaticky přidat mezi účastníky [persistentní položka] | [optional] 
**LockingMode** | **int?** | Způsob zamykání dokumentů [persistentní položka] | [optional] 
**LockingModeStr** | **string** | Způsob zamykání dokumentů | [optional] 
**ExternalFile** | **bool?** | Externí soubor [persistentní položka] | [optional] 
**ExternalFilePath** | **string** | Cesta k externímu souboru [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

