# Veit.Abra.Model.Plmroutine
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**RevidedID** | **string** | ID revidovaného objektu; ID objektu Technologický postup | [optional] 
**RevisionDescription** | **string** | Popis revize | [optional] 
**RevisionDateDATE** | **DateTimeOffset?** | Datum revize | [optional] 
**RevisionAuthorID** | **string** | Autor revize; ID objektu Uživatel | [optional] 
**Revision** | **int?** | Číslo revize | [optional] 
**Rows** | [**List&lt;Plmroutinesrow&gt;**](Plmroutinesrow.md) | Řádky; kolekce BO Technologický postup - řádek [nepersistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedDATE** | **DateTimeOffset?** | Vytvořen [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedDATE** | **DateTimeOffset?** | Opraven [persistentní položka] | [optional] 
**ReleasedByID** | **string** | Schválil; ID objektu Uživatel [persistentní položka] | [optional] 
**ReleasedDATE** | **DateTimeOffset?** | Schválen [persistentní položka] | [optional] 
**RoutineTypeID** | **string** | Typ postupu; ID objektu Typ technologického postupu [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch. případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

