# Veit.Abra.Model.Actionpricelist
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**Comment** | **string** | Poznámka | [optional] 
**ManagedByID** | **string** | Správce ceníku; ID objektu Uživatel [persistentní položka] | [optional] 
**CreationDateDATE** | **DateTimeOffset?** | Datum vytvoření ceníku [persistentní položka] | [optional] 
**DealerDiscountExcluded** | **bool?** | Neuplatňovat dealerské slevy [persistentní položka] | [optional] 
**IndividualDiscountExcluded** | **bool?** | Neuplatňovat individuální slevy [persistentní položka] | [optional] 
**FinancialDiscountExcluded** | **bool?** | Neuplatňovat finanční slevy [persistentní položka] | [optional] 
**QuantityDiscountExcluded** | **bool?** | Neuplatňovat množstevní slevy [persistentní položka] | [optional] 
**DocumentDiscountExcluded** | **bool?** | Vyloučeno z dodatečné slevy [persistentní položka] | [optional] 
**Priority** | **int?** | Priorita [persistentní položka] | [optional] 
**DateFromDATE** | **DateTimeOffset?** | Datum platnosti od [persistentní položka] | [optional] 
**DateToDATE** | **DateTimeOffset?** | Datum platnosti do [persistentní položka] | [optional] 
**ByTime** | **bool?** | Omezovat časovým omezením [persistentní položka] | [optional] 
**TimeFrom** | **DateTimeOffset?** | čas od [persistentní položka] | [optional] 
**TimeTo** | **DateTimeOffset?** | čas do [persistentní položka] | [optional] 
**ByDateOfWeek** | **bool?** | Omezovat omezením dne v týdnu [persistentní položka] | [optional] 
**Monday** | **bool?** | Pondělí [persistentní položka] | [optional] 
**Tuesday** | **bool?** | Úterý [persistentní položka] | [optional] 
**Wednesday** | **bool?** | Středa [persistentní položka] | [optional] 
**Thursday** | **bool?** | Čtvrtek [persistentní položka] | [optional] 
**Friday** | **bool?** | Pátek [persistentní položka] | [optional] 
**Saturday** | **bool?** | Sobota [persistentní položka] | [optional] 
**Sunday** | **bool?** | Neděle [persistentní položka] | [optional] 
**PriceListRoundings** | [**List&lt;Actionpricelistrounding&gt;**](Actionpricelistrounding.md) | Zaokrouhlování ceníku; kolekce BO Zaokrouhlování akčního ceníku [nepersistentní položka] | [optional] 
**RestrictionUses** | [**List&lt;Actionpricelistrestrictionuse&gt;**](Actionpricelistrestrictionuse.md) | Omezení použití; kolekce BO Omezení použití akčního ceníku [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

