# Veit.Abra.Model.Plmreqinputitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**PLMReqInputItemReservations** | [**List&lt;Plmreqinputitemreservation&gt;**](Plmreqinputitemreservation.md) | Rezervace; kolekce BO Požadavek - rezervace [nepersistentní položka] | [optional] 
**Replaceable** | **bool?** | Aut. náhrada [persistentní položka] | [optional] 
**RealStoreCardID** | **string** | Skutečně použitý materiál; ID objektu Skladová karta [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství v ev.jedn. [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství | [optional] 
**AllowMix** | **bool?** | Mix [persistentní položka] | [optional] 
**PhaseID** | **string** | Etapa; ID objektu Etapa [persistentní položka] | [optional] 
**Description** | **string** | Označení [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**CostingPrice** | **double?** | Cena pro kalkulaci [persistentní položka] | [optional] 
**CostingMethod** | **int?** | Způsob výpočtu ceny pro kalkulaci [persistentní položka] | [optional] 
**RecordsSN** | **bool?** | Evidovat SČ [persistentní položka] | [optional] 
**SupposedStoreID** | **string** | Předpokládaný sklad; ID objektu Sklad [persistentní položka] | [optional] 
**WastePercentage** | **double?** | Ztráty [%] [persistentní položka] | [optional] 
**QuantityRounding** | **int?** | Zaokrouhlení množství [persistentní položka] | [optional] 
**DemandStatus** | **int?** | Stav poptávky [persistentní položka] | [optional] 
**OwnerID** | **string** | Vlastník; ID objektu Uzel stromu požadavku na výrobu [persistentní položka] | [optional] 
**DoNotMultiply** | **bool?** | Konst. mn. [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

