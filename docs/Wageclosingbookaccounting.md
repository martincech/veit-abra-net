# Veit.Abra.Model.Wageclosingbookaccounting
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ClosingDefinitionID** | **string** | Definice zaúčtování; ID objektu Definice výpočtu uzávěrky [persistentní položka] | [optional] 
**AccPresetDefID** | **string** | Předkontace; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**WageListID** | **string** | Mzdový list; ID objektu Mzdový list dílčí [persistentní položka] | [optional] 
**WageOperationID** | **string** | Druh výkonu; ID objektu Výkon [persistentní položka] | [optional] 
**DockID** | **string** | Srážka; ID objektu Srážka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

