# Veit.Abra.Model.Plmworker
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**SecurityUserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**DIVISION_ID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**SalaryClassID** | **string** | Tarifní třída; ID objektu Tarifní třída [persistentní položka] | [optional] 
**WorkerName** | **string** | Jméno | [optional] 
**WorkingRelationID** | **string** | Pracovní poměr [persistentní položka] | [optional] 
**WorkingRelationCode** | **string** | Kód Pracovního poměru | [optional] 
**PersonalNumber** | **string** | Osobní číslo | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

