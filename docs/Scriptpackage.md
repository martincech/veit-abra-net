# Veit.Abra.Model.Scriptpackage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Scriptpackagerow&gt;**](Scriptpackagerow.md) | Řádky; kolekce BO Skript [nepersistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**Compiled** | **bool?** | Zkompilováno [persistentní položka] | [optional] 
**UsageState** | **int?** | Stav [persistentní položka] | [optional] 
**RunOrder** | **int?** | Pořadí spouštění [persistentní položka] | [optional] 
**WithoutSources** | **bool?** | Bez zdr.kódu | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**HashPassword** | **string** | Heslo [persistentní položka] | [optional] 
**CryptForExport** | **bool?** | Zakódovat při exportu [persistentní položka] | [optional] 
**CompileOnSave** | **bool?** | Zkompilovat při uložení | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

