# Veit.Abra.Model.Paymentorderdocument
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Platební příkaz - řádek [persistentní položka] | [optional] 
**PDocumentID** | **string** | Placený doklad; ID objektu Dokument [persistentní položka] | [optional] 
**PDocumentType** | **string** | Typ plac. dokladu [persistentní položka] | [optional] 
**Amount** | **double?** | Placená částka dokl. [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

