# Veit.Abra.Model.Plmbalancedepreciation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Vyrovnání nedokončené výroby [persistentní položka] | [optional] 
**CLSID** | **string** | Třída objektu [persistentní položka] | [optional] 
**ObjID** | **string** | ID objektu [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**CLSIDText** | **string** | Název třídy objektu | [optional] 
**ObjectName** | **string** | Název objektu | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

