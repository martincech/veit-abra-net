# Veit.Abra.Model.Storecardcomponent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Skladová karta [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**RowType** | **int?** | Typ [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**StoreCardID** | **string** | Hlavní karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**UnitPrice** | **double?** | Jedn.cena [persistentní položka] | [optional] 
**TotalPrice** | **double?** | Celk.cena [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**PriceWithVAT** | **bool?** | S daní [persistentní položka] | [optional] 
**VATRate** | **double?** | DPH sazba [persistentní položka] | [optional] 
**VATRateID** | **string** | %DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

