# Veit.Abra.Model.Mlbdistance
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Place1ID** | **string** | Odkud; ID objektu Místa [persistentní položka] | [optional] 
**Place2ID** | **string** | Kam; ID objektu Místa [persistentní položka] | [optional] 
**Distance** | **double?** | Vzdálenost [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

