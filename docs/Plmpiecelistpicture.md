# Veit.Abra.Model.Plmpiecelistpicture
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**RevidedID** | **string** | ID revidovaného objektu; ID objektu Obrázek ke kusovníku | [optional] 
**RevisionDescription** | **string** | Popis revize | [optional] 
**RevisionDateDATE** | **DateTimeOffset?** | Datum revize | [optional] 
**RevisionAuthorID** | **string** | Autor revize; ID objektu Uživatel | [optional] 
**Revision** | **int?** | Číslo revize | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Kusovník [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**PictureID** | [**Picture**](Picture.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

