# Veit.Abra.Model.Scriptpackagerow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Balíček skriptů [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**ScriptKind** | **int?** | Druh skriptu [persistentní položka] | [optional] 
**ScriptID** | **string** | ID skriptu [persistentní položka] | [optional] 
**ScriptData** | **string** | Skript [persistentní položka] | [optional] 
**CompiledScriptData** | **byte[]** | Zkompilovaný skript [persistentní položka] | [optional] 
**Compiled** | **bool?** | Zkompilováno [persistentní položka] | [optional] 
**CompilerVersion** | **int?** | Zkompilováno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

