# Veit.Abra.Model.Refundedcashpaidrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Vrácení pokladního výdeje [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**TAmount** | **double?** | Celkem [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Celkem lokálně [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | O.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**TAmountWithoutVAT** | **double?** | Bez daně [persistentní položka] | [optional] 
**VATRateID** | **string** | %DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**VATIndexID** | **string** | DPHIndex; ID objektu DPH index [persistentní položka] | [optional] 
**VATRate** | **double?** | DPH sazba [persistentní položka] | [optional] 
**LocalTAmountWithoutVAT** | **double?** | Bez daně lokálně [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**VATMode** | **int?** | Režim DPH [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**VATTAmount** | **double?** | DPH | [optional] 
**DRCArticleID** | **string** | Typ plnění; ID objektu Kód typu plnění [persistentní položka] | [optional] 
**DRCQuantity** | **double?** | Vykazované množství [persistentní položka] | [optional] 
**DRCQUnit** | **string** | Vykazovaná jednotka [persistentní položka] | [optional] 
**LocalByHand** | **bool?** | Ručně zadáno [persistentní položka] | [optional] 
**LocalVATTAmount** | **double?** | DPH (lok.) | [optional] 
**UsedRatio** | **bool?** | Použit poměr [persistentní položka] | [optional] 
**RSourceID** | **string** | Ř. vraceného dokladu; ID objektu Pokladní výdej - řádek [persistentní položka] | [optional] 
**MaxTAmount** | **double?** | Max.částka | [optional] 
**MaxTAmountWithoutVAT** | **double?** | Max.částka bez daně | [optional] 
**ExpenseTypeID** | **string** | Typ výdaje; ID objektu Typ výdaje [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

