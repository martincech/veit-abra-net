# Veit.Abra.Model.Logstorepreferencerow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Preference skladových pozic [persistentní položka] | [optional] 
**PreferredObjectType** | **int?** | Typ preference [persistentní položka] | [optional] 
**PreferredObjectTypeDesc** | **string** | Typ preference - popis | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreCardCategoryID** | **string** | Typ skladové karty; ID objektu Typ skladové karty [persistentní položka] | [optional] 
**StoreMenuItemID** | **string** | Skladové menu; ID objektu Skladové menu [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**Strictly** | **int?** | Striktní [persistentní položka] | [optional] 
**StrictlyDesc** | **string** | Striktní - popis | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

