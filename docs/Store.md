# Veit.Abra.Model.Store
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**AddressID** | [**Address**](Address.md) |  | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**AccountID** | **string** | Účet; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**PriceListID** | **string** | Ceník; ID objektu Ceník [persistentní položka] | [optional] 
**InventoryState** | **int?** | Je inventura [persistentní položka] | [optional] 
**MachineName** | **string** | Stanice [persistentní položka] | [optional] 
**InvStartedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**RefundStoreID** | **string** | Vracet do; ID objektu Sklad [persistentní položka] | [optional] 
**FirstOpenPeriodID** | **string** | První období; ID objektu Období [persistentní položka] | [optional] 
**LastOpenPeriodID** | **string** | Akt.období; ID objektu Období [persistentní položka] | [optional] 
**FIFO** | **bool?** | FIFO [persistentní položka] | [optional] 
**ToAccount** | **bool?** | Účtovat [persistentní položka] | [optional] 
**OutOfStockDelivery** | **int?** | Vyskl. [persistentní položka] | [optional] 
**OutOfStockBatchDelivery** | **int?** | Vyskl. šarží/s.čísel [persistentní položka] | [optional] 
**IntrastatInputStatisticID** | **string** | Stat.hodnota na vstupu; ID objektu Definice statistické hodnoty na vstupu [persistentní položka] | [optional] 
**IntrastatOutputStatisticID** | **string** | Stat.hodnota na výstupu; ID objektu Definice statistické hodnoty na výstupu [persistentní položka] | [optional] 
**IntrastatRegionID** | **string** | Kraj původu; ID objektu Kraj [persistentní položka] | [optional] 
**IsLogistic** | **bool?** | Sklad je polohovaný [persistentní položka] | [optional] 
**IsLogisticFromDateDATE** | **DateTimeOffset?** | Dat. zahájení poloh. [persistentní položka] | [optional] 
**RegisterBusOrders** | **bool?** | Sklad je veden zakázkově [persistentní položka] | [optional] 
**IgnoreSCOutOfStockDelivery** | **bool?** | Ignorovat nastavení vyskladnění do minusu na skladové kartě [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

