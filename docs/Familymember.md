# Veit.Abra.Model.Familymember
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Zaměstnanec [persistentní položka] | [optional] 
**LastName** | **string** | Příjmení [persistentní položka] | [optional] 
**FirstName** | **string** | Křestní jméno [persistentní položka] | [optional] 
**BirthNumber** | **string** | Rodné číslo [persistentní položka] | [optional] 
**FamilyRelationType** | **int?** | Vztah [persistentní položka] | [optional] 
**CountOffTypeID** | **string** | Odpočet; ID objektu Typ odpočtu/slevy [persistentní položka] | [optional] 
**ValidFromID** | **string** | Platí od; ID objektu Mzdové období [persistentní položka] | [optional] 
**ValidToID** | **string** | Platí do; ID objektu Mzdové období [persistentní položka] | [optional] 
**DateOfBirthDATE** | **DateTimeOffset?** | Datum narození [persistentní položka] | [optional] 
**Age** | **int?** | Věk | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

