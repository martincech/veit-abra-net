# Veit.Abra.Model.Intrastatcommodity
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**UnitCode** | **string** | Dop.měr.jednotka [persistentní položka] | [optional] 
**WeightIsOptional** | **bool?** | Nepovinná hmotnost [persistentní položka] | [optional] 
**ConstantWeight** | **double?** | Vlastní hmotnost - konstantní hodnota [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

