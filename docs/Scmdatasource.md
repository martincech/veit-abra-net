# Veit.Abra.Model.Scmdatasource
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Scmdatasourcerow&gt;**](Scmdatasourcerow.md) | Řádky; kolekce BO Řádek datového zdroje [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Kind** | **int?** | Druh [persistentní položka] | [optional] 
**DocumentType** | **string** | Typ dokumentu [persistentní položka] | [optional] 
**SQLQuery** | **string** | SQL dotaz [persistentní položka] | [optional] 
**System** | **bool?** | Systémový [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

