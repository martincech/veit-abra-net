# Veit.Abra.Model.Assetleasingcard
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Karta - leasing | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**DateOfChange** | **DateTimeOffset?** | Datum změny | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Code** | **string** | Kód CPA [persistentní položka] | [optional] 
**InventoryNr** | **string** | Inventární číslo [persistentní položka] | [optional] 
**Name** | **string** | Název majetku [persistentní položka] | [optional] 
**AssetLocationID** | **string** | Umístění; ID objektu Umístění majetku [persistentní položka] | [optional] 
**ResponsibleID** | **string** | Odpověd.osoba; ID objektu Odpovědná osoba [persistentní položka] | [optional] 
**ProductNr** | **string** | Výrobní čís. [persistentní položka] | [optional] 
**YearOfProduction** | **int?** | Rok výr. [persistentní položka] | [optional] 
**EvidenceDivisionID** | **string** | Ev.stř.; ID objektu Středisko [persistentní položka] | [optional] 
**ExpensesDivisionID** | **string** | Nákl.stř.; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**PrefixInventoryNr** | **string** | Prefix inventárního čísla | [optional] 
**BodyInventoryNr** | **string** | Tělo inventárního čísla | [optional] 
**SuffixInventoryNr** | **string** | Sufix inventárního čísla | [optional] 
**CardType** | **string** | Typ karty | [optional] 
**EAN** | **string** | EAN [persistentní položka] | [optional] 
**AssetTypeID** | **string** | Druh majetku; ID objektu Druh majetku [persistentní položka] | [optional] 
**MonthOfProduction** | **int?** | Měsíc výr. [persistentní položka] | [optional] 
**ServiceLife** | **int?** | Životnost [persistentní položka] | [optional] 
**IsCollection** | **int?** | Je kolekce [persistentní položka] | [optional] 
**ContractDateDATE** | **DateTimeOffset?** | Datum sml. [persistentní položka] | [optional] 
**LeasingCompanyID** | **string** | Leas.firma; ID objektu Firma [persistentní položka] | [optional] 
**LeasingLength** | **int?** | Délka leas. [persistentní položka] | [optional] 
**AssetTotalPriceBase** | **double?** | Celk.cena [persistentní položka] | [optional] 
**OverTakenDateDATE** | **DateTimeOffset?** | Datum převzetí [persistentní položka] | [optional] 
**AssetTotalPriceVAT** | **double?** | Celk.DPH [persistentní položka] | [optional] 
**AssetVATRate** | **double?** | % DPH [persistentní položka] | [optional] 
**VATSubtractable** | **bool?** | DPH odpočitatelná [persistentní položka] | [optional] 
**ServicePriceBase** | **double?** | Cena pronájmu [persistentní položka] | [optional] 
**ServicePriceVAT** | **double?** | DPH pronájmu [persistentní položka] | [optional] 
**ServiceVATRate** | **double?** | % DPH [persistentní položka] | [optional] 
**InsuaranceBase** | **double?** | Cena pojištění [persistentní položka] | [optional] 
**InsuaranceVAT** | **double?** | DPH pojištění [persistentní položka] | [optional] 
**InsuaranceVATRate** | **double?** | %DPH [persistentní položka] | [optional] 
**RemainderBase** | **double?** | Zůstat.cena [persistentní položka] | [optional] 
**RemainderVAT** | **double?** | DPH zůst.ceny [persistentní položka] | [optional] 
**Deposit** | **double?** | Záloha [persistentní položka] | [optional] 
**DepositType** | **int?** | Typ zálohy [persistentní položka] | [optional] 
**DepositDateDATE** | **DateTimeOffset?** | Splatnost zál. [persistentní položka] | [optional] 
**LeasingFinished** | **double?** | Ukončen [persistentní položka] | [optional] 
**LeasingFinishedDateDATE** | **DateTimeOffset?** | Datum ukonč. [persistentní položka] | [optional] 
**FinishedComment** | **string** | Poznámka [persistentní položka] | [optional] 
**AssetCardID** | **string** | Následná karta; ID objektu Karta majetku [persistentní položka] | [optional] 
**HireDocQueueID** | **string** | Řada dokl.-nájem; ID objektu Řada dokladů [persistentní položka] | [optional] 
**HireAccPresetDefID** | **string** | Předkontace pronájmu; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**InstDocQueueID** | **string** | Řada dokl.-splátky; ID objektu Řada dokladů [persistentní položka] | [optional] 
**InstAccPresetDefID** | **string** | Předkontace splátek; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**FinalPurchaseDocType** | **string** | Typ [persistentní položka] | [optional] 
**FinalPurchaseDocID** | **string** | Odkaz na odkup.doklad; ID objektu Dokument [persistentní položka] | [optional] 
**DayInMonthOfPayment** | **int?** | Den v měsíci [persistentní položka] | [optional] 
**PictureID** | **string** | Obrázek; ID objektu Data obrázku [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**Comment** | **string** | Poznámka | [optional] 
**Payments** | [**List&lt;Assetleasingpayment&gt;**](Assetleasingpayment.md) | Subkolekce plateb; kolekce BO Splátka majetku na leasing [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

