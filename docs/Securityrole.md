# Veit.Abra.Model.Securityrole
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Jméno [persistentní položka] | [optional] 
**ShortName** | **string** | Zkratka [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**Comment** | **string** | Poznámka | [optional] 
**ParentID** | **string** | Nadřízená r.; ID objektu Role [persistentní položka] | [optional] 
**DisplayParent** | **string** | Nadřízená(zobr.) | [optional] 
**IsImpersonal** | **bool?** | Nepersonální [persistentní položka] | [optional] 
**ShiftCalendarID** | **string** | Pracovní kalendář; ID objektu Pracovní kalendář [persistentní položka] | [optional] 
**ExcludedTimes** | [**List&lt;Securityroleexcludedtime&gt;**](Securityroleexcludedtime.md) | Vyloučené časy; kolekce BO Nepracovní časy rolí [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

