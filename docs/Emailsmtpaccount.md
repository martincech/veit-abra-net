# Veit.Abra.Model.Emailsmtpaccount
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**AccountName** | **string** | Název účtu [persistentní položka] | [optional] 
**Server** | **string** | SMTP server [persistentní položka] | [optional] 
**Port** | **int?** | Port [persistentní položka] | [optional] 
**Login** | **string** | Přihlašovací jméno [persistentní položka] | [optional] 
**SecPassword** | **string** | Zakódované heslo [persistentní položka] | [optional] 
**Password** | **string** | Heslo | [optional] 
**ConnectionSecurity** | **int?** | Zabezpečení [persistentní položka] | [optional] 
**CheckUserRcptLimit** | **int?** | Max.počet adresátů v relaci [persistentní položka] | [optional] 
**PauseBetweenSessions** | **int?** | Časová prodleva mezi relacemi [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

