# Veit.Abra.Model.Generalcalendar
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**FixedHoliday** | **bool?** | Pevný [persistentní položka] | [optional] 
**HolidayMonthDay** | **int?** | Měsíc, den [persistentní položka] | [optional] 
**ValidFromYear** | **int?** | Od roku [persistentní položka] | [optional] 
**ValidToYear** | **int?** | Do roku [persistentní položka] | [optional] 
**HolidayDay** | **int?** | Den | [optional] 
**HolidayMonth** | **int?** | Měsíc | [optional] 
**MonthDayText** | **string** | Den, měsíc | [optional] 
**HolidayDate** | **DateTimeOffset?** | Datum | [optional] 
**FromYearText** | **string** | Od roku | [optional] 
**ToYearText** | **string** | Do roku | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

