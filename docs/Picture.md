# Veit.Abra.Model.Picture
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**BlobData** | **byte[]** | Data [persistentní položka] | [optional] 
**PictureTitle** | **string** | Popis [persistentní položka] | [optional] 
**ExternalFile** | **bool?** | Externí [persistentní položka] | [optional] 
**PathAndFileName** | **string** | Cesta a soubor [persistentní položka] | [optional] 
**RefCount** | **int?** | Počet referencí [persistentní položka] | [optional] 
**IsProtected** | **bool?** | Chráněn [persistentní položka] | [optional] 
**ClassID** | **string** | Třída vlastníka - GUID [persistentní položka] | [optional] 
**ClassName** | **string** | Třída vlastníka - název | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

