# Veit.Abra.Model.Maininvprotocolbatch
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavní inventární protokol - řádek [persistentní položka] | [optional] 
**StoreBatchID** | **string** | Šarže/sér.číslo; ID objektu Šarže/sériové číslo [persistentní položka] | [optional] 
**DocumentedQuantity** | **double?** | Evidenční množství [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**RealQuantity** | **double?** | Zjištěné množství [persistentní položka] | [optional] 
**UnitDocumentedQuantity** | **double?** | Evidenční množství | [optional] 
**UnitRealQuantity** | **double?** | Zjištěné množství | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

