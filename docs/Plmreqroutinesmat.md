# Veit.Abra.Model.Plmreqroutinesmat
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Požadavek na výrobu - technologický postup [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**AllQuantity** | **bool?** | AllQuantity [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství pro zajištění [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství pro zajištění | [optional] 
**UnusedMat** | **bool?** | Nepoužité [persistentní položka] | [optional] 
**Generated** | **bool?** | Generováno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

