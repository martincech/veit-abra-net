# Veit.Abra.Model.Device
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Node** | **string** | ID síť. karty [persistentní položka] | [optional] 
**Name** | **string** | Název počítače [persistentní položka] | [optional] 
**DeviceName** | **string** | Název zařízení [persistentní položka] | [optional] 
**DeviceType** | **string** | Zařízení [persistentní položka] | [optional] 
**DeviceDriver** | **string** | Ovladač [persistentní položka] | [optional] 
**Configuration** | **byte[]** | Konfigurace [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

