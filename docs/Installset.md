# Veit.Abra.Model.Installset
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Installsetsrow&gt;**](Installsetsrow.md) | Řádky; kolekce BO Instalační sada - řádek [nepersistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ImportNoteStart** | **string** | Poznámka k zahájení importu [persistentní položka] | [optional] 
**ImportNoteFinish** | **string** | Poznámka k ukončení importu [persistentní položka] | [optional] 
**SystemVersion** | **string** | Verze ABRY [persistentní položka] | [optional] 
**VersionSupport** | **bool?** | Podpora verzí [persistentní položka] | [optional] 
**Versions** | [**List&lt;Installsetversion&gt;**](Installsetversion.md) | Verze; kolekce BO Verze instalační sady [nepersistentní položka] | [optional] 
**AdvancedOption** | **bool?** | Rozšířené zobrazení [persistentní položka] | [optional] 
**CurrentVersion** | **int?** | Aktuální verze | [optional] 
**NeedSynchronize** | **bool?** | Vyžaduje synchronizaci | [optional] 
**NeedNewVersion** | **bool?** | Vyžaduje založení nové verze | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

