# Veit.Abra.Model.Assetinvlistresultcomp
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Karta majetku - výsledek inventury [persistentní položka] | [optional] 
**ComponentID** | **string** | Odkaz na prvek; ID objektu Prvek karty majetku [persistentní položka] | [optional] 
**Name** | **string** | Name [persistentní položka] | [optional] 
**ComponentStatus** | **int?** | Stav prvku majetku [persistentní položka] | [optional] 
**IsImported** | **bool?** | Importován [persistentní položka] | [optional] 
**ImportDescription** | **string** | Poznámka k importu [persistentní položka] | [optional] 
**ApprovalStatus** | **int?** | Stav schválení [persistentní položka] | [optional] 
**AppliedInCard** | **bool?** | Změna přenesena [persistentní položka] | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

