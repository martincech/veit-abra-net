# Veit.Abra.Model.Bankstatement
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Bankstatementrow&gt;**](Bankstatementrow.md) | Řádky; kolekce BO Bankovní výpis - řádek [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**CreditAmount** | **double?** | Celkem kredit [persistentní položka] | [optional] 
**DebitAmount** | **double?** | Celkem debet [persistentní položka] | [optional] 
**LocalCreditAmount** | **double?** | Celkem lok. kredit [persistentní položka] | [optional] 
**LocalDebitAmount** | **double?** | Celkem lok.debet [persistentní položka] | [optional] 
**AccDocQueueID** | **string** | Účetní řada; ID objektu Účetní řada dokladů [persistentní položka] | [optional] 
**ExternalNumber** | **string** | Externí číslo [persistentní položka] | [optional] 
**Dirty** | **bool?** | Zakázané přepočítání [persistentní položka] | [optional] 
**BankAccountID** | **string** | Bankovní účet; ID objektu Bankovní účet [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

