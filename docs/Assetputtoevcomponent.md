# Veit.Abra.Model.Assetputtoevcomponent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Zařazení majetku do evidence [persistentní položka] | [optional] 
**ComponentID** | **string** | Odkaz do seznamu prvků; ID objektu Prvek karty majetku [persistentní položka] | [optional] 
**TaxBasePrice** | **double?** | Daň.vstup.cena [persistentní položka] | [optional] 
**AccBasePrice** | **double?** | Účet.vstup.cena [persistentní položka] | [optional] 
**TaxRemainderPrice** | **double?** | Daň.zůst.cena [persistentní položka] | [optional] 
**AccRemainderPrice** | **double?** | Účet.zůst.cena [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

