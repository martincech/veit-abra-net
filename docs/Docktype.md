# Veit.Abra.Model.Docktype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**BaseType** | **int?** | Typ [persistentní položka] | [optional] 
**Periodicity** | **int?** | Perioda [persistentní položka] | [optional] 
**InternalDock** | **bool?** | Interní [persistentní položka] | [optional] 
**DebtCorrect** | **bool?** | Dluh [persistentní položka] | [optional] 
**CompToMinimum** | **bool?** | Minimum [persistentní položka] | [optional] 
**PartialDock** | **bool?** | Část [persistentní položka] | [optional] 
**PrintGroup** | **string** | Tisk [persistentní položka] | [optional] 
**BaseTypeText** | **string** | Typ | [optional] 
**AgreementDock** | **bool?** | Srážka dohodou [persistentní položka] | [optional] 
**DockFromAgreements** | **bool?** | Srážet i z dohod [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

