# Veit.Abra.Model.Paymenttype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**PaymentKind** | **int?** | Typ [persistentní položka] | [optional] 
**CommentTitle** | **string** | Nadpis komentáře [persistentní položka] | [optional] 
**CommentRequired** | **bool?** | Vyžadovat komentář [persistentní položka] | [optional] 
**AuthCodeRequired** | **bool?** | Vyžadovat autorizační kód [persistentní položka] | [optional] 
**SummarizeDisabled** | **bool?** | Nesčítat [persistentní položka] | [optional] 
**PrintComment** | **bool?** | Tisknout popis [persistentní položka] | [optional] 
**MaximumGiveBackAmount** | **double?** | Max.vracená částka [persistentní položka] | [optional] 
**AnalyticalAccount** | **string** | Analytika [persistentní položka] | [optional] 
**MaximumPaymentAmount** | **double?** | Maximální částka platby [persistentní položka] | [optional] 
**EET** | **bool?** | EET [persistentní položka] | [optional] 
**XNameEN** | **string** | Anglický název [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

