# Veit.Abra.Model.Logstoretransferrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Polohovací doklad [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreBatchID** | **string** | Sériové číslo/šarže; ID objektu Šarže/sériové číslo [persistentní položka] | [optional] 
**StoreDocRowID** | **string** | Odkaz na skladový řádek [persistentní položka] | [optional] 
**Quantity** | **double?** | Počet v ev.jedn. [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**StorePositionID** | **string** | Skladová pozice; ID objektu Skladová pozice [persistentní položka] | [optional] 
**RestQuantity** | **double?** | Zbývá | [optional] 
**UnitRestQuantity** | **double?** | Zbývá v ev.jedn. | [optional] 
**InPositionQuantity** | **double?** | V pozici | [optional] 
**MasterRowID** | **string** | Řádek se zdroj. pozicí [persistentní položka] | [optional] 
**SortKey** | **string** | Klíč | [optional] 
**ContentUnit** | **string** | Jednotka obsahu pozice [persistentní položka] | [optional] 
**ContentUnitRate** | **double?** | Vztah jednotky obsahu pozice [persistentní položka] | [optional] 
**OrderFlow** | **int?** | Pořadové číslo pohybu [persistentní položka] | [optional] 
**IncomingStorePositionID** | **string** | Do pozice; ID objektu Skladová pozice | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

