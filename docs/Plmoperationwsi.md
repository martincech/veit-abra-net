# Veit.Abra.Model.Plmoperationwsi
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Pracovní lístek [persistentní položka] | [optional] 
**WorkScheduleItemID** | **string** | Dílenské úkony; ID objektu Dílenský úkon [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

