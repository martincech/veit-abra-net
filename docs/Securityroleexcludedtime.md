# Veit.Abra.Model.Securityroleexcludedtime
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Role [persistentní položka] | [optional] 
**DateFromDATE** | **DateTimeOffset?** | Začátek [persistentní položka] | [optional] 
**DateToDATE** | **DateTimeOffset?** | Konec [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**TimeFromAsString** | **string** | Hodina od | [optional] 
**TimeToAsString** | **string** | Hodina do | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

