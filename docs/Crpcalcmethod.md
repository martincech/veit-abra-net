# Veit.Abra.Model.Crpcalcmethod
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**IsSystem** | **bool?** | Systémová [persistentní položka] | [optional] 
**IsPublic** | **bool?** | Veřejná [persistentní položka] | [optional] 
**IsBackward** | **bool?** | Dozadná [persistentní položka] | [optional] 
**IsWithPrevious** | **bool?** | Včetně závislých úkonů | [optional] 
**IsTogetherOnExtrem** | **bool?** | Přetížit datum [persistentní položka] | [optional] 
**ForwardMethodID** | **string** | Metoda pro přeplánování; ID objektu Metoda výpočtu kapacitního plánu [persistentní položka] | [optional] 
**CapacityKind** | **int?** | Způsob - kapacita [persistentní položka] | [optional] 
**SortDocumentKind** | **int?** | Řazení dokladů [persistentní položka] | [optional] 
**SortWSItemKind** | **int?** | Řazení úkonů [persistentní položka] | [optional] 
**SupposedDateKind** | **int?** | Způsob - datum [persistentní položka] | [optional] 
**IsIgnoredStartDate** | **bool?** | Ignorovat datum spuštění [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

