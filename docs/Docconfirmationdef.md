# Veit.Abra.Model.Docconfirmationdef
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada dokladů; ID objektu Řada dokladů [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatorRoleID** | **string** | Tvůrce dokladu; ID objektu Role [persistentní položka] | [optional] 
**Condition** | **string** | Podmínka [persistentní položka] | [optional] 
**ConfirmatorRoleID** | **string** | Role schvalovatele; ID objektu Role [persistentní položka] | [optional] 
**ConfirmatorUserID** | **string** | Schvalovatel; ID objektu Uživatel [persistentní položka] | [optional] 
**ConfirmatorOrder** | **int?** | Pořadí schvalovatele [persistentní položka] | [optional] 
**EvaluateConfirmator** | **bool?** | Schvalovatel výrazem [persistentní položka] | [optional] 
**ExpressionForEvalConfirmator** | **string** | Výraz pro vyhodnocení role resp. uživatele, který provádí schvalování [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

