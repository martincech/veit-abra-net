# Veit.Abra.Model.Docconfirmationbreak
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocType** | **string** | Typ dokladu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada dokladů; ID objektu Řada dokladů [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CanEditConfirmedDoc** | **bool?** | Povoleno pro schválený [persistentní položka] | [optional] 
**CanEditRefusedDoc** | **bool?** | Povoleno pro zamítnutý [persistentní položka] | [optional] 
**UseDecisiveFields** | **bool?** | Používat rozhodné položky [persistentní položka] | [optional] 
**DecisiveFields** | [**List&lt;Docconfbreakdecisivefield&gt;**](Docconfbreakdecisivefield.md) | Rozhodné položky; kolekce BO Povolení oprav - rozhodné položky pro schválení při opravě [nepersistentní položka] | [optional] 
**KindDecisiveFields** | **int?** | Druh rozhodných položek [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

