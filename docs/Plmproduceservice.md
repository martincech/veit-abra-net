# Veit.Abra.Model.Plmproduceservice
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**SN_ID** | **string** | Sériové číslo; ID objektu VP - sériové číslo [persistentní položka] | [optional] 
**FaultTypeID** | **string** | Neshoda; ID objektu Neshoda [persistentní položka] | [optional] 
**JOInputItemID** | **string** | Komponenta; ID objektu VP - kusovník [persistentní položka] | [optional] 
**RoutineID** | **string** | Operace; ID objektu VP - technologický postup [persistentní položka] | [optional] 
**ServicedByID** | **string** | Nápravu provedl; ID objektu Pracovník [persistentní položka] | [optional] 
**ServicedAtDATE** | **DateTimeOffset?** | Náprava provedena [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch. případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**OperationID** | **string** | Pracovní lístek; ID objektu Pracovní lístek [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**SourceRoutineID** | **string** | Zdrojová operace; ID objektu VP - technologický postup [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**ServiceState** | **int?** | Stav [persistentní položka] | [optional] 
**ServiceStateText** | **string** | Stav - textově | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

