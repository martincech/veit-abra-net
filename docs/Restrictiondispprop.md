# Veit.Abra.Model.Restrictiondispprop
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Restrictiondispproprow&gt;**](Restrictiondispproprow.md) | Řádky; kolekce BO Řádek vlastností zobrazení definic omezení [nepersistentní položka] | [optional] 
**Site** | **string** | Hnízdo [persistentní položka] | [optional] 
**ProgramPoint** | **string** | Místo v programu (program point) [persistentní položka] | [optional] 
**Kind** | **int?** | Druh [persistentní položka] | [optional] 
**UserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

