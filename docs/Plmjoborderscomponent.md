# Veit.Abra.Model.Plmjoborderscomponent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu VP - sériové číslo [persistentní položka] | [optional] 
**InputItemID** | **string** | Kusovník; ID objektu VP - kusovník [persistentní položka] | [optional] 
**Component** | **string** | Komponenta [persistentní položka] | [optional] 
**ComponentID** | **string** | Komponenta; ID objektu Šarže/sériové číslo [persistentní položka] | [optional] 
**AssembledAtDATE** | **DateTimeOffset?** | Zamontováno [persistentní položka] | [optional] 
**AssembledByID** | **string** | Zamontoval; ID objektu Pracovník [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

