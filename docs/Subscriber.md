# Veit.Abra.Model.Subscriber
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ExternalNumber** | **string** | Ext. kód [persistentní položka] | [optional] 
**Name** | **string** | Ext. název [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skl. karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**ExternalSpecification** | **string** | Ext. specifikace [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

