# Veit.Abra.Model.Wagetaxtablerow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Daňová tabulka [persistentní položka] | [optional] 
**WageBase** | **double?** | Dolní mez [persistentní položka] | [optional] 
**TaxBase** | **double?** | Daň [persistentní položka] | [optional] 
**PerCentOverBase** | **double?** | Procento [persistentní položka] | [optional] 
**System** | **bool?** | Systémová [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

