# Veit.Abra.Model.Compensationrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Vzájemný zápočet [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**CurrRate** | **double?** | Kurz [persistentní položka] | [optional] 
**RefCurrRate** | **double?** | Vztažný [persistentní položka] | [optional] 
**Credit** | **bool?** | Kredit [persistentní položka] | [optional] 
**VarSymbol** | **string** | Var.symbol [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**TAmount** | **double?** | Celkem [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Celkem lokální [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**PAmount** | **double?** | Platba [persistentní položka] | [optional] 
**PDocumentType** | **string** | Typ plac.dokl. [persistentní položka] | [optional] 
**PDocumentID** | **string** | Plac.doklad; ID objektu Dokument [persistentní položka] | [optional] 
**Coef** | **int?** | Koeficient [persistentní položka] | [optional] 
**LocalCoef** | **int?** | Lokální koeficient [persistentní položka] | [optional] 
**ZoneID** | **string** | Zóna; ID objektu Měna [persistentní položka] | [optional] 
**LocalZoneID** | **string** | Lokální zóna; ID objektu Měna [persistentní položka] | [optional] 
**RefCurrencyID** | **string** | Ref.měna; ID objektu Měna | [optional] 
**LocalRefCurrencyID** | **string** | Lok.ref.měna; ID objektu Měna | [optional] 
**AccPresetDefID** | **string** | Předkontace; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**AccDateDATE** | **DateTimeOffset?** | Datum účt. [persistentní položka] | [optional] 
**PDisKind** | **int?** | Rozdělení [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**CorrectionRow** | **bool?** | Korekční řádek [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

