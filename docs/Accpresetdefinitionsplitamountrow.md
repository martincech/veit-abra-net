# Veit.Abra.Model.Accpresetdefinitionsplitamountrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Účetní předkontace [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**Condition** | **string** | Podmínka [persistentní položka] | [optional] 
**DebitAccountID** | **string** | Účet MD; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**CreditAccountID** | **string** | Účet D; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**DebitDivisionID** | **string** | Středisko MD; ID objektu Středisko [persistentní položka] | [optional] 
**CreditDivisionID** | **string** | Středisko D; ID objektu Středisko [persistentní položka] | [optional] 
**DebitBusOrderID** | **string** | Zakázka MD; ID objektu Zakázka [persistentní položka] | [optional] 
**CreditBusOrderID** | **string** | Zakázka D; ID objektu Zakázka [persistentní položka] | [optional] 
**DebitBusTransactionID** | **string** | O.případ MD; ID objektu Obchodní případ [persistentní položka] | [optional] 
**CreditBusTransactionID** | **string** | O.případ D; ID objektu Obchodní případ [persistentní položka] | [optional] 
**DebitBusProjectID** | **string** | Projekt MD; ID objektu Projekt [persistentní položka] | [optional] 
**CreditBusProjectID** | **string** | Projekt D; ID objektu Projekt [persistentní položka] | [optional] 
**TextExpr** | **string** | Text [persistentní položka] | [optional] 
**Text2** | **string** | Text | [optional] 
**RowType** | **int?** | Typ [persistentní položka] | [optional] 
**ExpressionRow** | **bool?** | Výraz [persistentní položka] | [optional] 
**DebitAccountExpr** | **string** | Výraz Účet MD [persistentní položka] | [optional] 
**CreditAccountExpr** | **string** | Výraz Účet D [persistentní položka] | [optional] 
**DebitDivisionExpr** | **string** | Výraz Středisko MD [persistentní položka] | [optional] 
**CreditDivisionExpr** | **string** | Výraz Středisko D [persistentní položka] | [optional] 
**DebitBusOrderExpr** | **string** | Výraz Zakázka MD [persistentní položka] | [optional] 
**CreditBusOrderExpr** | **string** | Výraz Zakázka D [persistentní položka] | [optional] 
**DebitBusTransactionExpr** | **string** | Výraz Obchodní případ MD [persistentní položka] | [optional] 
**CreditBusTransactionExpr** | **string** | Výraz Obchodní případ D [persistentní položka] | [optional] 
**DebitBusProjectExpr** | **string** | Výraz Projekt MD [persistentní položka] | [optional] 
**CreditBusProjectExpr** | **string** | Výraz Projekt D [persistentní položka] | [optional] 
**AmountExpr** | **string** | Výraz rozúčtování částky [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

