# Veit.Abra.Model.Supplierpricelist
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Supplierpricelistrow&gt;**](Supplierpricelistrow.md) | Řádky; kolekce BO Položka dod. ceníku [nepersistentní položka] | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**FirmOfficeID** | **string** | Provozovna; ID objektu Provozovna [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ValidFromDateDATE** | **DateTimeOffset?** | Platný od [persistentní položka] | [optional] 
**LastImportDateDATE** | **DateTimeOffset?** | Datum importu [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**Discount1** | **double?** | Sleva 1 [persistentní položka] | [optional] 
**Discount2** | **double?** | Sleva 2 [persistentní položka] | [optional] 
**Discount3** | **double?** | Sleva 3 [persistentní položka] | [optional] 
**PricesWithVAT** | **bool?** | Cena s DPH [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**TotalDiscount** | **double?** | Celková sleva | [optional] 
**CountOfAssigned** | **int?** | Připojených | [optional] 
**CountOfUnAssigned** | **int?** | Nepřipojených | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

