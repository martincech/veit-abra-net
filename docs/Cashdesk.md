# Veit.Abra.Model.Cashdesk
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Cashdeskbeginning&gt;**](Cashdeskbeginning.md) | Počátky; kolekce BO Počátek pokladny [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**AccountID** | **string** | Účet; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**FirstOpenPeriodID** | **string** | První období; ID objektu Období [persistentní položka] | [optional] 
**LastOpenPeriodID** | **string** | Akt.období; ID objektu Období [persistentní položka] | [optional] 
**FirstLocalAmount** | **double?** | Lokál.částka prvního poč. | [optional] 
**FirstAmount** | **double?** | Částka prvního poč. | [optional] 
**BalanceAmount** | **double?** | Zůstatek | [optional] 
**LocalBalanceAmount** | **double?** | Zůstatek lok. | [optional] 
**FiscalizationPaymentType** | **int?** | Způsob úhrady [persistentní položka] | [optional] 
**Fiscal** | **bool?** | Fiskální pokladna [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

