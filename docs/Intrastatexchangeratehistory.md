# Veit.Abra.Model.Intrastatexchangeratehistory
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DateDATE** | **DateTimeOffset?** | Datum [persistentní položka] | [optional] 
**ParentID** | **string** | ID lístku [persistentní položka] | [optional] 
**CurrRate** | **double?** | Kurz [persistentní položka] | [optional] 
**RefCurrRate** | **double?** | Kurz vztažný [persistentní položka] | [optional] 
**YearByDate** | **int?** | Rok | [optional] 
**MonthByDate** | **int?** | Měsíc | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

