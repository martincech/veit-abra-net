# Veit.Abra.Model.Evaluationcriterion
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Evaluationcriteriondescscore&gt;**](Evaluationcriteriondescscore.md) | Řádky; kolekce BO Hodnotící kritérium - popisy bodového hodnocení [nepersistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**CriterionType** | **int?** | Typ kritéria [persistentní položka] | [optional] 
**Expression** | **string** | Výraz [persistentní položka] | [optional] 
**MinScore** | **int?** | Minimální počet bodů [persistentní položka] | [optional] 
**MaxScore** | **int?** | Maximální počet bodů [persistentní položka] | [optional] 
**SatisfactoryScore** | **int?** | Počet bodů postačujících ke schválení [persistentní položka] | [optional] 
**DataSources** | [**List&lt;Evaluationcriteriondatasource&gt;**](Evaluationcriteriondatasource.md) | Datové zdroje; kolekce BO Hodnotící kritérium - datové zdroje [nepersistentní položka] | [optional] 
**ConditionForTakeover** | **string** | Podmínka pro převzetí [persistentní položka] | [optional] 
**System** | **bool?** | Systémový [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

