# Veit.Abra.Model.Logstoresettings
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Logstoresettingsrow&gt;**](Logstoresettingsrow.md) | Řádky; kolekce BO Nastavení nasklad./vysklad. z pozic [nepersistentní položka] | [optional] 
**ExecutionMode** | **int?** | Způsob nastavení dokl. do stavu Provedeno [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

