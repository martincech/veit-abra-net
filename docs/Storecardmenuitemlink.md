# Veit.Abra.Model.Storecardmenuitemlink
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreMenuItemID** | **string** | Skladové menu; ID objektu Skladové menu [persistentní položka] | [optional] 
**IsBasicLink** | **bool?** | Výchozí | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

