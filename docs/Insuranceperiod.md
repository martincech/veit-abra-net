# Veit.Abra.Model.Insuranceperiod
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavičkový objekt [persistentní položka] | [optional] 
**InsuranceBaseType** | **int?** | Typ pojištění [persistentní položka] | [optional] 
**PeriodBeginDATE** | **DateTimeOffset?** | Začátek [persistentní položka] | [optional] 
**PeriodEndDATE** | **DateTimeOffset?** | Konec [persistentní položka] | [optional] 
**InsuranceCode** | **string** | Kód pojištění [persistentní položka] | [optional] 
**InsurancePeriodType** | **int?** | Typ doby [persistentní položka] | [optional] 
**InsurancePeriodTypeAsText** | **string** | Typ doby | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**InsuranceBase** | **double?** | Vym.základ | [optional] 
**ExcludedDaysInsBase** | **double?** | Vym.z.při vyl.d. | [optional] 
**ExcludedDays** | **int?** | Vyl.doba | [optional] 
**InsuranceDetails** | [**List&lt;Insuranceperioddetail&gt;**](Insuranceperioddetail.md) | Detail průběhu pojištění; kolekce BO Detail období pojištění [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

