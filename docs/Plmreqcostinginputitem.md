# Veit.Abra.Model.Plmreqcostinginputitem
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah jednotky [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství | [optional] 
**PhaseID** | **string** | Etapa; ID objektu Etapa [persistentní položka] | [optional] 
**CostingPrice** | **double?** | Cena pro kalkulaci [persistentní položka] | [optional] 
**CostingMethod** | **int?** | Způsob kalkulace [persistentní položka] | [optional] 
**MaterialAmount** | **double?** | Materiál - částka [persistentní položka] | [optional] 
**SemiFinProdAmount** | **double?** | Polotovary - částka [persistentní položka] | [optional] 
**ConsumablesAmount** | **double?** | Spotř. materiál - částka [persistentní položka] | [optional] 
**TotalAmount** | **double?** | Materiál | [optional] 
**MaterialExpense** | **double?** | Materiálová režie [persistentní položka] | [optional] 
**OwnerID** | **string** | Vlastník; ID objektu Uzel stromu kalkulace požadavku na výrobu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

