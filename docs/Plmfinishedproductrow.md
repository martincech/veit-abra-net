# Veit.Abra.Model.Plmfinishedproductrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Hlavička zaúčtování; ID objektu Dokončený výrobek [persistentní položka] | [optional] 
**JOOutputItemID** | **string** | Vyráběná položka; ID objektu VP - vyráběná položka [persistentní položka] | [optional] 
**JobOrdersSNID** | **string** | Sériové číslo/šarže; ID objektu VP - sériové číslo [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství v ev.jedn. [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**ReceivedByID** | **string** | Přijal; ID objektu Uživatel [persistentní položka] | [optional] 
**ReceivedAtDATE** | **DateTimeOffset?** | Datum přijetí [persistentní položka] | [optional] 
**CheckedByID** | **string** | Zkontroloval; ID objektu Uživatel [persistentní položka] | [optional] 
**CheckedAtDATE** | **DateTimeOffset?** | Datum kontroly [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství | [optional] 
**StoreDoc2ID** | **string** | Řádek příjemky; ID objektu Příjem hotových výrobků - řádek [persistentní položka] | [optional] 
**MaterialExpenseAmount** | **double?** | Materiálová režie [persistentní položka] | [optional] 
**ConsumablesAmount** | **double?** | Spotřební materiál [persistentní položka] | [optional] 
**PriceAmount** | **double?** | Pevná cena [persistentní položka] | [optional] 
**DocQueueID** | **string** | Řada; ID objektu Řada dokladů | [optional] 
**PeriodID** | **string** | Období; ID objektu Období | [optional] 
**OrdNumber** | **int?** | Pořadové číslo | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel | [optional] 
**AccPresetDefID** | **string** | Předkontace; ID objektu Účetní předkontace | [optional] 
**AccDateDate** | **DateTimeOffset?** | Datum účt. | [optional] 
**ProductionDateDATE** | **DateTimeOffset?** | Datum výroby [persistentní položka] | [optional] 
**Cooperation** | **bool?** | Kooperace [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

