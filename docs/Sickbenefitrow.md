# Veit.Abra.Model.Sickbenefitrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Nemocenská dávka [persistentní položka] | [optional] 
**RowType** | **int?** | Typ [persistentní položka] | [optional] 
**WagePeriodID** | **string** | Mzdové období; ID objektu Mzdové období [persistentní položka] | [optional] 
**PaidFromDATE** | **DateTimeOffset?** | Propl. od [persistentní položka] | [optional] 
**PaidToDATE** | **DateTimeOffset?** | Propl. do [persistentní položka] | [optional] 
**AbsentFromDATE** | **DateTimeOffset?** | Omluven od [persistentní položka] | [optional] 
**AbsentToDATE** | **DateTimeOffset?** | Omluven do [persistentní položka] | [optional] 
**BenefitCorrection** | **double?** | Oprava [persistentní položka] | [optional] 
**TotalBenefit** | **double?** | Celkem [persistentní položka] | [optional] 
**TotalAbsentHours** | **double?** | Zameškáno (h) [persistentní položka] | [optional] 
**ShorteningPercent** | **double?** | Procento krácení [persistentní položka] | [optional] 
**Average** | **double?** | Průměr [persistentní položka] | [optional] 
**ImportSourceID** | **string** | Zdroj importu; ID objektu Import do Nemocenských dávek [persistentní položka] | [optional] 
**CreatedInClosedPeriod** | **bool?** | Vytvořeno v uzavřeném období [persistentní položka] | [optional] 
**ShortenedInClosedPeriod** | **bool?** | Ukončeno v uzavřeném období [persistentní položka] | [optional] 
**DeletedinClosedPeriod** | **bool?** | Smazáno v uzavřeném období [persistentní položka] | [optional] 
**InClosedPeriodUserID** | **string** | Uživatel, který zadal ND do uz. období; ID objektu Uživatel [persistentní položka] | [optional] 
**OriginalAbsenceEndDATE** | **DateTimeOffset?** | Původní konec absence [persistentní položka] | [optional] 
**CompensationBoost** | **double?** | Navýšení [persistentní položka] | [optional] 
**Dirty** | **bool?** | Nepočítaný objekt | [optional] 
**CorrectedBenefit** | **double?** | Korigovaná částka | [optional] 
**BoostedCompensation** | **double?** | Navýšená částka | [optional] 
**BoostCorrection** | **double?** | Oprava navýšení náhrady [persistentní položka] | [optional] 
**OldWagePeriodID** | **string** | Pův. mzd. období; ID objektu Mzdové období | [optional] 
**DayBase1** | **double?** | DVZ1 | [optional] 
**DayBase2** | **double?** | DVZ2 | [optional] 
**HourBase** | **double?** | RPV | [optional] 
**DaysInRate01** | **double?** | V sazbě 1 | [optional] 
**AmountInRate01** | **double?** | Částka 1 | [optional] 
**DaysInRate02** | **double?** | V sazbě 2 | [optional] 
**AmountInRate02** | **double?** | Částka 2 | [optional] 
**DaysInRate03** | **double?** | V sazbě 3 | [optional] 
**AmountInRate03** | **double?** | Částka 3 | [optional] 
**DaysInRate04** | **double?** | V sazbě 4 | [optional] 
**AmountInRate04** | **double?** | Částka 4 | [optional] 
**DaysInRate05** | **double?** | V sazbě 5 | [optional] 
**AmountInRate05** | **double?** | Částka 5 | [optional] 
**DaysInRate06** | **double?** | V sazbě 6 | [optional] 
**AmountInRate06** | **double?** | Částka 6 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

