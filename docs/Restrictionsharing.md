# Veit.Abra.Model.Restrictionsharing
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Definice omezení [persistentní položka] | [optional] 
**VisibleForUserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**VisibleForRoleID** | **string** | Role; ID objektu Role [persistentní položka] | [optional] 
**VisibleForGroupID** | **string** | Skupina rolí; ID objektu Skupina rolí [persistentní položka] | [optional] 
**RowType** | **int?** | Typ | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

