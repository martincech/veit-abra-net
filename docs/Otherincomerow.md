# Veit.Abra.Model.Otherincomerow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Ostatní příjem [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**TAmount** | **double?** | Celkem [persistentní položka] | [optional] 
**LocalTAmount** | **double?** | Celkem lokálně [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | O.případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**TAmountWithoutVAT** | **double?** | Bez daně [persistentní položka] | [optional] 
**VATRateID** | **string** | %DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**VATIndexID** | **string** | DPHIndex; ID objektu DPH index [persistentní položka] | [optional] 
**VATRate** | **double?** | DPH sazba [persistentní položka] | [optional] 
**LocalTAmountWithoutVAT** | **double?** | Bez daně lokálně [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**Text** | **string** | Text [persistentní položka] | [optional] 
**VATTAmount** | **double?** | DPH | [optional] 
**IncomeTypeID** | **string** | Typ příjmu; ID objektu Typ příjmu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

