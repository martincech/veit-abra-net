# Veit.Abra.Model.Plmreqnode
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Požadavek na výrobu [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pozice [persistentní položka] | [optional] 
**Decomposition** | **bool?** | Decomposition [persistentní položka] | [optional] 
**Issue** | **int?** | Typ řádku [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**MasterID** | **string** | Nadřízený uzel; ID objektu Uzel stromu požadavku na výrobu [persistentní položka] | [optional] 
**OutputItemID** | [**Plmreqoutputitem**](Plmreqoutputitem.md) |  | [optional] 
**InputItemID** | [**Plmreqinputitem**](Plmreqinputitem.md) |  | [optional] 
**Rows** | [**List&lt;Plmreqnode&gt;**](Plmreqnode.md) | Řádky; kolekce BO Uzel stromu požadavku na výrobu [nepersistentní položka] | [optional] 
**TreePath** | **string** | Pořadí dle cesty ve stromu | [optional] 
**TreePathHumanReadable** | **string** | Pořadí dle cesty ve stromu | [optional] 
**TreeLevel** | **int?** | Úroveň | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad | [optional] 
**Quantity** | **double?** | Množství | [optional] 
**UnitQuantity** | **double?** | Počet | [optional] 
**QUnit** | **string** | Jednotka | [optional] 
**UnitRate** | **double?** | Vztah | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

