# Veit.Abra.Model.Logstoreaddresscodedefrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Definice adres skladových pozic [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**SegmentName** | **string** | Název segmentu [persistentní položka] | [optional] 
**SegmentType** | **int?** | Typ segmentu [persistentní položka] | [optional] 
**SegmentLength** | **int?** | Délka segmentu [persistentní položka] | [optional] 
**Separator** | **string** | Oddělovač [persistentní položka] | [optional] 
**SegmentLowBound** | **string** | Spodní omezení | [optional] 
**SegmentHighBound** | **string** | Horní omezení | [optional] 
**SegmentTypeDesc** | **string** | Typ segmentu - popis | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

