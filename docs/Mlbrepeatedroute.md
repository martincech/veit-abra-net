# Veit.Abra.Model.Mlbrepeatedroute
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**RouteType** | **int?** | Typ jízdy [persistentní položka] | [optional] 
**RepetitionPause** | **int?** | Počet dní mezi opakováním [persistentní položka] | [optional] 
**TRMCarID** | **string** | Vozidlo; ID objektu Vozidlo [persistentní položka] | [optional] 
**TRMDriverID** | **string** | Řidič; ID objektu Řidič [persistentní položka] | [optional] 
**MLBRouteID** | **string** | Vzor jízdy; ID objektu Vzor jízdy [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Definice platná od [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Definice platná do [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obchodní případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**Monday** | **bool?** | Lze generovat v pondělí [persistentní položka] | [optional] 
**Tuesday** | **bool?** | Lze generovat v úterý [persistentní položka] | [optional] 
**Wednesday** | **bool?** | Lze generovat ve středu [persistentní položka] | [optional] 
**Thursday** | **bool?** | Lze generovat ve čtvrtek [persistentní položka] | [optional] 
**Friday** | **bool?** | Lze generovat v pátek [persistentní položka] | [optional] 
**Saturday** | **bool?** | Lze generovat v sobotu [persistentní položka] | [optional] 
**Sunday** | **bool?** | Lze generovat v neděli [persistentní položka] | [optional] 
**DepartTimeFromDATE** | **DateTimeOffset?** | Čas odjezdu od [persistentní položka] | [optional] 
**DepartTimeToDATE** | **DateTimeOffset?** | Čas odjezdu do [persistentní položka] | [optional] 
**SpeedFrom** | **int?** | Rychlost jízdy od [persistentní položka] | [optional] 
**SpeedTo** | **int?** | Rychlost jízdy do [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

