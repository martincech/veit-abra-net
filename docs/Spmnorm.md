# Veit.Abra.Model.Spmnorm
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Spmnormmaterials&gt;**](Spmnormmaterials.md) | Řádky; kolekce BO Norma kompletace - řádek [nepersistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreCardCode** | **string** | Skladová karta - kód | [optional] 
**StoreCardName** | **string** | Skladová karta - název | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah | [optional] 
**Quantity** | **double?** | Počet v jedn.se vzt.1 [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Počet | [optional] 
**WeightInGrams** | **double?** | Celková hmotnost v gramech | [optional] 
**WeightInGrCalculated** | **double?** | Celk. hmot. v gr. vypočtená | [optional] 
**UnitWeightInGrams** | **double?** | Hmotnost jednotky v gramech | [optional] 
**UnitWeightInGrCalculated** | **double?** | Hmotnost jedn. v gr. vypočtená | [optional] 
**OperationDescription** | **string** | Pracovní postup [persistentní položka] | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedDateDATE** | **DateTimeOffset?** | Datum vytvoření [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedDateDATE** | **DateTimeOffset?** | Datum opravy [persistentní položka] | [optional] 
**PictureID** | [**Picture**](Picture.md) |  | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**CalculatedUnitPrice** | **double?** | Cena jednotky | [optional] 
**CalculatedPrice** | **double?** | Cena [persistentní položka] | [optional] 
**Pictures** | [**List&lt;Spmnormpicture&gt;**](Spmnormpicture.md) | Obrázky; kolekce BO Norma kompletace - obrázek [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

