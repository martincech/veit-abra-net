# Veit.Abra.Model.Wagepaymentorderssources
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**PaidAmount** | **double?** | Zapl. částka [persistentní položka] | [optional] 
**DueDateDATE** | **DateTimeOffset?** | Datum splatnosti [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**BankAccountID** | **string** | Zdrojový bank.ú.; ID objektu Bankovní účet [persistentní položka] | [optional] 
**TargetBankAccount** | **string** | Cílový bank.ú. [persistentní položka] | [optional] 
**TargetBankName** | **string** | Cílová banka [persistentní položka] | [optional] 
**VarSymbol** | **string** | Var.symbol [persistentní položka] | [optional] 
**ConstSymbolID** | **string** | Konst.symb.; ID objektu Konstantní symbol [persistentní položka] | [optional] 
**SpecSymbol** | **string** | Spec.symbol [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**TargetBankCountryID** | **string** | Země banky; ID objektu Země [persistentní položka] | [optional] 
**ForeignIssue** | **int?** | Dodatečné popl. [persistentní položka] | [optional] 
**Urgent** | **int?** | Způsob provedení pl. [persistentní položka] | [optional] 
**SwiftCode** | **string** | SWIFT kód [persistentní položka] | [optional] 
**PaymentOrderID** | **string** | Plat.příkaz; ID objektu Platební příkaz - řádek | [optional] 
**PaymentOrderIDIsValid** | **bool?** | Platný odkaz na plat.příkaz | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**WageClosingBookID** | **string** | Mzdová uzávěrka; ID objektu Uzávěrka mezd [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch. případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**BaseType** | **int?** | Zákl. druh [persistentní položka] | [optional] 
**Flag** | **string** | Rozl. řetězec [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**IsProfit** | **bool?** | Výnos | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

