# Veit.Abra.Model.Serviceworkcategory
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**AmountPerHour** | **double?** | Hodinová sazba bez DPH [persistentní položka] | [optional] 
**AmountPerHourWithVAT** | **double?** | Hodinová sazba s daní [persistentní položka] | [optional] 
**CostPriceWithoutVAT** | **double?** | Nákl.cena bez daně [persistentní položka] | [optional] 
**CostPriceWithVAT** | **double?** | Nákl.cena s daní [persistentní položka] | [optional] 
**VATRateID** | **string** | Sazba DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**Rows** | [**List&lt;Serviceworkcategoryrow&gt;**](Serviceworkcategoryrow.md) | řádek; kolekce BO Řádek servisní odbornosti [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

