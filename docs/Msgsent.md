# Veit.Abra.Model.Msgsent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**SentDateDATE** | **DateTimeOffset?** | Datum odeslání [persistentní položka] | [optional] 
**SenderUserID** | **string** | Odesílatel; ID objektu Uživatel [persistentní položka] | [optional] 
**MsgSubject** | **string** | Předmět [persistentní položka] | [optional] 
**MsgBody** | **string** | Obsah [persistentní položka] | [optional] 
**ValidToDateDATE** | **DateTimeOffset?** | Datum platnosti [persistentní položka] | [optional] 
**DeleteAfterDeletingByAll** | **bool?** | Smazat po smazání všemi [persistentní položka] | [optional] 
**ConfirmReading** | **bool?** | Potvrzení přečtení [persistentní položka] | [optional] 
**Attachments** | [**List&lt;Msgattachment&gt;**](Msgattachment.md) | Přílohy; kolekce BO Příloha [nepersistentní položka] | [optional] 
**Links** | [**List&lt;Msglink&gt;**](Msglink.md) | Odkazy; kolekce BO Odkaz [nepersistentní položka] | [optional] 
**Recipients** | [**List&lt;Msgrecipient&gt;**](Msgrecipient.md) | Adresáti; kolekce BO Adresát vzkazu [nepersistentní položka] | [optional] 
**RecipientsInOneLine** | **string** | Souhrn adresátů | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

