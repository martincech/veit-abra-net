# Veit.Abra.Model.Countofftype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CountOffKind** | **int?** | Základní typ [persistentní položka] | [optional] 
**FixedYearly** | **bool?** | Fix/rok [persistentní položka] | [optional] 
**FixedMonthly** | **bool?** | Fix/měsíc [persistentní položka] | [optional] 
**FreeYearly** | **bool?** | Rok [persistentní položka] | [optional] 
**FreeMonthly** | **bool?** | Měsíc [persistentní položka] | [optional] 
**YearlyAmountID** | **string** | Částka za rok; ID objektu Globální proměnná [persistentní položka] | [optional] 
**MonAmIsTwelfth** | **bool?** | x1/12 [persistentní položka] | [optional] 
**MonthlyAmountID** | **string** | Částka za měsíc; ID objektu Globální proměnná [persistentní položka] | [optional] 
**Cancellate** | **bool?** | Krátit? [persistentní položka] | [optional] 
**TaxAdvantage** | **bool?** | Daňová sleva [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platnost od [persistentní položka] | [optional] 
**ValidToDATE** | **DateTimeOffset?** | Platnost do [persistentní položka] | [optional] 
**CountOffKindText** | **string** | Základní typ | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

