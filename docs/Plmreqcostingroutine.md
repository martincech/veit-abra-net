# Veit.Abra.Model.Plmreqcostingroutine
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Požadavek na výrobu - kalkulace vyráběná položka [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**WorkPlaceID** | **string** | Pracoviště; ID objektu Pracoviště a stroj [persistentní položka] | [optional] 
**PhaseID** | **string** | Etapa; ID objektu Etapa [persistentní položka] | [optional] 
**PartTime** | **double?** | Kusový čas v minutách | [optional] 
**SetupTime** | **double?** | Dávkový čas v minutách | [optional] 
**Title** | **string** | Název [persistentní položka] | [optional] 
**Cooperation** | **bool?** | Kooperace [persistentní položka] | [optional] 
**SalaryClassID** | **string** | Tarifní třída; ID objektu Tarifní třída [persistentní položka] | [optional] 
**TotalTime** | **double?** | Celkový čas v sekundách [persistentní položka] | [optional] 
**Salary** | **double?** | Mzda [persistentní položka] | [optional] 
**OverheadCosts** | **double?** | Výrobní režie [persistentní položka] | [optional] 
**GeneralExpense** | **double?** | Správní režie [persistentní položka] | [optional] 
**TotalOverheads** | **double?** | Režie celkem | [optional] 
**TAC** | **double?** | Dávkový čas v sekundách [persistentní položka] | [optional] 
**TBC** | **double?** | Kusový čas v sekundách [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

