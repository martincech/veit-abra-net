# Veit.Abra.Model.Bustransactionsource
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Obchodní případ [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**SourceRoleID** | **string** | Role zdroje; ID objektu Role [persistentní položka] | [optional] 
**SourceOrder** | **int?** | Pořadí [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**CRMActivityAreaID** | **string** | Oblast aktivity; ID objektu Oblast aktivity [persistentní položka] | [optional] 
**CRMActivityTypeID** | **string** | Typ aktivity; ID objektu Typ aktivit [persistentní položka] | [optional] 
**CRMActivityQueueID** | **string** | Řada akt.; ID objektu Řada aktivit [persistentní položka] | [optional] 
**PlannedTotalHours** | **double?** | Plánováno [persistentní položka] | [optional] 
**RealTotalHours** | **double?** | Odpracováno [persistentní položka] | [optional] 
**CanExceedPlan** | **int?** | Povol.přesčas [persistentní položka] | [optional] 
**Invoicing** | **bool?** | Fakturovat [persistentní položka] | [optional] 
**SalePriceType** | **int?** | Typ ceny [persistentní položka] | [optional] 
**HourRate** | **double?** | Zákl.sazba [persistentní položka] | [optional] 
**OverTimeHourRate** | **double?** | Přesčas.sazba [persistentní položka] | [optional] 
**VATRateID** | **string** | Sazba DPH; ID objektu DPH sazba [persistentní položka] | [optional] 
**CostHourRate** | **double?** | Náklad.sazba [persistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta; ID objektu Skladová karta [persistentní položka] | [optional] 
**StoreUnitID** | **string** | Jednotka; ID objektu Jednotka skladové karty [persistentní položka] | [optional] 
**StoreID** | **string** | Sklad; ID objektu Sklad [persistentní položka] | [optional] 
**PriceDefinition1ID** | **string** | Def.ceny zákl.; ID objektu Definice ceny [persistentní položka] | [optional] 
**PriceDefinition2ID** | **string** | Def.ceny přesč.; ID objektu Definice ceny [persistentní položka] | [optional] 
**Finished** | **bool?** | Dokončeno [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**BusOrderID** | **string** | zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusProjectID** | **string** | projekt; ID objektu Projekt [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

