# Veit.Abra.Model.Datachangeslog
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Site** | **string** | Hnízdo [persistentní položka] | [optional] 
**ClientCreatedAtDATE** | **DateTimeOffset?** | Klientský čas vzniku [persistentní položka] | [optional] 
**ServerCreatedAtDATE** | **DateTimeOffset?** | Centrální čas vzniku [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CLSID** | **string** | Třída [persistentní položka] | [optional] 
**ObjID** | **string** | ID objektu [persistentní položka] | [optional] 
**LogData** | **byte[]** | Data logu [persistentní položka] | [optional] 
**Status** | **int?** | Typ změny [persistentní položka] | [optional] 
**ObjectName** | **string** | Název objektu [persistentní položka] | [optional] 
**LogDataFormat** | **int?** | Formát dat logu [persistentní položka] | [optional] 
**CLSIDText** | **string** | Třída (textově) | [optional] 
**NetworkCardID** | **string** | ID síť. karty [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

