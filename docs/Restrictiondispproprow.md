# Veit.Abra.Model.Restrictiondispproprow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Vlastnost zobrazení definic omezení [persistentní položka] | [optional] 
**PosIndex** | **int?** | Pořadí [persistentní položka] | [optional] 
**RestrictionDefinitionID** | **string** | Definice omezení; ID objektu Definice omezení [persistentní položka] | [optional] 
**IsVisible** | **bool?** | Viditelná [persistentní položka] | [optional] 
**IsFavorite** | **bool?** | Oblíbená [persistentní položka] | [optional] 
**IsDefault** | **bool?** | Výchozí [persistentní položka] | [optional] 
**Name** | **string** | Název | [optional] 
**SpecialKind** | **int?** | Typ | [optional] 
**IsRed** | **bool?** | Červené omezení | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

