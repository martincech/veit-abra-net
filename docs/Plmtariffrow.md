# Veit.Abra.Model.Plmtariffrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Režijní sazba [persistentní položka] | [optional] 
**WorkPlaceID** | **string** | Pracoviště; ID objektu Pracoviště a stroj [persistentní položka] | [optional] 
**OverheadCosts** | **double?** | Výrobní režie/hod. [persistentní položka] | [optional] 
**OverheadCostsPerc** | **double?** | Výrobní režie (%) [persistentní položka] | [optional] 
**GeneralExpense** | **double?** | Správní režie (%) [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

