# Veit.Abra.Model.Servicedefect
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**ParentID** | **string** | Nadřízený; ID objektu Servisní závada [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Popis [persistentní položka] | [optional] 
**Description** | **string** | Detailní popis [persistentní položka] | [optional] 
**ServiceTypeID** | **string** | Typ servisního případu; ID objektu Typ servisního případu [persistentní položka] | [optional] 
**Operations** | [**List&lt;Servicedefectoperation&gt;**](Servicedefectoperation.md) | Operace; kolekce BO Servisní závada - řádek [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

