# Veit.Abra.Model.Crmactivityqueueunusednumber
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Poslední číslo dokladu; ID objektu Řada aktivit - období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Rezervované číslo dokladu [persistentní položka] | [optional] 
**Reserved** | **bool?** | Rezervováno [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Datum a čas vytvoření [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Datum a čas poslední opravy [persistentní položka] | [optional] 
**Origin** | **int?** | Původ [persistentní položka] | [optional] 
**PriorityUserID** | **string** | Vyhrazený uživatel; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

