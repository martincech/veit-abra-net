# Veit.Abra.Model.Intrastatexchangerate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**RefCurrencyID** | **string** | Vztažná měna; ID objektu Měna [persistentní položka] | [optional] 
**DateDATE** | **DateTimeOffset?** | Datum | [optional] 
**CurrRate** | **double?** | Kurz | [optional] 
**RefCurrRate** | **double?** | Kurz vztažný | [optional] 
**YearByDate** | **int?** | Rok | [optional] 
**MonthByDate** | **int?** | Měsíc | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

