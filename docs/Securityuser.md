# Veit.Abra.Model.Securityuser
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Name** | **string** | Jméno [persistentní položka] | [optional] 
**ShortName** | **string** | Zkratka [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**Comment** | **string** | Poznámka | [optional] 
**AddressID** | [**Address**](Address.md) |  | [optional] 
**LoginName** | **string** | Přihlašovací jméno [persistentní položka] | [optional] 
**Locked** | **bool?** | Uzamčen [persistentní položka] | [optional] 
**SecPassword** | **string** | Heslo [persistentní položka] | [optional] 
**SecToken** | **string** | Token [persistentní položka] | [optional] 
**SecTokenFastLogin** | **bool?** | Rychlé přihlášení [persistentní položka] | [optional] 
**IsActive** | **bool?** | Aktivní | [optional] 
**LogonPassword** | **string** | Heslo | [optional] 
**PortalLoginName** | **string** | Přihlašovací jméno  [persistentní položka] | [optional] 
**PortalSecPassword** | **string** | Heslo [persistentní položka] | [optional] 
**OfferToLogin** | **bool?** | Nabízet v přihlašovacím okně [persistentní položka] | [optional] 
**WebAPIAccess** | **bool?** | Přihlášení nevizuálního uživatele API [persistentní položka] | [optional] 
**BIAccess** | **bool?** | Vizualizace dat [persistentní položka] | [optional] 
**BI_GID** | **string** | Identifikace ve Vizualizaci dat [persistentní položka] | [optional] 
**AskToNPS** | **bool?** | Zobrazovat NPS [persistentní položka] | [optional] 
**DPBypass** | **bool?** | Obejít ochranu dat [persistentní položka] | [optional] 
**XHidePrice** | **bool?** | Skrýt ceny [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

