# Veit.Abra.Model.Assetinvlistresult
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Assetinvlistresultcomp&gt;**](Assetinvlistresultcomp.md) | Řádky; kolekce BO Prvek majetku výsledek inventury [nepersistentní položka] | [optional] 
**ParentID** | **string** | Odkaz na seznam; ID objektu Inventarizační seznam majetku [persistentní položka] | [optional] 
**AssetInvID** | **string** | Odkaz na inventuru; ID objektu Inventura majetku [persistentní položka] | [optional] 
**AssetCardType** | **int?** | Typ majetku [persistentní položka] | [optional] 
**AssetCardID** | **string** | Odkaz na kartu majetku; ID objektu Karta majetku [persistentní položka] | [optional] 
**IsCollection** | **int?** | Je kolekce [persistentní položka] | [optional] 
**SmallSubCardID** | **string** | Karta DrM; ID objektu Položka drobného majetku [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**ResponsibleID** | **string** | Odpovědná osoba; ID objektu Odpovědná osoba [persistentní položka] | [optional] 
**AssetLocationID** | **string** | Umístění; ID objektu Umístění majetku [persistentní položka] | [optional] 
**EvidenceDivisionID** | **string** | EvidenceDivision_ID; ID objektu Středisko [persistentní položka] | [optional] 
**ExpensesDivisionID** | **string** | ExpensesDivision_ID; ID objektu Středisko [persistentní položka] | [optional] 
**Quantity** | **int?** | Počet [persistentní položka] | [optional] 
**AssetCardStatus** | **int?** | Stav majetku [persistentní položka] | [optional] 
**IsImported** | **bool?** | Importován [persistentní položka] | [optional] 
**ImportDescription** | **string** | Poznámka k importu [persistentní položka] | [optional] 
**ApprovalStatus** | **int?** | Stav schválení [persistentní položka] | [optional] 
**AppliedInCard** | **bool?** | Změna přenesena [persistentní položka] | [optional] 
**Description** | **string** | Poznámka [persistentní položka] | [optional] 
**ReadEAN** | **bool?** | EAN je načten [persistentní položka] | [optional] 
**ReadDateDATE** | **DateTimeOffset?** | Datum a čas načtení [persistentní položka] | [optional] 
**EditedByID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

