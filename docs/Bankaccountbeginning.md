# Veit.Abra.Model.Bankaccountbeginning
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Bankovní účet [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**BeginningLocal** | **double?** | Počáteční stav lokální [persistentní položka] | [optional] 
**Beginning** | **double?** | Počáteční stav [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

