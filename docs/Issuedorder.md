# Veit.Abra.Model.Issuedorder
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**RevidedID** | **string** | ID revidovaného objektu; ID objektu Objednávka vydaná | [optional] 
**RevisionDescription** | **string** | Popis revize | [optional] 
**RevisionDateDATE** | **DateTimeOffset?** | Datum revize | [optional] 
**RevisionAuthorID** | **string** | Autor revize; ID objektu Uživatel | [optional] 
**Revision** | **int?** | Číslo revize | [optional] 
**Rows** | [**List&lt;Issuedorderrow&gt;**](Issuedorderrow.md) | Řádky; kolekce BO Objednávka vydaná - řádek [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**FirmID** | **string** | Firma; ID objektu Firma [persistentní položka] | [optional] 
**FirmOfficeID** | **string** | Provozovna; ID objektu Provozovna [persistentní položka] | [optional] 
**PersonID** | **string** | Osoba; ID objektu Osoba [persistentní položka] | [optional] 
**Description** | **string** | Popis [persistentní položka] | [optional] 
**AccountingType** | **int?** | Jak účtovat | [optional] 
**IsAccounted** | **bool?** | Účtováno | [optional] 
**Dirty** | **bool?** | Zakázané přepočítání | [optional] 
**CountryID** | **string** | Země dodavatele; ID objektu Země [persistentní položka] | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**CurrRate** | **double?** | Kurz měny [persistentní položka] | [optional] 
**RefCurrRate** | **double?** | Kurz vztažný [persistentní položka] | [optional] 
**Coef** | **int?** | Koeficient [persistentní položka] | [optional] 
**LocalCoef** | **int?** | Lokální koeficient [persistentní položka] | [optional] 
**ZoneID** | **string** | Zóna; ID objektu Měna [persistentní položka] | [optional] 
**LocalZoneID** | **string** | Lokální zóna; ID objektu Měna [persistentní položka] | [optional] 
**RefCurrencyID** | **string** | Ref.měna; ID objektu Měna | [optional] 
**LocalRefCurrencyID** | **string** | Lok.ref.měna; ID objektu Měna | [optional] 
**CurrRateInfo** | **string** | Kurz měny textově | [optional] 
**Amount** | **double?** | Celkem [persistentní položka] | [optional] 
**LocalAmount** | **double?** | Celkem lokálně [persistentní položka] | [optional] 
**VATDocument** | **bool?** | DPH doklad [persistentní položka] | [optional] 
**AmountWithoutVAT** | **double?** | Celkem bez [persistentní položka] | [optional] 
**LocalAmountWithoutVAT** | **double?** | Celkem lokálně bez daně [persistentní položka] | [optional] 
**VATAmount** | **double?** | DPH | [optional] 
**LocalVATAmount** | **double?** | DPH lok. | [optional] 
**RoundingAmount** | **double?** | Hal. [persistentní položka] | [optional] 
**LocalRoundingAmount** | **double?** | Hal.vyr.lok. [persistentní položka] | [optional] 
**IsAccountedLaterVAT** | **bool?** | DPH pozdější odpočet | [optional] 
**PricesWithVAT** | **bool?** | Ceny s daní [persistentní položka] | [optional] 
**VATRounding** | **int?** | Zaokrouhlení DPH [persistentní položka] | [optional] 
**TotalRounding** | **int?** | Zaokrouhlení celkové [persistentní položka] | [optional] 
**DocumentDiscount** | **double?** | Dodatečná sleva [persistentní položka] | [optional] 
**IsRowDiscount** | **bool?** | Je řádk. sleva [persistentní položka] | [optional] 
**AddressID** | **string** | Vlastní adresa; ID objektu Adresa [persistentní položka] | [optional] 
**TotalDiscountAmount** | **double?** | Celková slev.částka | [optional] 
**FrozenDiscounts** | **bool?** | Zmrazení slev | [optional] 
**Weight** | **double?** | Celk.hmot. | [optional] 
**Capacity** | **double?** | Celk.obj. | [optional] 
**WeightUnit** | **int?** | Jedn.celk.hm. | [optional] 
**CapacityUnit** | **int?** | Jedn.celk.obj. | [optional] 
**VATFromAbovePrecision** | **int?** | Přesnost zdanění zaokrouhlení [persistentní položka] | [optional] 
**VATFromAboveType** | **int?** | Zdanění zaokrouhlení [persistentní položka] | [optional] 
**PricePrecision** | **int?** | Zobrazení desetinných míst pro zadávací částky [persistentní položka] | [optional] 
**DiscountCalcKind** | **int?** | Způsob uplatnění slevy [persistentní položka] | [optional] 
**VATCountryID** | **string** | Země přiznání DPH; ID objektu Země [persistentní položka] | [optional] 
**IntrastatDeliveryTermID** | **string** | Dodací podmínky; ID objektu Dodací podmínky [persistentní položka] | [optional] 
**IntrastatTransportationTypeID** | **string** | Druh dopravy INTRASTAT; ID objektu Druh dopravy INTRASTAT [persistentní položka] | [optional] 
**IntrastatTransactionTypeID** | **string** | Povaha transakce; ID objektu Povaha transakce [persistentní položka] | [optional] 
**TradeType** | **int?** | Typ obchodu [persistentní položka] | [optional] 
**TradeTypeDescription** | **string** | Popis typu obchodu | [optional] 
**Confirmed** | **bool?** | Potvrzeno [persistentní položka] | [optional] 
**Closed** | **bool?** | Vyřízeno [persistentní položka] | [optional] 
**WithPrices** | **bool?** | S cenami [persistentní položka] | [optional] 
**Issued** | **bool?** | Odesláno [persistentní položka] | [optional] 
**ExternalNumber** | **string** | Externí číslo [persistentní položka] | [optional] 
**IsAvailableForDelivery** | **bool?** | Je čerpatelný [persistentní položka] | [optional] 
**IsSupplierApproved** | **bool?** | Je dodavatel schválený | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 
**XInvoicesSent** | **bool?** | Faktura předána [persistentní položka] | [optional] 
**XDescription3** | **string** | Popis objednání [persistentní položka] | [optional] 
**XDescription2** | **string** | Popis výroba [persistentní položka] | [optional] 
**XInvoicesNote** | **string** | Popis fakturace [persistentní položka] | [optional] 
**XConfirmationLink** | **string** | Confirmation Link [persistentní položka] | [optional] 
**XCheck** | **int?** | Kontrola objednávky [persistentní položka] | [optional] 
**XCheckInfo** | **string** | Neprošlo kontrolou - info [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

