# Veit.Abra.Model.Calcchartscript
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Výpočtové schéma [persistentní položka] | [optional] 
**ScriptBefore** | **string** | Skript před [persistentní položka] | [optional] 
**ScriptAfter** | **string** | Skript po [persistentní položka] | [optional] 
**ValidFromDATE** | **DateTimeOffset?** | Platí od [persistentní položka] | [optional] 
**System** | **bool?** | Syst. definice [persistentní položka] | [optional] 
**Explanation** | **string** | Vysvětlení [persistentní položka] | [optional] 
**UserScriptBefore** | **string** | Uživ. skript před | [optional] 
**UserScriptAfter** | **string** | Uživ. skript po | [optional] 
**UserExplanation** | **string** | Uživ. vysvětlení | [optional] 
**SupressScriptBefore** | **int?** | Použít skript před | [optional] 
**SupressScriptAfter** | **int?** | Použít skript po | [optional] 
**SupressScriptBeforeText** | **string** | Použít skript před | [optional] 
**SupressScriptAfterText** | **string** | Použít skript po | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

