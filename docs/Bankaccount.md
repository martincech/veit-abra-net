# Veit.Abra.Model.Bankaccount
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Bankaccountbeginning&gt;**](Bankaccountbeginning.md) | Počátky; kolekce BO Počátek bankovního účtu [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**_BankAccount** | **string** | Bank.účet [persistentní položka] | [optional] 
**SpecSymbol** | **string** | Spec.symbol [persistentní položka] | [optional] 
**BankStatementID** | **string** | Bankovní výpis; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PaymentOrderID** | **string** | Platební příkaz; ID objektu Řada dokladů [persistentní položka] | [optional] 
**AccountID** | **string** | Účet; ID objektu Účet účetního rozvrhu [persistentní položka] | [optional] 
**DivisionID** | **string** | Středisko; ID objektu Středisko [persistentní položka] | [optional] 
**FirstOpenPeriodID** | **string** | První období; ID objektu Období [persistentní položka] | [optional] 
**LastOpenPeriodID** | **string** | Akt.období; ID objektu Období [persistentní položka] | [optional] 
**FirstLocalAmount** | **double?** | Lokál.částka prvního poč. | [optional] 
**FirstAmount** | **double?** | Částka prvního poč. | [optional] 
**CurrencyID** | **string** | Měna; ID objektu Měna [persistentní položka] | [optional] 
**SwiftCode** | **string** | SWIFT [persistentní položka] | [optional] 
**IBANCode** | **string** | IBAN [persistentní položka] | [optional] 
**PostalAccountNumber** | **string** | Číslo poštovního účtu [persistentní položka] | [optional] 
**ClientIdentificationNumber** | **string** | Identifikační číslo klienta [persistentní položka] | [optional] 
**BankCountryID** | **string** | Země banky; ID objektu Země [persistentní položka] | [optional] 
**AddressID** | [**Address**](Address.md) |  | [optional] 
**XPostCode** | **string** | PSČ [persistentní položka] | [optional] 
**XCity** | **string** | Město [persistentní položka] | [optional] 
**XStreet** | **string** | Ulice [persistentní položka] | [optional] 
**XName** | **string** | Název banky (úplný) [persistentní položka] | [optional] 
**XCountry** | **string** | Stát [persistentní položka] | [optional] 
**XNameAccount** | **string** | Název účtu [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

