# Veit.Abra.Model.Employpattern
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**CalcPriority** | **int?** | Priorita [persistentní položka] | [optional] 
**FieldCLSID** | **string** | Třída hodnot | [optional] 
**ChartCLSID** | **string** | Třída schémat | [optional] 
**Fields** | [**List&lt;Employpatternfield&gt;**](Employpatternfield.md) | Hodnoty; kolekce BO Výchozí hodnota [nepersistentní položka] | [optional] 
**Charts** | [**List&lt;Employpatternchart&gt;**](Employpatternchart.md) | Schémata; kolekce BO Výpočtové schéma pracovního poměru [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

