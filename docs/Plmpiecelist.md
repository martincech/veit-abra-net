# Veit.Abra.Model.Plmpiecelist
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**RevidedID** | **string** | ID revidovaného objektu; ID objektu Kusovník | [optional] 
**RevisionDescription** | **string** | Popis revize | [optional] 
**RevisionDateDATE** | **DateTimeOffset?** | Datum revize | [optional] 
**RevisionAuthorID** | **string** | Autor revize; ID objektu Uživatel | [optional] 
**Revision** | **int?** | Číslo revize | [optional] 
**Rows** | [**List&lt;Plmpiecelistsrow&gt;**](Plmpiecelistsrow.md) | Řádky; kolekce BO Kusovník - řádek [nepersistentní položka] | [optional] 
**StoreCardID** | **string** | Skladová karta kusovníku; ID objektu Skladová karta [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedDATE** | **DateTimeOffset?** | Vytvořen [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedDATE** | **DateTimeOffset?** | Opraven [persistentní položka] | [optional] 
**ReleasedByID** | **string** | Schválil; ID objektu Uživatel [persistentní položka] | [optional] 
**ReleasedDATE** | **DateTimeOffset?** | Schválen [persistentní položka] | [optional] 
**PictureID** | [**Picture**](Picture.md) |  | [optional] 
**PlanedMaterial** | **double?** | Plánovaný materiál [persistentní položka] | [optional] 
**PlanedCoopMat** | **double?** | Plánovaný materiál z kooperace [persistentní položka] | [optional] 
**PlanedCooperation** | **double?** | Plánované náklady na kooperaci [persistentní položka] | [optional] 
**DevelopmentCosts** | **double?** | Náklady na vývoj [persistentní položka] | [optional] 
**PieceListType** | **int?** | Typ [persistentní položka] | [optional] 
**PriceForReceipt** | **double?** | Cena pro příjem [persistentní položka] | [optional] 
**BusOrderID** | **string** | Zakázka; ID objektu Zakázka [persistentní položka] | [optional] 
**BusTransactionID** | **string** | Obch. případ; ID objektu Obchodní případ [persistentní položka] | [optional] 
**Quantity** | **double?** | Množství [persistentní položka] | [optional] 
**QUnit** | **string** | Jednotka [persistentní položka] | [optional] 
**UnitRate** | **double?** | Vztah [persistentní položka] | [optional] 
**UnitQuantity** | **double?** | Množství | [optional] 
**RoutineStoreCardID** | **string** | Skladová karta TP; ID objektu Skladová karta [persistentní položka] | [optional] 
**BusProjectID** | **string** | Projekt; ID objektu Projekt [persistentní položka] | [optional] 
**Pictures** | [**List&lt;Plmpiecelistpicture&gt;**](Plmpiecelistpicture.md) | Obrázky; kolekce BO Obrázek ke kusovníku [nepersistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

