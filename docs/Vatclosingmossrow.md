# Veit.Abra.Model.Vatclosingmossrow
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Uzávěrka DPH [persistentní položka] | [optional] 
**VATCountryID** | **string** | Stát dodavatele; ID objektu Země [persistentní položka] | [optional] 
**CountryID** | **string** | Stát určení; ID objektu Země [persistentní položka] | [optional] 
**VATRateID** | **string** | DPH sazba; ID objektu DPH sazba [persistentní položka] | [optional] 
**OldTaxableAmount** | **double?** | Původní základ [persistentní položka] | [optional] 
**OldVATAmount** | **double?** | Původní daň [persistentní položka] | [optional] 
**CalcTaxableAmount** | **double?** | Vypočtený základ [persistentní položka] | [optional] 
**CalcVATAmount** | **double?** | Vypočtená daň [persistentní položka] | [optional] 
**CorrTaxableAmount** | **double?** | Oprava základu [persistentní položka] | [optional] 
**CorrVATAmount** | **double?** | Oprava daně [persistentní položka] | [optional] 
**TaxableAmount** | **double?** | Základ [persistentní položka] | [optional] 
**VATAmount** | **double?** | Daň [persistentní položka] | [optional] 
**VATIdentNumber** | **string** | Identifikátor DPH [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

