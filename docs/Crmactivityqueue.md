# Veit.Abra.Model.Crmactivityqueue
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Code** | **string** | Zkratka [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**LastNumbers** | [**List&lt;Crmactivityqueueperiod&gt;**](Crmactivityqueueperiod.md) | Čísla; kolekce BO Řada aktivit - období [nepersistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**AutoFillHole** | **bool?** | Zaplňovat mezery [persistentní položka] | [optional] 
**FirstOpenPeriodID** | **string** | První otevřené období; ID objektu Období [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

