# Veit.Abra.Model.Demandsheet
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Číslo dok. | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Demandsheetrow&gt;**](Demandsheetrow.md) | Řádky; kolekce BO Poptávkový list - řádek [nepersistentní položka] | [optional] 
**DocQueueID** | **string** | Zdrojová řada; ID objektu Řada dokladů [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**OrdNumber** | **int?** | Pořadové číslo [persistentní položka] | [optional] 
**DocDateDATE** | **DateTimeOffset?** | Datum dok. [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**NewRelatedType** | **int?** | Typ relace | [optional] 
**NewRelatedDocumentID** | **string** | ID dokladu pro připojení | [optional] 
**RequestedClosingDateDATE** | **DateTimeOffset?** | Požadovaný termín uzavření [persistentní položka] | [optional] 
**RealClosingDateDATE** | **DateTimeOffset?** | Skutečný termín uzavření [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**DemandingDocTxt** | **string** | Zdrojový doklad | [optional] 
**Closed** | **bool?** | Příznak uzavření [persistentní položka] | [optional] 
**AddressID** | **string** | Vlastní adresa; ID objektu Adresa [persistentní položka] | [optional] 
**PricePrecision** | **int?** | Zobrazení desetinných míst pro zadávací částky [persistentní položka] | [optional] 
**AutoAddSuppliers** | **int?** | Doplňovat oslovené dodavatele [persistentní položka] | [optional] 
**AutoAddOnlyDoDemand** | **bool?** | Omezení automatického plnění oslovených dodavatelů. [persistentní položka] | [optional] 
**CloneDemandAnswers** | **bool?** | Zkopírovat i oslovené dodavatele | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

