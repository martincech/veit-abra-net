# Veit.Abra.Model.Approvingareacriterion
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavičkový objekt [persistentní položka] | [optional] 
**EvaluationCriterionID** | **string** | Hodnotící kritérium; ID objektu Hodnotící kritérium [persistentní položka] | [optional] 
**Frequency** | **int?** | Frekvence vyhodnocování [persistentní položka] | [optional] 
**RequiredFromDateDATE** | **DateTimeOffset?** | Odkdy požadováno [persistentní položka] | [optional] 
**WeightingCoef** | **double?** | Váhový koeficient kritéria [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

