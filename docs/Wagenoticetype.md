# Veit.Abra.Model.Wagenoticetype
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**NoticeText** | **string** | Popis úkolu [persistentní položka] | [optional] 
**Repeated** | **bool?** | Opakovat [persistentní položka] | [optional] 
**PeriodType** | **int?** | Typ opakování [persistentní položka] | [optional] 
**PeriodLength** | **int?** | Délka [persistentní položka] | [optional] 
**Code** | **string** | Kód [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

