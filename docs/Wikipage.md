# Veit.Abra.Model.Wikipage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Title** | **string** | Titulek [persistentní položka] | [optional] 
**CreatedAtDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**CorrectedAtDATE** | **DateTimeOffset?** | Opraveno [persistentní položka] | [optional] 
**Body** | **string** | Tělo stránky [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**LockedByID** | **string** | Uzamkl; ID objektu Uživatel [persistentní položka] | [optional] 
**SpaceID** | **string** | Prostor; ID objektu Wiki prostor [persistentní položka] | [optional] 
**Locked** | **bool?** | Uzamčena [persistentní položka] | [optional] 
**CamelCaseEnable** | **bool?** | Rozlišovat CamelCase [persistentní položka] | [optional] 
**UseSelfOptions** | **bool?** | Použít vlastní nastavení CamelCase [persistentní položka] | [optional] 
**PageStyleID** | **string** | Styl stránky; ID objektu Systémový soubor stylů [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

