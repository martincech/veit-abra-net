# Veit.Abra.Model.Vatregistration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Rows** | [**List&lt;Vatregistrationrow&gt;**](Vatregistrationrow.md) | Řádky; kolekce BO DPH registrace v zemi EU - řádek [nepersistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**CountryID** | **string** | Země; ID objektu Země [persistentní položka] | [optional] 
**DocIssueLimit** | **int?** | Limit data plnění [persistentní položka] | [optional] 
**VATRounding** | **int?** | Zaokrouhlení DPH [persistentní položka] | [optional] 
**VATAllowance** | **double?** | Tolerance DPH [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

