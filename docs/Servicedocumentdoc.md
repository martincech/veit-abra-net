# Veit.Abra.Model.Servicedocumentdoc
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Servisní list [persistentní položka] | [optional] 
**DocType** | **string** | Typ [persistentní položka] | [optional] 
**SubType** | **int?** | Subtyp dokladu [persistentní položka] | [optional] 
**StoreDocID** | **string** | Skladový doklad; ID objektu Skladový doklad [persistentní položka] | [optional] 
**DocumentID** | **string** | Doklad; ID objektu Dokument [persistentní položka] | [optional] 
**IsGroupInvoice** | **bool?** | Hromadná [persistentní položka] | [optional] 
**RangeDateFrom** | **DateTimeOffset?** | Datum od [persistentní položka] | [optional] 
**RangeDateTo** | **DateTimeOffset?** | Datum do [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

