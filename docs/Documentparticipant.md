# Veit.Abra.Model.Documentparticipant
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**RevidedID** | **string** | ID revidovaného objektu; ID objektu Účastník dokumentu | [optional] 
**RevisionDescription** | **string** | Popis revize | [optional] 
**RevisionDateDATE** | **DateTimeOffset?** | Datum revize | [optional] 
**RevisionAuthorID** | **string** | Autor revize; ID objektu Uživatel | [optional] 
**Revision** | **int?** | Číslo revize | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Dokument [persistentní položka] | [optional] 
**SecurityRoleID** | **string** | Role; ID objektu Role [persistentní položka] | [optional] 
**SecurityUserID** | **string** | Uživatel; ID objektu Uživatel [persistentní položka] | [optional] 
**DocumentCompetence** | **int?** | Oprávnění [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

