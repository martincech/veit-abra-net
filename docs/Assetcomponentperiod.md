# Veit.Abra.Model.Assetcomponentperiod
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu Hlavičkový objekt [persistentní položka] | [optional] 
**PeriodID** | **string** | Období; ID objektu Období [persistentní položka] | [optional] 
**ValorisationAmount** | **double?** | Částka techn.zhodn. [persistentní položka] | [optional] 
**IncreaseAmount** | **double?** | Částka techn.zh. nepromítnutá [persistentní položka] | [optional] 
**ValueChangeAmount** | **double?** | Částka změny ceny [persistentní položka] | [optional] 
**AccValorisationAmount** | **double?** | Částka techn.zhodn. účetní [persistentní položka] | [optional] 
**AccIncreaseAmount** | **double?** | Částka techn.zh. nepromítnutá účetní [persistentní položka] | [optional] 
**AccValueChangeAmount** | **double?** | Částka změny ceny účetní [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

