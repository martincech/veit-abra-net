# Veit.Abra.Model.Plmjobordersroutinead
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**ParentID** | **string** | Vlastník; ID objektu VP - technologický postup [persistentní položka] | [optional] 
**AdditionalCostsID** | **string** | Vedlejší pořizovací náklady; ID objektu Příjemka - Vedlejší pořizovací náklady [persistentní položka] | [optional] 
**AmountTime** | **double?** | Doba trvání [persistentní položka] | [optional] 
**Amount** | **double?** | Částka [persistentní položka] | [optional] 
**SalaryCosts** | **double?** | Mzdové náklady [persistentní položka] | [optional] 
**OverheadCosts** | **double?** | Výrobní režie [persistentní položka] | [optional] 
**GeneralExpense** | **double?** | Správní režie [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

