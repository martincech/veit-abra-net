# Veit.Abra.Model.Plmjobordersnotice
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**JobOrderID** | **string** | Výrobní příkaz; ID objektu Výrobní příkaz [persistentní položka] | [optional] 
**SN_ID** | **string** | Výrobek; ID objektu VP - sériové číslo [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CreatedDATE** | **DateTimeOffset?** | Vytvořeno [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

