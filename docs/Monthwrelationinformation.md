# Veit.Abra.Model.Monthwrelationinformation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**WagePeriodID** | **string** | Mzdové období; ID objektu Mzdové období [persistentní položka] | [optional] 
**CreatedByID** | **string** | Vytvořil; ID objektu Uživatel [persistentní položka] | [optional] 
**CorrectedByID** | **string** | Opravil; ID objektu Uživatel [persistentní položka] | [optional] 
**WorkingRelationID** | **string** | Pracovní poměr; ID objektu Pracovní poměr [persistentní položka] | [optional] 
**WagePeriodAuthorID** | **string** | Mzdové období vzniku; ID objektu Mzdové období [persistentní položka] | [optional] 
**EmployeeID** | **string** | Zaměstnanec; ID objektu Zaměstnanec | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

