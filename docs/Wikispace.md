# Veit.Abra.Model.Wikispace
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Název | [optional] 
**ID** | **string** | Vlastní ID [persistentní položka] | [optional] 
**ClassID** | **string** | ID třídy | [optional] 
**ObjVersion** | **int?** | Verze objektu [persistentní položka] | [optional] 
**Hidden** | **bool?** | Skrytý [persistentní položka] | [optional] 
**Name** | **string** | Název [persistentní položka] | [optional] 
**Title** | **string** | Titulek [persistentní položka] | [optional] 
**Note** | **string** | Poznámka [persistentní položka] | [optional] 
**HomePageName** | **string** | Jméno domácí stránky [persistentní položka] | [optional] 
**HomePageID** | **string** | Domácí stránka; ID objektu Wiki stránka | [optional] 
**CamelCaseEnable** | **bool?** | Rozlišení Camel Case [persistentní položka] | [optional] 
**PageStyleID** | **string** | Styl stránky; ID objektu Systémový soubor stylů [persistentní položka] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

