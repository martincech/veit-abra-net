/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Storebatch
    /// </summary>
    [DataContract]
    public partial class Storebatch :  IEquatable<Storebatch>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Storebatch" /> class.
        /// </summary>
        /// <param name="hidden">Skrytý [persistentní položka].</param>
        /// <param name="storeCardID">Skl. karta; ID objektu Skladová karta [persistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="expirationDateDATE">Datum expirace [persistentní položka].</param>
        /// <param name="serialNumber">Sér. číslo [persistentní položka].</param>
        /// <param name="prefixCode">Prefix kódu.</param>
        /// <param name="suffixCode">Sufix kódu.</param>
        /// <param name="bodyCode">Číslo kódu.</param>
        /// <param name="note">Poznámka [persistentní položka].</param>
        /// <param name="specification">Specifikace [persistentní položka].</param>
        /// <param name="comment">Poznámka.</param>
        /// <param name="skipNewCode">Negeneruj nový kód.</param>
        /// <param name="productionDateDATE">Datum a čas výroby [persistentní položka].</param>
        public Storebatch(bool? hidden = default(bool?), string storeCardID = default(string), string name = default(string), DateTimeOffset? expirationDateDATE = default(DateTimeOffset?), bool? serialNumber = default(bool?), string prefixCode = default(string), string suffixCode = default(string), string bodyCode = default(string), string note = default(string), string specification = default(string), string comment = default(string), bool? skipNewCode = default(bool?), DateTimeOffset? productionDateDATE = default(DateTimeOffset?))
        {
            this.Hidden = hidden;
            this.StoreCardID = storeCardID;
            this.Name = name;
            this.ExpirationDateDATE = expirationDateDATE;
            this.SerialNumber = serialNumber;
            this.PrefixCode = prefixCode;
            this.SuffixCode = suffixCode;
            this.BodyCode = bodyCode;
            this.Note = note;
            this.Specification = specification;
            this.Comment = comment;
            this.SkipNewCode = skipNewCode;
            this.ProductionDateDATE = productionDateDATE;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Skrytý [persistentní položka]
        /// </summary>
        /// <value>Skrytý [persistentní položka]</value>
        [DataMember(Name="Hidden", EmitDefaultValue=false)]
        public bool? Hidden { get; set; }

        /// <summary>
        /// Skl. karta; ID objektu Skladová karta [persistentní položka]
        /// </summary>
        /// <value>Skl. karta; ID objektu Skladová karta [persistentní položka]</value>
        [DataMember(Name="StoreCard_ID", EmitDefaultValue=false)]
        public string StoreCardID { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Datum expirace [persistentní položka]
        /// </summary>
        /// <value>Datum expirace [persistentní položka]</value>
        [DataMember(Name="ExpirationDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ExpirationDateDATE { get; set; }

        /// <summary>
        /// Sér. číslo [persistentní položka]
        /// </summary>
        /// <value>Sér. číslo [persistentní položka]</value>
        [DataMember(Name="SerialNumber", EmitDefaultValue=false)]
        public bool? SerialNumber { get; set; }

        /// <summary>
        /// Prefix kódu
        /// </summary>
        /// <value>Prefix kódu</value>
        [DataMember(Name="PrefixCode", EmitDefaultValue=false)]
        public string PrefixCode { get; set; }

        /// <summary>
        /// Sufix kódu
        /// </summary>
        /// <value>Sufix kódu</value>
        [DataMember(Name="SuffixCode", EmitDefaultValue=false)]
        public string SuffixCode { get; set; }

        /// <summary>
        /// Číslo kódu
        /// </summary>
        /// <value>Číslo kódu</value>
        [DataMember(Name="BodyCode", EmitDefaultValue=false)]
        public string BodyCode { get; set; }

        /// <summary>
        /// Poznámka [persistentní položka]
        /// </summary>
        /// <value>Poznámka [persistentní položka]</value>
        [DataMember(Name="Note", EmitDefaultValue=false)]
        public string Note { get; set; }

        /// <summary>
        /// Specifikace [persistentní položka]
        /// </summary>
        /// <value>Specifikace [persistentní položka]</value>
        [DataMember(Name="Specification", EmitDefaultValue=false)]
        public string Specification { get; set; }

        /// <summary>
        /// Poznámka
        /// </summary>
        /// <value>Poznámka</value>
        [DataMember(Name="Comment", EmitDefaultValue=false)]
        public string Comment { get; set; }

        /// <summary>
        /// Negeneruj nový kód
        /// </summary>
        /// <value>Negeneruj nový kód</value>
        [DataMember(Name="SkipNewCode", EmitDefaultValue=false)]
        public bool? SkipNewCode { get; set; }

        /// <summary>
        /// Datum a čas výroby [persistentní položka]
        /// </summary>
        /// <value>Datum a čas výroby [persistentní položka]</value>
        [DataMember(Name="ProductionDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ProductionDateDATE { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Storebatch {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Hidden: ").Append(Hidden).Append("\n");
            sb.Append("  StoreCardID: ").Append(StoreCardID).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  ExpirationDateDATE: ").Append(ExpirationDateDATE).Append("\n");
            sb.Append("  SerialNumber: ").Append(SerialNumber).Append("\n");
            sb.Append("  PrefixCode: ").Append(PrefixCode).Append("\n");
            sb.Append("  SuffixCode: ").Append(SuffixCode).Append("\n");
            sb.Append("  BodyCode: ").Append(BodyCode).Append("\n");
            sb.Append("  Note: ").Append(Note).Append("\n");
            sb.Append("  Specification: ").Append(Specification).Append("\n");
            sb.Append("  Comment: ").Append(Comment).Append("\n");
            sb.Append("  SkipNewCode: ").Append(SkipNewCode).Append("\n");
            sb.Append("  ProductionDateDATE: ").Append(ProductionDateDATE).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Storebatch);
        }

        /// <summary>
        /// Returns true if Storebatch instances are equal
        /// </summary>
        /// <param name="input">Instance of Storebatch to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Storebatch input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Hidden == input.Hidden ||
                    (this.Hidden != null &&
                    this.Hidden.Equals(input.Hidden))
                ) && 
                (
                    this.StoreCardID == input.StoreCardID ||
                    (this.StoreCardID != null &&
                    this.StoreCardID.Equals(input.StoreCardID))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.ExpirationDateDATE == input.ExpirationDateDATE ||
                    (this.ExpirationDateDATE != null &&
                    this.ExpirationDateDATE.Equals(input.ExpirationDateDATE))
                ) && 
                (
                    this.SerialNumber == input.SerialNumber ||
                    (this.SerialNumber != null &&
                    this.SerialNumber.Equals(input.SerialNumber))
                ) && 
                (
                    this.PrefixCode == input.PrefixCode ||
                    (this.PrefixCode != null &&
                    this.PrefixCode.Equals(input.PrefixCode))
                ) && 
                (
                    this.SuffixCode == input.SuffixCode ||
                    (this.SuffixCode != null &&
                    this.SuffixCode.Equals(input.SuffixCode))
                ) && 
                (
                    this.BodyCode == input.BodyCode ||
                    (this.BodyCode != null &&
                    this.BodyCode.Equals(input.BodyCode))
                ) && 
                (
                    this.Note == input.Note ||
                    (this.Note != null &&
                    this.Note.Equals(input.Note))
                ) && 
                (
                    this.Specification == input.Specification ||
                    (this.Specification != null &&
                    this.Specification.Equals(input.Specification))
                ) && 
                (
                    this.Comment == input.Comment ||
                    (this.Comment != null &&
                    this.Comment.Equals(input.Comment))
                ) && 
                (
                    this.SkipNewCode == input.SkipNewCode ||
                    (this.SkipNewCode != null &&
                    this.SkipNewCode.Equals(input.SkipNewCode))
                ) && 
                (
                    this.ProductionDateDATE == input.ProductionDateDATE ||
                    (this.ProductionDateDATE != null &&
                    this.ProductionDateDATE.Equals(input.ProductionDateDATE))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Hidden != null)
                    hashCode = hashCode * 59 + this.Hidden.GetHashCode();
                if (this.StoreCardID != null)
                    hashCode = hashCode * 59 + this.StoreCardID.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.ExpirationDateDATE != null)
                    hashCode = hashCode * 59 + this.ExpirationDateDATE.GetHashCode();
                if (this.SerialNumber != null)
                    hashCode = hashCode * 59 + this.SerialNumber.GetHashCode();
                if (this.PrefixCode != null)
                    hashCode = hashCode * 59 + this.PrefixCode.GetHashCode();
                if (this.SuffixCode != null)
                    hashCode = hashCode * 59 + this.SuffixCode.GetHashCode();
                if (this.BodyCode != null)
                    hashCode = hashCode * 59 + this.BodyCode.GetHashCode();
                if (this.Note != null)
                    hashCode = hashCode * 59 + this.Note.GetHashCode();
                if (this.Specification != null)
                    hashCode = hashCode * 59 + this.Specification.GetHashCode();
                if (this.Comment != null)
                    hashCode = hashCode * 59 + this.Comment.GetHashCode();
                if (this.SkipNewCode != null)
                    hashCode = hashCode * 59 + this.SkipNewCode.GetHashCode();
                if (this.ProductionDateDATE != null)
                    hashCode = hashCode * 59 + this.ProductionDateDATE.GetHashCode();
                return hashCode;
            }
        }
    }

}
