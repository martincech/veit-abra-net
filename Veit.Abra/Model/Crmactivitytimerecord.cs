/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Crmactivitytimerecord
    /// </summary>
    [DataContract]
    public partial class Crmactivitytimerecord :  IEquatable<Crmactivitytimerecord>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Crmactivitytimerecord" /> class.
        /// </summary>
        /// <param name="dateOfRecordDATE">Ve dni [persistentní položka].</param>
        /// <param name="workerID">Pracovník; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="timeOfWork">Strávený čas [persistentní položka].</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="description">Poznámka [persistentní položka].</param>
        /// <param name="startDateDATE">Od [persistentní položka].</param>
        /// <param name="endDateDATE">Do [persistentní položka].</param>
        public Crmactivitytimerecord(DateTimeOffset? dateOfRecordDATE = default(DateTimeOffset?), string workerID = default(string), double? timeOfWork = default(double?), string createdByID = default(string), string correctedByID = default(string), string description = default(string), DateTimeOffset? startDateDATE = default(DateTimeOffset?), DateTimeOffset? endDateDATE = default(DateTimeOffset?))
        {
            this.DateOfRecordDATE = dateOfRecordDATE;
            this.WorkerID = workerID;
            this.TimeOfWork = timeOfWork;
            this.CreatedByID = createdByID;
            this.CorrectedByID = correctedByID;
            this.Description = description;
            this.StartDateDATE = startDateDATE;
            this.EndDateDATE = endDateDATE;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Hlavičkový objekt [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Hlavičkový objekt [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Ve dni [persistentní položka]
        /// </summary>
        /// <value>Ve dni [persistentní položka]</value>
        [DataMember(Name="DateOfRecord$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DateOfRecordDATE { get; set; }

        /// <summary>
        /// Pracovník; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Pracovník; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="Worker_ID", EmitDefaultValue=false)]
        public string WorkerID { get; set; }

        /// <summary>
        /// Strávený čas [persistentní položka]
        /// </summary>
        /// <value>Strávený čas [persistentní položka]</value>
        [DataMember(Name="TimeOfWork", EmitDefaultValue=false)]
        public double? TimeOfWork { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Poznámka [persistentní položka]
        /// </summary>
        /// <value>Poznámka [persistentní položka]</value>
        [DataMember(Name="Description", EmitDefaultValue=false)]
        public string Description { get; set; }

        /// <summary>
        /// Od [persistentní položka]
        /// </summary>
        /// <value>Od [persistentní položka]</value>
        [DataMember(Name="StartDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? StartDateDATE { get; set; }

        /// <summary>
        /// Do [persistentní položka]
        /// </summary>
        /// <value>Do [persistentní položka]</value>
        [DataMember(Name="EndDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? EndDateDATE { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Crmactivitytimerecord {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  DateOfRecordDATE: ").Append(DateOfRecordDATE).Append("\n");
            sb.Append("  WorkerID: ").Append(WorkerID).Append("\n");
            sb.Append("  TimeOfWork: ").Append(TimeOfWork).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("  StartDateDATE: ").Append(StartDateDATE).Append("\n");
            sb.Append("  EndDateDATE: ").Append(EndDateDATE).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Crmactivitytimerecord);
        }

        /// <summary>
        /// Returns true if Crmactivitytimerecord instances are equal
        /// </summary>
        /// <param name="input">Instance of Crmactivitytimerecord to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Crmactivitytimerecord input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.DateOfRecordDATE == input.DateOfRecordDATE ||
                    (this.DateOfRecordDATE != null &&
                    this.DateOfRecordDATE.Equals(input.DateOfRecordDATE))
                ) && 
                (
                    this.WorkerID == input.WorkerID ||
                    (this.WorkerID != null &&
                    this.WorkerID.Equals(input.WorkerID))
                ) && 
                (
                    this.TimeOfWork == input.TimeOfWork ||
                    (this.TimeOfWork != null &&
                    this.TimeOfWork.Equals(input.TimeOfWork))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.Description == input.Description ||
                    (this.Description != null &&
                    this.Description.Equals(input.Description))
                ) && 
                (
                    this.StartDateDATE == input.StartDateDATE ||
                    (this.StartDateDATE != null &&
                    this.StartDateDATE.Equals(input.StartDateDATE))
                ) && 
                (
                    this.EndDateDATE == input.EndDateDATE ||
                    (this.EndDateDATE != null &&
                    this.EndDateDATE.Equals(input.EndDateDATE))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.DateOfRecordDATE != null)
                    hashCode = hashCode * 59 + this.DateOfRecordDATE.GetHashCode();
                if (this.WorkerID != null)
                    hashCode = hashCode * 59 + this.WorkerID.GetHashCode();
                if (this.TimeOfWork != null)
                    hashCode = hashCode * 59 + this.TimeOfWork.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.Description != null)
                    hashCode = hashCode * 59 + this.Description.GetHashCode();
                if (this.StartDateDATE != null)
                    hashCode = hashCode * 59 + this.StartDateDATE.GetHashCode();
                if (this.EndDateDATE != null)
                    hashCode = hashCode * 59 + this.EndDateDATE.GetHashCode();
                return hashCode;
            }
        }
    }

}
