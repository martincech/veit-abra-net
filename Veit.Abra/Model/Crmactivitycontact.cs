/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Crmactivitycontact
    /// </summary>
    [DataContract]
    public partial class Crmactivitycontact :  IEquatable<Crmactivitycontact>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Crmactivitycontact" /> class.
        /// </summary>
        /// <param name="posIndex">Pořadí [persistentní položka].</param>
        /// <param name="firmID">Firma; ID objektu Firma [persistentní položka].</param>
        /// <param name="personID">Osoba; ID objektu Osoba [persistentní položka].</param>
        /// <param name="firmOfficeID">Provozovna; ID objektu Provozovna [persistentní položka].</param>
        /// <param name="note">Poznámka [persistentní položka].</param>
        /// <param name="addressID">Adresa; ID objektu Adresa.</param>
        /// <param name="personAddressID">Adresa osoby; ID objektu Adresa.</param>
        public Crmactivitycontact(int? posIndex = default(int?), string firmID = default(string), string personID = default(string), string firmOfficeID = default(string), string note = default(string), string addressID = default(string), string personAddressID = default(string))
        {
            this.PosIndex = posIndex;
            this.FirmID = firmID;
            this.PersonID = personID;
            this.FirmOfficeID = firmOfficeID;
            this.Note = note;
            this.AddressID = addressID;
            this.PersonAddressID = personAddressID;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Aktivita [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Aktivita [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Pořadí [persistentní položka]
        /// </summary>
        /// <value>Pořadí [persistentní položka]</value>
        [DataMember(Name="PosIndex", EmitDefaultValue=false)]
        public int? PosIndex { get; set; }

        /// <summary>
        /// Firma; ID objektu Firma [persistentní položka]
        /// </summary>
        /// <value>Firma; ID objektu Firma [persistentní položka]</value>
        [DataMember(Name="Firm_ID", EmitDefaultValue=false)]
        public string FirmID { get; set; }

        /// <summary>
        /// Osoba; ID objektu Osoba [persistentní položka]
        /// </summary>
        /// <value>Osoba; ID objektu Osoba [persistentní položka]</value>
        [DataMember(Name="Person_ID", EmitDefaultValue=false)]
        public string PersonID { get; set; }

        /// <summary>
        /// Provozovna; ID objektu Provozovna [persistentní položka]
        /// </summary>
        /// <value>Provozovna; ID objektu Provozovna [persistentní položka]</value>
        [DataMember(Name="FirmOffice_ID", EmitDefaultValue=false)]
        public string FirmOfficeID { get; set; }

        /// <summary>
        /// Poznámka [persistentní položka]
        /// </summary>
        /// <value>Poznámka [persistentní položka]</value>
        [DataMember(Name="Note", EmitDefaultValue=false)]
        public string Note { get; set; }

        /// <summary>
        /// Adresa; ID objektu Adresa
        /// </summary>
        /// <value>Adresa; ID objektu Adresa</value>
        [DataMember(Name="Address_ID", EmitDefaultValue=false)]
        public string AddressID { get; set; }

        /// <summary>
        /// Adresa osoby; ID objektu Adresa
        /// </summary>
        /// <value>Adresa osoby; ID objektu Adresa</value>
        [DataMember(Name="PersonAddress_ID", EmitDefaultValue=false)]
        public string PersonAddressID { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Crmactivitycontact {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  PosIndex: ").Append(PosIndex).Append("\n");
            sb.Append("  FirmID: ").Append(FirmID).Append("\n");
            sb.Append("  PersonID: ").Append(PersonID).Append("\n");
            sb.Append("  FirmOfficeID: ").Append(FirmOfficeID).Append("\n");
            sb.Append("  Note: ").Append(Note).Append("\n");
            sb.Append("  AddressID: ").Append(AddressID).Append("\n");
            sb.Append("  PersonAddressID: ").Append(PersonAddressID).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Crmactivitycontact);
        }

        /// <summary>
        /// Returns true if Crmactivitycontact instances are equal
        /// </summary>
        /// <param name="input">Instance of Crmactivitycontact to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Crmactivitycontact input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.PosIndex == input.PosIndex ||
                    (this.PosIndex != null &&
                    this.PosIndex.Equals(input.PosIndex))
                ) && 
                (
                    this.FirmID == input.FirmID ||
                    (this.FirmID != null &&
                    this.FirmID.Equals(input.FirmID))
                ) && 
                (
                    this.PersonID == input.PersonID ||
                    (this.PersonID != null &&
                    this.PersonID.Equals(input.PersonID))
                ) && 
                (
                    this.FirmOfficeID == input.FirmOfficeID ||
                    (this.FirmOfficeID != null &&
                    this.FirmOfficeID.Equals(input.FirmOfficeID))
                ) && 
                (
                    this.Note == input.Note ||
                    (this.Note != null &&
                    this.Note.Equals(input.Note))
                ) && 
                (
                    this.AddressID == input.AddressID ||
                    (this.AddressID != null &&
                    this.AddressID.Equals(input.AddressID))
                ) && 
                (
                    this.PersonAddressID == input.PersonAddressID ||
                    (this.PersonAddressID != null &&
                    this.PersonAddressID.Equals(input.PersonAddressID))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.PosIndex != null)
                    hashCode = hashCode * 59 + this.PosIndex.GetHashCode();
                if (this.FirmID != null)
                    hashCode = hashCode * 59 + this.FirmID.GetHashCode();
                if (this.PersonID != null)
                    hashCode = hashCode * 59 + this.PersonID.GetHashCode();
                if (this.FirmOfficeID != null)
                    hashCode = hashCode * 59 + this.FirmOfficeID.GetHashCode();
                if (this.Note != null)
                    hashCode = hashCode * 59 + this.Note.GetHashCode();
                if (this.AddressID != null)
                    hashCode = hashCode * 59 + this.AddressID.GetHashCode();
                if (this.PersonAddressID != null)
                    hashCode = hashCode * 59 + this.PersonAddressID.GetHashCode();
                return hashCode;
            }
        }
    }

}
