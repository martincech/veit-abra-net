/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Plmreqcostingroutine
    /// </summary>
    [DataContract]
    public partial class Plmreqcostingroutine :  IEquatable<Plmreqcostingroutine>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Plmreqcostingroutine" /> class.
        /// </summary>
        /// <param name="posIndex">Pořadí [persistentní položka].</param>
        /// <param name="workPlaceID">Pracoviště; ID objektu Pracoviště a stroj [persistentní položka].</param>
        /// <param name="phaseID">Etapa; ID objektu Etapa [persistentní položka].</param>
        /// <param name="partTime">Kusový čas v minutách.</param>
        /// <param name="setupTime">Dávkový čas v minutách.</param>
        /// <param name="title">Název [persistentní položka].</param>
        /// <param name="cooperation">Kooperace [persistentní položka].</param>
        /// <param name="salaryClassID">Tarifní třída; ID objektu Tarifní třída [persistentní položka].</param>
        /// <param name="totalTime">Celkový čas v sekundách [persistentní položka].</param>
        /// <param name="salary">Mzda [persistentní položka].</param>
        /// <param name="overheadCosts">Výrobní režie [persistentní položka].</param>
        /// <param name="generalExpense">Správní režie [persistentní položka].</param>
        /// <param name="totalOverheads">Režie celkem.</param>
        /// <param name="tAC">Dávkový čas v sekundách [persistentní položka].</param>
        /// <param name="tBC">Kusový čas v sekundách [persistentní položka].</param>
        public Plmreqcostingroutine(int? posIndex = default(int?), string workPlaceID = default(string), string phaseID = default(string), double? partTime = default(double?), double? setupTime = default(double?), string title = default(string), bool? cooperation = default(bool?), string salaryClassID = default(string), double? totalTime = default(double?), double? salary = default(double?), double? overheadCosts = default(double?), double? generalExpense = default(double?), double? totalOverheads = default(double?), double? tAC = default(double?), double? tBC = default(double?))
        {
            this.PosIndex = posIndex;
            this.WorkPlaceID = workPlaceID;
            this.PhaseID = phaseID;
            this.PartTime = partTime;
            this.SetupTime = setupTime;
            this.Title = title;
            this.Cooperation = cooperation;
            this.SalaryClassID = salaryClassID;
            this.TotalTime = totalTime;
            this.Salary = salary;
            this.OverheadCosts = overheadCosts;
            this.GeneralExpense = generalExpense;
            this.TotalOverheads = totalOverheads;
            this.TAC = tAC;
            this.TBC = tBC;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Požadavek na výrobu - kalkulace vyráběná položka [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Požadavek na výrobu - kalkulace vyráběná položka [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Pořadí [persistentní položka]
        /// </summary>
        /// <value>Pořadí [persistentní položka]</value>
        [DataMember(Name="PosIndex", EmitDefaultValue=false)]
        public int? PosIndex { get; set; }

        /// <summary>
        /// Pracoviště; ID objektu Pracoviště a stroj [persistentní položka]
        /// </summary>
        /// <value>Pracoviště; ID objektu Pracoviště a stroj [persistentní položka]</value>
        [DataMember(Name="WorkPlace_ID", EmitDefaultValue=false)]
        public string WorkPlaceID { get; set; }

        /// <summary>
        /// Etapa; ID objektu Etapa [persistentní položka]
        /// </summary>
        /// <value>Etapa; ID objektu Etapa [persistentní položka]</value>
        [DataMember(Name="Phase_ID", EmitDefaultValue=false)]
        public string PhaseID { get; set; }

        /// <summary>
        /// Kusový čas v minutách
        /// </summary>
        /// <value>Kusový čas v minutách</value>
        [DataMember(Name="PartTime", EmitDefaultValue=false)]
        public double? PartTime { get; set; }

        /// <summary>
        /// Dávkový čas v minutách
        /// </summary>
        /// <value>Dávkový čas v minutách</value>
        [DataMember(Name="SetupTime", EmitDefaultValue=false)]
        public double? SetupTime { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Title", EmitDefaultValue=false)]
        public string Title { get; set; }

        /// <summary>
        /// Kooperace [persistentní položka]
        /// </summary>
        /// <value>Kooperace [persistentní položka]</value>
        [DataMember(Name="Cooperation", EmitDefaultValue=false)]
        public bool? Cooperation { get; set; }

        /// <summary>
        /// Tarifní třída; ID objektu Tarifní třída [persistentní položka]
        /// </summary>
        /// <value>Tarifní třída; ID objektu Tarifní třída [persistentní položka]</value>
        [DataMember(Name="SalaryClass_ID", EmitDefaultValue=false)]
        public string SalaryClassID { get; set; }

        /// <summary>
        /// Celkový čas v sekundách [persistentní položka]
        /// </summary>
        /// <value>Celkový čas v sekundách [persistentní položka]</value>
        [DataMember(Name="TotalTime", EmitDefaultValue=false)]
        public double? TotalTime { get; set; }

        /// <summary>
        /// Mzda [persistentní položka]
        /// </summary>
        /// <value>Mzda [persistentní položka]</value>
        [DataMember(Name="Salary", EmitDefaultValue=false)]
        public double? Salary { get; set; }

        /// <summary>
        /// Výrobní režie [persistentní položka]
        /// </summary>
        /// <value>Výrobní režie [persistentní položka]</value>
        [DataMember(Name="OverheadCosts", EmitDefaultValue=false)]
        public double? OverheadCosts { get; set; }

        /// <summary>
        /// Správní režie [persistentní položka]
        /// </summary>
        /// <value>Správní režie [persistentní položka]</value>
        [DataMember(Name="GeneralExpense", EmitDefaultValue=false)]
        public double? GeneralExpense { get; set; }

        /// <summary>
        /// Režie celkem
        /// </summary>
        /// <value>Režie celkem</value>
        [DataMember(Name="TotalOverheads", EmitDefaultValue=false)]
        public double? TotalOverheads { get; set; }

        /// <summary>
        /// Dávkový čas v sekundách [persistentní položka]
        /// </summary>
        /// <value>Dávkový čas v sekundách [persistentní položka]</value>
        [DataMember(Name="TAC", EmitDefaultValue=false)]
        public double? TAC { get; set; }

        /// <summary>
        /// Kusový čas v sekundách [persistentní položka]
        /// </summary>
        /// <value>Kusový čas v sekundách [persistentní položka]</value>
        [DataMember(Name="TBC", EmitDefaultValue=false)]
        public double? TBC { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Plmreqcostingroutine {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  PosIndex: ").Append(PosIndex).Append("\n");
            sb.Append("  WorkPlaceID: ").Append(WorkPlaceID).Append("\n");
            sb.Append("  PhaseID: ").Append(PhaseID).Append("\n");
            sb.Append("  PartTime: ").Append(PartTime).Append("\n");
            sb.Append("  SetupTime: ").Append(SetupTime).Append("\n");
            sb.Append("  Title: ").Append(Title).Append("\n");
            sb.Append("  Cooperation: ").Append(Cooperation).Append("\n");
            sb.Append("  SalaryClassID: ").Append(SalaryClassID).Append("\n");
            sb.Append("  TotalTime: ").Append(TotalTime).Append("\n");
            sb.Append("  Salary: ").Append(Salary).Append("\n");
            sb.Append("  OverheadCosts: ").Append(OverheadCosts).Append("\n");
            sb.Append("  GeneralExpense: ").Append(GeneralExpense).Append("\n");
            sb.Append("  TotalOverheads: ").Append(TotalOverheads).Append("\n");
            sb.Append("  TAC: ").Append(TAC).Append("\n");
            sb.Append("  TBC: ").Append(TBC).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Plmreqcostingroutine);
        }

        /// <summary>
        /// Returns true if Plmreqcostingroutine instances are equal
        /// </summary>
        /// <param name="input">Instance of Plmreqcostingroutine to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Plmreqcostingroutine input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.PosIndex == input.PosIndex ||
                    (this.PosIndex != null &&
                    this.PosIndex.Equals(input.PosIndex))
                ) && 
                (
                    this.WorkPlaceID == input.WorkPlaceID ||
                    (this.WorkPlaceID != null &&
                    this.WorkPlaceID.Equals(input.WorkPlaceID))
                ) && 
                (
                    this.PhaseID == input.PhaseID ||
                    (this.PhaseID != null &&
                    this.PhaseID.Equals(input.PhaseID))
                ) && 
                (
                    this.PartTime == input.PartTime ||
                    (this.PartTime != null &&
                    this.PartTime.Equals(input.PartTime))
                ) && 
                (
                    this.SetupTime == input.SetupTime ||
                    (this.SetupTime != null &&
                    this.SetupTime.Equals(input.SetupTime))
                ) && 
                (
                    this.Title == input.Title ||
                    (this.Title != null &&
                    this.Title.Equals(input.Title))
                ) && 
                (
                    this.Cooperation == input.Cooperation ||
                    (this.Cooperation != null &&
                    this.Cooperation.Equals(input.Cooperation))
                ) && 
                (
                    this.SalaryClassID == input.SalaryClassID ||
                    (this.SalaryClassID != null &&
                    this.SalaryClassID.Equals(input.SalaryClassID))
                ) && 
                (
                    this.TotalTime == input.TotalTime ||
                    (this.TotalTime != null &&
                    this.TotalTime.Equals(input.TotalTime))
                ) && 
                (
                    this.Salary == input.Salary ||
                    (this.Salary != null &&
                    this.Salary.Equals(input.Salary))
                ) && 
                (
                    this.OverheadCosts == input.OverheadCosts ||
                    (this.OverheadCosts != null &&
                    this.OverheadCosts.Equals(input.OverheadCosts))
                ) && 
                (
                    this.GeneralExpense == input.GeneralExpense ||
                    (this.GeneralExpense != null &&
                    this.GeneralExpense.Equals(input.GeneralExpense))
                ) && 
                (
                    this.TotalOverheads == input.TotalOverheads ||
                    (this.TotalOverheads != null &&
                    this.TotalOverheads.Equals(input.TotalOverheads))
                ) && 
                (
                    this.TAC == input.TAC ||
                    (this.TAC != null &&
                    this.TAC.Equals(input.TAC))
                ) && 
                (
                    this.TBC == input.TBC ||
                    (this.TBC != null &&
                    this.TBC.Equals(input.TBC))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.PosIndex != null)
                    hashCode = hashCode * 59 + this.PosIndex.GetHashCode();
                if (this.WorkPlaceID != null)
                    hashCode = hashCode * 59 + this.WorkPlaceID.GetHashCode();
                if (this.PhaseID != null)
                    hashCode = hashCode * 59 + this.PhaseID.GetHashCode();
                if (this.PartTime != null)
                    hashCode = hashCode * 59 + this.PartTime.GetHashCode();
                if (this.SetupTime != null)
                    hashCode = hashCode * 59 + this.SetupTime.GetHashCode();
                if (this.Title != null)
                    hashCode = hashCode * 59 + this.Title.GetHashCode();
                if (this.Cooperation != null)
                    hashCode = hashCode * 59 + this.Cooperation.GetHashCode();
                if (this.SalaryClassID != null)
                    hashCode = hashCode * 59 + this.SalaryClassID.GetHashCode();
                if (this.TotalTime != null)
                    hashCode = hashCode * 59 + this.TotalTime.GetHashCode();
                if (this.Salary != null)
                    hashCode = hashCode * 59 + this.Salary.GetHashCode();
                if (this.OverheadCosts != null)
                    hashCode = hashCode * 59 + this.OverheadCosts.GetHashCode();
                if (this.GeneralExpense != null)
                    hashCode = hashCode * 59 + this.GeneralExpense.GetHashCode();
                if (this.TotalOverheads != null)
                    hashCode = hashCode * 59 + this.TotalOverheads.GetHashCode();
                if (this.TAC != null)
                    hashCode = hashCode * 59 + this.TAC.GetHashCode();
                if (this.TBC != null)
                    hashCode = hashCode * 59 + this.TBC.GetHashCode();
                return hashCode;
            }
        }
    }

}
