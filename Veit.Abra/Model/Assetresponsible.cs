/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Assetresponsible
    /// </summary>
    [DataContract]
    public partial class Assetresponsible :  IEquatable<Assetresponsible>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Assetresponsible" /> class.
        /// </summary>
        /// <param name="personID">Odpovědná osoba; ID objektu Osoba [persistentní položka].</param>
        /// <param name="personName">Jméno.</param>
        /// <param name="personalNumber">Osob.číslo.</param>
        public Assetresponsible(string personID = default(string), string personName = default(string), string personalNumber = default(string))
        {
            this.PersonID = personID;
            this.PersonName = personName;
            this.PersonalNumber = personalNumber;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Odpovědná osoba; ID objektu Osoba [persistentní položka]
        /// </summary>
        /// <value>Odpovědná osoba; ID objektu Osoba [persistentní položka]</value>
        [DataMember(Name="Person_ID", EmitDefaultValue=false)]
        public string PersonID { get; set; }

        /// <summary>
        /// Jméno
        /// </summary>
        /// <value>Jméno</value>
        [DataMember(Name="PersonName", EmitDefaultValue=false)]
        public string PersonName { get; set; }

        /// <summary>
        /// Osob.číslo
        /// </summary>
        /// <value>Osob.číslo</value>
        [DataMember(Name="PersonalNumber", EmitDefaultValue=false)]
        public string PersonalNumber { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Assetresponsible {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  PersonID: ").Append(PersonID).Append("\n");
            sb.Append("  PersonName: ").Append(PersonName).Append("\n");
            sb.Append("  PersonalNumber: ").Append(PersonalNumber).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Assetresponsible);
        }

        /// <summary>
        /// Returns true if Assetresponsible instances are equal
        /// </summary>
        /// <param name="input">Instance of Assetresponsible to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Assetresponsible input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.PersonID == input.PersonID ||
                    (this.PersonID != null &&
                    this.PersonID.Equals(input.PersonID))
                ) && 
                (
                    this.PersonName == input.PersonName ||
                    (this.PersonName != null &&
                    this.PersonName.Equals(input.PersonName))
                ) && 
                (
                    this.PersonalNumber == input.PersonalNumber ||
                    (this.PersonalNumber != null &&
                    this.PersonalNumber.Equals(input.PersonalNumber))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.PersonID != null)
                    hashCode = hashCode * 59 + this.PersonID.GetHashCode();
                if (this.PersonName != null)
                    hashCode = hashCode * 59 + this.PersonName.GetHashCode();
                if (this.PersonalNumber != null)
                    hashCode = hashCode * 59 + this.PersonalNumber.GetHashCode();
                return hashCode;
            }
        }
    }

}
