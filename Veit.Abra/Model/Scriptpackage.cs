/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Scriptpackage
    /// </summary>
    [DataContract]
    public partial class Scriptpackage :  IEquatable<Scriptpackage>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Scriptpackage" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Skript [nepersistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="description">Popis [persistentní položka].</param>
        /// <param name="compiled">Zkompilováno [persistentní položka].</param>
        /// <param name="usageState">Stav [persistentní položka].</param>
        /// <param name="runOrder">Pořadí spouštění [persistentní položka].</param>
        /// <param name="withoutSources">Bez zdr.kódu.</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="note">Poznámka [persistentní položka].</param>
        /// <param name="hashPassword">Heslo [persistentní položka].</param>
        /// <param name="cryptForExport">Zakódovat při exportu [persistentní položka].</param>
        /// <param name="compileOnSave">Zkompilovat při uložení.</param>
        public Scriptpackage(List<Scriptpackagerow> rows = default(List<Scriptpackagerow>), string name = default(string), string description = default(string), bool? compiled = default(bool?), int? usageState = default(int?), int? runOrder = default(int?), bool? withoutSources = default(bool?), string createdByID = default(string), string correctedByID = default(string), string note = default(string), string hashPassword = default(string), bool? cryptForExport = default(bool?), bool? compileOnSave = default(bool?))
        {
            this.Rows = rows;
            this.Name = name;
            this.Description = description;
            this.Compiled = compiled;
            this.UsageState = usageState;
            this.RunOrder = runOrder;
            this.WithoutSources = withoutSources;
            this.CreatedByID = createdByID;
            this.CorrectedByID = correctedByID;
            this.Note = note;
            this.HashPassword = hashPassword;
            this.CryptForExport = cryptForExport;
            this.CompileOnSave = compileOnSave;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Skript [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Skript [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Scriptpackagerow> Rows { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Popis [persistentní položka]
        /// </summary>
        /// <value>Popis [persistentní položka]</value>
        [DataMember(Name="Description", EmitDefaultValue=false)]
        public string Description { get; set; }

        /// <summary>
        /// Zkompilováno [persistentní položka]
        /// </summary>
        /// <value>Zkompilováno [persistentní položka]</value>
        [DataMember(Name="Compiled", EmitDefaultValue=false)]
        public bool? Compiled { get; set; }

        /// <summary>
        /// Stav [persistentní položka]
        /// </summary>
        /// <value>Stav [persistentní položka]</value>
        [DataMember(Name="UsageState", EmitDefaultValue=false)]
        public int? UsageState { get; set; }

        /// <summary>
        /// Pořadí spouštění [persistentní položka]
        /// </summary>
        /// <value>Pořadí spouštění [persistentní položka]</value>
        [DataMember(Name="RunOrder", EmitDefaultValue=false)]
        public int? RunOrder { get; set; }

        /// <summary>
        /// Bez zdr.kódu
        /// </summary>
        /// <value>Bez zdr.kódu</value>
        [DataMember(Name="WithoutSources", EmitDefaultValue=false)]
        public bool? WithoutSources { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Poznámka [persistentní položka]
        /// </summary>
        /// <value>Poznámka [persistentní položka]</value>
        [DataMember(Name="Note", EmitDefaultValue=false)]
        public string Note { get; set; }

        /// <summary>
        /// Heslo [persistentní položka]
        /// </summary>
        /// <value>Heslo [persistentní položka]</value>
        [DataMember(Name="HashPassword", EmitDefaultValue=false)]
        public string HashPassword { get; set; }

        /// <summary>
        /// Zakódovat při exportu [persistentní položka]
        /// </summary>
        /// <value>Zakódovat při exportu [persistentní položka]</value>
        [DataMember(Name="CryptForExport", EmitDefaultValue=false)]
        public bool? CryptForExport { get; set; }

        /// <summary>
        /// Zkompilovat při uložení
        /// </summary>
        /// <value>Zkompilovat při uložení</value>
        [DataMember(Name="CompileOnSave", EmitDefaultValue=false)]
        public bool? CompileOnSave { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Scriptpackage {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("  Compiled: ").Append(Compiled).Append("\n");
            sb.Append("  UsageState: ").Append(UsageState).Append("\n");
            sb.Append("  RunOrder: ").Append(RunOrder).Append("\n");
            sb.Append("  WithoutSources: ").Append(WithoutSources).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  Note: ").Append(Note).Append("\n");
            sb.Append("  HashPassword: ").Append(HashPassword).Append("\n");
            sb.Append("  CryptForExport: ").Append(CryptForExport).Append("\n");
            sb.Append("  CompileOnSave: ").Append(CompileOnSave).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Scriptpackage);
        }

        /// <summary>
        /// Returns true if Scriptpackage instances are equal
        /// </summary>
        /// <param name="input">Instance of Scriptpackage to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Scriptpackage input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.Description == input.Description ||
                    (this.Description != null &&
                    this.Description.Equals(input.Description))
                ) && 
                (
                    this.Compiled == input.Compiled ||
                    (this.Compiled != null &&
                    this.Compiled.Equals(input.Compiled))
                ) && 
                (
                    this.UsageState == input.UsageState ||
                    (this.UsageState != null &&
                    this.UsageState.Equals(input.UsageState))
                ) && 
                (
                    this.RunOrder == input.RunOrder ||
                    (this.RunOrder != null &&
                    this.RunOrder.Equals(input.RunOrder))
                ) && 
                (
                    this.WithoutSources == input.WithoutSources ||
                    (this.WithoutSources != null &&
                    this.WithoutSources.Equals(input.WithoutSources))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.Note == input.Note ||
                    (this.Note != null &&
                    this.Note.Equals(input.Note))
                ) && 
                (
                    this.HashPassword == input.HashPassword ||
                    (this.HashPassword != null &&
                    this.HashPassword.Equals(input.HashPassword))
                ) && 
                (
                    this.CryptForExport == input.CryptForExport ||
                    (this.CryptForExport != null &&
                    this.CryptForExport.Equals(input.CryptForExport))
                ) && 
                (
                    this.CompileOnSave == input.CompileOnSave ||
                    (this.CompileOnSave != null &&
                    this.CompileOnSave.Equals(input.CompileOnSave))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.Description != null)
                    hashCode = hashCode * 59 + this.Description.GetHashCode();
                if (this.Compiled != null)
                    hashCode = hashCode * 59 + this.Compiled.GetHashCode();
                if (this.UsageState != null)
                    hashCode = hashCode * 59 + this.UsageState.GetHashCode();
                if (this.RunOrder != null)
                    hashCode = hashCode * 59 + this.RunOrder.GetHashCode();
                if (this.WithoutSources != null)
                    hashCode = hashCode * 59 + this.WithoutSources.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.Note != null)
                    hashCode = hashCode * 59 + this.Note.GetHashCode();
                if (this.HashPassword != null)
                    hashCode = hashCode * 59 + this.HashPassword.GetHashCode();
                if (this.CryptForExport != null)
                    hashCode = hashCode * 59 + this.CryptForExport.GetHashCode();
                if (this.CompileOnSave != null)
                    hashCode = hashCode * 59 + this.CompileOnSave.GetHashCode();
                return hashCode;
            }
        }
    }

}
