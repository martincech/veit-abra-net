/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Report
    /// </summary>
    [DataContract]
    public partial class Report :  IEquatable<Report>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Report" /> class.
        /// </summary>
        /// <param name="title">Název [persistentní položka].</param>
        /// <param name="system">Systémová [persistentní položka].</param>
        /// <param name="dataSource">Datový zdroj [persistentní položka].</param>
        /// <param name="reportID">GUID sestavy [persistentní položka].</param>
        /// <param name="data">Data sestavy [persistentní položka].</param>
        /// <param name="isForm">Je formulář [persistentní položka].</param>
        /// <param name="ownerID">Vlastník; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="visibleFromDATE">Viditelnost od [persistentní položka].</param>
        /// <param name="visibleToDATE">Viditelnost do [persistentní položka].</param>
        /// <param name="hash">Hash [persistentní položka].</param>
        public Report(string title = default(string), bool? system = default(bool?), string dataSource = default(string), string reportID = default(string), byte[] data = default(byte[]), bool? isForm = default(bool?), string ownerID = default(string), string createdByID = default(string), string correctedByID = default(string), DateTimeOffset? visibleFromDATE = default(DateTimeOffset?), DateTimeOffset? visibleToDATE = default(DateTimeOffset?), string hash = default(string))
        {
            this.Title = title;
            this.System = system;
            this.DataSource = dataSource;
            this.ReportID = reportID;
            this.Data = data;
            this.IsForm = isForm;
            this.OwnerID = ownerID;
            this.CreatedByID = createdByID;
            this.CorrectedByID = correctedByID;
            this.VisibleFromDATE = visibleFromDATE;
            this.VisibleToDATE = visibleToDATE;
            this.Hash = hash;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Title", EmitDefaultValue=false)]
        public string Title { get; set; }

        /// <summary>
        /// Systémová [persistentní položka]
        /// </summary>
        /// <value>Systémová [persistentní položka]</value>
        [DataMember(Name="System", EmitDefaultValue=false)]
        public bool? System { get; set; }

        /// <summary>
        /// Datový zdroj [persistentní položka]
        /// </summary>
        /// <value>Datový zdroj [persistentní položka]</value>
        [DataMember(Name="DataSource", EmitDefaultValue=false)]
        public string DataSource { get; set; }

        /// <summary>
        /// GUID sestavy [persistentní položka]
        /// </summary>
        /// <value>GUID sestavy [persistentní položka]</value>
        [DataMember(Name="ReportID", EmitDefaultValue=false)]
        public string ReportID { get; set; }

        /// <summary>
        /// Data sestavy [persistentní položka]
        /// </summary>
        /// <value>Data sestavy [persistentní položka]</value>
        [DataMember(Name="Data", EmitDefaultValue=false)]
        public byte[] Data { get; set; }

        /// <summary>
        /// Je formulář [persistentní položka]
        /// </summary>
        /// <value>Je formulář [persistentní položka]</value>
        [DataMember(Name="IsForm", EmitDefaultValue=false)]
        public bool? IsForm { get; set; }

        /// <summary>
        /// Vlastník; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="Owner_ID", EmitDefaultValue=false)]
        public string OwnerID { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Viditelnost od [persistentní položka]
        /// </summary>
        /// <value>Viditelnost od [persistentní položka]</value>
        [DataMember(Name="VisibleFrom$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? VisibleFromDATE { get; set; }

        /// <summary>
        /// Viditelnost do [persistentní položka]
        /// </summary>
        /// <value>Viditelnost do [persistentní položka]</value>
        [DataMember(Name="VisibleTo$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? VisibleToDATE { get; set; }

        /// <summary>
        /// Hash [persistentní položka]
        /// </summary>
        /// <value>Hash [persistentní položka]</value>
        [DataMember(Name="Hash", EmitDefaultValue=false)]
        public string Hash { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Report {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Title: ").Append(Title).Append("\n");
            sb.Append("  System: ").Append(System).Append("\n");
            sb.Append("  DataSource: ").Append(DataSource).Append("\n");
            sb.Append("  ReportID: ").Append(ReportID).Append("\n");
            sb.Append("  Data: ").Append(Data).Append("\n");
            sb.Append("  IsForm: ").Append(IsForm).Append("\n");
            sb.Append("  OwnerID: ").Append(OwnerID).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  VisibleFromDATE: ").Append(VisibleFromDATE).Append("\n");
            sb.Append("  VisibleToDATE: ").Append(VisibleToDATE).Append("\n");
            sb.Append("  Hash: ").Append(Hash).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Report);
        }

        /// <summary>
        /// Returns true if Report instances are equal
        /// </summary>
        /// <param name="input">Instance of Report to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Report input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Title == input.Title ||
                    (this.Title != null &&
                    this.Title.Equals(input.Title))
                ) && 
                (
                    this.System == input.System ||
                    (this.System != null &&
                    this.System.Equals(input.System))
                ) && 
                (
                    this.DataSource == input.DataSource ||
                    (this.DataSource != null &&
                    this.DataSource.Equals(input.DataSource))
                ) && 
                (
                    this.ReportID == input.ReportID ||
                    (this.ReportID != null &&
                    this.ReportID.Equals(input.ReportID))
                ) && 
                (
                    this.Data == input.Data ||
                    (this.Data != null &&
                    this.Data.Equals(input.Data))
                ) && 
                (
                    this.IsForm == input.IsForm ||
                    (this.IsForm != null &&
                    this.IsForm.Equals(input.IsForm))
                ) && 
                (
                    this.OwnerID == input.OwnerID ||
                    (this.OwnerID != null &&
                    this.OwnerID.Equals(input.OwnerID))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.VisibleFromDATE == input.VisibleFromDATE ||
                    (this.VisibleFromDATE != null &&
                    this.VisibleFromDATE.Equals(input.VisibleFromDATE))
                ) && 
                (
                    this.VisibleToDATE == input.VisibleToDATE ||
                    (this.VisibleToDATE != null &&
                    this.VisibleToDATE.Equals(input.VisibleToDATE))
                ) && 
                (
                    this.Hash == input.Hash ||
                    (this.Hash != null &&
                    this.Hash.Equals(input.Hash))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Title != null)
                    hashCode = hashCode * 59 + this.Title.GetHashCode();
                if (this.System != null)
                    hashCode = hashCode * 59 + this.System.GetHashCode();
                if (this.DataSource != null)
                    hashCode = hashCode * 59 + this.DataSource.GetHashCode();
                if (this.ReportID != null)
                    hashCode = hashCode * 59 + this.ReportID.GetHashCode();
                if (this.Data != null)
                    hashCode = hashCode * 59 + this.Data.GetHashCode();
                if (this.IsForm != null)
                    hashCode = hashCode * 59 + this.IsForm.GetHashCode();
                if (this.OwnerID != null)
                    hashCode = hashCode * 59 + this.OwnerID.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.VisibleFromDATE != null)
                    hashCode = hashCode * 59 + this.VisibleFromDATE.GetHashCode();
                if (this.VisibleToDATE != null)
                    hashCode = hashCode * 59 + this.VisibleToDATE.GetHashCode();
                if (this.Hash != null)
                    hashCode = hashCode * 59 + this.Hash.GetHashCode();
                return hashCode;
            }
        }
    }

}
