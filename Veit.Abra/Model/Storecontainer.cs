/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Storecontainer
    /// </summary>
    [DataContract]
    public partial class Storecontainer :  IEquatable<Storecontainer>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Storecontainer" /> class.
        /// </summary>
        /// <param name="unitQuantity">Množství [persistentní položka].</param>
        /// <param name="storeCardID">Karta obalu; ID objektu Skladová karta [persistentní položka].</param>
        /// <param name="qUnit">Jednotka [persistentní položka].</param>
        /// <param name="unitRate">Vztah.</param>
        public Storecontainer(double? unitQuantity = default(double?), string storeCardID = default(string), string qUnit = default(string), double? unitRate = default(double?))
        {
            this.UnitQuantity = unitQuantity;
            this.StoreCardID = storeCardID;
            this.QUnit = qUnit;
            this.UnitRate = unitRate;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Jednotka skladové karty [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Jednotka skladové karty [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Množství [persistentní položka]
        /// </summary>
        /// <value>Množství [persistentní položka]</value>
        [DataMember(Name="UnitQuantity", EmitDefaultValue=false)]
        public double? UnitQuantity { get; set; }

        /// <summary>
        /// Karta obalu; ID objektu Skladová karta [persistentní položka]
        /// </summary>
        /// <value>Karta obalu; ID objektu Skladová karta [persistentní položka]</value>
        [DataMember(Name="StoreCard_ID", EmitDefaultValue=false)]
        public string StoreCardID { get; set; }

        /// <summary>
        /// Jednotka [persistentní položka]
        /// </summary>
        /// <value>Jednotka [persistentní položka]</value>
        [DataMember(Name="QUnit", EmitDefaultValue=false)]
        public string QUnit { get; set; }

        /// <summary>
        /// Vztah
        /// </summary>
        /// <value>Vztah</value>
        [DataMember(Name="UnitRate", EmitDefaultValue=false)]
        public double? UnitRate { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Storecontainer {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  UnitQuantity: ").Append(UnitQuantity).Append("\n");
            sb.Append("  StoreCardID: ").Append(StoreCardID).Append("\n");
            sb.Append("  QUnit: ").Append(QUnit).Append("\n");
            sb.Append("  UnitRate: ").Append(UnitRate).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Storecontainer);
        }

        /// <summary>
        /// Returns true if Storecontainer instances are equal
        /// </summary>
        /// <param name="input">Instance of Storecontainer to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Storecontainer input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.UnitQuantity == input.UnitQuantity ||
                    (this.UnitQuantity != null &&
                    this.UnitQuantity.Equals(input.UnitQuantity))
                ) && 
                (
                    this.StoreCardID == input.StoreCardID ||
                    (this.StoreCardID != null &&
                    this.StoreCardID.Equals(input.StoreCardID))
                ) && 
                (
                    this.QUnit == input.QUnit ||
                    (this.QUnit != null &&
                    this.QUnit.Equals(input.QUnit))
                ) && 
                (
                    this.UnitRate == input.UnitRate ||
                    (this.UnitRate != null &&
                    this.UnitRate.Equals(input.UnitRate))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.UnitQuantity != null)
                    hashCode = hashCode * 59 + this.UnitQuantity.GetHashCode();
                if (this.StoreCardID != null)
                    hashCode = hashCode * 59 + this.StoreCardID.GetHashCode();
                if (this.QUnit != null)
                    hashCode = hashCode * 59 + this.QUnit.GetHashCode();
                if (this.UnitRate != null)
                    hashCode = hashCode * 59 + this.UnitRate.GetHashCode();
                return hashCode;
            }
        }
    }

}
