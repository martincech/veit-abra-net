/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Vatissueddepositcreditnoterow
    /// </summary>
    [DataContract]
    public partial class Vatissueddepositcreditnoterow :  IEquatable<Vatissueddepositcreditnoterow>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vatissueddepositcreditnoterow" /> class.
        /// </summary>
        /// <param name="posIndex">Pořadí [persistentní položka].</param>
        /// <param name="tAmount">Celkem [persistentní položka].</param>
        /// <param name="localTAmount">Celkem lokálně [persistentní položka].</param>
        /// <param name="text">Text [persistentní položka].</param>
        /// <param name="divisionID">Středisko; ID objektu Středisko [persistentní položka].</param>
        /// <param name="busOrderID">Zakázka; ID objektu Zakázka [persistentní položka].</param>
        /// <param name="busTransactionID">Obch.případ; ID objektu Obchodní případ [persistentní položka].</param>
        /// <param name="busProjectID">Projekt; ID objektu Projekt [persistentní položka].</param>
        /// <param name="rowType">Typ [persistentní položka].</param>
        /// <param name="vATRateID">%DPH; ID objektu DPH sazba [persistentní položka].</param>
        /// <param name="vATIndexID">DPHIndex; ID objektu DPH index [persistentní položka].</param>
        /// <param name="tAmountWithoutVAT">Bez daně [persistentní položka].</param>
        /// <param name="toESL">Do ESL [persistentní položka].</param>
        /// <param name="paymentAmount">Částka platby [persistentní položka].</param>
        /// <param name="roundingAmount">Zaok.v nulové sazbě [persistentní položka].</param>
        /// <param name="localRoundingAmount">Zaok.v nulové sazbě lokálně [persistentní položka].</param>
        /// <param name="eSLIndicatorID">Rozlišení typu plnění(ESL); ID objektu Rozlišení typu plnění(ESL) [persistentní položka].</param>
        /// <param name="eSLDateDATE">Datum pro souhrnné hlášení ESL [persistentní položka].</param>
        /// <param name="dRCArticleID">Typ plnění; ID objektu Kód typu plnění [persistentní položka].</param>
        /// <param name="dRCQuantity">Vykazované množství [persistentní položka].</param>
        /// <param name="dRCQUnit">Vykazovaná jednotka [persistentní položka].</param>
        /// <param name="vATMode">Režim DPH [persistentní položka].</param>
        /// <param name="mOSSServiceID">Druh poskytnuté služby MOSS; ID objektu Druhy poskytovaných služeb MOSS [persistentní položka].</param>
        /// <param name="rSourceID">Ř. dobr.dokladu; ID objektu Daňový zálohový list vydaný - řádek [persistentní položka].</param>
        public Vatissueddepositcreditnoterow(int? posIndex = default(int?), double? tAmount = default(double?), double? localTAmount = default(double?), string text = default(string), string divisionID = default(string), string busOrderID = default(string), string busTransactionID = default(string), string busProjectID = default(string), int? rowType = default(int?), string vATRateID = default(string), string vATIndexID = default(string), double? tAmountWithoutVAT = default(double?), bool? toESL = default(bool?), double? paymentAmount = default(double?), double? roundingAmount = default(double?), double? localRoundingAmount = default(double?), string eSLIndicatorID = default(string), DateTimeOffset? eSLDateDATE = default(DateTimeOffset?), string dRCArticleID = default(string), double? dRCQuantity = default(double?), string dRCQUnit = default(string), int? vATMode = default(int?), string mOSSServiceID = default(string), string rSourceID = default(string))
        {
            this.PosIndex = posIndex;
            this.TAmount = tAmount;
            this.LocalTAmount = localTAmount;
            this.Text = text;
            this.DivisionID = divisionID;
            this.BusOrderID = busOrderID;
            this.BusTransactionID = busTransactionID;
            this.BusProjectID = busProjectID;
            this.RowType = rowType;
            this.VATRateID = vATRateID;
            this.VATIndexID = vATIndexID;
            this.TAmountWithoutVAT = tAmountWithoutVAT;
            this.ToESL = toESL;
            this.PaymentAmount = paymentAmount;
            this.RoundingAmount = roundingAmount;
            this.LocalRoundingAmount = localRoundingAmount;
            this.ESLIndicatorID = eSLIndicatorID;
            this.ESLDateDATE = eSLDateDATE;
            this.DRCArticleID = dRCArticleID;
            this.DRCQuantity = dRCQuantity;
            this.DRCQUnit = dRCQUnit;
            this.VATMode = vATMode;
            this.MOSSServiceID = mOSSServiceID;
            this.RSourceID = rSourceID;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Dobropis daň.zál.listu vydaného [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Dobropis daň.zál.listu vydaného [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Pořadí [persistentní položka]
        /// </summary>
        /// <value>Pořadí [persistentní položka]</value>
        [DataMember(Name="PosIndex", EmitDefaultValue=false)]
        public int? PosIndex { get; set; }

        /// <summary>
        /// Celkem [persistentní položka]
        /// </summary>
        /// <value>Celkem [persistentní položka]</value>
        [DataMember(Name="TAmount", EmitDefaultValue=false)]
        public double? TAmount { get; set; }

        /// <summary>
        /// Celkem lokálně [persistentní položka]
        /// </summary>
        /// <value>Celkem lokálně [persistentní položka]</value>
        [DataMember(Name="LocalTAmount", EmitDefaultValue=false)]
        public double? LocalTAmount { get; set; }

        /// <summary>
        /// Text [persistentní položka]
        /// </summary>
        /// <value>Text [persistentní položka]</value>
        [DataMember(Name="Text", EmitDefaultValue=false)]
        public string Text { get; set; }

        /// <summary>
        /// Středisko; ID objektu Středisko [persistentní položka]
        /// </summary>
        /// <value>Středisko; ID objektu Středisko [persistentní položka]</value>
        [DataMember(Name="Division_ID", EmitDefaultValue=false)]
        public string DivisionID { get; set; }

        /// <summary>
        /// Zakázka; ID objektu Zakázka [persistentní položka]
        /// </summary>
        /// <value>Zakázka; ID objektu Zakázka [persistentní položka]</value>
        [DataMember(Name="BusOrder_ID", EmitDefaultValue=false)]
        public string BusOrderID { get; set; }

        /// <summary>
        /// Obch.případ; ID objektu Obchodní případ [persistentní položka]
        /// </summary>
        /// <value>Obch.případ; ID objektu Obchodní případ [persistentní položka]</value>
        [DataMember(Name="BusTransaction_ID", EmitDefaultValue=false)]
        public string BusTransactionID { get; set; }

        /// <summary>
        /// Projekt; ID objektu Projekt [persistentní položka]
        /// </summary>
        /// <value>Projekt; ID objektu Projekt [persistentní položka]</value>
        [DataMember(Name="BusProject_ID", EmitDefaultValue=false)]
        public string BusProjectID { get; set; }

        /// <summary>
        /// Typ [persistentní položka]
        /// </summary>
        /// <value>Typ [persistentní položka]</value>
        [DataMember(Name="RowType", EmitDefaultValue=false)]
        public int? RowType { get; set; }

        /// <summary>
        /// %DPH; ID objektu DPH sazba [persistentní položka]
        /// </summary>
        /// <value>%DPH; ID objektu DPH sazba [persistentní položka]</value>
        [DataMember(Name="VATRate_ID", EmitDefaultValue=false)]
        public string VATRateID { get; set; }

        /// <summary>
        /// DPHIndex; ID objektu DPH index [persistentní položka]
        /// </summary>
        /// <value>DPHIndex; ID objektu DPH index [persistentní položka]</value>
        [DataMember(Name="VATIndex_ID", EmitDefaultValue=false)]
        public string VATIndexID { get; set; }

        /// <summary>
        /// %DPH [persistentní položka]
        /// </summary>
        /// <value>%DPH [persistentní položka]</value>
        [DataMember(Name="VATRate", EmitDefaultValue=false)]
        public double? VATRate { get; private set; }

        /// <summary>
        /// Bez daně [persistentní položka]
        /// </summary>
        /// <value>Bez daně [persistentní položka]</value>
        [DataMember(Name="TAmountWithoutVAT", EmitDefaultValue=false)]
        public double? TAmountWithoutVAT { get; set; }

        /// <summary>
        /// Bez daně lokálně [persistentní položka]
        /// </summary>
        /// <value>Bez daně lokálně [persistentní položka]</value>
        [DataMember(Name="LocalTAmountWithoutVAT", EmitDefaultValue=false)]
        public double? LocalTAmountWithoutVAT { get; private set; }

        /// <summary>
        /// Do ESL [persistentní položka]
        /// </summary>
        /// <value>Do ESL [persistentní položka]</value>
        [DataMember(Name="ToESL", EmitDefaultValue=false)]
        public bool? ToESL { get; set; }

        /// <summary>
        /// Do ESL
        /// </summary>
        /// <value>Do ESL</value>
        [DataMember(Name="ESLStatus", EmitDefaultValue=false)]
        public int? ESLStatus { get; private set; }

        /// <summary>
        /// Částka platby [persistentní položka]
        /// </summary>
        /// <value>Částka platby [persistentní položka]</value>
        [DataMember(Name="PaymentAmount", EmitDefaultValue=false)]
        public double? PaymentAmount { get; set; }

        /// <summary>
        /// Zaok.v nulové sazbě [persistentní položka]
        /// </summary>
        /// <value>Zaok.v nulové sazbě [persistentní položka]</value>
        [DataMember(Name="RoundingAmount", EmitDefaultValue=false)]
        public double? RoundingAmount { get; set; }

        /// <summary>
        /// Zaok.v nulové sazbě lokálně [persistentní položka]
        /// </summary>
        /// <value>Zaok.v nulové sazbě lokálně [persistentní položka]</value>
        [DataMember(Name="LocalRoundingAmount", EmitDefaultValue=false)]
        public double? LocalRoundingAmount { get; set; }

        /// <summary>
        /// Rozlišení typu plnění(ESL); ID objektu Rozlišení typu plnění(ESL) [persistentní položka]
        /// </summary>
        /// <value>Rozlišení typu plnění(ESL); ID objektu Rozlišení typu plnění(ESL) [persistentní položka]</value>
        [DataMember(Name="ESLIndicator_ID", EmitDefaultValue=false)]
        public string ESLIndicatorID { get; set; }

        /// <summary>
        /// Datum pro souhrnné hlášení ESL [persistentní položka]
        /// </summary>
        /// <value>Datum pro souhrnné hlášení ESL [persistentní položka]</value>
        [DataMember(Name="ESLDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ESLDateDATE { get; set; }

        /// <summary>
        /// Typ plnění; ID objektu Kód typu plnění [persistentní položka]
        /// </summary>
        /// <value>Typ plnění; ID objektu Kód typu plnění [persistentní položka]</value>
        [DataMember(Name="DRCArticle_ID", EmitDefaultValue=false)]
        public string DRCArticleID { get; set; }

        /// <summary>
        /// Vykazované množství [persistentní položka]
        /// </summary>
        /// <value>Vykazované množství [persistentní položka]</value>
        [DataMember(Name="DRCQuantity", EmitDefaultValue=false)]
        public double? DRCQuantity { get; set; }

        /// <summary>
        /// Vykazovaná jednotka [persistentní položka]
        /// </summary>
        /// <value>Vykazovaná jednotka [persistentní položka]</value>
        [DataMember(Name="DRCQUnit", EmitDefaultValue=false)]
        public string DRCQUnit { get; set; }

        /// <summary>
        /// Režim DPH [persistentní položka]
        /// </summary>
        /// <value>Režim DPH [persistentní položka]</value>
        [DataMember(Name="VATMode", EmitDefaultValue=false)]
        public int? VATMode { get; set; }

        /// <summary>
        /// Druh poskytnuté služby MOSS; ID objektu Druhy poskytovaných služeb MOSS [persistentní položka]
        /// </summary>
        /// <value>Druh poskytnuté služby MOSS; ID objektu Druhy poskytovaných služeb MOSS [persistentní položka]</value>
        [DataMember(Name="MOSSService_ID", EmitDefaultValue=false)]
        public string MOSSServiceID { get; set; }

        /// <summary>
        /// Ř. dobr.dokladu; ID objektu Daňový zálohový list vydaný - řádek [persistentní položka]
        /// </summary>
        /// <value>Ř. dobr.dokladu; ID objektu Daňový zálohový list vydaný - řádek [persistentní položka]</value>
        [DataMember(Name="RSource_ID", EmitDefaultValue=false)]
        public string RSourceID { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Vatissueddepositcreditnoterow {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  PosIndex: ").Append(PosIndex).Append("\n");
            sb.Append("  TAmount: ").Append(TAmount).Append("\n");
            sb.Append("  LocalTAmount: ").Append(LocalTAmount).Append("\n");
            sb.Append("  Text: ").Append(Text).Append("\n");
            sb.Append("  DivisionID: ").Append(DivisionID).Append("\n");
            sb.Append("  BusOrderID: ").Append(BusOrderID).Append("\n");
            sb.Append("  BusTransactionID: ").Append(BusTransactionID).Append("\n");
            sb.Append("  BusProjectID: ").Append(BusProjectID).Append("\n");
            sb.Append("  RowType: ").Append(RowType).Append("\n");
            sb.Append("  VATRateID: ").Append(VATRateID).Append("\n");
            sb.Append("  VATIndexID: ").Append(VATIndexID).Append("\n");
            sb.Append("  VATRate: ").Append(VATRate).Append("\n");
            sb.Append("  TAmountWithoutVAT: ").Append(TAmountWithoutVAT).Append("\n");
            sb.Append("  LocalTAmountWithoutVAT: ").Append(LocalTAmountWithoutVAT).Append("\n");
            sb.Append("  ToESL: ").Append(ToESL).Append("\n");
            sb.Append("  ESLStatus: ").Append(ESLStatus).Append("\n");
            sb.Append("  PaymentAmount: ").Append(PaymentAmount).Append("\n");
            sb.Append("  RoundingAmount: ").Append(RoundingAmount).Append("\n");
            sb.Append("  LocalRoundingAmount: ").Append(LocalRoundingAmount).Append("\n");
            sb.Append("  ESLIndicatorID: ").Append(ESLIndicatorID).Append("\n");
            sb.Append("  ESLDateDATE: ").Append(ESLDateDATE).Append("\n");
            sb.Append("  DRCArticleID: ").Append(DRCArticleID).Append("\n");
            sb.Append("  DRCQuantity: ").Append(DRCQuantity).Append("\n");
            sb.Append("  DRCQUnit: ").Append(DRCQUnit).Append("\n");
            sb.Append("  VATMode: ").Append(VATMode).Append("\n");
            sb.Append("  MOSSServiceID: ").Append(MOSSServiceID).Append("\n");
            sb.Append("  RSourceID: ").Append(RSourceID).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Vatissueddepositcreditnoterow);
        }

        /// <summary>
        /// Returns true if Vatissueddepositcreditnoterow instances are equal
        /// </summary>
        /// <param name="input">Instance of Vatissueddepositcreditnoterow to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Vatissueddepositcreditnoterow input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.PosIndex == input.PosIndex ||
                    (this.PosIndex != null &&
                    this.PosIndex.Equals(input.PosIndex))
                ) && 
                (
                    this.TAmount == input.TAmount ||
                    (this.TAmount != null &&
                    this.TAmount.Equals(input.TAmount))
                ) && 
                (
                    this.LocalTAmount == input.LocalTAmount ||
                    (this.LocalTAmount != null &&
                    this.LocalTAmount.Equals(input.LocalTAmount))
                ) && 
                (
                    this.Text == input.Text ||
                    (this.Text != null &&
                    this.Text.Equals(input.Text))
                ) && 
                (
                    this.DivisionID == input.DivisionID ||
                    (this.DivisionID != null &&
                    this.DivisionID.Equals(input.DivisionID))
                ) && 
                (
                    this.BusOrderID == input.BusOrderID ||
                    (this.BusOrderID != null &&
                    this.BusOrderID.Equals(input.BusOrderID))
                ) && 
                (
                    this.BusTransactionID == input.BusTransactionID ||
                    (this.BusTransactionID != null &&
                    this.BusTransactionID.Equals(input.BusTransactionID))
                ) && 
                (
                    this.BusProjectID == input.BusProjectID ||
                    (this.BusProjectID != null &&
                    this.BusProjectID.Equals(input.BusProjectID))
                ) && 
                (
                    this.RowType == input.RowType ||
                    (this.RowType != null &&
                    this.RowType.Equals(input.RowType))
                ) && 
                (
                    this.VATRateID == input.VATRateID ||
                    (this.VATRateID != null &&
                    this.VATRateID.Equals(input.VATRateID))
                ) && 
                (
                    this.VATIndexID == input.VATIndexID ||
                    (this.VATIndexID != null &&
                    this.VATIndexID.Equals(input.VATIndexID))
                ) && 
                (
                    this.VATRate == input.VATRate ||
                    (this.VATRate != null &&
                    this.VATRate.Equals(input.VATRate))
                ) && 
                (
                    this.TAmountWithoutVAT == input.TAmountWithoutVAT ||
                    (this.TAmountWithoutVAT != null &&
                    this.TAmountWithoutVAT.Equals(input.TAmountWithoutVAT))
                ) && 
                (
                    this.LocalTAmountWithoutVAT == input.LocalTAmountWithoutVAT ||
                    (this.LocalTAmountWithoutVAT != null &&
                    this.LocalTAmountWithoutVAT.Equals(input.LocalTAmountWithoutVAT))
                ) && 
                (
                    this.ToESL == input.ToESL ||
                    (this.ToESL != null &&
                    this.ToESL.Equals(input.ToESL))
                ) && 
                (
                    this.ESLStatus == input.ESLStatus ||
                    (this.ESLStatus != null &&
                    this.ESLStatus.Equals(input.ESLStatus))
                ) && 
                (
                    this.PaymentAmount == input.PaymentAmount ||
                    (this.PaymentAmount != null &&
                    this.PaymentAmount.Equals(input.PaymentAmount))
                ) && 
                (
                    this.RoundingAmount == input.RoundingAmount ||
                    (this.RoundingAmount != null &&
                    this.RoundingAmount.Equals(input.RoundingAmount))
                ) && 
                (
                    this.LocalRoundingAmount == input.LocalRoundingAmount ||
                    (this.LocalRoundingAmount != null &&
                    this.LocalRoundingAmount.Equals(input.LocalRoundingAmount))
                ) && 
                (
                    this.ESLIndicatorID == input.ESLIndicatorID ||
                    (this.ESLIndicatorID != null &&
                    this.ESLIndicatorID.Equals(input.ESLIndicatorID))
                ) && 
                (
                    this.ESLDateDATE == input.ESLDateDATE ||
                    (this.ESLDateDATE != null &&
                    this.ESLDateDATE.Equals(input.ESLDateDATE))
                ) && 
                (
                    this.DRCArticleID == input.DRCArticleID ||
                    (this.DRCArticleID != null &&
                    this.DRCArticleID.Equals(input.DRCArticleID))
                ) && 
                (
                    this.DRCQuantity == input.DRCQuantity ||
                    (this.DRCQuantity != null &&
                    this.DRCQuantity.Equals(input.DRCQuantity))
                ) && 
                (
                    this.DRCQUnit == input.DRCQUnit ||
                    (this.DRCQUnit != null &&
                    this.DRCQUnit.Equals(input.DRCQUnit))
                ) && 
                (
                    this.VATMode == input.VATMode ||
                    (this.VATMode != null &&
                    this.VATMode.Equals(input.VATMode))
                ) && 
                (
                    this.MOSSServiceID == input.MOSSServiceID ||
                    (this.MOSSServiceID != null &&
                    this.MOSSServiceID.Equals(input.MOSSServiceID))
                ) && 
                (
                    this.RSourceID == input.RSourceID ||
                    (this.RSourceID != null &&
                    this.RSourceID.Equals(input.RSourceID))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.PosIndex != null)
                    hashCode = hashCode * 59 + this.PosIndex.GetHashCode();
                if (this.TAmount != null)
                    hashCode = hashCode * 59 + this.TAmount.GetHashCode();
                if (this.LocalTAmount != null)
                    hashCode = hashCode * 59 + this.LocalTAmount.GetHashCode();
                if (this.Text != null)
                    hashCode = hashCode * 59 + this.Text.GetHashCode();
                if (this.DivisionID != null)
                    hashCode = hashCode * 59 + this.DivisionID.GetHashCode();
                if (this.BusOrderID != null)
                    hashCode = hashCode * 59 + this.BusOrderID.GetHashCode();
                if (this.BusTransactionID != null)
                    hashCode = hashCode * 59 + this.BusTransactionID.GetHashCode();
                if (this.BusProjectID != null)
                    hashCode = hashCode * 59 + this.BusProjectID.GetHashCode();
                if (this.RowType != null)
                    hashCode = hashCode * 59 + this.RowType.GetHashCode();
                if (this.VATRateID != null)
                    hashCode = hashCode * 59 + this.VATRateID.GetHashCode();
                if (this.VATIndexID != null)
                    hashCode = hashCode * 59 + this.VATIndexID.GetHashCode();
                if (this.VATRate != null)
                    hashCode = hashCode * 59 + this.VATRate.GetHashCode();
                if (this.TAmountWithoutVAT != null)
                    hashCode = hashCode * 59 + this.TAmountWithoutVAT.GetHashCode();
                if (this.LocalTAmountWithoutVAT != null)
                    hashCode = hashCode * 59 + this.LocalTAmountWithoutVAT.GetHashCode();
                if (this.ToESL != null)
                    hashCode = hashCode * 59 + this.ToESL.GetHashCode();
                if (this.ESLStatus != null)
                    hashCode = hashCode * 59 + this.ESLStatus.GetHashCode();
                if (this.PaymentAmount != null)
                    hashCode = hashCode * 59 + this.PaymentAmount.GetHashCode();
                if (this.RoundingAmount != null)
                    hashCode = hashCode * 59 + this.RoundingAmount.GetHashCode();
                if (this.LocalRoundingAmount != null)
                    hashCode = hashCode * 59 + this.LocalRoundingAmount.GetHashCode();
                if (this.ESLIndicatorID != null)
                    hashCode = hashCode * 59 + this.ESLIndicatorID.GetHashCode();
                if (this.ESLDateDATE != null)
                    hashCode = hashCode * 59 + this.ESLDateDATE.GetHashCode();
                if (this.DRCArticleID != null)
                    hashCode = hashCode * 59 + this.DRCArticleID.GetHashCode();
                if (this.DRCQuantity != null)
                    hashCode = hashCode * 59 + this.DRCQuantity.GetHashCode();
                if (this.DRCQUnit != null)
                    hashCode = hashCode * 59 + this.DRCQUnit.GetHashCode();
                if (this.VATMode != null)
                    hashCode = hashCode * 59 + this.VATMode.GetHashCode();
                if (this.MOSSServiceID != null)
                    hashCode = hashCode * 59 + this.MOSSServiceID.GetHashCode();
                if (this.RSourceID != null)
                    hashCode = hashCode * 59 + this.RSourceID.GetHashCode();
                return hashCode;
            }
        }
    }

}
