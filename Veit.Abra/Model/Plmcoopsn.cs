/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Plmcoopsn
    /// </summary>
    [DataContract]
    public partial class Plmcoopsn :  IEquatable<Plmcoopsn>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Plmcoopsn" /> class.
        /// </summary>
        /// <param name="sNID">Sér.č./šarže; ID objektu VP - sériové číslo [persistentní položka].</param>
        /// <param name="returnedAtDATE">Přivezeno zpět [persistentní položka].</param>
        /// <param name="quantity">Množství do kooperace v ev.jedn. [persistentní položka].</param>
        /// <param name="qUnit">Jednotka [persistentní položka].</param>
        /// <param name="unitRate">Vztah [persistentní položka].</param>
        /// <param name="unitQuantity">Množství do kooperace.</param>
        /// <param name="returnedQuantity">Množství z kooperace v ev.jedn. [persistentní položka].</param>
        /// <param name="returnedUnitQuantity">Množství z kooperace.</param>
        public Plmcoopsn(string sNID = default(string), DateTimeOffset? returnedAtDATE = default(DateTimeOffset?), double? quantity = default(double?), string qUnit = default(string), double? unitRate = default(double?), double? unitQuantity = default(double?), double? returnedQuantity = default(double?), double? returnedUnitQuantity = default(double?))
        {
            this.SN_ID = sNID;
            this.ReturnedAtDATE = returnedAtDATE;
            this.Quantity = quantity;
            this.QUnit = qUnit;
            this.UnitRate = unitRate;
            this.UnitQuantity = unitQuantity;
            this.ReturnedQuantity = returnedQuantity;
            this.ReturnedUnitQuantity = returnedUnitQuantity;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Kooperace - vyráběná položka [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Kooperace - vyráběná položka [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Sér.č./šarže; ID objektu VP - sériové číslo [persistentní položka]
        /// </summary>
        /// <value>Sér.č./šarže; ID objektu VP - sériové číslo [persistentní položka]</value>
        [DataMember(Name="SN_ID", EmitDefaultValue=false)]
        public string SN_ID { get; set; }

        /// <summary>
        /// Přivezeno zpět [persistentní položka]
        /// </summary>
        /// <value>Přivezeno zpět [persistentní položka]</value>
        [DataMember(Name="ReturnedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ReturnedAtDATE { get; set; }

        /// <summary>
        /// Množství do kooperace v ev.jedn. [persistentní položka]
        /// </summary>
        /// <value>Množství do kooperace v ev.jedn. [persistentní položka]</value>
        [DataMember(Name="Quantity", EmitDefaultValue=false)]
        public double? Quantity { get; set; }

        /// <summary>
        /// Jednotka [persistentní položka]
        /// </summary>
        /// <value>Jednotka [persistentní položka]</value>
        [DataMember(Name="QUnit", EmitDefaultValue=false)]
        public string QUnit { get; set; }

        /// <summary>
        /// Vztah [persistentní položka]
        /// </summary>
        /// <value>Vztah [persistentní položka]</value>
        [DataMember(Name="UnitRate", EmitDefaultValue=false)]
        public double? UnitRate { get; set; }

        /// <summary>
        /// Množství do kooperace
        /// </summary>
        /// <value>Množství do kooperace</value>
        [DataMember(Name="UnitQuantity", EmitDefaultValue=false)]
        public double? UnitQuantity { get; set; }

        /// <summary>
        /// Množství z kooperace v ev.jedn. [persistentní položka]
        /// </summary>
        /// <value>Množství z kooperace v ev.jedn. [persistentní položka]</value>
        [DataMember(Name="ReturnedQuantity", EmitDefaultValue=false)]
        public double? ReturnedQuantity { get; set; }

        /// <summary>
        /// Množství z kooperace
        /// </summary>
        /// <value>Množství z kooperace</value>
        [DataMember(Name="ReturnedUnitQuantity", EmitDefaultValue=false)]
        public double? ReturnedUnitQuantity { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Plmcoopsn {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  SN_ID: ").Append(SN_ID).Append("\n");
            sb.Append("  ReturnedAtDATE: ").Append(ReturnedAtDATE).Append("\n");
            sb.Append("  Quantity: ").Append(Quantity).Append("\n");
            sb.Append("  QUnit: ").Append(QUnit).Append("\n");
            sb.Append("  UnitRate: ").Append(UnitRate).Append("\n");
            sb.Append("  UnitQuantity: ").Append(UnitQuantity).Append("\n");
            sb.Append("  ReturnedQuantity: ").Append(ReturnedQuantity).Append("\n");
            sb.Append("  ReturnedUnitQuantity: ").Append(ReturnedUnitQuantity).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Plmcoopsn);
        }

        /// <summary>
        /// Returns true if Plmcoopsn instances are equal
        /// </summary>
        /// <param name="input">Instance of Plmcoopsn to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Plmcoopsn input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.SN_ID == input.SN_ID ||
                    (this.SN_ID != null &&
                    this.SN_ID.Equals(input.SN_ID))
                ) && 
                (
                    this.ReturnedAtDATE == input.ReturnedAtDATE ||
                    (this.ReturnedAtDATE != null &&
                    this.ReturnedAtDATE.Equals(input.ReturnedAtDATE))
                ) && 
                (
                    this.Quantity == input.Quantity ||
                    (this.Quantity != null &&
                    this.Quantity.Equals(input.Quantity))
                ) && 
                (
                    this.QUnit == input.QUnit ||
                    (this.QUnit != null &&
                    this.QUnit.Equals(input.QUnit))
                ) && 
                (
                    this.UnitRate == input.UnitRate ||
                    (this.UnitRate != null &&
                    this.UnitRate.Equals(input.UnitRate))
                ) && 
                (
                    this.UnitQuantity == input.UnitQuantity ||
                    (this.UnitQuantity != null &&
                    this.UnitQuantity.Equals(input.UnitQuantity))
                ) && 
                (
                    this.ReturnedQuantity == input.ReturnedQuantity ||
                    (this.ReturnedQuantity != null &&
                    this.ReturnedQuantity.Equals(input.ReturnedQuantity))
                ) && 
                (
                    this.ReturnedUnitQuantity == input.ReturnedUnitQuantity ||
                    (this.ReturnedUnitQuantity != null &&
                    this.ReturnedUnitQuantity.Equals(input.ReturnedUnitQuantity))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.SN_ID != null)
                    hashCode = hashCode * 59 + this.SN_ID.GetHashCode();
                if (this.ReturnedAtDATE != null)
                    hashCode = hashCode * 59 + this.ReturnedAtDATE.GetHashCode();
                if (this.Quantity != null)
                    hashCode = hashCode * 59 + this.Quantity.GetHashCode();
                if (this.QUnit != null)
                    hashCode = hashCode * 59 + this.QUnit.GetHashCode();
                if (this.UnitRate != null)
                    hashCode = hashCode * 59 + this.UnitRate.GetHashCode();
                if (this.UnitQuantity != null)
                    hashCode = hashCode * 59 + this.UnitQuantity.GetHashCode();
                if (this.ReturnedQuantity != null)
                    hashCode = hashCode * 59 + this.ReturnedQuantity.GetHashCode();
                if (this.ReturnedUnitQuantity != null)
                    hashCode = hashCode * 59 + this.ReturnedUnitQuantity.GetHashCode();
                return hashCode;
            }
        }
    }

}
