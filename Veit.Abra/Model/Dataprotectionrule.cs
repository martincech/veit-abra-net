/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Dataprotectionrule
    /// </summary>
    [DataContract]
    public partial class Dataprotectionrule :  IEquatable<Dataprotectionrule>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Dataprotectionrule" /> class.
        /// </summary>
        /// <param name="hidden">Skrytý [persistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="note">Poznámka [persistentní položka].</param>
        /// <param name="definitionID">Definice; ID objektu Definice ochrany dat [persistentní položka].</param>
        /// <param name="area">Oblast [persistentní položka].</param>
        /// <param name="_params">Parametry [persistentní položka].</param>
        /// <param name="validMonths">Platnost (měsíců) [persistentní položka].</param>
        /// <param name="ruleType">Typ pravidla [persistentní položka].</param>
        /// <param name="code">Kód [persistentní položka].</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="createdAtDATE">Vytvořeno [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedAtDATE">Opraveno [persistentní položka].</param>
        public Dataprotectionrule(bool? hidden = default(bool?), string name = default(string), string note = default(string), string definitionID = default(string), int? area = default(int?), string _params = default(string), int? validMonths = default(int?), int? ruleType = default(int?), string code = default(string), string createdByID = default(string), DateTimeOffset? createdAtDATE = default(DateTimeOffset?), string correctedByID = default(string), DateTimeOffset? correctedAtDATE = default(DateTimeOffset?))
        {
            this.Hidden = hidden;
            this.Name = name;
            this.Note = note;
            this.DefinitionID = definitionID;
            this.Area = area;
            this.Params = _params;
            this.ValidMonths = validMonths;
            this.RuleType = ruleType;
            this.Code = code;
            this.CreatedByID = createdByID;
            this.CreatedAtDATE = createdAtDATE;
            this.CorrectedByID = correctedByID;
            this.CorrectedAtDATE = correctedAtDATE;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Skrytý [persistentní položka]
        /// </summary>
        /// <value>Skrytý [persistentní položka]</value>
        [DataMember(Name="Hidden", EmitDefaultValue=false)]
        public bool? Hidden { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Poznámka [persistentní položka]
        /// </summary>
        /// <value>Poznámka [persistentní položka]</value>
        [DataMember(Name="Note", EmitDefaultValue=false)]
        public string Note { get; set; }

        /// <summary>
        /// Definice; ID objektu Definice ochrany dat [persistentní položka]
        /// </summary>
        /// <value>Definice; ID objektu Definice ochrany dat [persistentní položka]</value>
        [DataMember(Name="Definition_ID", EmitDefaultValue=false)]
        public string DefinitionID { get; set; }

        /// <summary>
        /// Oblast [persistentní položka]
        /// </summary>
        /// <value>Oblast [persistentní položka]</value>
        [DataMember(Name="Area", EmitDefaultValue=false)]
        public int? Area { get; set; }

        /// <summary>
        /// Oblast
        /// </summary>
        /// <value>Oblast</value>
        [DataMember(Name="AreaAsText", EmitDefaultValue=false)]
        public string AreaAsText { get; private set; }

        /// <summary>
        /// Parametry [persistentní položka]
        /// </summary>
        /// <value>Parametry [persistentní položka]</value>
        [DataMember(Name="Params", EmitDefaultValue=false)]
        public string Params { get; set; }

        /// <summary>
        /// Platnost (měsíců) [persistentní položka]
        /// </summary>
        /// <value>Platnost (měsíců) [persistentní položka]</value>
        [DataMember(Name="ValidMonths", EmitDefaultValue=false)]
        public int? ValidMonths { get; set; }

        /// <summary>
        /// Typ pravidla [persistentní položka]
        /// </summary>
        /// <value>Typ pravidla [persistentní položka]</value>
        [DataMember(Name="RuleType", EmitDefaultValue=false)]
        public int? RuleType { get; set; }

        /// <summary>
        /// Typ pravidla
        /// </summary>
        /// <value>Typ pravidla</value>
        [DataMember(Name="RuleTypeAsText", EmitDefaultValue=false)]
        public string RuleTypeAsText { get; private set; }

        /// <summary>
        /// Kód [persistentní položka]
        /// </summary>
        /// <value>Kód [persistentní položka]</value>
        [DataMember(Name="Code", EmitDefaultValue=false)]
        public string Code { get; set; }

        /// <summary>
        /// Jedinečný identifikátor [persistentní položka]
        /// </summary>
        /// <value>Jedinečný identifikátor [persistentní položka]</value>
        [DataMember(Name="UniqueID", EmitDefaultValue=false)]
        public string UniqueID { get; private set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Vytvořeno [persistentní položka]
        /// </summary>
        /// <value>Vytvořeno [persistentní položka]</value>
        [DataMember(Name="CreatedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CreatedAtDATE { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Opraveno [persistentní položka]
        /// </summary>
        /// <value>Opraveno [persistentní položka]</value>
        [DataMember(Name="CorrectedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CorrectedAtDATE { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Dataprotectionrule {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Hidden: ").Append(Hidden).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Note: ").Append(Note).Append("\n");
            sb.Append("  DefinitionID: ").Append(DefinitionID).Append("\n");
            sb.Append("  Area: ").Append(Area).Append("\n");
            sb.Append("  AreaAsText: ").Append(AreaAsText).Append("\n");
            sb.Append("  Params: ").Append(Params).Append("\n");
            sb.Append("  ValidMonths: ").Append(ValidMonths).Append("\n");
            sb.Append("  RuleType: ").Append(RuleType).Append("\n");
            sb.Append("  RuleTypeAsText: ").Append(RuleTypeAsText).Append("\n");
            sb.Append("  Code: ").Append(Code).Append("\n");
            sb.Append("  UniqueID: ").Append(UniqueID).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CreatedAtDATE: ").Append(CreatedAtDATE).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  CorrectedAtDATE: ").Append(CorrectedAtDATE).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Dataprotectionrule);
        }

        /// <summary>
        /// Returns true if Dataprotectionrule instances are equal
        /// </summary>
        /// <param name="input">Instance of Dataprotectionrule to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Dataprotectionrule input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Hidden == input.Hidden ||
                    (this.Hidden != null &&
                    this.Hidden.Equals(input.Hidden))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.Note == input.Note ||
                    (this.Note != null &&
                    this.Note.Equals(input.Note))
                ) && 
                (
                    this.DefinitionID == input.DefinitionID ||
                    (this.DefinitionID != null &&
                    this.DefinitionID.Equals(input.DefinitionID))
                ) && 
                (
                    this.Area == input.Area ||
                    (this.Area != null &&
                    this.Area.Equals(input.Area))
                ) && 
                (
                    this.AreaAsText == input.AreaAsText ||
                    (this.AreaAsText != null &&
                    this.AreaAsText.Equals(input.AreaAsText))
                ) && 
                (
                    this.Params == input.Params ||
                    (this.Params != null &&
                    this.Params.Equals(input.Params))
                ) && 
                (
                    this.ValidMonths == input.ValidMonths ||
                    (this.ValidMonths != null &&
                    this.ValidMonths.Equals(input.ValidMonths))
                ) && 
                (
                    this.RuleType == input.RuleType ||
                    (this.RuleType != null &&
                    this.RuleType.Equals(input.RuleType))
                ) && 
                (
                    this.RuleTypeAsText == input.RuleTypeAsText ||
                    (this.RuleTypeAsText != null &&
                    this.RuleTypeAsText.Equals(input.RuleTypeAsText))
                ) && 
                (
                    this.Code == input.Code ||
                    (this.Code != null &&
                    this.Code.Equals(input.Code))
                ) && 
                (
                    this.UniqueID == input.UniqueID ||
                    (this.UniqueID != null &&
                    this.UniqueID.Equals(input.UniqueID))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CreatedAtDATE == input.CreatedAtDATE ||
                    (this.CreatedAtDATE != null &&
                    this.CreatedAtDATE.Equals(input.CreatedAtDATE))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.CorrectedAtDATE == input.CorrectedAtDATE ||
                    (this.CorrectedAtDATE != null &&
                    this.CorrectedAtDATE.Equals(input.CorrectedAtDATE))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Hidden != null)
                    hashCode = hashCode * 59 + this.Hidden.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.Note != null)
                    hashCode = hashCode * 59 + this.Note.GetHashCode();
                if (this.DefinitionID != null)
                    hashCode = hashCode * 59 + this.DefinitionID.GetHashCode();
                if (this.Area != null)
                    hashCode = hashCode * 59 + this.Area.GetHashCode();
                if (this.AreaAsText != null)
                    hashCode = hashCode * 59 + this.AreaAsText.GetHashCode();
                if (this.Params != null)
                    hashCode = hashCode * 59 + this.Params.GetHashCode();
                if (this.ValidMonths != null)
                    hashCode = hashCode * 59 + this.ValidMonths.GetHashCode();
                if (this.RuleType != null)
                    hashCode = hashCode * 59 + this.RuleType.GetHashCode();
                if (this.RuleTypeAsText != null)
                    hashCode = hashCode * 59 + this.RuleTypeAsText.GetHashCode();
                if (this.Code != null)
                    hashCode = hashCode * 59 + this.Code.GetHashCode();
                if (this.UniqueID != null)
                    hashCode = hashCode * 59 + this.UniqueID.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CreatedAtDATE != null)
                    hashCode = hashCode * 59 + this.CreatedAtDATE.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.CorrectedAtDATE != null)
                    hashCode = hashCode * 59 + this.CorrectedAtDATE.GetHashCode();
                return hashCode;
            }
        }
    }

}
