/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Restrictionusagesrow
    /// </summary>
    [DataContract]
    public partial class Restrictionusagesrow :  IEquatable<Restrictionusagesrow>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Restrictionusagesrow" /> class.
        /// </summary>
        /// <param name="conditionID">Podmínka [persistentní položka].</param>
        /// <param name="numberOfUse">Počet použití [persistentní položka].</param>
        public Restrictionusagesrow(string conditionID = default(string), int? numberOfUse = default(int?))
        {
            this.ConditionID = conditionID;
            this.NumberOfUse = numberOfUse;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Statistika používání definic omezení [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Statistika používání definic omezení [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Podmínka [persistentní položka]
        /// </summary>
        /// <value>Podmínka [persistentní položka]</value>
        [DataMember(Name="ConditionID", EmitDefaultValue=false)]
        public string ConditionID { get; set; }

        /// <summary>
        /// Počet použití [persistentní položka]
        /// </summary>
        /// <value>Počet použití [persistentní položka]</value>
        [DataMember(Name="NumberOfUse", EmitDefaultValue=false)]
        public int? NumberOfUse { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Restrictionusagesrow {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  ConditionID: ").Append(ConditionID).Append("\n");
            sb.Append("  NumberOfUse: ").Append(NumberOfUse).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Restrictionusagesrow);
        }

        /// <summary>
        /// Returns true if Restrictionusagesrow instances are equal
        /// </summary>
        /// <param name="input">Instance of Restrictionusagesrow to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Restrictionusagesrow input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.ConditionID == input.ConditionID ||
                    (this.ConditionID != null &&
                    this.ConditionID.Equals(input.ConditionID))
                ) && 
                (
                    this.NumberOfUse == input.NumberOfUse ||
                    (this.NumberOfUse != null &&
                    this.NumberOfUse.Equals(input.NumberOfUse))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.ConditionID != null)
                    hashCode = hashCode * 59 + this.ConditionID.GetHashCode();
                if (this.NumberOfUse != null)
                    hashCode = hashCode * 59 + this.NumberOfUse.GetHashCode();
                return hashCode;
            }
        }
    }

}
