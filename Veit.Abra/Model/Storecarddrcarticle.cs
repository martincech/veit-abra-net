/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Storecarddrcarticle
    /// </summary>
    [DataContract]
    public partial class Storecarddrcarticle :  IEquatable<Storecarddrcarticle>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Storecarddrcarticle" /> class.
        /// </summary>
        /// <param name="countryID">Země; ID objektu Země [persistentní položka].</param>
        /// <param name="dRCVATMode">Režim Přenesení daňové povinnosti [persistentní položka].</param>
        /// <param name="dRCArticleID">Typ plnění - přenesení DP; ID objektu Kód typu plnění [persistentní položka].</param>
        /// <param name="dRCArticleUnitRate">Vztah jednotky typu plnění [persistentní položka].</param>
        /// <param name="dRCArticleUnitRateRef">Ref.vztah jednotky typu plnění [persistentní položka].</param>
        /// <param name="dRCArticleQUnit">Vykazovaná jednotka [persistentní položka].</param>
        public Storecarddrcarticle(string countryID = default(string), bool? dRCVATMode = default(bool?), string dRCArticleID = default(string), double? dRCArticleUnitRate = default(double?), double? dRCArticleUnitRateRef = default(double?), string dRCArticleQUnit = default(string))
        {
            this.CountryID = countryID;
            this.DRCVATMode = dRCVATMode;
            this.DRCArticleID = dRCArticleID;
            this.DRCArticleUnitRate = dRCArticleUnitRate;
            this.DRCArticleUnitRateRef = dRCArticleUnitRateRef;
            this.DRCArticleQUnit = dRCArticleQUnit;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Hlavičkový objekt [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Hlavičkový objekt [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Země; ID objektu Země [persistentní položka]
        /// </summary>
        /// <value>Země; ID objektu Země [persistentní položka]</value>
        [DataMember(Name="Country_ID", EmitDefaultValue=false)]
        public string CountryID { get; set; }

        /// <summary>
        /// Režim Přenesení daňové povinnosti [persistentní položka]
        /// </summary>
        /// <value>Režim Přenesení daňové povinnosti [persistentní položka]</value>
        [DataMember(Name="DRCVATMode", EmitDefaultValue=false)]
        public bool? DRCVATMode { get; set; }

        /// <summary>
        /// Typ plnění - přenesení DP; ID objektu Kód typu plnění [persistentní položka]
        /// </summary>
        /// <value>Typ plnění - přenesení DP; ID objektu Kód typu plnění [persistentní položka]</value>
        [DataMember(Name="DRCArticle_ID", EmitDefaultValue=false)]
        public string DRCArticleID { get; set; }

        /// <summary>
        /// Vztah jednotky typu plnění [persistentní položka]
        /// </summary>
        /// <value>Vztah jednotky typu plnění [persistentní položka]</value>
        [DataMember(Name="DRCArticleUnitRate", EmitDefaultValue=false)]
        public double? DRCArticleUnitRate { get; set; }

        /// <summary>
        /// Ref.vztah jednotky typu plnění [persistentní položka]
        /// </summary>
        /// <value>Ref.vztah jednotky typu plnění [persistentní položka]</value>
        [DataMember(Name="DRCArticleUnitRateRef", EmitDefaultValue=false)]
        public double? DRCArticleUnitRateRef { get; set; }

        /// <summary>
        /// Vykazovaná jednotka [persistentní položka]
        /// </summary>
        /// <value>Vykazovaná jednotka [persistentní položka]</value>
        [DataMember(Name="DRCArticleQUnit", EmitDefaultValue=false)]
        public string DRCArticleQUnit { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Storecarddrcarticle {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  CountryID: ").Append(CountryID).Append("\n");
            sb.Append("  DRCVATMode: ").Append(DRCVATMode).Append("\n");
            sb.Append("  DRCArticleID: ").Append(DRCArticleID).Append("\n");
            sb.Append("  DRCArticleUnitRate: ").Append(DRCArticleUnitRate).Append("\n");
            sb.Append("  DRCArticleUnitRateRef: ").Append(DRCArticleUnitRateRef).Append("\n");
            sb.Append("  DRCArticleQUnit: ").Append(DRCArticleQUnit).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Storecarddrcarticle);
        }

        /// <summary>
        /// Returns true if Storecarddrcarticle instances are equal
        /// </summary>
        /// <param name="input">Instance of Storecarddrcarticle to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Storecarddrcarticle input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.CountryID == input.CountryID ||
                    (this.CountryID != null &&
                    this.CountryID.Equals(input.CountryID))
                ) && 
                (
                    this.DRCVATMode == input.DRCVATMode ||
                    (this.DRCVATMode != null &&
                    this.DRCVATMode.Equals(input.DRCVATMode))
                ) && 
                (
                    this.DRCArticleID == input.DRCArticleID ||
                    (this.DRCArticleID != null &&
                    this.DRCArticleID.Equals(input.DRCArticleID))
                ) && 
                (
                    this.DRCArticleUnitRate == input.DRCArticleUnitRate ||
                    (this.DRCArticleUnitRate != null &&
                    this.DRCArticleUnitRate.Equals(input.DRCArticleUnitRate))
                ) && 
                (
                    this.DRCArticleUnitRateRef == input.DRCArticleUnitRateRef ||
                    (this.DRCArticleUnitRateRef != null &&
                    this.DRCArticleUnitRateRef.Equals(input.DRCArticleUnitRateRef))
                ) && 
                (
                    this.DRCArticleQUnit == input.DRCArticleQUnit ||
                    (this.DRCArticleQUnit != null &&
                    this.DRCArticleQUnit.Equals(input.DRCArticleQUnit))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.CountryID != null)
                    hashCode = hashCode * 59 + this.CountryID.GetHashCode();
                if (this.DRCVATMode != null)
                    hashCode = hashCode * 59 + this.DRCVATMode.GetHashCode();
                if (this.DRCArticleID != null)
                    hashCode = hashCode * 59 + this.DRCArticleID.GetHashCode();
                if (this.DRCArticleUnitRate != null)
                    hashCode = hashCode * 59 + this.DRCArticleUnitRate.GetHashCode();
                if (this.DRCArticleUnitRateRef != null)
                    hashCode = hashCode * 59 + this.DRCArticleUnitRateRef.GetHashCode();
                if (this.DRCArticleQUnit != null)
                    hashCode = hashCode * 59 + this.DRCArticleQUnit.GetHashCode();
                return hashCode;
            }
        }
    }

}
