/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Contract
    /// </summary>
    [DataContract]
    public partial class Contract :  IEquatable<Contract>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Contract" /> class.
        /// </summary>
        /// <param name="docQueueID">Zdrojová řada; ID objektu Řada dokladů [persistentní položka].</param>
        /// <param name="periodID">Období; ID objektu Období [persistentní položka].</param>
        /// <param name="ordNumber">Pořadové číslo [persistentní položka].</param>
        /// <param name="docDateDATE">Datum dok. [persistentní položka].</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="newRelatedType">Typ relace.</param>
        /// <param name="newRelatedDocumentID">ID dokladu pro připojení.</param>
        /// <param name="firmID">Firma; ID objektu Firma [persistentní položka].</param>
        /// <param name="personID">Osoba; ID objektu Osoba [persistentní položka].</param>
        /// <param name="shortDescription">Popis [persistentní položka].</param>
        /// <param name="validFromDATE">Platná od [persistentní položka].</param>
        /// <param name="validToDATE">Platná do [persistentní položka].</param>
        /// <param name="autoDataProcessingPermit">Automatické povolení ke zpracování dat [persistentní položka].</param>
        /// <param name="externalNumber">Externí číslo [persistentní položka].</param>
        /// <param name="contractTypeID">Typ; ID objektu Typ smlouvy [persistentní položka].</param>
        /// <param name="partnerType">Typ žadatele.</param>
        /// <param name="createdAtDATE">Vytvořeno [persistentní položka].</param>
        /// <param name="correctedAtDATE">Opraveno [persistentní položka].</param>
        public Contract(string docQueueID = default(string), string periodID = default(string), int? ordNumber = default(int?), DateTimeOffset? docDateDATE = default(DateTimeOffset?), string createdByID = default(string), string correctedByID = default(string), int? newRelatedType = default(int?), string newRelatedDocumentID = default(string), string firmID = default(string), string personID = default(string), string shortDescription = default(string), DateTimeOffset? validFromDATE = default(DateTimeOffset?), DateTimeOffset? validToDATE = default(DateTimeOffset?), bool? autoDataProcessingPermit = default(bool?), string externalNumber = default(string), string contractTypeID = default(string), int? partnerType = default(int?), DateTimeOffset? createdAtDATE = default(DateTimeOffset?), DateTimeOffset? correctedAtDATE = default(DateTimeOffset?))
        {
            this.DocQueueID = docQueueID;
            this.PeriodID = periodID;
            this.OrdNumber = ordNumber;
            this.DocDateDATE = docDateDATE;
            this.CreatedByID = createdByID;
            this.CorrectedByID = correctedByID;
            this.NewRelatedType = newRelatedType;
            this.NewRelatedDocumentID = newRelatedDocumentID;
            this.FirmID = firmID;
            this.PersonID = personID;
            this.ShortDescription = shortDescription;
            this.ValidFromDATE = validFromDATE;
            this.ValidToDATE = validToDATE;
            this.AutoDataProcessingPermit = autoDataProcessingPermit;
            this.ExternalNumber = externalNumber;
            this.ContractTypeID = contractTypeID;
            this.PartnerType = partnerType;
            this.CreatedAtDATE = createdAtDATE;
            this.CorrectedAtDATE = correctedAtDATE;
        }
        
        /// <summary>
        /// Číslo dok.
        /// </summary>
        /// <value>Číslo dok.</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Zdrojová řada; ID objektu Řada dokladů [persistentní položka]
        /// </summary>
        /// <value>Zdrojová řada; ID objektu Řada dokladů [persistentní položka]</value>
        [DataMember(Name="DocQueue_ID", EmitDefaultValue=false)]
        public string DocQueueID { get; set; }

        /// <summary>
        /// Období; ID objektu Období [persistentní položka]
        /// </summary>
        /// <value>Období; ID objektu Období [persistentní položka]</value>
        [DataMember(Name="Period_ID", EmitDefaultValue=false)]
        public string PeriodID { get; set; }

        /// <summary>
        /// Pořadové číslo [persistentní položka]
        /// </summary>
        /// <value>Pořadové číslo [persistentní položka]</value>
        [DataMember(Name="OrdNumber", EmitDefaultValue=false)]
        public int? OrdNumber { get; set; }

        /// <summary>
        /// Datum dok. [persistentní položka]
        /// </summary>
        /// <value>Datum dok. [persistentní položka]</value>
        [DataMember(Name="DocDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DocDateDATE { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Typ relace
        /// </summary>
        /// <value>Typ relace</value>
        [DataMember(Name="NewRelatedType", EmitDefaultValue=false)]
        public int? NewRelatedType { get; set; }

        /// <summary>
        /// ID dokladu pro připojení
        /// </summary>
        /// <value>ID dokladu pro připojení</value>
        [DataMember(Name="NewRelatedDocument_ID", EmitDefaultValue=false)]
        public string NewRelatedDocumentID { get; set; }

        /// <summary>
        /// Firma; ID objektu Firma [persistentní položka]
        /// </summary>
        /// <value>Firma; ID objektu Firma [persistentní položka]</value>
        [DataMember(Name="Firm_ID", EmitDefaultValue=false)]
        public string FirmID { get; set; }

        /// <summary>
        /// Osoba; ID objektu Osoba [persistentní položka]
        /// </summary>
        /// <value>Osoba; ID objektu Osoba [persistentní položka]</value>
        [DataMember(Name="Person_ID", EmitDefaultValue=false)]
        public string PersonID { get; set; }

        /// <summary>
        /// Popis [persistentní položka]
        /// </summary>
        /// <value>Popis [persistentní položka]</value>
        [DataMember(Name="ShortDescription", EmitDefaultValue=false)]
        public string ShortDescription { get; set; }

        /// <summary>
        /// Platná od [persistentní položka]
        /// </summary>
        /// <value>Platná od [persistentní položka]</value>
        [DataMember(Name="ValidFrom$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ValidFromDATE { get; set; }

        /// <summary>
        /// Platná do [persistentní položka]
        /// </summary>
        /// <value>Platná do [persistentní položka]</value>
        [DataMember(Name="ValidTo$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ValidToDATE { get; set; }

        /// <summary>
        /// Automatické povolení ke zpracování dat [persistentní položka]
        /// </summary>
        /// <value>Automatické povolení ke zpracování dat [persistentní položka]</value>
        [DataMember(Name="AutoDataProcessingPermit", EmitDefaultValue=false)]
        public bool? AutoDataProcessingPermit { get; set; }

        /// <summary>
        /// Externí číslo [persistentní položka]
        /// </summary>
        /// <value>Externí číslo [persistentní položka]</value>
        [DataMember(Name="ExternalNumber", EmitDefaultValue=false)]
        public string ExternalNumber { get; set; }

        /// <summary>
        /// Typ; ID objektu Typ smlouvy [persistentní položka]
        /// </summary>
        /// <value>Typ; ID objektu Typ smlouvy [persistentní položka]</value>
        [DataMember(Name="ContractType_ID", EmitDefaultValue=false)]
        public string ContractTypeID { get; set; }

        /// <summary>
        /// Typ žadatele
        /// </summary>
        /// <value>Typ žadatele</value>
        [DataMember(Name="PartnerType", EmitDefaultValue=false)]
        public int? PartnerType { get; set; }

        /// <summary>
        /// Vytvořeno [persistentní položka]
        /// </summary>
        /// <value>Vytvořeno [persistentní položka]</value>
        [DataMember(Name="CreatedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CreatedAtDATE { get; set; }

        /// <summary>
        /// Opraveno [persistentní položka]
        /// </summary>
        /// <value>Opraveno [persistentní položka]</value>
        [DataMember(Name="CorrectedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CorrectedAtDATE { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Contract {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  DocQueueID: ").Append(DocQueueID).Append("\n");
            sb.Append("  PeriodID: ").Append(PeriodID).Append("\n");
            sb.Append("  OrdNumber: ").Append(OrdNumber).Append("\n");
            sb.Append("  DocDateDATE: ").Append(DocDateDATE).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  NewRelatedType: ").Append(NewRelatedType).Append("\n");
            sb.Append("  NewRelatedDocumentID: ").Append(NewRelatedDocumentID).Append("\n");
            sb.Append("  FirmID: ").Append(FirmID).Append("\n");
            sb.Append("  PersonID: ").Append(PersonID).Append("\n");
            sb.Append("  ShortDescription: ").Append(ShortDescription).Append("\n");
            sb.Append("  ValidFromDATE: ").Append(ValidFromDATE).Append("\n");
            sb.Append("  ValidToDATE: ").Append(ValidToDATE).Append("\n");
            sb.Append("  AutoDataProcessingPermit: ").Append(AutoDataProcessingPermit).Append("\n");
            sb.Append("  ExternalNumber: ").Append(ExternalNumber).Append("\n");
            sb.Append("  ContractTypeID: ").Append(ContractTypeID).Append("\n");
            sb.Append("  PartnerType: ").Append(PartnerType).Append("\n");
            sb.Append("  CreatedAtDATE: ").Append(CreatedAtDATE).Append("\n");
            sb.Append("  CorrectedAtDATE: ").Append(CorrectedAtDATE).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Contract);
        }

        /// <summary>
        /// Returns true if Contract instances are equal
        /// </summary>
        /// <param name="input">Instance of Contract to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Contract input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.DocQueueID == input.DocQueueID ||
                    (this.DocQueueID != null &&
                    this.DocQueueID.Equals(input.DocQueueID))
                ) && 
                (
                    this.PeriodID == input.PeriodID ||
                    (this.PeriodID != null &&
                    this.PeriodID.Equals(input.PeriodID))
                ) && 
                (
                    this.OrdNumber == input.OrdNumber ||
                    (this.OrdNumber != null &&
                    this.OrdNumber.Equals(input.OrdNumber))
                ) && 
                (
                    this.DocDateDATE == input.DocDateDATE ||
                    (this.DocDateDATE != null &&
                    this.DocDateDATE.Equals(input.DocDateDATE))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.NewRelatedType == input.NewRelatedType ||
                    (this.NewRelatedType != null &&
                    this.NewRelatedType.Equals(input.NewRelatedType))
                ) && 
                (
                    this.NewRelatedDocumentID == input.NewRelatedDocumentID ||
                    (this.NewRelatedDocumentID != null &&
                    this.NewRelatedDocumentID.Equals(input.NewRelatedDocumentID))
                ) && 
                (
                    this.FirmID == input.FirmID ||
                    (this.FirmID != null &&
                    this.FirmID.Equals(input.FirmID))
                ) && 
                (
                    this.PersonID == input.PersonID ||
                    (this.PersonID != null &&
                    this.PersonID.Equals(input.PersonID))
                ) && 
                (
                    this.ShortDescription == input.ShortDescription ||
                    (this.ShortDescription != null &&
                    this.ShortDescription.Equals(input.ShortDescription))
                ) && 
                (
                    this.ValidFromDATE == input.ValidFromDATE ||
                    (this.ValidFromDATE != null &&
                    this.ValidFromDATE.Equals(input.ValidFromDATE))
                ) && 
                (
                    this.ValidToDATE == input.ValidToDATE ||
                    (this.ValidToDATE != null &&
                    this.ValidToDATE.Equals(input.ValidToDATE))
                ) && 
                (
                    this.AutoDataProcessingPermit == input.AutoDataProcessingPermit ||
                    (this.AutoDataProcessingPermit != null &&
                    this.AutoDataProcessingPermit.Equals(input.AutoDataProcessingPermit))
                ) && 
                (
                    this.ExternalNumber == input.ExternalNumber ||
                    (this.ExternalNumber != null &&
                    this.ExternalNumber.Equals(input.ExternalNumber))
                ) && 
                (
                    this.ContractTypeID == input.ContractTypeID ||
                    (this.ContractTypeID != null &&
                    this.ContractTypeID.Equals(input.ContractTypeID))
                ) && 
                (
                    this.PartnerType == input.PartnerType ||
                    (this.PartnerType != null &&
                    this.PartnerType.Equals(input.PartnerType))
                ) && 
                (
                    this.CreatedAtDATE == input.CreatedAtDATE ||
                    (this.CreatedAtDATE != null &&
                    this.CreatedAtDATE.Equals(input.CreatedAtDATE))
                ) && 
                (
                    this.CorrectedAtDATE == input.CorrectedAtDATE ||
                    (this.CorrectedAtDATE != null &&
                    this.CorrectedAtDATE.Equals(input.CorrectedAtDATE))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.DocQueueID != null)
                    hashCode = hashCode * 59 + this.DocQueueID.GetHashCode();
                if (this.PeriodID != null)
                    hashCode = hashCode * 59 + this.PeriodID.GetHashCode();
                if (this.OrdNumber != null)
                    hashCode = hashCode * 59 + this.OrdNumber.GetHashCode();
                if (this.DocDateDATE != null)
                    hashCode = hashCode * 59 + this.DocDateDATE.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.NewRelatedType != null)
                    hashCode = hashCode * 59 + this.NewRelatedType.GetHashCode();
                if (this.NewRelatedDocumentID != null)
                    hashCode = hashCode * 59 + this.NewRelatedDocumentID.GetHashCode();
                if (this.FirmID != null)
                    hashCode = hashCode * 59 + this.FirmID.GetHashCode();
                if (this.PersonID != null)
                    hashCode = hashCode * 59 + this.PersonID.GetHashCode();
                if (this.ShortDescription != null)
                    hashCode = hashCode * 59 + this.ShortDescription.GetHashCode();
                if (this.ValidFromDATE != null)
                    hashCode = hashCode * 59 + this.ValidFromDATE.GetHashCode();
                if (this.ValidToDATE != null)
                    hashCode = hashCode * 59 + this.ValidToDATE.GetHashCode();
                if (this.AutoDataProcessingPermit != null)
                    hashCode = hashCode * 59 + this.AutoDataProcessingPermit.GetHashCode();
                if (this.ExternalNumber != null)
                    hashCode = hashCode * 59 + this.ExternalNumber.GetHashCode();
                if (this.ContractTypeID != null)
                    hashCode = hashCode * 59 + this.ContractTypeID.GetHashCode();
                if (this.PartnerType != null)
                    hashCode = hashCode * 59 + this.PartnerType.GetHashCode();
                if (this.CreatedAtDATE != null)
                    hashCode = hashCode * 59 + this.CreatedAtDATE.GetHashCode();
                if (this.CorrectedAtDATE != null)
                    hashCode = hashCode * 59 + this.CorrectedAtDATE.GetHashCode();
                return hashCode;
            }
        }
    }

}
