/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Plmbalancedepreciation
    /// </summary>
    [DataContract]
    public partial class Plmbalancedepreciation :  IEquatable<Plmbalancedepreciation>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Plmbalancedepreciation" /> class.
        /// </summary>
        /// <param name="cLSID">Třída objektu [persistentní položka].</param>
        /// <param name="objID">ID objektu [persistentní položka].</param>
        /// <param name="amount">Částka [persistentní položka].</param>
        public Plmbalancedepreciation(string cLSID = default(string), string objID = default(string), double? amount = default(double?))
        {
            this.CLSID = cLSID;
            this.ObjID = objID;
            this.Amount = amount;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Vyrovnání nedokončené výroby [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Vyrovnání nedokončené výroby [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Třída objektu [persistentní položka]
        /// </summary>
        /// <value>Třída objektu [persistentní položka]</value>
        [DataMember(Name="CLSID", EmitDefaultValue=false)]
        public string CLSID { get; set; }

        /// <summary>
        /// ID objektu [persistentní položka]
        /// </summary>
        /// <value>ID objektu [persistentní položka]</value>
        [DataMember(Name="Obj_ID", EmitDefaultValue=false)]
        public string ObjID { get; set; }

        /// <summary>
        /// Částka [persistentní položka]
        /// </summary>
        /// <value>Částka [persistentní položka]</value>
        [DataMember(Name="Amount", EmitDefaultValue=false)]
        public double? Amount { get; set; }

        /// <summary>
        /// Název třídy objektu
        /// </summary>
        /// <value>Název třídy objektu</value>
        [DataMember(Name="CLSIDText", EmitDefaultValue=false)]
        public string CLSIDText { get; private set; }

        /// <summary>
        /// Název objektu
        /// </summary>
        /// <value>Název objektu</value>
        [DataMember(Name="ObjectName", EmitDefaultValue=false)]
        public string ObjectName { get; private set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Plmbalancedepreciation {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  CLSID: ").Append(CLSID).Append("\n");
            sb.Append("  ObjID: ").Append(ObjID).Append("\n");
            sb.Append("  Amount: ").Append(Amount).Append("\n");
            sb.Append("  CLSIDText: ").Append(CLSIDText).Append("\n");
            sb.Append("  ObjectName: ").Append(ObjectName).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Plmbalancedepreciation);
        }

        /// <summary>
        /// Returns true if Plmbalancedepreciation instances are equal
        /// </summary>
        /// <param name="input">Instance of Plmbalancedepreciation to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Plmbalancedepreciation input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.CLSID == input.CLSID ||
                    (this.CLSID != null &&
                    this.CLSID.Equals(input.CLSID))
                ) && 
                (
                    this.ObjID == input.ObjID ||
                    (this.ObjID != null &&
                    this.ObjID.Equals(input.ObjID))
                ) && 
                (
                    this.Amount == input.Amount ||
                    (this.Amount != null &&
                    this.Amount.Equals(input.Amount))
                ) && 
                (
                    this.CLSIDText == input.CLSIDText ||
                    (this.CLSIDText != null &&
                    this.CLSIDText.Equals(input.CLSIDText))
                ) && 
                (
                    this.ObjectName == input.ObjectName ||
                    (this.ObjectName != null &&
                    this.ObjectName.Equals(input.ObjectName))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.CLSID != null)
                    hashCode = hashCode * 59 + this.CLSID.GetHashCode();
                if (this.ObjID != null)
                    hashCode = hashCode * 59 + this.ObjID.GetHashCode();
                if (this.Amount != null)
                    hashCode = hashCode * 59 + this.Amount.GetHashCode();
                if (this.CLSIDText != null)
                    hashCode = hashCode * 59 + this.CLSIDText.GetHashCode();
                if (this.ObjectName != null)
                    hashCode = hashCode * 59 + this.ObjectName.GetHashCode();
                return hashCode;
            }
        }
    }

}
