/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Plmpiecelist
    /// </summary>
    [DataContract]
    public partial class Plmpiecelist :  IEquatable<Plmpiecelist>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Plmpiecelist" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Kusovník - řádek [nepersistentní položka].</param>
        /// <param name="storeCardID">Skladová karta kusovníku; ID objektu Skladová karta [persistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="note">Poznámka [persistentní položka].</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="createdDATE">Vytvořen [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedDATE">Opraven [persistentní položka].</param>
        /// <param name="releasedByID">Schválil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="releasedDATE">Schválen [persistentní položka].</param>
        /// <param name="pictureID">pictureID.</param>
        /// <param name="planedMaterial">Plánovaný materiál [persistentní položka].</param>
        /// <param name="planedCoopMat">Plánovaný materiál z kooperace [persistentní položka].</param>
        /// <param name="planedCooperation">Plánované náklady na kooperaci [persistentní položka].</param>
        /// <param name="developmentCosts">Náklady na vývoj [persistentní položka].</param>
        /// <param name="pieceListType">Typ [persistentní položka].</param>
        /// <param name="priceForReceipt">Cena pro příjem [persistentní položka].</param>
        /// <param name="busOrderID">Zakázka; ID objektu Zakázka [persistentní položka].</param>
        /// <param name="busTransactionID">Obch. případ; ID objektu Obchodní případ [persistentní položka].</param>
        /// <param name="quantity">Množství [persistentní položka].</param>
        /// <param name="qUnit">Jednotka [persistentní položka].</param>
        /// <param name="unitRate">Vztah [persistentní položka].</param>
        /// <param name="unitQuantity">Množství.</param>
        /// <param name="routineStoreCardID">Skladová karta TP; ID objektu Skladová karta [persistentní položka].</param>
        /// <param name="busProjectID">Projekt; ID objektu Projekt [persistentní položka].</param>
        /// <param name="pictures">Obrázky; kolekce BO Obrázek ke kusovníku [nepersistentní položka].</param>
        public Plmpiecelist(List<Plmpiecelistsrow> rows = default(List<Plmpiecelistsrow>), string storeCardID = default(string), string name = default(string), string note = default(string), string createdByID = default(string), DateTimeOffset? createdDATE = default(DateTimeOffset?), string correctedByID = default(string), DateTimeOffset? correctedDATE = default(DateTimeOffset?), string releasedByID = default(string), DateTimeOffset? releasedDATE = default(DateTimeOffset?), Picture pictureID = default(Picture), double? planedMaterial = default(double?), double? planedCoopMat = default(double?), double? planedCooperation = default(double?), double? developmentCosts = default(double?), int? pieceListType = default(int?), double? priceForReceipt = default(double?), string busOrderID = default(string), string busTransactionID = default(string), double? quantity = default(double?), string qUnit = default(string), double? unitRate = default(double?), double? unitQuantity = default(double?), string routineStoreCardID = default(string), string busProjectID = default(string), List<Plmpiecelistpicture> pictures = default(List<Plmpiecelistpicture>))
        {
            this.Rows = rows;
            this.StoreCardID = storeCardID;
            this.Name = name;
            this.Note = note;
            this.CreatedByID = createdByID;
            this.CreatedDATE = createdDATE;
            this.CorrectedByID = correctedByID;
            this.CorrectedDATE = correctedDATE;
            this.ReleasedByID = releasedByID;
            this.ReleasedDATE = releasedDATE;
            this.PictureID = pictureID;
            this.PlanedMaterial = planedMaterial;
            this.PlanedCoopMat = planedCoopMat;
            this.PlanedCooperation = planedCooperation;
            this.DevelopmentCosts = developmentCosts;
            this.PieceListType = pieceListType;
            this.PriceForReceipt = priceForReceipt;
            this.BusOrderID = busOrderID;
            this.BusTransactionID = busTransactionID;
            this.Quantity = quantity;
            this.QUnit = qUnit;
            this.UnitRate = unitRate;
            this.UnitQuantity = unitQuantity;
            this.RoutineStoreCardID = routineStoreCardID;
            this.BusProjectID = busProjectID;
            this.Pictures = pictures;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// ID revidovaného objektu; ID objektu Kusovník
        /// </summary>
        /// <value>ID revidovaného objektu; ID objektu Kusovník</value>
        [DataMember(Name="Revided_ID", EmitDefaultValue=false)]
        public string RevidedID { get; private set; }

        /// <summary>
        /// Popis revize
        /// </summary>
        /// <value>Popis revize</value>
        [DataMember(Name="RevisionDescription", EmitDefaultValue=false)]
        public string RevisionDescription { get; private set; }

        /// <summary>
        /// Datum revize
        /// </summary>
        /// <value>Datum revize</value>
        [DataMember(Name="RevisionDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? RevisionDateDATE { get; private set; }

        /// <summary>
        /// Autor revize; ID objektu Uživatel
        /// </summary>
        /// <value>Autor revize; ID objektu Uživatel</value>
        [DataMember(Name="RevisionAuthor_ID", EmitDefaultValue=false)]
        public string RevisionAuthorID { get; private set; }

        /// <summary>
        /// Číslo revize
        /// </summary>
        /// <value>Číslo revize</value>
        [DataMember(Name="Revision", EmitDefaultValue=false)]
        public int? Revision { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Kusovník - řádek [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Kusovník - řádek [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Plmpiecelistsrow> Rows { get; set; }

        /// <summary>
        /// Skladová karta kusovníku; ID objektu Skladová karta [persistentní položka]
        /// </summary>
        /// <value>Skladová karta kusovníku; ID objektu Skladová karta [persistentní položka]</value>
        [DataMember(Name="StoreCard_ID", EmitDefaultValue=false)]
        public string StoreCardID { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Poznámka [persistentní položka]
        /// </summary>
        /// <value>Poznámka [persistentní položka]</value>
        [DataMember(Name="Note", EmitDefaultValue=false)]
        public string Note { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Vytvořen [persistentní položka]
        /// </summary>
        /// <value>Vytvořen [persistentní položka]</value>
        [DataMember(Name="Created$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CreatedDATE { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Opraven [persistentní položka]
        /// </summary>
        /// <value>Opraven [persistentní položka]</value>
        [DataMember(Name="Corrected$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CorrectedDATE { get; set; }

        /// <summary>
        /// Schválil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Schválil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="ReleasedBy_ID", EmitDefaultValue=false)]
        public string ReleasedByID { get; set; }

        /// <summary>
        /// Schválen [persistentní položka]
        /// </summary>
        /// <value>Schválen [persistentní položka]</value>
        [DataMember(Name="Released$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ReleasedDATE { get; set; }

        /// <summary>
        /// Gets or Sets PictureID
        /// </summary>
        [DataMember(Name="Picture_ID", EmitDefaultValue=false)]
        public Picture PictureID { get; set; }

        /// <summary>
        /// Plánovaný materiál [persistentní položka]
        /// </summary>
        /// <value>Plánovaný materiál [persistentní položka]</value>
        [DataMember(Name="PlanedMaterial", EmitDefaultValue=false)]
        public double? PlanedMaterial { get; set; }

        /// <summary>
        /// Plánovaný materiál z kooperace [persistentní položka]
        /// </summary>
        /// <value>Plánovaný materiál z kooperace [persistentní položka]</value>
        [DataMember(Name="PlanedCoopMat", EmitDefaultValue=false)]
        public double? PlanedCoopMat { get; set; }

        /// <summary>
        /// Plánované náklady na kooperaci [persistentní položka]
        /// </summary>
        /// <value>Plánované náklady na kooperaci [persistentní položka]</value>
        [DataMember(Name="PlanedCooperation", EmitDefaultValue=false)]
        public double? PlanedCooperation { get; set; }

        /// <summary>
        /// Náklady na vývoj [persistentní položka]
        /// </summary>
        /// <value>Náklady na vývoj [persistentní položka]</value>
        [DataMember(Name="DevelopmentCosts", EmitDefaultValue=false)]
        public double? DevelopmentCosts { get; set; }

        /// <summary>
        /// Typ [persistentní položka]
        /// </summary>
        /// <value>Typ [persistentní položka]</value>
        [DataMember(Name="PieceListType", EmitDefaultValue=false)]
        public int? PieceListType { get; set; }

        /// <summary>
        /// Cena pro příjem [persistentní položka]
        /// </summary>
        /// <value>Cena pro příjem [persistentní položka]</value>
        [DataMember(Name="PriceForReceipt", EmitDefaultValue=false)]
        public double? PriceForReceipt { get; set; }

        /// <summary>
        /// Zakázka; ID objektu Zakázka [persistentní položka]
        /// </summary>
        /// <value>Zakázka; ID objektu Zakázka [persistentní položka]</value>
        [DataMember(Name="BusOrder_ID", EmitDefaultValue=false)]
        public string BusOrderID { get; set; }

        /// <summary>
        /// Obch. případ; ID objektu Obchodní případ [persistentní položka]
        /// </summary>
        /// <value>Obch. případ; ID objektu Obchodní případ [persistentní položka]</value>
        [DataMember(Name="BusTransaction_ID", EmitDefaultValue=false)]
        public string BusTransactionID { get; set; }

        /// <summary>
        /// Množství [persistentní položka]
        /// </summary>
        /// <value>Množství [persistentní položka]</value>
        [DataMember(Name="Quantity", EmitDefaultValue=false)]
        public double? Quantity { get; set; }

        /// <summary>
        /// Jednotka [persistentní položka]
        /// </summary>
        /// <value>Jednotka [persistentní položka]</value>
        [DataMember(Name="QUnit", EmitDefaultValue=false)]
        public string QUnit { get; set; }

        /// <summary>
        /// Vztah [persistentní položka]
        /// </summary>
        /// <value>Vztah [persistentní položka]</value>
        [DataMember(Name="UnitRate", EmitDefaultValue=false)]
        public double? UnitRate { get; set; }

        /// <summary>
        /// Množství
        /// </summary>
        /// <value>Množství</value>
        [DataMember(Name="UnitQuantity", EmitDefaultValue=false)]
        public double? UnitQuantity { get; set; }

        /// <summary>
        /// Skladová karta TP; ID objektu Skladová karta [persistentní položka]
        /// </summary>
        /// <value>Skladová karta TP; ID objektu Skladová karta [persistentní položka]</value>
        [DataMember(Name="RoutineStoreCard_ID", EmitDefaultValue=false)]
        public string RoutineStoreCardID { get; set; }

        /// <summary>
        /// Projekt; ID objektu Projekt [persistentní položka]
        /// </summary>
        /// <value>Projekt; ID objektu Projekt [persistentní položka]</value>
        [DataMember(Name="BusProject_ID", EmitDefaultValue=false)]
        public string BusProjectID { get; set; }

        /// <summary>
        /// Obrázky; kolekce BO Obrázek ke kusovníku [nepersistentní položka]
        /// </summary>
        /// <value>Obrázky; kolekce BO Obrázek ke kusovníku [nepersistentní položka]</value>
        [DataMember(Name="Pictures", EmitDefaultValue=false)]
        public List<Plmpiecelistpicture> Pictures { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Plmpiecelist {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  RevidedID: ").Append(RevidedID).Append("\n");
            sb.Append("  RevisionDescription: ").Append(RevisionDescription).Append("\n");
            sb.Append("  RevisionDateDATE: ").Append(RevisionDateDATE).Append("\n");
            sb.Append("  RevisionAuthorID: ").Append(RevisionAuthorID).Append("\n");
            sb.Append("  Revision: ").Append(Revision).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  StoreCardID: ").Append(StoreCardID).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Note: ").Append(Note).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CreatedDATE: ").Append(CreatedDATE).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  CorrectedDATE: ").Append(CorrectedDATE).Append("\n");
            sb.Append("  ReleasedByID: ").Append(ReleasedByID).Append("\n");
            sb.Append("  ReleasedDATE: ").Append(ReleasedDATE).Append("\n");
            sb.Append("  PictureID: ").Append(PictureID).Append("\n");
            sb.Append("  PlanedMaterial: ").Append(PlanedMaterial).Append("\n");
            sb.Append("  PlanedCoopMat: ").Append(PlanedCoopMat).Append("\n");
            sb.Append("  PlanedCooperation: ").Append(PlanedCooperation).Append("\n");
            sb.Append("  DevelopmentCosts: ").Append(DevelopmentCosts).Append("\n");
            sb.Append("  PieceListType: ").Append(PieceListType).Append("\n");
            sb.Append("  PriceForReceipt: ").Append(PriceForReceipt).Append("\n");
            sb.Append("  BusOrderID: ").Append(BusOrderID).Append("\n");
            sb.Append("  BusTransactionID: ").Append(BusTransactionID).Append("\n");
            sb.Append("  Quantity: ").Append(Quantity).Append("\n");
            sb.Append("  QUnit: ").Append(QUnit).Append("\n");
            sb.Append("  UnitRate: ").Append(UnitRate).Append("\n");
            sb.Append("  UnitQuantity: ").Append(UnitQuantity).Append("\n");
            sb.Append("  RoutineStoreCardID: ").Append(RoutineStoreCardID).Append("\n");
            sb.Append("  BusProjectID: ").Append(BusProjectID).Append("\n");
            sb.Append("  Pictures: ").Append(Pictures).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Plmpiecelist);
        }

        /// <summary>
        /// Returns true if Plmpiecelist instances are equal
        /// </summary>
        /// <param name="input">Instance of Plmpiecelist to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Plmpiecelist input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.RevidedID == input.RevidedID ||
                    (this.RevidedID != null &&
                    this.RevidedID.Equals(input.RevidedID))
                ) && 
                (
                    this.RevisionDescription == input.RevisionDescription ||
                    (this.RevisionDescription != null &&
                    this.RevisionDescription.Equals(input.RevisionDescription))
                ) && 
                (
                    this.RevisionDateDATE == input.RevisionDateDATE ||
                    (this.RevisionDateDATE != null &&
                    this.RevisionDateDATE.Equals(input.RevisionDateDATE))
                ) && 
                (
                    this.RevisionAuthorID == input.RevisionAuthorID ||
                    (this.RevisionAuthorID != null &&
                    this.RevisionAuthorID.Equals(input.RevisionAuthorID))
                ) && 
                (
                    this.Revision == input.Revision ||
                    (this.Revision != null &&
                    this.Revision.Equals(input.Revision))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.StoreCardID == input.StoreCardID ||
                    (this.StoreCardID != null &&
                    this.StoreCardID.Equals(input.StoreCardID))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.Note == input.Note ||
                    (this.Note != null &&
                    this.Note.Equals(input.Note))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CreatedDATE == input.CreatedDATE ||
                    (this.CreatedDATE != null &&
                    this.CreatedDATE.Equals(input.CreatedDATE))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.CorrectedDATE == input.CorrectedDATE ||
                    (this.CorrectedDATE != null &&
                    this.CorrectedDATE.Equals(input.CorrectedDATE))
                ) && 
                (
                    this.ReleasedByID == input.ReleasedByID ||
                    (this.ReleasedByID != null &&
                    this.ReleasedByID.Equals(input.ReleasedByID))
                ) && 
                (
                    this.ReleasedDATE == input.ReleasedDATE ||
                    (this.ReleasedDATE != null &&
                    this.ReleasedDATE.Equals(input.ReleasedDATE))
                ) && 
                (
                    this.PictureID == input.PictureID ||
                    (this.PictureID != null &&
                    this.PictureID.Equals(input.PictureID))
                ) && 
                (
                    this.PlanedMaterial == input.PlanedMaterial ||
                    (this.PlanedMaterial != null &&
                    this.PlanedMaterial.Equals(input.PlanedMaterial))
                ) && 
                (
                    this.PlanedCoopMat == input.PlanedCoopMat ||
                    (this.PlanedCoopMat != null &&
                    this.PlanedCoopMat.Equals(input.PlanedCoopMat))
                ) && 
                (
                    this.PlanedCooperation == input.PlanedCooperation ||
                    (this.PlanedCooperation != null &&
                    this.PlanedCooperation.Equals(input.PlanedCooperation))
                ) && 
                (
                    this.DevelopmentCosts == input.DevelopmentCosts ||
                    (this.DevelopmentCosts != null &&
                    this.DevelopmentCosts.Equals(input.DevelopmentCosts))
                ) && 
                (
                    this.PieceListType == input.PieceListType ||
                    (this.PieceListType != null &&
                    this.PieceListType.Equals(input.PieceListType))
                ) && 
                (
                    this.PriceForReceipt == input.PriceForReceipt ||
                    (this.PriceForReceipt != null &&
                    this.PriceForReceipt.Equals(input.PriceForReceipt))
                ) && 
                (
                    this.BusOrderID == input.BusOrderID ||
                    (this.BusOrderID != null &&
                    this.BusOrderID.Equals(input.BusOrderID))
                ) && 
                (
                    this.BusTransactionID == input.BusTransactionID ||
                    (this.BusTransactionID != null &&
                    this.BusTransactionID.Equals(input.BusTransactionID))
                ) && 
                (
                    this.Quantity == input.Quantity ||
                    (this.Quantity != null &&
                    this.Quantity.Equals(input.Quantity))
                ) && 
                (
                    this.QUnit == input.QUnit ||
                    (this.QUnit != null &&
                    this.QUnit.Equals(input.QUnit))
                ) && 
                (
                    this.UnitRate == input.UnitRate ||
                    (this.UnitRate != null &&
                    this.UnitRate.Equals(input.UnitRate))
                ) && 
                (
                    this.UnitQuantity == input.UnitQuantity ||
                    (this.UnitQuantity != null &&
                    this.UnitQuantity.Equals(input.UnitQuantity))
                ) && 
                (
                    this.RoutineStoreCardID == input.RoutineStoreCardID ||
                    (this.RoutineStoreCardID != null &&
                    this.RoutineStoreCardID.Equals(input.RoutineStoreCardID))
                ) && 
                (
                    this.BusProjectID == input.BusProjectID ||
                    (this.BusProjectID != null &&
                    this.BusProjectID.Equals(input.BusProjectID))
                ) && 
                (
                    this.Pictures == input.Pictures ||
                    this.Pictures != null &&
                    this.Pictures.SequenceEqual(input.Pictures)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.RevidedID != null)
                    hashCode = hashCode * 59 + this.RevidedID.GetHashCode();
                if (this.RevisionDescription != null)
                    hashCode = hashCode * 59 + this.RevisionDescription.GetHashCode();
                if (this.RevisionDateDATE != null)
                    hashCode = hashCode * 59 + this.RevisionDateDATE.GetHashCode();
                if (this.RevisionAuthorID != null)
                    hashCode = hashCode * 59 + this.RevisionAuthorID.GetHashCode();
                if (this.Revision != null)
                    hashCode = hashCode * 59 + this.Revision.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.StoreCardID != null)
                    hashCode = hashCode * 59 + this.StoreCardID.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.Note != null)
                    hashCode = hashCode * 59 + this.Note.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CreatedDATE != null)
                    hashCode = hashCode * 59 + this.CreatedDATE.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.CorrectedDATE != null)
                    hashCode = hashCode * 59 + this.CorrectedDATE.GetHashCode();
                if (this.ReleasedByID != null)
                    hashCode = hashCode * 59 + this.ReleasedByID.GetHashCode();
                if (this.ReleasedDATE != null)
                    hashCode = hashCode * 59 + this.ReleasedDATE.GetHashCode();
                if (this.PictureID != null)
                    hashCode = hashCode * 59 + this.PictureID.GetHashCode();
                if (this.PlanedMaterial != null)
                    hashCode = hashCode * 59 + this.PlanedMaterial.GetHashCode();
                if (this.PlanedCoopMat != null)
                    hashCode = hashCode * 59 + this.PlanedCoopMat.GetHashCode();
                if (this.PlanedCooperation != null)
                    hashCode = hashCode * 59 + this.PlanedCooperation.GetHashCode();
                if (this.DevelopmentCosts != null)
                    hashCode = hashCode * 59 + this.DevelopmentCosts.GetHashCode();
                if (this.PieceListType != null)
                    hashCode = hashCode * 59 + this.PieceListType.GetHashCode();
                if (this.PriceForReceipt != null)
                    hashCode = hashCode * 59 + this.PriceForReceipt.GetHashCode();
                if (this.BusOrderID != null)
                    hashCode = hashCode * 59 + this.BusOrderID.GetHashCode();
                if (this.BusTransactionID != null)
                    hashCode = hashCode * 59 + this.BusTransactionID.GetHashCode();
                if (this.Quantity != null)
                    hashCode = hashCode * 59 + this.Quantity.GetHashCode();
                if (this.QUnit != null)
                    hashCode = hashCode * 59 + this.QUnit.GetHashCode();
                if (this.UnitRate != null)
                    hashCode = hashCode * 59 + this.UnitRate.GetHashCode();
                if (this.UnitQuantity != null)
                    hashCode = hashCode * 59 + this.UnitQuantity.GetHashCode();
                if (this.RoutineStoreCardID != null)
                    hashCode = hashCode * 59 + this.RoutineStoreCardID.GetHashCode();
                if (this.BusProjectID != null)
                    hashCode = hashCode * 59 + this.BusProjectID.GetHashCode();
                if (this.Pictures != null)
                    hashCode = hashCode * 59 + this.Pictures.GetHashCode();
                return hashCode;
            }
        }
    }

}
