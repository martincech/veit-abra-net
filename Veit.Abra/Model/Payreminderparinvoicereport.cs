/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Payreminderparinvoicereport
    /// </summary>
    [DataContract]
    public partial class Payreminderparinvoicereport :  IEquatable<Payreminderparinvoicereport>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Payreminderparinvoicereport" /> class.
        /// </summary>
        /// <param name="orderNum">Pořadí [persistentní položka].</param>
        /// <param name="reportID">Odkaz na report; ID objektu Definice reportu [persistentní položka].</param>
        /// <param name="conditionExpr">Podmínka [persistentní položka].</param>
        public Payreminderparinvoicereport(int? orderNum = default(int?), string reportID = default(string), string conditionExpr = default(string))
        {
            this.OrderNum = orderNum;
            this.ReportID = reportID;
            this.ConditionExpr = conditionExpr;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Parametry automatických upomínek [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Parametry automatických upomínek [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Pořadí [persistentní položka]
        /// </summary>
        /// <value>Pořadí [persistentní položka]</value>
        [DataMember(Name="OrderNum", EmitDefaultValue=false)]
        public int? OrderNum { get; set; }

        /// <summary>
        /// Odkaz na report; ID objektu Definice reportu [persistentní položka]
        /// </summary>
        /// <value>Odkaz na report; ID objektu Definice reportu [persistentní položka]</value>
        [DataMember(Name="Report_ID", EmitDefaultValue=false)]
        public string ReportID { get; set; }

        /// <summary>
        /// Podmínka [persistentní položka]
        /// </summary>
        /// <value>Podmínka [persistentní položka]</value>
        [DataMember(Name="ConditionExpr", EmitDefaultValue=false)]
        public string ConditionExpr { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Payreminderparinvoicereport {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  OrderNum: ").Append(OrderNum).Append("\n");
            sb.Append("  ReportID: ").Append(ReportID).Append("\n");
            sb.Append("  ConditionExpr: ").Append(ConditionExpr).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Payreminderparinvoicereport);
        }

        /// <summary>
        /// Returns true if Payreminderparinvoicereport instances are equal
        /// </summary>
        /// <param name="input">Instance of Payreminderparinvoicereport to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Payreminderparinvoicereport input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.OrderNum == input.OrderNum ||
                    (this.OrderNum != null &&
                    this.OrderNum.Equals(input.OrderNum))
                ) && 
                (
                    this.ReportID == input.ReportID ||
                    (this.ReportID != null &&
                    this.ReportID.Equals(input.ReportID))
                ) && 
                (
                    this.ConditionExpr == input.ConditionExpr ||
                    (this.ConditionExpr != null &&
                    this.ConditionExpr.Equals(input.ConditionExpr))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.OrderNum != null)
                    hashCode = hashCode * 59 + this.OrderNum.GetHashCode();
                if (this.ReportID != null)
                    hashCode = hashCode * 59 + this.ReportID.GetHashCode();
                if (this.ConditionExpr != null)
                    hashCode = hashCode * 59 + this.ConditionExpr.GetHashCode();
                return hashCode;
            }
        }
    }

}
