/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Plmoperation
    /// </summary>
    [DataContract]
    public partial class Plmoperation :  IEquatable<Plmoperation>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Plmoperation" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Změna pracovního lístku [nepersistentní položka].</param>
        /// <param name="jobOrdersRoutinesID">Odkaz na výrobní postup; ID objektu VP - technologický postup [persistentní položka].</param>
        /// <param name="jobOrdersSNID">Odkaz na SN; ID objektu VP - sériové číslo [persistentní položka].</param>
        /// <param name="performedByID">Pracovník; ID objektu Pracovník [persistentní položka].</param>
        /// <param name="startedAtDATE">Začátek [persistentní položka].</param>
        /// <param name="finishedAtDATE">Ukončení [persistentní položka].</param>
        /// <param name="duration">Doba trvání v hod..</param>
        /// <param name="operationResult">Výsledek [persistentní položka].</param>
        /// <param name="quantity">Dokončeno v ev.jedn. [persistentní položka].</param>
        /// <param name="divisionID">Středisko; ID objektu Středisko [persistentní položka].</param>
        /// <param name="salaryClassID">Tarifní třída skutečná; ID objektu Tarifní třída [persistentní položka].</param>
        /// <param name="hourlyRate">Sazba [persistentní položka].</param>
        /// <param name="salaryCosts">Mzdové náklady [persistentní položka].</param>
        /// <param name="overheadCosts">Výrobní režie [persistentní položka].</param>
        /// <param name="generalExpense">Správní režie [persistentní položka].</param>
        /// <param name="cooperation">Kooperace [persistentní položka].</param>
        /// <param name="aggregateWorkTicketID">Souhrnné prac. lístky; ID objektu Souhrnný pracovní lístek [persistentní položka].</param>
        /// <param name="busOrderID">Zakázka; ID objektu Zakázka [persistentní položka].</param>
        /// <param name="busTransactionID">Obch. případ; ID objektu Obchodní případ [persistentní položka].</param>
        /// <param name="qUnit">Jednotka.</param>
        /// <param name="unitRate">Vztah.</param>
        /// <param name="unitQuantity">Dokončeno.</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="docDateDATE">Datum dok. [persistentní položka].</param>
        /// <param name="valueTransfer">Způsob přenosu hodnoty [persistentní položka].</param>
        /// <param name="wageOperationTypeID">Druh výkonu; ID objektu Druhy výkonů [persistentní položka].</param>
        /// <param name="finishedProductRowID">Dokončený výrobek; ID objektu Dokončený výrobek - řádek [persistentní položka].</param>
        /// <param name="busProjectID">Projekt; ID objektu Projekt [persistentní položka].</param>
        /// <param name="workPlaceID">Skutečné pracoviště; ID objektu Pracoviště a stroj [persistentní položka].</param>
        /// <param name="totalTAC">Doba TAC [persistentní položka].</param>
        /// <param name="totalTBC">Doba TBC [persistentní položka].</param>
        /// <param name="totalTime">Doba trvání [persistentní položka].</param>
        /// <param name="operationWSIs">Dílenské úkony; kolekce BO Úkony na prac. lístku [nepersistentní položka].</param>
        /// <param name="groupID">Sdružení [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedAtDATE">Opraveno [persistentní položka].</param>
        public Plmoperation(List<Plmoperationchange> rows = default(List<Plmoperationchange>), string jobOrdersRoutinesID = default(string), string jobOrdersSNID = default(string), string performedByID = default(string), DateTimeOffset? startedAtDATE = default(DateTimeOffset?), DateTimeOffset? finishedAtDATE = default(DateTimeOffset?), double? duration = default(double?), bool? operationResult = default(bool?), double? quantity = default(double?), string divisionID = default(string), string salaryClassID = default(string), double? hourlyRate = default(double?), double? salaryCosts = default(double?), double? overheadCosts = default(double?), double? generalExpense = default(double?), bool? cooperation = default(bool?), string aggregateWorkTicketID = default(string), string busOrderID = default(string), string busTransactionID = default(string), string qUnit = default(string), double? unitRate = default(double?), double? unitQuantity = default(double?), string createdByID = default(string), DateTimeOffset? docDateDATE = default(DateTimeOffset?), int? valueTransfer = default(int?), string wageOperationTypeID = default(string), string finishedProductRowID = default(string), string busProjectID = default(string), string workPlaceID = default(string), double? totalTAC = default(double?), double? totalTBC = default(double?), double? totalTime = default(double?), List<Plmoperationwsi> operationWSIs = default(List<Plmoperationwsi>), string groupID = default(string), string correctedByID = default(string), DateTimeOffset? correctedAtDATE = default(DateTimeOffset?))
        {
            this.Rows = rows;
            this.JobOrdersRoutinesID = jobOrdersRoutinesID;
            this.JobOrdersSNID = jobOrdersSNID;
            this.PerformedByID = performedByID;
            this.StartedAtDATE = startedAtDATE;
            this.FinishedAtDATE = finishedAtDATE;
            this.Duration = duration;
            this.OperationResult = operationResult;
            this.Quantity = quantity;
            this.DivisionID = divisionID;
            this.SalaryClassID = salaryClassID;
            this.HourlyRate = hourlyRate;
            this.SalaryCosts = salaryCosts;
            this.OverheadCosts = overheadCosts;
            this.GeneralExpense = generalExpense;
            this.Cooperation = cooperation;
            this.AggregateWorkTicketID = aggregateWorkTicketID;
            this.BusOrderID = busOrderID;
            this.BusTransactionID = busTransactionID;
            this.QUnit = qUnit;
            this.UnitRate = unitRate;
            this.UnitQuantity = unitQuantity;
            this.CreatedByID = createdByID;
            this.DocDateDATE = docDateDATE;
            this.ValueTransfer = valueTransfer;
            this.WageOperationTypeID = wageOperationTypeID;
            this.FinishedProductRowID = finishedProductRowID;
            this.BusProjectID = busProjectID;
            this.WorkPlaceID = workPlaceID;
            this.TotalTAC = totalTAC;
            this.TotalTBC = totalTBC;
            this.TotalTime = totalTime;
            this.OperationWSIs = operationWSIs;
            this.GroupID = groupID;
            this.CorrectedByID = correctedByID;
            this.CorrectedAtDATE = correctedAtDATE;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Změna pracovního lístku [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Změna pracovního lístku [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Plmoperationchange> Rows { get; set; }

        /// <summary>
        /// Odkaz na výrobní postup; ID objektu VP - technologický postup [persistentní položka]
        /// </summary>
        /// <value>Odkaz na výrobní postup; ID objektu VP - technologický postup [persistentní položka]</value>
        [DataMember(Name="JobOrdersRoutines_ID", EmitDefaultValue=false)]
        public string JobOrdersRoutinesID { get; set; }

        /// <summary>
        /// Odkaz na SN; ID objektu VP - sériové číslo [persistentní položka]
        /// </summary>
        /// <value>Odkaz na SN; ID objektu VP - sériové číslo [persistentní položka]</value>
        [DataMember(Name="JobOrdersSN_ID", EmitDefaultValue=false)]
        public string JobOrdersSNID { get; set; }

        /// <summary>
        /// Pracovník; ID objektu Pracovník [persistentní položka]
        /// </summary>
        /// <value>Pracovník; ID objektu Pracovník [persistentní položka]</value>
        [DataMember(Name="PerformedBy_ID", EmitDefaultValue=false)]
        public string PerformedByID { get; set; }

        /// <summary>
        /// Začátek [persistentní položka]
        /// </summary>
        /// <value>Začátek [persistentní položka]</value>
        [DataMember(Name="StartedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? StartedAtDATE { get; set; }

        /// <summary>
        /// Ukončení [persistentní položka]
        /// </summary>
        /// <value>Ukončení [persistentní položka]</value>
        [DataMember(Name="FinishedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? FinishedAtDATE { get; set; }

        /// <summary>
        /// Doba trvání v hod.
        /// </summary>
        /// <value>Doba trvání v hod.</value>
        [DataMember(Name="Duration", EmitDefaultValue=false)]
        public double? Duration { get; set; }

        /// <summary>
        /// Výsledek [persistentní položka]
        /// </summary>
        /// <value>Výsledek [persistentní položka]</value>
        [DataMember(Name="OperationResult", EmitDefaultValue=false)]
        public bool? OperationResult { get; set; }

        /// <summary>
        /// Dokončeno v ev.jedn. [persistentní položka]
        /// </summary>
        /// <value>Dokončeno v ev.jedn. [persistentní položka]</value>
        [DataMember(Name="Quantity", EmitDefaultValue=false)]
        public double? Quantity { get; set; }

        /// <summary>
        /// Středisko; ID objektu Středisko [persistentní položka]
        /// </summary>
        /// <value>Středisko; ID objektu Středisko [persistentní položka]</value>
        [DataMember(Name="Division_ID", EmitDefaultValue=false)]
        public string DivisionID { get; set; }

        /// <summary>
        /// Tarifní třída skutečná; ID objektu Tarifní třída [persistentní položka]
        /// </summary>
        /// <value>Tarifní třída skutečná; ID objektu Tarifní třída [persistentní položka]</value>
        [DataMember(Name="SalaryClass_ID", EmitDefaultValue=false)]
        public string SalaryClassID { get; set; }

        /// <summary>
        /// Sazba [persistentní položka]
        /// </summary>
        /// <value>Sazba [persistentní položka]</value>
        [DataMember(Name="HourlyRate", EmitDefaultValue=false)]
        public double? HourlyRate { get; set; }

        /// <summary>
        /// Mzdové náklady [persistentní položka]
        /// </summary>
        /// <value>Mzdové náklady [persistentní položka]</value>
        [DataMember(Name="SalaryCosts", EmitDefaultValue=false)]
        public double? SalaryCosts { get; set; }

        /// <summary>
        /// Výrobní režie [persistentní položka]
        /// </summary>
        /// <value>Výrobní režie [persistentní položka]</value>
        [DataMember(Name="OverheadCosts", EmitDefaultValue=false)]
        public double? OverheadCosts { get; set; }

        /// <summary>
        /// Správní režie [persistentní položka]
        /// </summary>
        /// <value>Správní režie [persistentní položka]</value>
        [DataMember(Name="GeneralExpense", EmitDefaultValue=false)]
        public double? GeneralExpense { get; set; }

        /// <summary>
        /// Kooperace [persistentní položka]
        /// </summary>
        /// <value>Kooperace [persistentní položka]</value>
        [DataMember(Name="Cooperation", EmitDefaultValue=false)]
        public bool? Cooperation { get; set; }

        /// <summary>
        /// Souhrnné prac. lístky; ID objektu Souhrnný pracovní lístek [persistentní položka]
        /// </summary>
        /// <value>Souhrnné prac. lístky; ID objektu Souhrnný pracovní lístek [persistentní položka]</value>
        [DataMember(Name="AggregateWorkTicket_ID", EmitDefaultValue=false)]
        public string AggregateWorkTicketID { get; set; }

        /// <summary>
        /// Zakázka; ID objektu Zakázka [persistentní položka]
        /// </summary>
        /// <value>Zakázka; ID objektu Zakázka [persistentní položka]</value>
        [DataMember(Name="BusOrder_ID", EmitDefaultValue=false)]
        public string BusOrderID { get; set; }

        /// <summary>
        /// Obch. případ; ID objektu Obchodní případ [persistentní položka]
        /// </summary>
        /// <value>Obch. případ; ID objektu Obchodní případ [persistentní položka]</value>
        [DataMember(Name="BusTransaction_ID", EmitDefaultValue=false)]
        public string BusTransactionID { get; set; }

        /// <summary>
        /// Jednotka
        /// </summary>
        /// <value>Jednotka</value>
        [DataMember(Name="QUnit", EmitDefaultValue=false)]
        public string QUnit { get; set; }

        /// <summary>
        /// Vztah
        /// </summary>
        /// <value>Vztah</value>
        [DataMember(Name="UnitRate", EmitDefaultValue=false)]
        public double? UnitRate { get; set; }

        /// <summary>
        /// Dokončeno
        /// </summary>
        /// <value>Dokončeno</value>
        [DataMember(Name="UnitQuantity", EmitDefaultValue=false)]
        public double? UnitQuantity { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Datum dok. [persistentní položka]
        /// </summary>
        /// <value>Datum dok. [persistentní položka]</value>
        [DataMember(Name="DocDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DocDateDATE { get; set; }

        /// <summary>
        /// Způsob přenosu hodnoty [persistentní položka]
        /// </summary>
        /// <value>Způsob přenosu hodnoty [persistentní položka]</value>
        [DataMember(Name="ValueTransfer", EmitDefaultValue=false)]
        public int? ValueTransfer { get; set; }

        /// <summary>
        /// Druh výkonu; ID objektu Druhy výkonů [persistentní položka]
        /// </summary>
        /// <value>Druh výkonu; ID objektu Druhy výkonů [persistentní položka]</value>
        [DataMember(Name="WageOperationType_ID", EmitDefaultValue=false)]
        public string WageOperationTypeID { get; set; }

        /// <summary>
        /// Dokončený výrobek; ID objektu Dokončený výrobek - řádek [persistentní položka]
        /// </summary>
        /// <value>Dokončený výrobek; ID objektu Dokončený výrobek - řádek [persistentní položka]</value>
        [DataMember(Name="FinishedProductRow_ID", EmitDefaultValue=false)]
        public string FinishedProductRowID { get; set; }

        /// <summary>
        /// Projekt; ID objektu Projekt [persistentní položka]
        /// </summary>
        /// <value>Projekt; ID objektu Projekt [persistentní položka]</value>
        [DataMember(Name="BusProject_ID", EmitDefaultValue=false)]
        public string BusProjectID { get; set; }

        /// <summary>
        /// Skutečné pracoviště; ID objektu Pracoviště a stroj [persistentní položka]
        /// </summary>
        /// <value>Skutečné pracoviště; ID objektu Pracoviště a stroj [persistentní položka]</value>
        [DataMember(Name="WorkPlace_ID", EmitDefaultValue=false)]
        public string WorkPlaceID { get; set; }

        /// <summary>
        /// Doba TAC [persistentní položka]
        /// </summary>
        /// <value>Doba TAC [persistentní položka]</value>
        [DataMember(Name="TotalTAC", EmitDefaultValue=false)]
        public double? TotalTAC { get; set; }

        /// <summary>
        /// Doba TBC [persistentní položka]
        /// </summary>
        /// <value>Doba TBC [persistentní položka]</value>
        [DataMember(Name="TotalTBC", EmitDefaultValue=false)]
        public double? TotalTBC { get; set; }

        /// <summary>
        /// Doba trvání [persistentní položka]
        /// </summary>
        /// <value>Doba trvání [persistentní položka]</value>
        [DataMember(Name="TotalTime", EmitDefaultValue=false)]
        public double? TotalTime { get; set; }

        /// <summary>
        /// Dílenské úkony; kolekce BO Úkony na prac. lístku [nepersistentní položka]
        /// </summary>
        /// <value>Dílenské úkony; kolekce BO Úkony na prac. lístku [nepersistentní položka]</value>
        [DataMember(Name="OperationWSIs", EmitDefaultValue=false)]
        public List<Plmoperationwsi> OperationWSIs { get; set; }

        /// <summary>
        /// Sdružení [persistentní položka]
        /// </summary>
        /// <value>Sdružení [persistentní položka]</value>
        [DataMember(Name="GroupID", EmitDefaultValue=false)]
        public string GroupID { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Opraveno [persistentní položka]
        /// </summary>
        /// <value>Opraveno [persistentní položka]</value>
        [DataMember(Name="CorrectedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CorrectedAtDATE { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Plmoperation {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  JobOrdersRoutinesID: ").Append(JobOrdersRoutinesID).Append("\n");
            sb.Append("  JobOrdersSNID: ").Append(JobOrdersSNID).Append("\n");
            sb.Append("  PerformedByID: ").Append(PerformedByID).Append("\n");
            sb.Append("  StartedAtDATE: ").Append(StartedAtDATE).Append("\n");
            sb.Append("  FinishedAtDATE: ").Append(FinishedAtDATE).Append("\n");
            sb.Append("  Duration: ").Append(Duration).Append("\n");
            sb.Append("  OperationResult: ").Append(OperationResult).Append("\n");
            sb.Append("  Quantity: ").Append(Quantity).Append("\n");
            sb.Append("  DivisionID: ").Append(DivisionID).Append("\n");
            sb.Append("  SalaryClassID: ").Append(SalaryClassID).Append("\n");
            sb.Append("  HourlyRate: ").Append(HourlyRate).Append("\n");
            sb.Append("  SalaryCosts: ").Append(SalaryCosts).Append("\n");
            sb.Append("  OverheadCosts: ").Append(OverheadCosts).Append("\n");
            sb.Append("  GeneralExpense: ").Append(GeneralExpense).Append("\n");
            sb.Append("  Cooperation: ").Append(Cooperation).Append("\n");
            sb.Append("  AggregateWorkTicketID: ").Append(AggregateWorkTicketID).Append("\n");
            sb.Append("  BusOrderID: ").Append(BusOrderID).Append("\n");
            sb.Append("  BusTransactionID: ").Append(BusTransactionID).Append("\n");
            sb.Append("  QUnit: ").Append(QUnit).Append("\n");
            sb.Append("  UnitRate: ").Append(UnitRate).Append("\n");
            sb.Append("  UnitQuantity: ").Append(UnitQuantity).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  DocDateDATE: ").Append(DocDateDATE).Append("\n");
            sb.Append("  ValueTransfer: ").Append(ValueTransfer).Append("\n");
            sb.Append("  WageOperationTypeID: ").Append(WageOperationTypeID).Append("\n");
            sb.Append("  FinishedProductRowID: ").Append(FinishedProductRowID).Append("\n");
            sb.Append("  BusProjectID: ").Append(BusProjectID).Append("\n");
            sb.Append("  WorkPlaceID: ").Append(WorkPlaceID).Append("\n");
            sb.Append("  TotalTAC: ").Append(TotalTAC).Append("\n");
            sb.Append("  TotalTBC: ").Append(TotalTBC).Append("\n");
            sb.Append("  TotalTime: ").Append(TotalTime).Append("\n");
            sb.Append("  OperationWSIs: ").Append(OperationWSIs).Append("\n");
            sb.Append("  GroupID: ").Append(GroupID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  CorrectedAtDATE: ").Append(CorrectedAtDATE).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Plmoperation);
        }

        /// <summary>
        /// Returns true if Plmoperation instances are equal
        /// </summary>
        /// <param name="input">Instance of Plmoperation to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Plmoperation input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.JobOrdersRoutinesID == input.JobOrdersRoutinesID ||
                    (this.JobOrdersRoutinesID != null &&
                    this.JobOrdersRoutinesID.Equals(input.JobOrdersRoutinesID))
                ) && 
                (
                    this.JobOrdersSNID == input.JobOrdersSNID ||
                    (this.JobOrdersSNID != null &&
                    this.JobOrdersSNID.Equals(input.JobOrdersSNID))
                ) && 
                (
                    this.PerformedByID == input.PerformedByID ||
                    (this.PerformedByID != null &&
                    this.PerformedByID.Equals(input.PerformedByID))
                ) && 
                (
                    this.StartedAtDATE == input.StartedAtDATE ||
                    (this.StartedAtDATE != null &&
                    this.StartedAtDATE.Equals(input.StartedAtDATE))
                ) && 
                (
                    this.FinishedAtDATE == input.FinishedAtDATE ||
                    (this.FinishedAtDATE != null &&
                    this.FinishedAtDATE.Equals(input.FinishedAtDATE))
                ) && 
                (
                    this.Duration == input.Duration ||
                    (this.Duration != null &&
                    this.Duration.Equals(input.Duration))
                ) && 
                (
                    this.OperationResult == input.OperationResult ||
                    (this.OperationResult != null &&
                    this.OperationResult.Equals(input.OperationResult))
                ) && 
                (
                    this.Quantity == input.Quantity ||
                    (this.Quantity != null &&
                    this.Quantity.Equals(input.Quantity))
                ) && 
                (
                    this.DivisionID == input.DivisionID ||
                    (this.DivisionID != null &&
                    this.DivisionID.Equals(input.DivisionID))
                ) && 
                (
                    this.SalaryClassID == input.SalaryClassID ||
                    (this.SalaryClassID != null &&
                    this.SalaryClassID.Equals(input.SalaryClassID))
                ) && 
                (
                    this.HourlyRate == input.HourlyRate ||
                    (this.HourlyRate != null &&
                    this.HourlyRate.Equals(input.HourlyRate))
                ) && 
                (
                    this.SalaryCosts == input.SalaryCosts ||
                    (this.SalaryCosts != null &&
                    this.SalaryCosts.Equals(input.SalaryCosts))
                ) && 
                (
                    this.OverheadCosts == input.OverheadCosts ||
                    (this.OverheadCosts != null &&
                    this.OverheadCosts.Equals(input.OverheadCosts))
                ) && 
                (
                    this.GeneralExpense == input.GeneralExpense ||
                    (this.GeneralExpense != null &&
                    this.GeneralExpense.Equals(input.GeneralExpense))
                ) && 
                (
                    this.Cooperation == input.Cooperation ||
                    (this.Cooperation != null &&
                    this.Cooperation.Equals(input.Cooperation))
                ) && 
                (
                    this.AggregateWorkTicketID == input.AggregateWorkTicketID ||
                    (this.AggregateWorkTicketID != null &&
                    this.AggregateWorkTicketID.Equals(input.AggregateWorkTicketID))
                ) && 
                (
                    this.BusOrderID == input.BusOrderID ||
                    (this.BusOrderID != null &&
                    this.BusOrderID.Equals(input.BusOrderID))
                ) && 
                (
                    this.BusTransactionID == input.BusTransactionID ||
                    (this.BusTransactionID != null &&
                    this.BusTransactionID.Equals(input.BusTransactionID))
                ) && 
                (
                    this.QUnit == input.QUnit ||
                    (this.QUnit != null &&
                    this.QUnit.Equals(input.QUnit))
                ) && 
                (
                    this.UnitRate == input.UnitRate ||
                    (this.UnitRate != null &&
                    this.UnitRate.Equals(input.UnitRate))
                ) && 
                (
                    this.UnitQuantity == input.UnitQuantity ||
                    (this.UnitQuantity != null &&
                    this.UnitQuantity.Equals(input.UnitQuantity))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.DocDateDATE == input.DocDateDATE ||
                    (this.DocDateDATE != null &&
                    this.DocDateDATE.Equals(input.DocDateDATE))
                ) && 
                (
                    this.ValueTransfer == input.ValueTransfer ||
                    (this.ValueTransfer != null &&
                    this.ValueTransfer.Equals(input.ValueTransfer))
                ) && 
                (
                    this.WageOperationTypeID == input.WageOperationTypeID ||
                    (this.WageOperationTypeID != null &&
                    this.WageOperationTypeID.Equals(input.WageOperationTypeID))
                ) && 
                (
                    this.FinishedProductRowID == input.FinishedProductRowID ||
                    (this.FinishedProductRowID != null &&
                    this.FinishedProductRowID.Equals(input.FinishedProductRowID))
                ) && 
                (
                    this.BusProjectID == input.BusProjectID ||
                    (this.BusProjectID != null &&
                    this.BusProjectID.Equals(input.BusProjectID))
                ) && 
                (
                    this.WorkPlaceID == input.WorkPlaceID ||
                    (this.WorkPlaceID != null &&
                    this.WorkPlaceID.Equals(input.WorkPlaceID))
                ) && 
                (
                    this.TotalTAC == input.TotalTAC ||
                    (this.TotalTAC != null &&
                    this.TotalTAC.Equals(input.TotalTAC))
                ) && 
                (
                    this.TotalTBC == input.TotalTBC ||
                    (this.TotalTBC != null &&
                    this.TotalTBC.Equals(input.TotalTBC))
                ) && 
                (
                    this.TotalTime == input.TotalTime ||
                    (this.TotalTime != null &&
                    this.TotalTime.Equals(input.TotalTime))
                ) && 
                (
                    this.OperationWSIs == input.OperationWSIs ||
                    this.OperationWSIs != null &&
                    this.OperationWSIs.SequenceEqual(input.OperationWSIs)
                ) && 
                (
                    this.GroupID == input.GroupID ||
                    (this.GroupID != null &&
                    this.GroupID.Equals(input.GroupID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.CorrectedAtDATE == input.CorrectedAtDATE ||
                    (this.CorrectedAtDATE != null &&
                    this.CorrectedAtDATE.Equals(input.CorrectedAtDATE))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.JobOrdersRoutinesID != null)
                    hashCode = hashCode * 59 + this.JobOrdersRoutinesID.GetHashCode();
                if (this.JobOrdersSNID != null)
                    hashCode = hashCode * 59 + this.JobOrdersSNID.GetHashCode();
                if (this.PerformedByID != null)
                    hashCode = hashCode * 59 + this.PerformedByID.GetHashCode();
                if (this.StartedAtDATE != null)
                    hashCode = hashCode * 59 + this.StartedAtDATE.GetHashCode();
                if (this.FinishedAtDATE != null)
                    hashCode = hashCode * 59 + this.FinishedAtDATE.GetHashCode();
                if (this.Duration != null)
                    hashCode = hashCode * 59 + this.Duration.GetHashCode();
                if (this.OperationResult != null)
                    hashCode = hashCode * 59 + this.OperationResult.GetHashCode();
                if (this.Quantity != null)
                    hashCode = hashCode * 59 + this.Quantity.GetHashCode();
                if (this.DivisionID != null)
                    hashCode = hashCode * 59 + this.DivisionID.GetHashCode();
                if (this.SalaryClassID != null)
                    hashCode = hashCode * 59 + this.SalaryClassID.GetHashCode();
                if (this.HourlyRate != null)
                    hashCode = hashCode * 59 + this.HourlyRate.GetHashCode();
                if (this.SalaryCosts != null)
                    hashCode = hashCode * 59 + this.SalaryCosts.GetHashCode();
                if (this.OverheadCosts != null)
                    hashCode = hashCode * 59 + this.OverheadCosts.GetHashCode();
                if (this.GeneralExpense != null)
                    hashCode = hashCode * 59 + this.GeneralExpense.GetHashCode();
                if (this.Cooperation != null)
                    hashCode = hashCode * 59 + this.Cooperation.GetHashCode();
                if (this.AggregateWorkTicketID != null)
                    hashCode = hashCode * 59 + this.AggregateWorkTicketID.GetHashCode();
                if (this.BusOrderID != null)
                    hashCode = hashCode * 59 + this.BusOrderID.GetHashCode();
                if (this.BusTransactionID != null)
                    hashCode = hashCode * 59 + this.BusTransactionID.GetHashCode();
                if (this.QUnit != null)
                    hashCode = hashCode * 59 + this.QUnit.GetHashCode();
                if (this.UnitRate != null)
                    hashCode = hashCode * 59 + this.UnitRate.GetHashCode();
                if (this.UnitQuantity != null)
                    hashCode = hashCode * 59 + this.UnitQuantity.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.DocDateDATE != null)
                    hashCode = hashCode * 59 + this.DocDateDATE.GetHashCode();
                if (this.ValueTransfer != null)
                    hashCode = hashCode * 59 + this.ValueTransfer.GetHashCode();
                if (this.WageOperationTypeID != null)
                    hashCode = hashCode * 59 + this.WageOperationTypeID.GetHashCode();
                if (this.FinishedProductRowID != null)
                    hashCode = hashCode * 59 + this.FinishedProductRowID.GetHashCode();
                if (this.BusProjectID != null)
                    hashCode = hashCode * 59 + this.BusProjectID.GetHashCode();
                if (this.WorkPlaceID != null)
                    hashCode = hashCode * 59 + this.WorkPlaceID.GetHashCode();
                if (this.TotalTAC != null)
                    hashCode = hashCode * 59 + this.TotalTAC.GetHashCode();
                if (this.TotalTBC != null)
                    hashCode = hashCode * 59 + this.TotalTBC.GetHashCode();
                if (this.TotalTime != null)
                    hashCode = hashCode * 59 + this.TotalTime.GetHashCode();
                if (this.OperationWSIs != null)
                    hashCode = hashCode * 59 + this.OperationWSIs.GetHashCode();
                if (this.GroupID != null)
                    hashCode = hashCode * 59 + this.GroupID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.CorrectedAtDATE != null)
                    hashCode = hashCode * 59 + this.CorrectedAtDATE.GetHashCode();
                return hashCode;
            }
        }
    }

}
