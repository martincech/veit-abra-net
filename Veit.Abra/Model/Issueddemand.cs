/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Issueddemand
    /// </summary>
    [DataContract]
    public partial class Issueddemand :  IEquatable<Issueddemand>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Issueddemand" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Poptávka vydaná - řádek [nepersistentní položka].</param>
        /// <param name="docQueueID">Zdrojová řada; ID objektu Řada dokladů [persistentní položka].</param>
        /// <param name="periodID">Období; ID objektu Období [persistentní položka].</param>
        /// <param name="ordNumber">Pořadové číslo [persistentní položka].</param>
        /// <param name="docDateDATE">Datum dok. [persistentní položka].</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="newRelatedType">Typ relace.</param>
        /// <param name="newRelatedDocumentID">ID dokladu pro připojení.</param>
        /// <param name="firmID">Firma dodavatele; ID objektu Firma [persistentní položka].</param>
        /// <param name="firmOfficeID">Provozovna firmy dodavatele; ID objektu Provozovna [persistentní položka].</param>
        /// <param name="personID">Osoba ve firmě dodavatele; ID objektu Osoba [persistentní položka].</param>
        /// <param name="currencyID">Měna; ID objektu Měna [persistentní položka].</param>
        /// <param name="currRate">Kurz měny [persistentní položka].</param>
        /// <param name="refCurrRate">Vztažný kurz měny [persistentní položka].</param>
        /// <param name="requestedAnswerDateDATE">Požadovaný termín dodání odpovědi [persistentní položka].</param>
        /// <param name="validToDateDATE">Dokdy je nabídka dodavatele platná [persistentní položka].</param>
        /// <param name="transportationTypeID">Způsob dopravy; ID objektu Způsob dopravy [persistentní položka].</param>
        /// <param name="otherCosts">Odhad ostatních nákladů [persistentní položka].</param>
        /// <param name="localOtherCosts">Odhad ostatních nákladů v lokální měně [persistentní položka].</param>
        /// <param name="communicationTypeID">Způsob komunikace; ID objektu Způsob komunikace [persistentní položka].</param>
        /// <param name="note">Poznámka [persistentní položka].</param>
        /// <param name="addressID">Vlastní adresa; ID objektu Adresa [persistentní položka].</param>
        /// <param name="pricePrecision">Zobrazení desetinných míst pro zadávací částky [persistentní položka].</param>
        /// <param name="demandFailureReasonID">Důvod neúspěchu; ID objektu Důvod neúspěchu oslovených dodavatelů [persistentní položka].</param>
        /// <param name="demandFailureComment">Komentář důvodů neúspěchu [persistentní položka].</param>
        public Issueddemand(List<Issueddemandrow> rows = default(List<Issueddemandrow>), string docQueueID = default(string), string periodID = default(string), int? ordNumber = default(int?), DateTimeOffset? docDateDATE = default(DateTimeOffset?), string createdByID = default(string), string correctedByID = default(string), int? newRelatedType = default(int?), string newRelatedDocumentID = default(string), string firmID = default(string), string firmOfficeID = default(string), string personID = default(string), string currencyID = default(string), double? currRate = default(double?), double? refCurrRate = default(double?), DateTimeOffset? requestedAnswerDateDATE = default(DateTimeOffset?), DateTimeOffset? validToDateDATE = default(DateTimeOffset?), string transportationTypeID = default(string), double? otherCosts = default(double?), double? localOtherCosts = default(double?), string communicationTypeID = default(string), string note = default(string), string addressID = default(string), int? pricePrecision = default(int?), string demandFailureReasonID = default(string), string demandFailureComment = default(string))
        {
            this.Rows = rows;
            this.DocQueueID = docQueueID;
            this.PeriodID = periodID;
            this.OrdNumber = ordNumber;
            this.DocDateDATE = docDateDATE;
            this.CreatedByID = createdByID;
            this.CorrectedByID = correctedByID;
            this.NewRelatedType = newRelatedType;
            this.NewRelatedDocumentID = newRelatedDocumentID;
            this.FirmID = firmID;
            this.FirmOfficeID = firmOfficeID;
            this.PersonID = personID;
            this.CurrencyID = currencyID;
            this.CurrRate = currRate;
            this.RefCurrRate = refCurrRate;
            this.RequestedAnswerDateDATE = requestedAnswerDateDATE;
            this.ValidToDateDATE = validToDateDATE;
            this.TransportationTypeID = transportationTypeID;
            this.OtherCosts = otherCosts;
            this.LocalOtherCosts = localOtherCosts;
            this.CommunicationTypeID = communicationTypeID;
            this.Note = note;
            this.AddressID = addressID;
            this.PricePrecision = pricePrecision;
            this.DemandFailureReasonID = demandFailureReasonID;
            this.DemandFailureComment = demandFailureComment;
        }
        
        /// <summary>
        /// Číslo dok.
        /// </summary>
        /// <value>Číslo dok.</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Poptávka vydaná - řádek [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Poptávka vydaná - řádek [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Issueddemandrow> Rows { get; set; }

        /// <summary>
        /// Zdrojová řada; ID objektu Řada dokladů [persistentní položka]
        /// </summary>
        /// <value>Zdrojová řada; ID objektu Řada dokladů [persistentní položka]</value>
        [DataMember(Name="DocQueue_ID", EmitDefaultValue=false)]
        public string DocQueueID { get; set; }

        /// <summary>
        /// Období; ID objektu Období [persistentní položka]
        /// </summary>
        /// <value>Období; ID objektu Období [persistentní položka]</value>
        [DataMember(Name="Period_ID", EmitDefaultValue=false)]
        public string PeriodID { get; set; }

        /// <summary>
        /// Pořadové číslo [persistentní položka]
        /// </summary>
        /// <value>Pořadové číslo [persistentní položka]</value>
        [DataMember(Name="OrdNumber", EmitDefaultValue=false)]
        public int? OrdNumber { get; set; }

        /// <summary>
        /// Datum dok. [persistentní položka]
        /// </summary>
        /// <value>Datum dok. [persistentní položka]</value>
        [DataMember(Name="DocDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DocDateDATE { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Typ relace
        /// </summary>
        /// <value>Typ relace</value>
        [DataMember(Name="NewRelatedType", EmitDefaultValue=false)]
        public int? NewRelatedType { get; set; }

        /// <summary>
        /// ID dokladu pro připojení
        /// </summary>
        /// <value>ID dokladu pro připojení</value>
        [DataMember(Name="NewRelatedDocument_ID", EmitDefaultValue=false)]
        public string NewRelatedDocumentID { get; set; }

        /// <summary>
        /// Firma dodavatele; ID objektu Firma [persistentní položka]
        /// </summary>
        /// <value>Firma dodavatele; ID objektu Firma [persistentní položka]</value>
        [DataMember(Name="Firm_ID", EmitDefaultValue=false)]
        public string FirmID { get; set; }

        /// <summary>
        /// Provozovna firmy dodavatele; ID objektu Provozovna [persistentní položka]
        /// </summary>
        /// <value>Provozovna firmy dodavatele; ID objektu Provozovna [persistentní položka]</value>
        [DataMember(Name="FirmOffice_ID", EmitDefaultValue=false)]
        public string FirmOfficeID { get; set; }

        /// <summary>
        /// Osoba ve firmě dodavatele; ID objektu Osoba [persistentní položka]
        /// </summary>
        /// <value>Osoba ve firmě dodavatele; ID objektu Osoba [persistentní položka]</value>
        [DataMember(Name="Person_ID", EmitDefaultValue=false)]
        public string PersonID { get; set; }

        /// <summary>
        /// Měna; ID objektu Měna [persistentní položka]
        /// </summary>
        /// <value>Měna; ID objektu Měna [persistentní položka]</value>
        [DataMember(Name="Currency_ID", EmitDefaultValue=false)]
        public string CurrencyID { get; set; }

        /// <summary>
        /// Kurz měny [persistentní položka]
        /// </summary>
        /// <value>Kurz měny [persistentní položka]</value>
        [DataMember(Name="CurrRate", EmitDefaultValue=false)]
        public double? CurrRate { get; set; }

        /// <summary>
        /// Vztažný kurz měny [persistentní položka]
        /// </summary>
        /// <value>Vztažný kurz měny [persistentní položka]</value>
        [DataMember(Name="RefCurrRate", EmitDefaultValue=false)]
        public double? RefCurrRate { get; set; }

        /// <summary>
        /// Požadovaný termín dodání odpovědi [persistentní položka]
        /// </summary>
        /// <value>Požadovaný termín dodání odpovědi [persistentní položka]</value>
        [DataMember(Name="RequestedAnswerDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? RequestedAnswerDateDATE { get; set; }

        /// <summary>
        /// Dokdy je nabídka dodavatele platná [persistentní položka]
        /// </summary>
        /// <value>Dokdy je nabídka dodavatele platná [persistentní položka]</value>
        [DataMember(Name="ValidToDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ValidToDateDATE { get; set; }

        /// <summary>
        /// Způsob dopravy; ID objektu Způsob dopravy [persistentní položka]
        /// </summary>
        /// <value>Způsob dopravy; ID objektu Způsob dopravy [persistentní položka]</value>
        [DataMember(Name="TransportationType_ID", EmitDefaultValue=false)]
        public string TransportationTypeID { get; set; }

        /// <summary>
        /// Odhad ostatních nákladů [persistentní položka]
        /// </summary>
        /// <value>Odhad ostatních nákladů [persistentní položka]</value>
        [DataMember(Name="OtherCosts", EmitDefaultValue=false)]
        public double? OtherCosts { get; set; }

        /// <summary>
        /// Odhad ostatních nákladů v lokální měně [persistentní položka]
        /// </summary>
        /// <value>Odhad ostatních nákladů v lokální měně [persistentní položka]</value>
        [DataMember(Name="LocalOtherCosts", EmitDefaultValue=false)]
        public double? LocalOtherCosts { get; set; }

        /// <summary>
        /// Způsob komunikace; ID objektu Způsob komunikace [persistentní položka]
        /// </summary>
        /// <value>Způsob komunikace; ID objektu Způsob komunikace [persistentní položka]</value>
        [DataMember(Name="CommunicationType_ID", EmitDefaultValue=false)]
        public string CommunicationTypeID { get; set; }

        /// <summary>
        /// Poznámka [persistentní položka]
        /// </summary>
        /// <value>Poznámka [persistentní položka]</value>
        [DataMember(Name="Note", EmitDefaultValue=false)]
        public string Note { get; set; }

        /// <summary>
        /// Ref.měna; ID objektu Měna
        /// </summary>
        /// <value>Ref.měna; ID objektu Měna</value>
        [DataMember(Name="RefCurrency_ID", EmitDefaultValue=false)]
        public string RefCurrencyID { get; private set; }

        /// <summary>
        /// Lok.ref.měna; ID objektu Měna
        /// </summary>
        /// <value>Lok.ref.měna; ID objektu Měna</value>
        [DataMember(Name="LocalRefCurrency_ID", EmitDefaultValue=false)]
        public string LocalRefCurrencyID { get; private set; }

        /// <summary>
        /// Vlastní adresa; ID objektu Adresa [persistentní položka]
        /// </summary>
        /// <value>Vlastní adresa; ID objektu Adresa [persistentní položka]</value>
        [DataMember(Name="Address_ID", EmitDefaultValue=false)]
        public string AddressID { get; set; }

        /// <summary>
        /// Zobrazení desetinných míst pro zadávací částky [persistentní položka]
        /// </summary>
        /// <value>Zobrazení desetinných míst pro zadávací částky [persistentní položka]</value>
        [DataMember(Name="PricePrecision", EmitDefaultValue=false)]
        public int? PricePrecision { get; set; }

        /// <summary>
        /// Důvod neúspěchu; ID objektu Důvod neúspěchu oslovených dodavatelů [persistentní položka]
        /// </summary>
        /// <value>Důvod neúspěchu; ID objektu Důvod neúspěchu oslovených dodavatelů [persistentní položka]</value>
        [DataMember(Name="DemandFailureReason_ID", EmitDefaultValue=false)]
        public string DemandFailureReasonID { get; set; }

        /// <summary>
        /// Komentář důvodů neúspěchu [persistentní položka]
        /// </summary>
        /// <value>Komentář důvodů neúspěchu [persistentní položka]</value>
        [DataMember(Name="DemandFailureComment", EmitDefaultValue=false)]
        public string DemandFailureComment { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Issueddemand {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  DocQueueID: ").Append(DocQueueID).Append("\n");
            sb.Append("  PeriodID: ").Append(PeriodID).Append("\n");
            sb.Append("  OrdNumber: ").Append(OrdNumber).Append("\n");
            sb.Append("  DocDateDATE: ").Append(DocDateDATE).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  NewRelatedType: ").Append(NewRelatedType).Append("\n");
            sb.Append("  NewRelatedDocumentID: ").Append(NewRelatedDocumentID).Append("\n");
            sb.Append("  FirmID: ").Append(FirmID).Append("\n");
            sb.Append("  FirmOfficeID: ").Append(FirmOfficeID).Append("\n");
            sb.Append("  PersonID: ").Append(PersonID).Append("\n");
            sb.Append("  CurrencyID: ").Append(CurrencyID).Append("\n");
            sb.Append("  CurrRate: ").Append(CurrRate).Append("\n");
            sb.Append("  RefCurrRate: ").Append(RefCurrRate).Append("\n");
            sb.Append("  RequestedAnswerDateDATE: ").Append(RequestedAnswerDateDATE).Append("\n");
            sb.Append("  ValidToDateDATE: ").Append(ValidToDateDATE).Append("\n");
            sb.Append("  TransportationTypeID: ").Append(TransportationTypeID).Append("\n");
            sb.Append("  OtherCosts: ").Append(OtherCosts).Append("\n");
            sb.Append("  LocalOtherCosts: ").Append(LocalOtherCosts).Append("\n");
            sb.Append("  CommunicationTypeID: ").Append(CommunicationTypeID).Append("\n");
            sb.Append("  Note: ").Append(Note).Append("\n");
            sb.Append("  RefCurrencyID: ").Append(RefCurrencyID).Append("\n");
            sb.Append("  LocalRefCurrencyID: ").Append(LocalRefCurrencyID).Append("\n");
            sb.Append("  AddressID: ").Append(AddressID).Append("\n");
            sb.Append("  PricePrecision: ").Append(PricePrecision).Append("\n");
            sb.Append("  DemandFailureReasonID: ").Append(DemandFailureReasonID).Append("\n");
            sb.Append("  DemandFailureComment: ").Append(DemandFailureComment).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Issueddemand);
        }

        /// <summary>
        /// Returns true if Issueddemand instances are equal
        /// </summary>
        /// <param name="input">Instance of Issueddemand to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Issueddemand input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.DocQueueID == input.DocQueueID ||
                    (this.DocQueueID != null &&
                    this.DocQueueID.Equals(input.DocQueueID))
                ) && 
                (
                    this.PeriodID == input.PeriodID ||
                    (this.PeriodID != null &&
                    this.PeriodID.Equals(input.PeriodID))
                ) && 
                (
                    this.OrdNumber == input.OrdNumber ||
                    (this.OrdNumber != null &&
                    this.OrdNumber.Equals(input.OrdNumber))
                ) && 
                (
                    this.DocDateDATE == input.DocDateDATE ||
                    (this.DocDateDATE != null &&
                    this.DocDateDATE.Equals(input.DocDateDATE))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.NewRelatedType == input.NewRelatedType ||
                    (this.NewRelatedType != null &&
                    this.NewRelatedType.Equals(input.NewRelatedType))
                ) && 
                (
                    this.NewRelatedDocumentID == input.NewRelatedDocumentID ||
                    (this.NewRelatedDocumentID != null &&
                    this.NewRelatedDocumentID.Equals(input.NewRelatedDocumentID))
                ) && 
                (
                    this.FirmID == input.FirmID ||
                    (this.FirmID != null &&
                    this.FirmID.Equals(input.FirmID))
                ) && 
                (
                    this.FirmOfficeID == input.FirmOfficeID ||
                    (this.FirmOfficeID != null &&
                    this.FirmOfficeID.Equals(input.FirmOfficeID))
                ) && 
                (
                    this.PersonID == input.PersonID ||
                    (this.PersonID != null &&
                    this.PersonID.Equals(input.PersonID))
                ) && 
                (
                    this.CurrencyID == input.CurrencyID ||
                    (this.CurrencyID != null &&
                    this.CurrencyID.Equals(input.CurrencyID))
                ) && 
                (
                    this.CurrRate == input.CurrRate ||
                    (this.CurrRate != null &&
                    this.CurrRate.Equals(input.CurrRate))
                ) && 
                (
                    this.RefCurrRate == input.RefCurrRate ||
                    (this.RefCurrRate != null &&
                    this.RefCurrRate.Equals(input.RefCurrRate))
                ) && 
                (
                    this.RequestedAnswerDateDATE == input.RequestedAnswerDateDATE ||
                    (this.RequestedAnswerDateDATE != null &&
                    this.RequestedAnswerDateDATE.Equals(input.RequestedAnswerDateDATE))
                ) && 
                (
                    this.ValidToDateDATE == input.ValidToDateDATE ||
                    (this.ValidToDateDATE != null &&
                    this.ValidToDateDATE.Equals(input.ValidToDateDATE))
                ) && 
                (
                    this.TransportationTypeID == input.TransportationTypeID ||
                    (this.TransportationTypeID != null &&
                    this.TransportationTypeID.Equals(input.TransportationTypeID))
                ) && 
                (
                    this.OtherCosts == input.OtherCosts ||
                    (this.OtherCosts != null &&
                    this.OtherCosts.Equals(input.OtherCosts))
                ) && 
                (
                    this.LocalOtherCosts == input.LocalOtherCosts ||
                    (this.LocalOtherCosts != null &&
                    this.LocalOtherCosts.Equals(input.LocalOtherCosts))
                ) && 
                (
                    this.CommunicationTypeID == input.CommunicationTypeID ||
                    (this.CommunicationTypeID != null &&
                    this.CommunicationTypeID.Equals(input.CommunicationTypeID))
                ) && 
                (
                    this.Note == input.Note ||
                    (this.Note != null &&
                    this.Note.Equals(input.Note))
                ) && 
                (
                    this.RefCurrencyID == input.RefCurrencyID ||
                    (this.RefCurrencyID != null &&
                    this.RefCurrencyID.Equals(input.RefCurrencyID))
                ) && 
                (
                    this.LocalRefCurrencyID == input.LocalRefCurrencyID ||
                    (this.LocalRefCurrencyID != null &&
                    this.LocalRefCurrencyID.Equals(input.LocalRefCurrencyID))
                ) && 
                (
                    this.AddressID == input.AddressID ||
                    (this.AddressID != null &&
                    this.AddressID.Equals(input.AddressID))
                ) && 
                (
                    this.PricePrecision == input.PricePrecision ||
                    (this.PricePrecision != null &&
                    this.PricePrecision.Equals(input.PricePrecision))
                ) && 
                (
                    this.DemandFailureReasonID == input.DemandFailureReasonID ||
                    (this.DemandFailureReasonID != null &&
                    this.DemandFailureReasonID.Equals(input.DemandFailureReasonID))
                ) && 
                (
                    this.DemandFailureComment == input.DemandFailureComment ||
                    (this.DemandFailureComment != null &&
                    this.DemandFailureComment.Equals(input.DemandFailureComment))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.DocQueueID != null)
                    hashCode = hashCode * 59 + this.DocQueueID.GetHashCode();
                if (this.PeriodID != null)
                    hashCode = hashCode * 59 + this.PeriodID.GetHashCode();
                if (this.OrdNumber != null)
                    hashCode = hashCode * 59 + this.OrdNumber.GetHashCode();
                if (this.DocDateDATE != null)
                    hashCode = hashCode * 59 + this.DocDateDATE.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.NewRelatedType != null)
                    hashCode = hashCode * 59 + this.NewRelatedType.GetHashCode();
                if (this.NewRelatedDocumentID != null)
                    hashCode = hashCode * 59 + this.NewRelatedDocumentID.GetHashCode();
                if (this.FirmID != null)
                    hashCode = hashCode * 59 + this.FirmID.GetHashCode();
                if (this.FirmOfficeID != null)
                    hashCode = hashCode * 59 + this.FirmOfficeID.GetHashCode();
                if (this.PersonID != null)
                    hashCode = hashCode * 59 + this.PersonID.GetHashCode();
                if (this.CurrencyID != null)
                    hashCode = hashCode * 59 + this.CurrencyID.GetHashCode();
                if (this.CurrRate != null)
                    hashCode = hashCode * 59 + this.CurrRate.GetHashCode();
                if (this.RefCurrRate != null)
                    hashCode = hashCode * 59 + this.RefCurrRate.GetHashCode();
                if (this.RequestedAnswerDateDATE != null)
                    hashCode = hashCode * 59 + this.RequestedAnswerDateDATE.GetHashCode();
                if (this.ValidToDateDATE != null)
                    hashCode = hashCode * 59 + this.ValidToDateDATE.GetHashCode();
                if (this.TransportationTypeID != null)
                    hashCode = hashCode * 59 + this.TransportationTypeID.GetHashCode();
                if (this.OtherCosts != null)
                    hashCode = hashCode * 59 + this.OtherCosts.GetHashCode();
                if (this.LocalOtherCosts != null)
                    hashCode = hashCode * 59 + this.LocalOtherCosts.GetHashCode();
                if (this.CommunicationTypeID != null)
                    hashCode = hashCode * 59 + this.CommunicationTypeID.GetHashCode();
                if (this.Note != null)
                    hashCode = hashCode * 59 + this.Note.GetHashCode();
                if (this.RefCurrencyID != null)
                    hashCode = hashCode * 59 + this.RefCurrencyID.GetHashCode();
                if (this.LocalRefCurrencyID != null)
                    hashCode = hashCode * 59 + this.LocalRefCurrencyID.GetHashCode();
                if (this.AddressID != null)
                    hashCode = hashCode * 59 + this.AddressID.GetHashCode();
                if (this.PricePrecision != null)
                    hashCode = hashCode * 59 + this.PricePrecision.GetHashCode();
                if (this.DemandFailureReasonID != null)
                    hashCode = hashCode * 59 + this.DemandFailureReasonID.GetHashCode();
                if (this.DemandFailureComment != null)
                    hashCode = hashCode * 59 + this.DemandFailureComment.GetHashCode();
                return hashCode;
            }
        }
    }

}
