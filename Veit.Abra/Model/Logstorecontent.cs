/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Logstorecontent
    /// </summary>
    [DataContract]
    public partial class Logstorecontent :  IEquatable<Logstorecontent>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Logstorecontent" /> class.
        /// </summary>
        /// <param name="storeCardID">Skladová karta; ID objektu Skladová karta [persistentní položka].</param>
        /// <param name="storeBatchID">Sériové číslo/šarže; ID objektu Šarže/sériové číslo [persistentní položka].</param>
        /// <param name="qUnit">Jednotka [persistentní položka].</param>
        /// <param name="unitRate">Převodní vztah jedn. [persistentní položka].</param>
        /// <param name="quantity">Množství [persistentní položka].</param>
        /// <param name="quantityReserved">Rezervované množství [persistentní položka].</param>
        /// <param name="quantityAwaited">Očekávané množství [persistentní položka].</param>
        /// <param name="dateOfStorageDATE">Datum naskladnění [persistentní položka].</param>
        /// <param name="invQuantity">Skutečné množství [persistentní položka].</param>
        /// <param name="invChange">Změna v průběhu inventury [persistentní položka].</param>
        /// <param name="unitEditInvQuantity">Skutečné množství (pro editaci) v jednotce.</param>
        /// <param name="invUnitQuantity">Skutečné množství v jednotce.</param>
        /// <param name="totalCapacity">Celkový objem v jednotce [persistentní položka].</param>
        /// <param name="capacityUnit">Jednotka objemu [persistentní položka].</param>
        /// <param name="totalWeight">Celková váha [persistentní položka].</param>
        /// <param name="weightUnit">Jednotka hmotnosti [persistentní položka].</param>
        public Logstorecontent(string storeCardID = default(string), string storeBatchID = default(string), string qUnit = default(string), double? unitRate = default(double?), double? quantity = default(double?), double? quantityReserved = default(double?), double? quantityAwaited = default(double?), DateTimeOffset? dateOfStorageDATE = default(DateTimeOffset?), double? invQuantity = default(double?), bool? invChange = default(bool?), double? unitEditInvQuantity = default(double?), double? invUnitQuantity = default(double?), double? totalCapacity = default(double?), int? capacityUnit = default(int?), double? totalWeight = default(double?), int? weightUnit = default(int?))
        {
            this.StoreCardID = storeCardID;
            this.StoreBatchID = storeBatchID;
            this.QUnit = qUnit;
            this.UnitRate = unitRate;
            this.Quantity = quantity;
            this.QuantityReserved = quantityReserved;
            this.QuantityAwaited = quantityAwaited;
            this.DateOfStorageDATE = dateOfStorageDATE;
            this.InvQuantity = invQuantity;
            this.InvChange = invChange;
            this.UnitEditInvQuantity = unitEditInvQuantity;
            this.InvUnitQuantity = invUnitQuantity;
            this.TotalCapacity = totalCapacity;
            this.CapacityUnit = capacityUnit;
            this.TotalWeight = totalWeight;
            this.WeightUnit = weightUnit;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Skladová pozice [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Skladová pozice [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Skladová karta; ID objektu Skladová karta [persistentní položka]
        /// </summary>
        /// <value>Skladová karta; ID objektu Skladová karta [persistentní položka]</value>
        [DataMember(Name="StoreCard_ID", EmitDefaultValue=false)]
        public string StoreCardID { get; set; }

        /// <summary>
        /// Sériové číslo/šarže; ID objektu Šarže/sériové číslo [persistentní položka]
        /// </summary>
        /// <value>Sériové číslo/šarže; ID objektu Šarže/sériové číslo [persistentní položka]</value>
        [DataMember(Name="StoreBatch_ID", EmitDefaultValue=false)]
        public string StoreBatchID { get; set; }

        /// <summary>
        /// Jednotka [persistentní položka]
        /// </summary>
        /// <value>Jednotka [persistentní položka]</value>
        [DataMember(Name="QUnit", EmitDefaultValue=false)]
        public string QUnit { get; set; }

        /// <summary>
        /// Převodní vztah jedn. [persistentní položka]
        /// </summary>
        /// <value>Převodní vztah jedn. [persistentní položka]</value>
        [DataMember(Name="UnitRate", EmitDefaultValue=false)]
        public double? UnitRate { get; set; }

        /// <summary>
        /// Množství [persistentní položka]
        /// </summary>
        /// <value>Množství [persistentní položka]</value>
        [DataMember(Name="Quantity", EmitDefaultValue=false)]
        public double? Quantity { get; set; }

        /// <summary>
        /// Rezervované množství [persistentní položka]
        /// </summary>
        /// <value>Rezervované množství [persistentní položka]</value>
        [DataMember(Name="QuantityReserved", EmitDefaultValue=false)]
        public double? QuantityReserved { get; set; }

        /// <summary>
        /// Očekávané množství [persistentní položka]
        /// </summary>
        /// <value>Očekávané množství [persistentní položka]</value>
        [DataMember(Name="QuantityAwaited", EmitDefaultValue=false)]
        public double? QuantityAwaited { get; set; }

        /// <summary>
        /// Datum naskladnění [persistentní položka]
        /// </summary>
        /// <value>Datum naskladnění [persistentní položka]</value>
        [DataMember(Name="DateOfStorage$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DateOfStorageDATE { get; set; }

        /// <summary>
        /// Skutečné množství [persistentní položka]
        /// </summary>
        /// <value>Skutečné množství [persistentní položka]</value>
        [DataMember(Name="InvQuantity", EmitDefaultValue=false)]
        public double? InvQuantity { get; set; }

        /// <summary>
        /// Změna v průběhu inventury [persistentní položka]
        /// </summary>
        /// <value>Změna v průběhu inventury [persistentní položka]</value>
        [DataMember(Name="InvChange", EmitDefaultValue=false)]
        public bool? InvChange { get; set; }

        /// <summary>
        /// Množství v jednotce
        /// </summary>
        /// <value>Množství v jednotce</value>
        [DataMember(Name="UnitQuantity", EmitDefaultValue=false)]
        public double? UnitQuantity { get; private set; }

        /// <summary>
        /// Rezervované množství v jednotce
        /// </summary>
        /// <value>Rezervované množství v jednotce</value>
        [DataMember(Name="UnitQuantityReserved", EmitDefaultValue=false)]
        public double? UnitQuantityReserved { get; private set; }

        /// <summary>
        /// Očekávané množství v jednotce
        /// </summary>
        /// <value>Očekávané množství v jednotce</value>
        [DataMember(Name="UnitQuantityAwaited", EmitDefaultValue=false)]
        public double? UnitQuantityAwaited { get; private set; }

        /// <summary>
        /// Skutečné množství (pro editaci) v jednotce
        /// </summary>
        /// <value>Skutečné množství (pro editaci) v jednotce</value>
        [DataMember(Name="UnitEditInvQuantity", EmitDefaultValue=false)]
        public double? UnitEditInvQuantity { get; set; }

        /// <summary>
        /// Skutečné množství v jednotce
        /// </summary>
        /// <value>Skutečné množství v jednotce</value>
        [DataMember(Name="InvUnitQuantity", EmitDefaultValue=false)]
        public double? InvUnitQuantity { get; set; }

        /// <summary>
        /// Celkový objem v jednotce [persistentní položka]
        /// </summary>
        /// <value>Celkový objem v jednotce [persistentní položka]</value>
        [DataMember(Name="TotalCapacity", EmitDefaultValue=false)]
        public double? TotalCapacity { get; set; }

        /// <summary>
        /// Jednotka objemu [persistentní položka]
        /// </summary>
        /// <value>Jednotka objemu [persistentní položka]</value>
        [DataMember(Name="CapacityUnit", EmitDefaultValue=false)]
        public int? CapacityUnit { get; set; }

        /// <summary>
        /// Celková váha [persistentní položka]
        /// </summary>
        /// <value>Celková váha [persistentní položka]</value>
        [DataMember(Name="TotalWeight", EmitDefaultValue=false)]
        public double? TotalWeight { get; set; }

        /// <summary>
        /// Jednotka hmotnosti [persistentní položka]
        /// </summary>
        /// <value>Jednotka hmotnosti [persistentní položka]</value>
        [DataMember(Name="WeightUnit", EmitDefaultValue=false)]
        public int? WeightUnit { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Logstorecontent {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  StoreCardID: ").Append(StoreCardID).Append("\n");
            sb.Append("  StoreBatchID: ").Append(StoreBatchID).Append("\n");
            sb.Append("  QUnit: ").Append(QUnit).Append("\n");
            sb.Append("  UnitRate: ").Append(UnitRate).Append("\n");
            sb.Append("  Quantity: ").Append(Quantity).Append("\n");
            sb.Append("  QuantityReserved: ").Append(QuantityReserved).Append("\n");
            sb.Append("  QuantityAwaited: ").Append(QuantityAwaited).Append("\n");
            sb.Append("  DateOfStorageDATE: ").Append(DateOfStorageDATE).Append("\n");
            sb.Append("  InvQuantity: ").Append(InvQuantity).Append("\n");
            sb.Append("  InvChange: ").Append(InvChange).Append("\n");
            sb.Append("  UnitQuantity: ").Append(UnitQuantity).Append("\n");
            sb.Append("  UnitQuantityReserved: ").Append(UnitQuantityReserved).Append("\n");
            sb.Append("  UnitQuantityAwaited: ").Append(UnitQuantityAwaited).Append("\n");
            sb.Append("  UnitEditInvQuantity: ").Append(UnitEditInvQuantity).Append("\n");
            sb.Append("  InvUnitQuantity: ").Append(InvUnitQuantity).Append("\n");
            sb.Append("  TotalCapacity: ").Append(TotalCapacity).Append("\n");
            sb.Append("  CapacityUnit: ").Append(CapacityUnit).Append("\n");
            sb.Append("  TotalWeight: ").Append(TotalWeight).Append("\n");
            sb.Append("  WeightUnit: ").Append(WeightUnit).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Logstorecontent);
        }

        /// <summary>
        /// Returns true if Logstorecontent instances are equal
        /// </summary>
        /// <param name="input">Instance of Logstorecontent to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Logstorecontent input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.StoreCardID == input.StoreCardID ||
                    (this.StoreCardID != null &&
                    this.StoreCardID.Equals(input.StoreCardID))
                ) && 
                (
                    this.StoreBatchID == input.StoreBatchID ||
                    (this.StoreBatchID != null &&
                    this.StoreBatchID.Equals(input.StoreBatchID))
                ) && 
                (
                    this.QUnit == input.QUnit ||
                    (this.QUnit != null &&
                    this.QUnit.Equals(input.QUnit))
                ) && 
                (
                    this.UnitRate == input.UnitRate ||
                    (this.UnitRate != null &&
                    this.UnitRate.Equals(input.UnitRate))
                ) && 
                (
                    this.Quantity == input.Quantity ||
                    (this.Quantity != null &&
                    this.Quantity.Equals(input.Quantity))
                ) && 
                (
                    this.QuantityReserved == input.QuantityReserved ||
                    (this.QuantityReserved != null &&
                    this.QuantityReserved.Equals(input.QuantityReserved))
                ) && 
                (
                    this.QuantityAwaited == input.QuantityAwaited ||
                    (this.QuantityAwaited != null &&
                    this.QuantityAwaited.Equals(input.QuantityAwaited))
                ) && 
                (
                    this.DateOfStorageDATE == input.DateOfStorageDATE ||
                    (this.DateOfStorageDATE != null &&
                    this.DateOfStorageDATE.Equals(input.DateOfStorageDATE))
                ) && 
                (
                    this.InvQuantity == input.InvQuantity ||
                    (this.InvQuantity != null &&
                    this.InvQuantity.Equals(input.InvQuantity))
                ) && 
                (
                    this.InvChange == input.InvChange ||
                    (this.InvChange != null &&
                    this.InvChange.Equals(input.InvChange))
                ) && 
                (
                    this.UnitQuantity == input.UnitQuantity ||
                    (this.UnitQuantity != null &&
                    this.UnitQuantity.Equals(input.UnitQuantity))
                ) && 
                (
                    this.UnitQuantityReserved == input.UnitQuantityReserved ||
                    (this.UnitQuantityReserved != null &&
                    this.UnitQuantityReserved.Equals(input.UnitQuantityReserved))
                ) && 
                (
                    this.UnitQuantityAwaited == input.UnitQuantityAwaited ||
                    (this.UnitQuantityAwaited != null &&
                    this.UnitQuantityAwaited.Equals(input.UnitQuantityAwaited))
                ) && 
                (
                    this.UnitEditInvQuantity == input.UnitEditInvQuantity ||
                    (this.UnitEditInvQuantity != null &&
                    this.UnitEditInvQuantity.Equals(input.UnitEditInvQuantity))
                ) && 
                (
                    this.InvUnitQuantity == input.InvUnitQuantity ||
                    (this.InvUnitQuantity != null &&
                    this.InvUnitQuantity.Equals(input.InvUnitQuantity))
                ) && 
                (
                    this.TotalCapacity == input.TotalCapacity ||
                    (this.TotalCapacity != null &&
                    this.TotalCapacity.Equals(input.TotalCapacity))
                ) && 
                (
                    this.CapacityUnit == input.CapacityUnit ||
                    (this.CapacityUnit != null &&
                    this.CapacityUnit.Equals(input.CapacityUnit))
                ) && 
                (
                    this.TotalWeight == input.TotalWeight ||
                    (this.TotalWeight != null &&
                    this.TotalWeight.Equals(input.TotalWeight))
                ) && 
                (
                    this.WeightUnit == input.WeightUnit ||
                    (this.WeightUnit != null &&
                    this.WeightUnit.Equals(input.WeightUnit))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.StoreCardID != null)
                    hashCode = hashCode * 59 + this.StoreCardID.GetHashCode();
                if (this.StoreBatchID != null)
                    hashCode = hashCode * 59 + this.StoreBatchID.GetHashCode();
                if (this.QUnit != null)
                    hashCode = hashCode * 59 + this.QUnit.GetHashCode();
                if (this.UnitRate != null)
                    hashCode = hashCode * 59 + this.UnitRate.GetHashCode();
                if (this.Quantity != null)
                    hashCode = hashCode * 59 + this.Quantity.GetHashCode();
                if (this.QuantityReserved != null)
                    hashCode = hashCode * 59 + this.QuantityReserved.GetHashCode();
                if (this.QuantityAwaited != null)
                    hashCode = hashCode * 59 + this.QuantityAwaited.GetHashCode();
                if (this.DateOfStorageDATE != null)
                    hashCode = hashCode * 59 + this.DateOfStorageDATE.GetHashCode();
                if (this.InvQuantity != null)
                    hashCode = hashCode * 59 + this.InvQuantity.GetHashCode();
                if (this.InvChange != null)
                    hashCode = hashCode * 59 + this.InvChange.GetHashCode();
                if (this.UnitQuantity != null)
                    hashCode = hashCode * 59 + this.UnitQuantity.GetHashCode();
                if (this.UnitQuantityReserved != null)
                    hashCode = hashCode * 59 + this.UnitQuantityReserved.GetHashCode();
                if (this.UnitQuantityAwaited != null)
                    hashCode = hashCode * 59 + this.UnitQuantityAwaited.GetHashCode();
                if (this.UnitEditInvQuantity != null)
                    hashCode = hashCode * 59 + this.UnitEditInvQuantity.GetHashCode();
                if (this.InvUnitQuantity != null)
                    hashCode = hashCode * 59 + this.InvUnitQuantity.GetHashCode();
                if (this.TotalCapacity != null)
                    hashCode = hashCode * 59 + this.TotalCapacity.GetHashCode();
                if (this.CapacityUnit != null)
                    hashCode = hashCode * 59 + this.CapacityUnit.GetHashCode();
                if (this.TotalWeight != null)
                    hashCode = hashCode * 59 + this.TotalWeight.GetHashCode();
                if (this.WeightUnit != null)
                    hashCode = hashCode * 59 + this.WeightUnit.GetHashCode();
                return hashCode;
            }
        }
    }

}
