/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Globdatavatreg
    /// </summary>
    [DataContract]
    public partial class Globdatavatreg :  IEquatable<Globdatavatreg>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Globdatavatreg" /> class.
        /// </summary>
        /// <param name="dateOfChangeDATE">Datum změny [persistentní položka].</param>
        /// <param name="vATIdentNumber">DIČ [persistentní položka].</param>
        /// <param name="vATByPayment">Uplatnění DPH na základě přijetí platby [persistentní položka].</param>
        /// <param name="vATReturnMethod">Metoda DPH přiznání [persistentní položka].</param>
        /// <param name="saldoVATRate1">1. sazba [persistentní položka].</param>
        /// <param name="saldoVATRate2">2. sazba [persistentní položka].</param>
        /// <param name="vATReturnMethodAsText">Metoda DPH přiznání.</param>
        public Globdatavatreg(DateTimeOffset? dateOfChangeDATE = default(DateTimeOffset?), string vATIdentNumber = default(string), bool? vATByPayment = default(bool?), int? vATReturnMethod = default(int?), double? saldoVATRate1 = default(double?), double? saldoVATRate2 = default(double?), string vATReturnMethodAsText = default(string))
        {
            this.DateOfChangeDATE = dateOfChangeDATE;
            this.VATIdentNumber = vATIdentNumber;
            this.VATByPayment = vATByPayment;
            this.VATReturnMethod = vATReturnMethod;
            this.SaldoVATRate1 = saldoVATRate1;
            this.SaldoVATRate2 = saldoVATRate2;
            this.VATReturnMethodAsText = vATReturnMethodAsText;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Globální data [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Globální data [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Datum změny [persistentní položka]
        /// </summary>
        /// <value>Datum změny [persistentní položka]</value>
        [DataMember(Name="DateOfChange$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DateOfChangeDATE { get; set; }

        /// <summary>
        /// DIČ [persistentní položka]
        /// </summary>
        /// <value>DIČ [persistentní položka]</value>
        [DataMember(Name="VATIdentNumber", EmitDefaultValue=false)]
        public string VATIdentNumber { get; set; }

        /// <summary>
        /// Uplatnění DPH na základě přijetí platby [persistentní položka]
        /// </summary>
        /// <value>Uplatnění DPH na základě přijetí platby [persistentní položka]</value>
        [DataMember(Name="VATByPayment", EmitDefaultValue=false)]
        public bool? VATByPayment { get; set; }

        /// <summary>
        /// Metoda DPH přiznání [persistentní položka]
        /// </summary>
        /// <value>Metoda DPH přiznání [persistentní položka]</value>
        [DataMember(Name="VATReturnMethod", EmitDefaultValue=false)]
        public int? VATReturnMethod { get; set; }

        /// <summary>
        /// 1. sazba [persistentní položka]
        /// </summary>
        /// <value>1. sazba [persistentní položka]</value>
        [DataMember(Name="SaldoVATRate1", EmitDefaultValue=false)]
        public double? SaldoVATRate1 { get; set; }

        /// <summary>
        /// 2. sazba [persistentní položka]
        /// </summary>
        /// <value>2. sazba [persistentní položka]</value>
        [DataMember(Name="SaldoVATRate2", EmitDefaultValue=false)]
        public double? SaldoVATRate2 { get; set; }

        /// <summary>
        /// Metoda DPH přiznání
        /// </summary>
        /// <value>Metoda DPH přiznání</value>
        [DataMember(Name="VATReturnMethodAsText", EmitDefaultValue=false)]
        public string VATReturnMethodAsText { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Globdatavatreg {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  DateOfChangeDATE: ").Append(DateOfChangeDATE).Append("\n");
            sb.Append("  VATIdentNumber: ").Append(VATIdentNumber).Append("\n");
            sb.Append("  VATByPayment: ").Append(VATByPayment).Append("\n");
            sb.Append("  VATReturnMethod: ").Append(VATReturnMethod).Append("\n");
            sb.Append("  SaldoVATRate1: ").Append(SaldoVATRate1).Append("\n");
            sb.Append("  SaldoVATRate2: ").Append(SaldoVATRate2).Append("\n");
            sb.Append("  VATReturnMethodAsText: ").Append(VATReturnMethodAsText).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Globdatavatreg);
        }

        /// <summary>
        /// Returns true if Globdatavatreg instances are equal
        /// </summary>
        /// <param name="input">Instance of Globdatavatreg to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Globdatavatreg input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.DateOfChangeDATE == input.DateOfChangeDATE ||
                    (this.DateOfChangeDATE != null &&
                    this.DateOfChangeDATE.Equals(input.DateOfChangeDATE))
                ) && 
                (
                    this.VATIdentNumber == input.VATIdentNumber ||
                    (this.VATIdentNumber != null &&
                    this.VATIdentNumber.Equals(input.VATIdentNumber))
                ) && 
                (
                    this.VATByPayment == input.VATByPayment ||
                    (this.VATByPayment != null &&
                    this.VATByPayment.Equals(input.VATByPayment))
                ) && 
                (
                    this.VATReturnMethod == input.VATReturnMethod ||
                    (this.VATReturnMethod != null &&
                    this.VATReturnMethod.Equals(input.VATReturnMethod))
                ) && 
                (
                    this.SaldoVATRate1 == input.SaldoVATRate1 ||
                    (this.SaldoVATRate1 != null &&
                    this.SaldoVATRate1.Equals(input.SaldoVATRate1))
                ) && 
                (
                    this.SaldoVATRate2 == input.SaldoVATRate2 ||
                    (this.SaldoVATRate2 != null &&
                    this.SaldoVATRate2.Equals(input.SaldoVATRate2))
                ) && 
                (
                    this.VATReturnMethodAsText == input.VATReturnMethodAsText ||
                    (this.VATReturnMethodAsText != null &&
                    this.VATReturnMethodAsText.Equals(input.VATReturnMethodAsText))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.DateOfChangeDATE != null)
                    hashCode = hashCode * 59 + this.DateOfChangeDATE.GetHashCode();
                if (this.VATIdentNumber != null)
                    hashCode = hashCode * 59 + this.VATIdentNumber.GetHashCode();
                if (this.VATByPayment != null)
                    hashCode = hashCode * 59 + this.VATByPayment.GetHashCode();
                if (this.VATReturnMethod != null)
                    hashCode = hashCode * 59 + this.VATReturnMethod.GetHashCode();
                if (this.SaldoVATRate1 != null)
                    hashCode = hashCode * 59 + this.SaldoVATRate1.GetHashCode();
                if (this.SaldoVATRate2 != null)
                    hashCode = hashCode * 59 + this.SaldoVATRate2.GetHashCode();
                if (this.VATReturnMethodAsText != null)
                    hashCode = hashCode * 59 + this.VATReturnMethodAsText.GetHashCode();
                return hashCode;
            }
        }
    }

}
