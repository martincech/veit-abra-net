/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Trmpublictranstariff
    /// </summary>
    [DataContract]
    public partial class Trmpublictranstariff :  IEquatable<Trmpublictranstariff>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Trmpublictranstariff" /> class.
        /// </summary>
        /// <param name="hidden">Skrytý [persistentní položka].</param>
        /// <param name="code">Kód [persistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="kmTariff">Cena za km [persistentní položka].</param>
        /// <param name="waitTariff">Cena za čekání [persistentní položka].</param>
        /// <param name="runtimeTariff">Cena za dobu provozu [persistentní položka].</param>
        public Trmpublictranstariff(bool? hidden = default(bool?), string code = default(string), string name = default(string), double? kmTariff = default(double?), double? waitTariff = default(double?), double? runtimeTariff = default(double?))
        {
            this.Hidden = hidden;
            this.Code = code;
            this.Name = name;
            this.KmTariff = kmTariff;
            this.WaitTariff = waitTariff;
            this.RuntimeTariff = runtimeTariff;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Skrytý [persistentní položka]
        /// </summary>
        /// <value>Skrytý [persistentní položka]</value>
        [DataMember(Name="Hidden", EmitDefaultValue=false)]
        public bool? Hidden { get; set; }

        /// <summary>
        /// Kód [persistentní položka]
        /// </summary>
        /// <value>Kód [persistentní položka]</value>
        [DataMember(Name="Code", EmitDefaultValue=false)]
        public string Code { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Cena za km [persistentní položka]
        /// </summary>
        /// <value>Cena za km [persistentní položka]</value>
        [DataMember(Name="KmTariff", EmitDefaultValue=false)]
        public double? KmTariff { get; set; }

        /// <summary>
        /// Cena za čekání [persistentní položka]
        /// </summary>
        /// <value>Cena za čekání [persistentní položka]</value>
        [DataMember(Name="WaitTariff", EmitDefaultValue=false)]
        public double? WaitTariff { get; set; }

        /// <summary>
        /// Cena za dobu provozu [persistentní položka]
        /// </summary>
        /// <value>Cena za dobu provozu [persistentní položka]</value>
        [DataMember(Name="RuntimeTariff", EmitDefaultValue=false)]
        public double? RuntimeTariff { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Trmpublictranstariff {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Hidden: ").Append(Hidden).Append("\n");
            sb.Append("  Code: ").Append(Code).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  KmTariff: ").Append(KmTariff).Append("\n");
            sb.Append("  WaitTariff: ").Append(WaitTariff).Append("\n");
            sb.Append("  RuntimeTariff: ").Append(RuntimeTariff).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Trmpublictranstariff);
        }

        /// <summary>
        /// Returns true if Trmpublictranstariff instances are equal
        /// </summary>
        /// <param name="input">Instance of Trmpublictranstariff to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Trmpublictranstariff input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Hidden == input.Hidden ||
                    (this.Hidden != null &&
                    this.Hidden.Equals(input.Hidden))
                ) && 
                (
                    this.Code == input.Code ||
                    (this.Code != null &&
                    this.Code.Equals(input.Code))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.KmTariff == input.KmTariff ||
                    (this.KmTariff != null &&
                    this.KmTariff.Equals(input.KmTariff))
                ) && 
                (
                    this.WaitTariff == input.WaitTariff ||
                    (this.WaitTariff != null &&
                    this.WaitTariff.Equals(input.WaitTariff))
                ) && 
                (
                    this.RuntimeTariff == input.RuntimeTariff ||
                    (this.RuntimeTariff != null &&
                    this.RuntimeTariff.Equals(input.RuntimeTariff))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Hidden != null)
                    hashCode = hashCode * 59 + this.Hidden.GetHashCode();
                if (this.Code != null)
                    hashCode = hashCode * 59 + this.Code.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.KmTariff != null)
                    hashCode = hashCode * 59 + this.KmTariff.GetHashCode();
                if (this.WaitTariff != null)
                    hashCode = hashCode * 59 + this.WaitTariff.GetHashCode();
                if (this.RuntimeTariff != null)
                    hashCode = hashCode * 59 + this.RuntimeTariff.GetHashCode();
                return hashCode;
            }
        }
    }

}
