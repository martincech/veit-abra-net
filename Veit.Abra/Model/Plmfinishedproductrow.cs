/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Plmfinishedproductrow
    /// </summary>
    [DataContract]
    public partial class Plmfinishedproductrow :  IEquatable<Plmfinishedproductrow>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Plmfinishedproductrow" /> class.
        /// </summary>
        /// <param name="parentID">Hlavička zaúčtování; ID objektu Dokončený výrobek [persistentní položka].</param>
        /// <param name="jOOutputItemID">Vyráběná položka; ID objektu VP - vyráběná položka [persistentní položka].</param>
        /// <param name="jobOrdersSNID">Sériové číslo/šarže; ID objektu VP - sériové číslo [persistentní položka].</param>
        /// <param name="quantity">Množství v ev.jedn. [persistentní položka].</param>
        /// <param name="qUnit">Jednotka [persistentní položka].</param>
        /// <param name="unitRate">Vztah [persistentní položka].</param>
        /// <param name="receivedByID">Přijal; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="receivedAtDATE">Datum přijetí [persistentní položka].</param>
        /// <param name="checkedByID">Zkontroloval; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="checkedAtDATE">Datum kontroly [persistentní položka].</param>
        /// <param name="unitQuantity">Množství.</param>
        /// <param name="storeDoc2ID">Řádek příjemky; ID objektu Příjem hotových výrobků - řádek [persistentní položka].</param>
        /// <param name="materialExpenseAmount">Materiálová režie [persistentní položka].</param>
        /// <param name="consumablesAmount">Spotřební materiál [persistentní položka].</param>
        /// <param name="priceAmount">Pevná cena [persistentní položka].</param>
        /// <param name="productionDateDATE">Datum výroby [persistentní položka].</param>
        /// <param name="cooperation">Kooperace [persistentní položka].</param>
        public Plmfinishedproductrow(string parentID = default(string), string jOOutputItemID = default(string), string jobOrdersSNID = default(string), double? quantity = default(double?), string qUnit = default(string), double? unitRate = default(double?), string receivedByID = default(string), DateTimeOffset? receivedAtDATE = default(DateTimeOffset?), string checkedByID = default(string), DateTimeOffset? checkedAtDATE = default(DateTimeOffset?), double? unitQuantity = default(double?), string storeDoc2ID = default(string), double? materialExpenseAmount = default(double?), double? consumablesAmount = default(double?), double? priceAmount = default(double?), DateTimeOffset? productionDateDATE = default(DateTimeOffset?), bool? cooperation = default(bool?))
        {
            this.ParentID = parentID;
            this.JOOutputItemID = jOOutputItemID;
            this.JobOrdersSNID = jobOrdersSNID;
            this.Quantity = quantity;
            this.QUnit = qUnit;
            this.UnitRate = unitRate;
            this.ReceivedByID = receivedByID;
            this.ReceivedAtDATE = receivedAtDATE;
            this.CheckedByID = checkedByID;
            this.CheckedAtDATE = checkedAtDATE;
            this.UnitQuantity = unitQuantity;
            this.StoreDoc2ID = storeDoc2ID;
            this.MaterialExpenseAmount = materialExpenseAmount;
            this.ConsumablesAmount = consumablesAmount;
            this.PriceAmount = priceAmount;
            this.ProductionDateDATE = productionDateDATE;
            this.Cooperation = cooperation;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Hlavička zaúčtování; ID objektu Dokončený výrobek [persistentní položka]
        /// </summary>
        /// <value>Hlavička zaúčtování; ID objektu Dokončený výrobek [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; set; }

        /// <summary>
        /// Vyráběná položka; ID objektu VP - vyráběná položka [persistentní položka]
        /// </summary>
        /// <value>Vyráběná položka; ID objektu VP - vyráběná položka [persistentní položka]</value>
        [DataMember(Name="JOOutputItem_ID", EmitDefaultValue=false)]
        public string JOOutputItemID { get; set; }

        /// <summary>
        /// Sériové číslo/šarže; ID objektu VP - sériové číslo [persistentní položka]
        /// </summary>
        /// <value>Sériové číslo/šarže; ID objektu VP - sériové číslo [persistentní položka]</value>
        [DataMember(Name="JobOrdersSN_ID", EmitDefaultValue=false)]
        public string JobOrdersSNID { get; set; }

        /// <summary>
        /// Množství v ev.jedn. [persistentní položka]
        /// </summary>
        /// <value>Množství v ev.jedn. [persistentní položka]</value>
        [DataMember(Name="Quantity", EmitDefaultValue=false)]
        public double? Quantity { get; set; }

        /// <summary>
        /// Jednotka [persistentní položka]
        /// </summary>
        /// <value>Jednotka [persistentní položka]</value>
        [DataMember(Name="QUnit", EmitDefaultValue=false)]
        public string QUnit { get; set; }

        /// <summary>
        /// Vztah [persistentní položka]
        /// </summary>
        /// <value>Vztah [persistentní položka]</value>
        [DataMember(Name="UnitRate", EmitDefaultValue=false)]
        public double? UnitRate { get; set; }

        /// <summary>
        /// Přijal; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Přijal; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="ReceivedBy_ID", EmitDefaultValue=false)]
        public string ReceivedByID { get; set; }

        /// <summary>
        /// Datum přijetí [persistentní položka]
        /// </summary>
        /// <value>Datum přijetí [persistentní položka]</value>
        [DataMember(Name="ReceivedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ReceivedAtDATE { get; set; }

        /// <summary>
        /// Zkontroloval; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Zkontroloval; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CheckedBy_ID", EmitDefaultValue=false)]
        public string CheckedByID { get; set; }

        /// <summary>
        /// Datum kontroly [persistentní položka]
        /// </summary>
        /// <value>Datum kontroly [persistentní položka]</value>
        [DataMember(Name="CheckedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CheckedAtDATE { get; set; }

        /// <summary>
        /// Množství
        /// </summary>
        /// <value>Množství</value>
        [DataMember(Name="UnitQuantity", EmitDefaultValue=false)]
        public double? UnitQuantity { get; set; }

        /// <summary>
        /// Řádek příjemky; ID objektu Příjem hotových výrobků - řádek [persistentní položka]
        /// </summary>
        /// <value>Řádek příjemky; ID objektu Příjem hotových výrobků - řádek [persistentní položka]</value>
        [DataMember(Name="StoreDoc2_ID", EmitDefaultValue=false)]
        public string StoreDoc2ID { get; set; }

        /// <summary>
        /// Materiálová režie [persistentní položka]
        /// </summary>
        /// <value>Materiálová režie [persistentní položka]</value>
        [DataMember(Name="MaterialExpenseAmount", EmitDefaultValue=false)]
        public double? MaterialExpenseAmount { get; set; }

        /// <summary>
        /// Spotřební materiál [persistentní položka]
        /// </summary>
        /// <value>Spotřební materiál [persistentní položka]</value>
        [DataMember(Name="ConsumablesAmount", EmitDefaultValue=false)]
        public double? ConsumablesAmount { get; set; }

        /// <summary>
        /// Pevná cena [persistentní položka]
        /// </summary>
        /// <value>Pevná cena [persistentní položka]</value>
        [DataMember(Name="PriceAmount", EmitDefaultValue=false)]
        public double? PriceAmount { get; set; }

        /// <summary>
        /// Řada; ID objektu Řada dokladů
        /// </summary>
        /// <value>Řada; ID objektu Řada dokladů</value>
        [DataMember(Name="DocQueue_ID", EmitDefaultValue=false)]
        public string DocQueueID { get; private set; }

        /// <summary>
        /// Období; ID objektu Období
        /// </summary>
        /// <value>Období; ID objektu Období</value>
        [DataMember(Name="Period_ID", EmitDefaultValue=false)]
        public string PeriodID { get; private set; }

        /// <summary>
        /// Pořadové číslo
        /// </summary>
        /// <value>Pořadové číslo</value>
        [DataMember(Name="OrdNumber", EmitDefaultValue=false)]
        public int? OrdNumber { get; private set; }

        /// <summary>
        /// Datum dok.
        /// </summary>
        /// <value>Datum dok.</value>
        [DataMember(Name="DocDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DocDateDATE { get; private set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; private set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; private set; }

        /// <summary>
        /// Předkontace; ID objektu Účetní předkontace
        /// </summary>
        /// <value>Předkontace; ID objektu Účetní předkontace</value>
        [DataMember(Name="AccPresetDef_ID", EmitDefaultValue=false)]
        public string AccPresetDefID { get; private set; }

        /// <summary>
        /// Datum účt.
        /// </summary>
        /// <value>Datum účt.</value>
        [DataMember(Name="AccDate$Date", EmitDefaultValue=false)]
        public DateTimeOffset? AccDateDate { get; private set; }

        /// <summary>
        /// Datum výroby [persistentní položka]
        /// </summary>
        /// <value>Datum výroby [persistentní položka]</value>
        [DataMember(Name="ProductionDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ProductionDateDATE { get; set; }

        /// <summary>
        /// Kooperace [persistentní položka]
        /// </summary>
        /// <value>Kooperace [persistentní položka]</value>
        [DataMember(Name="Cooperation", EmitDefaultValue=false)]
        public bool? Cooperation { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Plmfinishedproductrow {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  JOOutputItemID: ").Append(JOOutputItemID).Append("\n");
            sb.Append("  JobOrdersSNID: ").Append(JobOrdersSNID).Append("\n");
            sb.Append("  Quantity: ").Append(Quantity).Append("\n");
            sb.Append("  QUnit: ").Append(QUnit).Append("\n");
            sb.Append("  UnitRate: ").Append(UnitRate).Append("\n");
            sb.Append("  ReceivedByID: ").Append(ReceivedByID).Append("\n");
            sb.Append("  ReceivedAtDATE: ").Append(ReceivedAtDATE).Append("\n");
            sb.Append("  CheckedByID: ").Append(CheckedByID).Append("\n");
            sb.Append("  CheckedAtDATE: ").Append(CheckedAtDATE).Append("\n");
            sb.Append("  UnitQuantity: ").Append(UnitQuantity).Append("\n");
            sb.Append("  StoreDoc2ID: ").Append(StoreDoc2ID).Append("\n");
            sb.Append("  MaterialExpenseAmount: ").Append(MaterialExpenseAmount).Append("\n");
            sb.Append("  ConsumablesAmount: ").Append(ConsumablesAmount).Append("\n");
            sb.Append("  PriceAmount: ").Append(PriceAmount).Append("\n");
            sb.Append("  DocQueueID: ").Append(DocQueueID).Append("\n");
            sb.Append("  PeriodID: ").Append(PeriodID).Append("\n");
            sb.Append("  OrdNumber: ").Append(OrdNumber).Append("\n");
            sb.Append("  DocDateDATE: ").Append(DocDateDATE).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  AccPresetDefID: ").Append(AccPresetDefID).Append("\n");
            sb.Append("  AccDateDate: ").Append(AccDateDate).Append("\n");
            sb.Append("  ProductionDateDATE: ").Append(ProductionDateDATE).Append("\n");
            sb.Append("  Cooperation: ").Append(Cooperation).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Plmfinishedproductrow);
        }

        /// <summary>
        /// Returns true if Plmfinishedproductrow instances are equal
        /// </summary>
        /// <param name="input">Instance of Plmfinishedproductrow to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Plmfinishedproductrow input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.JOOutputItemID == input.JOOutputItemID ||
                    (this.JOOutputItemID != null &&
                    this.JOOutputItemID.Equals(input.JOOutputItemID))
                ) && 
                (
                    this.JobOrdersSNID == input.JobOrdersSNID ||
                    (this.JobOrdersSNID != null &&
                    this.JobOrdersSNID.Equals(input.JobOrdersSNID))
                ) && 
                (
                    this.Quantity == input.Quantity ||
                    (this.Quantity != null &&
                    this.Quantity.Equals(input.Quantity))
                ) && 
                (
                    this.QUnit == input.QUnit ||
                    (this.QUnit != null &&
                    this.QUnit.Equals(input.QUnit))
                ) && 
                (
                    this.UnitRate == input.UnitRate ||
                    (this.UnitRate != null &&
                    this.UnitRate.Equals(input.UnitRate))
                ) && 
                (
                    this.ReceivedByID == input.ReceivedByID ||
                    (this.ReceivedByID != null &&
                    this.ReceivedByID.Equals(input.ReceivedByID))
                ) && 
                (
                    this.ReceivedAtDATE == input.ReceivedAtDATE ||
                    (this.ReceivedAtDATE != null &&
                    this.ReceivedAtDATE.Equals(input.ReceivedAtDATE))
                ) && 
                (
                    this.CheckedByID == input.CheckedByID ||
                    (this.CheckedByID != null &&
                    this.CheckedByID.Equals(input.CheckedByID))
                ) && 
                (
                    this.CheckedAtDATE == input.CheckedAtDATE ||
                    (this.CheckedAtDATE != null &&
                    this.CheckedAtDATE.Equals(input.CheckedAtDATE))
                ) && 
                (
                    this.UnitQuantity == input.UnitQuantity ||
                    (this.UnitQuantity != null &&
                    this.UnitQuantity.Equals(input.UnitQuantity))
                ) && 
                (
                    this.StoreDoc2ID == input.StoreDoc2ID ||
                    (this.StoreDoc2ID != null &&
                    this.StoreDoc2ID.Equals(input.StoreDoc2ID))
                ) && 
                (
                    this.MaterialExpenseAmount == input.MaterialExpenseAmount ||
                    (this.MaterialExpenseAmount != null &&
                    this.MaterialExpenseAmount.Equals(input.MaterialExpenseAmount))
                ) && 
                (
                    this.ConsumablesAmount == input.ConsumablesAmount ||
                    (this.ConsumablesAmount != null &&
                    this.ConsumablesAmount.Equals(input.ConsumablesAmount))
                ) && 
                (
                    this.PriceAmount == input.PriceAmount ||
                    (this.PriceAmount != null &&
                    this.PriceAmount.Equals(input.PriceAmount))
                ) && 
                (
                    this.DocQueueID == input.DocQueueID ||
                    (this.DocQueueID != null &&
                    this.DocQueueID.Equals(input.DocQueueID))
                ) && 
                (
                    this.PeriodID == input.PeriodID ||
                    (this.PeriodID != null &&
                    this.PeriodID.Equals(input.PeriodID))
                ) && 
                (
                    this.OrdNumber == input.OrdNumber ||
                    (this.OrdNumber != null &&
                    this.OrdNumber.Equals(input.OrdNumber))
                ) && 
                (
                    this.DocDateDATE == input.DocDateDATE ||
                    (this.DocDateDATE != null &&
                    this.DocDateDATE.Equals(input.DocDateDATE))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.AccPresetDefID == input.AccPresetDefID ||
                    (this.AccPresetDefID != null &&
                    this.AccPresetDefID.Equals(input.AccPresetDefID))
                ) && 
                (
                    this.AccDateDate == input.AccDateDate ||
                    (this.AccDateDate != null &&
                    this.AccDateDate.Equals(input.AccDateDate))
                ) && 
                (
                    this.ProductionDateDATE == input.ProductionDateDATE ||
                    (this.ProductionDateDATE != null &&
                    this.ProductionDateDATE.Equals(input.ProductionDateDATE))
                ) && 
                (
                    this.Cooperation == input.Cooperation ||
                    (this.Cooperation != null &&
                    this.Cooperation.Equals(input.Cooperation))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.JOOutputItemID != null)
                    hashCode = hashCode * 59 + this.JOOutputItemID.GetHashCode();
                if (this.JobOrdersSNID != null)
                    hashCode = hashCode * 59 + this.JobOrdersSNID.GetHashCode();
                if (this.Quantity != null)
                    hashCode = hashCode * 59 + this.Quantity.GetHashCode();
                if (this.QUnit != null)
                    hashCode = hashCode * 59 + this.QUnit.GetHashCode();
                if (this.UnitRate != null)
                    hashCode = hashCode * 59 + this.UnitRate.GetHashCode();
                if (this.ReceivedByID != null)
                    hashCode = hashCode * 59 + this.ReceivedByID.GetHashCode();
                if (this.ReceivedAtDATE != null)
                    hashCode = hashCode * 59 + this.ReceivedAtDATE.GetHashCode();
                if (this.CheckedByID != null)
                    hashCode = hashCode * 59 + this.CheckedByID.GetHashCode();
                if (this.CheckedAtDATE != null)
                    hashCode = hashCode * 59 + this.CheckedAtDATE.GetHashCode();
                if (this.UnitQuantity != null)
                    hashCode = hashCode * 59 + this.UnitQuantity.GetHashCode();
                if (this.StoreDoc2ID != null)
                    hashCode = hashCode * 59 + this.StoreDoc2ID.GetHashCode();
                if (this.MaterialExpenseAmount != null)
                    hashCode = hashCode * 59 + this.MaterialExpenseAmount.GetHashCode();
                if (this.ConsumablesAmount != null)
                    hashCode = hashCode * 59 + this.ConsumablesAmount.GetHashCode();
                if (this.PriceAmount != null)
                    hashCode = hashCode * 59 + this.PriceAmount.GetHashCode();
                if (this.DocQueueID != null)
                    hashCode = hashCode * 59 + this.DocQueueID.GetHashCode();
                if (this.PeriodID != null)
                    hashCode = hashCode * 59 + this.PeriodID.GetHashCode();
                if (this.OrdNumber != null)
                    hashCode = hashCode * 59 + this.OrdNumber.GetHashCode();
                if (this.DocDateDATE != null)
                    hashCode = hashCode * 59 + this.DocDateDATE.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.AccPresetDefID != null)
                    hashCode = hashCode * 59 + this.AccPresetDefID.GetHashCode();
                if (this.AccDateDate != null)
                    hashCode = hashCode * 59 + this.AccDateDate.GetHashCode();
                if (this.ProductionDateDATE != null)
                    hashCode = hashCode * 59 + this.ProductionDateDATE.GetHashCode();
                if (this.Cooperation != null)
                    hashCode = hashCode * 59 + this.Cooperation.GetHashCode();
                return hashCode;
            }
        }
    }

}
