/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Plmsalaryclass
    /// </summary>
    [DataContract]
    public partial class Plmsalaryclass :  IEquatable<Plmsalaryclass>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Plmsalaryclass" /> class.
        /// </summary>
        /// <param name="dateOfChange">Datum změny.</param>
        /// <param name="code">Tarifní třída [persistentní položka].</param>
        /// <param name="hourlyRate">Sazba/hod. [persistentní položka].</param>
        public Plmsalaryclass(DateTimeOffset? dateOfChange = default(DateTimeOffset?), int? code = default(int?), double? hourlyRate = default(double?))
        {
            this.DateOfChange = dateOfChange;
            this.Code = code;
            this.HourlyRate = hourlyRate;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// Datum změny
        /// </summary>
        /// <value>Datum změny</value>
        [DataMember(Name="DateOfChange", EmitDefaultValue=false)]
        public DateTimeOffset? DateOfChange { get; set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Tarifní třída [persistentní položka]
        /// </summary>
        /// <value>Tarifní třída [persistentní položka]</value>
        [DataMember(Name="Code", EmitDefaultValue=false)]
        public int? Code { get; set; }

        /// <summary>
        /// Sazba/hod. [persistentní položka]
        /// </summary>
        /// <value>Sazba/hod. [persistentní položka]</value>
        [DataMember(Name="HourlyRate", EmitDefaultValue=false)]
        public double? HourlyRate { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Plmsalaryclass {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  DateOfChange: ").Append(DateOfChange).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Code: ").Append(Code).Append("\n");
            sb.Append("  HourlyRate: ").Append(HourlyRate).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Plmsalaryclass);
        }

        /// <summary>
        /// Returns true if Plmsalaryclass instances are equal
        /// </summary>
        /// <param name="input">Instance of Plmsalaryclass to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Plmsalaryclass input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.DateOfChange == input.DateOfChange ||
                    (this.DateOfChange != null &&
                    this.DateOfChange.Equals(input.DateOfChange))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Code == input.Code ||
                    (this.Code != null &&
                    this.Code.Equals(input.Code))
                ) && 
                (
                    this.HourlyRate == input.HourlyRate ||
                    (this.HourlyRate != null &&
                    this.HourlyRate.Equals(input.HourlyRate))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.DateOfChange != null)
                    hashCode = hashCode * 59 + this.DateOfChange.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Code != null)
                    hashCode = hashCode * 59 + this.Code.GetHashCode();
                if (this.HourlyRate != null)
                    hashCode = hashCode * 59 + this.HourlyRate.GetHashCode();
                return hashCode;
            }
        }
    }

}
