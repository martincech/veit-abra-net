/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Firmoffice
    /// </summary>
    [DataContract]
    public partial class Firmoffice :  IEquatable<Firmoffice>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Firmoffice" /> class.
        /// </summary>
        /// <param name="posIndex">Pořadí [persistentní položka].</param>
        /// <param name="addressID">addressID.</param>
        /// <param name="massCorrespondence">Hromadná korespondence [persistentní položka].</param>
        /// <param name="name">Název provozovny [persistentní položka].</param>
        /// <param name="synchronizeAddress">Synchronizovat [persistentní položka].</param>
        /// <param name="credit">Kredit [persistentní položka].</param>
        /// <param name="checkCredit">Sledovat kredit [persistentní položka].</param>
        /// <param name="storeID">Sklad; ID objektu Sklad [persistentní položka].</param>
        /// <param name="dealerCategoryID">Dealerská třída; ID objektu Dealerská třída [persistentní položka].</param>
        /// <param name="officeUniqueID">Interní identifikátor provozovny [persistentní položka].</param>
        /// <param name="lastChildID">Potomek; ID objektu Provozovna.</param>
        /// <param name="officeIdentNumber">IČP [persistentní položka].</param>
        /// <param name="electronicAddressID">electronicAddressID.</param>
        public Firmoffice(int? posIndex = default(int?), Address addressID = default(Address), bool? massCorrespondence = default(bool?), string name = default(string), bool? synchronizeAddress = default(bool?), double? credit = default(double?), bool? checkCredit = default(bool?), string storeID = default(string), string dealerCategoryID = default(string), string officeUniqueID = default(string), string lastChildID = default(string), string officeIdentNumber = default(string), Address electronicAddressID = default(Address))
        {
            this.PosIndex = posIndex;
            this.AddressID = addressID;
            this.MassCorrespondence = massCorrespondence;
            this.Name = name;
            this.SynchronizeAddress = synchronizeAddress;
            this.Credit = credit;
            this.CheckCredit = checkCredit;
            this.StoreID = storeID;
            this.DealerCategoryID = dealerCategoryID;
            this.OfficeUniqueID = officeUniqueID;
            this.LastChildID = lastChildID;
            this.OfficeIdentNumber = officeIdentNumber;
            this.ElectronicAddressID = electronicAddressID;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Firma [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Firma [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Pořadí [persistentní položka]
        /// </summary>
        /// <value>Pořadí [persistentní položka]</value>
        [DataMember(Name="PosIndex", EmitDefaultValue=false)]
        public int? PosIndex { get; set; }

        /// <summary>
        /// Gets or Sets AddressID
        /// </summary>
        [DataMember(Name="Address_ID", EmitDefaultValue=false)]
        public Address AddressID { get; set; }

        /// <summary>
        /// Hromadná korespondence [persistentní položka]
        /// </summary>
        /// <value>Hromadná korespondence [persistentní položka]</value>
        [DataMember(Name="MassCorrespondence", EmitDefaultValue=false)]
        public bool? MassCorrespondence { get; set; }

        /// <summary>
        /// Název provozovny [persistentní položka]
        /// </summary>
        /// <value>Název provozovny [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Synchronizovat [persistentní položka]
        /// </summary>
        /// <value>Synchronizovat [persistentní položka]</value>
        [DataMember(Name="SynchronizeAddress", EmitDefaultValue=false)]
        public bool? SynchronizeAddress { get; set; }

        /// <summary>
        /// Kredit [persistentní položka]
        /// </summary>
        /// <value>Kredit [persistentní položka]</value>
        [DataMember(Name="Credit", EmitDefaultValue=false)]
        public double? Credit { get; set; }

        /// <summary>
        /// Sledovat kredit [persistentní položka]
        /// </summary>
        /// <value>Sledovat kredit [persistentní položka]</value>
        [DataMember(Name="CheckCredit", EmitDefaultValue=false)]
        public bool? CheckCredit { get; set; }

        /// <summary>
        /// Sklad; ID objektu Sklad [persistentní položka]
        /// </summary>
        /// <value>Sklad; ID objektu Sklad [persistentní položka]</value>
        [DataMember(Name="Store_ID", EmitDefaultValue=false)]
        public string StoreID { get; set; }

        /// <summary>
        /// Dealerská třída; ID objektu Dealerská třída [persistentní položka]
        /// </summary>
        /// <value>Dealerská třída; ID objektu Dealerská třída [persistentní položka]</value>
        [DataMember(Name="DealerCategory_ID", EmitDefaultValue=false)]
        public string DealerCategoryID { get; set; }

        /// <summary>
        /// Interní identifikátor provozovny [persistentní položka]
        /// </summary>
        /// <value>Interní identifikátor provozovny [persistentní položka]</value>
        [DataMember(Name="OfficeUnique_ID", EmitDefaultValue=false)]
        public string OfficeUniqueID { get; set; }

        /// <summary>
        /// Potomek; ID objektu Provozovna
        /// </summary>
        /// <value>Potomek; ID objektu Provozovna</value>
        [DataMember(Name="LastChild_ID", EmitDefaultValue=false)]
        public string LastChildID { get; set; }

        /// <summary>
        /// IČP [persistentní položka]
        /// </summary>
        /// <value>IČP [persistentní položka]</value>
        [DataMember(Name="OfficeIdentNumber", EmitDefaultValue=false)]
        public string OfficeIdentNumber { get; set; }

        /// <summary>
        /// Gets or Sets ElectronicAddressID
        /// </summary>
        [DataMember(Name="ElectronicAddress_ID", EmitDefaultValue=false)]
        public Address ElectronicAddressID { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Firmoffice {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  PosIndex: ").Append(PosIndex).Append("\n");
            sb.Append("  AddressID: ").Append(AddressID).Append("\n");
            sb.Append("  MassCorrespondence: ").Append(MassCorrespondence).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  SynchronizeAddress: ").Append(SynchronizeAddress).Append("\n");
            sb.Append("  Credit: ").Append(Credit).Append("\n");
            sb.Append("  CheckCredit: ").Append(CheckCredit).Append("\n");
            sb.Append("  StoreID: ").Append(StoreID).Append("\n");
            sb.Append("  DealerCategoryID: ").Append(DealerCategoryID).Append("\n");
            sb.Append("  OfficeUniqueID: ").Append(OfficeUniqueID).Append("\n");
            sb.Append("  LastChildID: ").Append(LastChildID).Append("\n");
            sb.Append("  OfficeIdentNumber: ").Append(OfficeIdentNumber).Append("\n");
            sb.Append("  ElectronicAddressID: ").Append(ElectronicAddressID).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Firmoffice);
        }

        /// <summary>
        /// Returns true if Firmoffice instances are equal
        /// </summary>
        /// <param name="input">Instance of Firmoffice to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Firmoffice input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.PosIndex == input.PosIndex ||
                    (this.PosIndex != null &&
                    this.PosIndex.Equals(input.PosIndex))
                ) && 
                (
                    this.AddressID == input.AddressID ||
                    (this.AddressID != null &&
                    this.AddressID.Equals(input.AddressID))
                ) && 
                (
                    this.MassCorrespondence == input.MassCorrespondence ||
                    (this.MassCorrespondence != null &&
                    this.MassCorrespondence.Equals(input.MassCorrespondence))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.SynchronizeAddress == input.SynchronizeAddress ||
                    (this.SynchronizeAddress != null &&
                    this.SynchronizeAddress.Equals(input.SynchronizeAddress))
                ) && 
                (
                    this.Credit == input.Credit ||
                    (this.Credit != null &&
                    this.Credit.Equals(input.Credit))
                ) && 
                (
                    this.CheckCredit == input.CheckCredit ||
                    (this.CheckCredit != null &&
                    this.CheckCredit.Equals(input.CheckCredit))
                ) && 
                (
                    this.StoreID == input.StoreID ||
                    (this.StoreID != null &&
                    this.StoreID.Equals(input.StoreID))
                ) && 
                (
                    this.DealerCategoryID == input.DealerCategoryID ||
                    (this.DealerCategoryID != null &&
                    this.DealerCategoryID.Equals(input.DealerCategoryID))
                ) && 
                (
                    this.OfficeUniqueID == input.OfficeUniqueID ||
                    (this.OfficeUniqueID != null &&
                    this.OfficeUniqueID.Equals(input.OfficeUniqueID))
                ) && 
                (
                    this.LastChildID == input.LastChildID ||
                    (this.LastChildID != null &&
                    this.LastChildID.Equals(input.LastChildID))
                ) && 
                (
                    this.OfficeIdentNumber == input.OfficeIdentNumber ||
                    (this.OfficeIdentNumber != null &&
                    this.OfficeIdentNumber.Equals(input.OfficeIdentNumber))
                ) && 
                (
                    this.ElectronicAddressID == input.ElectronicAddressID ||
                    (this.ElectronicAddressID != null &&
                    this.ElectronicAddressID.Equals(input.ElectronicAddressID))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.PosIndex != null)
                    hashCode = hashCode * 59 + this.PosIndex.GetHashCode();
                if (this.AddressID != null)
                    hashCode = hashCode * 59 + this.AddressID.GetHashCode();
                if (this.MassCorrespondence != null)
                    hashCode = hashCode * 59 + this.MassCorrespondence.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.SynchronizeAddress != null)
                    hashCode = hashCode * 59 + this.SynchronizeAddress.GetHashCode();
                if (this.Credit != null)
                    hashCode = hashCode * 59 + this.Credit.GetHashCode();
                if (this.CheckCredit != null)
                    hashCode = hashCode * 59 + this.CheckCredit.GetHashCode();
                if (this.StoreID != null)
                    hashCode = hashCode * 59 + this.StoreID.GetHashCode();
                if (this.DealerCategoryID != null)
                    hashCode = hashCode * 59 + this.DealerCategoryID.GetHashCode();
                if (this.OfficeUniqueID != null)
                    hashCode = hashCode * 59 + this.OfficeUniqueID.GetHashCode();
                if (this.LastChildID != null)
                    hashCode = hashCode * 59 + this.LastChildID.GetHashCode();
                if (this.OfficeIdentNumber != null)
                    hashCode = hashCode * 59 + this.OfficeIdentNumber.GetHashCode();
                if (this.ElectronicAddressID != null)
                    hashCode = hashCode * 59 + this.ElectronicAddressID.GetHashCode();
                return hashCode;
            }
        }
    }

}
