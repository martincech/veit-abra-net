/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Accdocqueue
    /// </summary>
    [DataContract]
    public partial class Accdocqueue :  IEquatable<Accdocqueue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Accdocqueue" /> class.
        /// </summary>
        /// <param name="hidden">Skrytý [persistentní položka].</param>
        /// <param name="code">Zkratka [persistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="lastNumbers">Čísla; kolekce BO Účetní řada dokladů - období [nepersistentní položka].</param>
        /// <param name="note">Poznámka [persistentní položka].</param>
        /// <param name="autoFillHole">Zaplňovat mezery [persistentní položka].</param>
        /// <param name="documentType">Typ dokladu [persistentní položka].</param>
        /// <param name="accountWhere">Účtovat [persistentní položka].</param>
        /// <param name="summaryAccounted">Souhrnně [persistentní položka].</param>
        /// <param name="reverseAccounting">Obráceně [persistentní položka].</param>
        /// <param name="reverseDepositAccounting">Zálohy obráceně [persistentní položka].</param>
        public Accdocqueue(bool? hidden = default(bool?), string code = default(string), string name = default(string), List<Accdocqueueperiod> lastNumbers = default(List<Accdocqueueperiod>), string note = default(string), bool? autoFillHole = default(bool?), string documentType = default(string), bool? accountWhere = default(bool?), bool? summaryAccounted = default(bool?), bool? reverseAccounting = default(bool?), bool? reverseDepositAccounting = default(bool?))
        {
            this.Hidden = hidden;
            this.Code = code;
            this.Name = name;
            this.LastNumbers = lastNumbers;
            this.Note = note;
            this.AutoFillHole = autoFillHole;
            this.DocumentType = documentType;
            this.AccountWhere = accountWhere;
            this.SummaryAccounted = summaryAccounted;
            this.ReverseAccounting = reverseAccounting;
            this.ReverseDepositAccounting = reverseDepositAccounting;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Skrytý [persistentní položka]
        /// </summary>
        /// <value>Skrytý [persistentní položka]</value>
        [DataMember(Name="Hidden", EmitDefaultValue=false)]
        public bool? Hidden { get; set; }

        /// <summary>
        /// Zkratka [persistentní položka]
        /// </summary>
        /// <value>Zkratka [persistentní položka]</value>
        [DataMember(Name="Code", EmitDefaultValue=false)]
        public string Code { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Čísla; kolekce BO Účetní řada dokladů - období [nepersistentní položka]
        /// </summary>
        /// <value>Čísla; kolekce BO Účetní řada dokladů - období [nepersistentní položka]</value>
        [DataMember(Name="LastNumbers", EmitDefaultValue=false)]
        public List<Accdocqueueperiod> LastNumbers { get; set; }

        /// <summary>
        /// Poznámka [persistentní položka]
        /// </summary>
        /// <value>Poznámka [persistentní položka]</value>
        [DataMember(Name="Note", EmitDefaultValue=false)]
        public string Note { get; set; }

        /// <summary>
        /// Zaplňovat mezery [persistentní položka]
        /// </summary>
        /// <value>Zaplňovat mezery [persistentní položka]</value>
        [DataMember(Name="AutoFillHole", EmitDefaultValue=false)]
        public bool? AutoFillHole { get; set; }

        /// <summary>
        /// Typ dokladu [persistentní položka]
        /// </summary>
        /// <value>Typ dokladu [persistentní položka]</value>
        [DataMember(Name="DocumentType", EmitDefaultValue=false)]
        public string DocumentType { get; set; }

        /// <summary>
        /// Účtovat [persistentní položka]
        /// </summary>
        /// <value>Účtovat [persistentní položka]</value>
        [DataMember(Name="AccountWhere", EmitDefaultValue=false)]
        public bool? AccountWhere { get; set; }

        /// <summary>
        /// Souhrnně [persistentní položka]
        /// </summary>
        /// <value>Souhrnně [persistentní položka]</value>
        [DataMember(Name="SummaryAccounted", EmitDefaultValue=false)]
        public bool? SummaryAccounted { get; set; }

        /// <summary>
        /// Obráceně [persistentní položka]
        /// </summary>
        /// <value>Obráceně [persistentní položka]</value>
        [DataMember(Name="ReverseAccounting", EmitDefaultValue=false)]
        public bool? ReverseAccounting { get; set; }

        /// <summary>
        /// Zálohy obráceně [persistentní položka]
        /// </summary>
        /// <value>Zálohy obráceně [persistentní položka]</value>
        [DataMember(Name="ReverseDepositAccounting", EmitDefaultValue=false)]
        public bool? ReverseDepositAccounting { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Accdocqueue {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Hidden: ").Append(Hidden).Append("\n");
            sb.Append("  Code: ").Append(Code).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  LastNumbers: ").Append(LastNumbers).Append("\n");
            sb.Append("  Note: ").Append(Note).Append("\n");
            sb.Append("  AutoFillHole: ").Append(AutoFillHole).Append("\n");
            sb.Append("  DocumentType: ").Append(DocumentType).Append("\n");
            sb.Append("  AccountWhere: ").Append(AccountWhere).Append("\n");
            sb.Append("  SummaryAccounted: ").Append(SummaryAccounted).Append("\n");
            sb.Append("  ReverseAccounting: ").Append(ReverseAccounting).Append("\n");
            sb.Append("  ReverseDepositAccounting: ").Append(ReverseDepositAccounting).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Accdocqueue);
        }

        /// <summary>
        /// Returns true if Accdocqueue instances are equal
        /// </summary>
        /// <param name="input">Instance of Accdocqueue to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Accdocqueue input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Hidden == input.Hidden ||
                    (this.Hidden != null &&
                    this.Hidden.Equals(input.Hidden))
                ) && 
                (
                    this.Code == input.Code ||
                    (this.Code != null &&
                    this.Code.Equals(input.Code))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.LastNumbers == input.LastNumbers ||
                    this.LastNumbers != null &&
                    this.LastNumbers.SequenceEqual(input.LastNumbers)
                ) && 
                (
                    this.Note == input.Note ||
                    (this.Note != null &&
                    this.Note.Equals(input.Note))
                ) && 
                (
                    this.AutoFillHole == input.AutoFillHole ||
                    (this.AutoFillHole != null &&
                    this.AutoFillHole.Equals(input.AutoFillHole))
                ) && 
                (
                    this.DocumentType == input.DocumentType ||
                    (this.DocumentType != null &&
                    this.DocumentType.Equals(input.DocumentType))
                ) && 
                (
                    this.AccountWhere == input.AccountWhere ||
                    (this.AccountWhere != null &&
                    this.AccountWhere.Equals(input.AccountWhere))
                ) && 
                (
                    this.SummaryAccounted == input.SummaryAccounted ||
                    (this.SummaryAccounted != null &&
                    this.SummaryAccounted.Equals(input.SummaryAccounted))
                ) && 
                (
                    this.ReverseAccounting == input.ReverseAccounting ||
                    (this.ReverseAccounting != null &&
                    this.ReverseAccounting.Equals(input.ReverseAccounting))
                ) && 
                (
                    this.ReverseDepositAccounting == input.ReverseDepositAccounting ||
                    (this.ReverseDepositAccounting != null &&
                    this.ReverseDepositAccounting.Equals(input.ReverseDepositAccounting))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Hidden != null)
                    hashCode = hashCode * 59 + this.Hidden.GetHashCode();
                if (this.Code != null)
                    hashCode = hashCode * 59 + this.Code.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.LastNumbers != null)
                    hashCode = hashCode * 59 + this.LastNumbers.GetHashCode();
                if (this.Note != null)
                    hashCode = hashCode * 59 + this.Note.GetHashCode();
                if (this.AutoFillHole != null)
                    hashCode = hashCode * 59 + this.AutoFillHole.GetHashCode();
                if (this.DocumentType != null)
                    hashCode = hashCode * 59 + this.DocumentType.GetHashCode();
                if (this.AccountWhere != null)
                    hashCode = hashCode * 59 + this.AccountWhere.GetHashCode();
                if (this.SummaryAccounted != null)
                    hashCode = hashCode * 59 + this.SummaryAccounted.GetHashCode();
                if (this.ReverseAccounting != null)
                    hashCode = hashCode * 59 + this.ReverseAccounting.GetHashCode();
                if (this.ReverseDepositAccounting != null)
                    hashCode = hashCode * 59 + this.ReverseDepositAccounting.GetHashCode();
                return hashCode;
            }
        }
    }

}
