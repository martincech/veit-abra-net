/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Categoryitem
    /// </summary>
    [DataContract]
    public partial class Categoryitem :  IEquatable<Categoryitem>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Categoryitem" /> class.
        /// </summary>
        /// <param name="hidden">Skrytý [persistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="itemType">Typ [persistentní položka].</param>
        /// <param name="userFieldDef2ID">Uživatelská položka; ID objektu Definovatelná položka [persistentní položka].</param>
        /// <param name="categoryItemGroupID">Skupina; ID objektu Skupina kategorizačních údajů [persistentní položka].</param>
        /// <param name="expression">Výraz [persistentní položka].</param>
        /// <param name="fieldName">Jméno položky [persistentní položka].</param>
        /// <param name="fieldLabel">Popis položky.</param>
        /// <param name="fieldHint">Popiska položky.</param>
        /// <param name="itemTypeTXT">Typ.</param>
        /// <param name="dataTypeTXT">Datový typ.</param>
        /// <param name="fieldCode">Číslo položky.</param>
        /// <param name="sQLQuery">Definice [persistentní položka].</param>
        /// <param name="dataType">Datový typ [persistentní položka].</param>
        /// <param name="dataSize">Velikost [persistentní položka].</param>
        /// <param name="decimalPlaces">Počet des.míst [persistentní položka].</param>
        public Categoryitem(bool? hidden = default(bool?), string name = default(string), int? itemType = default(int?), string userFieldDef2ID = default(string), string categoryItemGroupID = default(string), string expression = default(string), string fieldName = default(string), string fieldLabel = default(string), string fieldHint = default(string), string itemTypeTXT = default(string), string dataTypeTXT = default(string), int? fieldCode = default(int?), string sQLQuery = default(string), int? dataType = default(int?), int? dataSize = default(int?), int? decimalPlaces = default(int?))
        {
            this.Hidden = hidden;
            this.Name = name;
            this.ItemType = itemType;
            this.UserFieldDef2ID = userFieldDef2ID;
            this.CategoryItemGroupID = categoryItemGroupID;
            this.Expression = expression;
            this.FieldName = fieldName;
            this.FieldLabel = fieldLabel;
            this.FieldHint = fieldHint;
            this.ItemTypeTXT = itemTypeTXT;
            this.DataTypeTXT = dataTypeTXT;
            this.FieldCode = fieldCode;
            this.SQLQuery = sQLQuery;
            this.DataType = dataType;
            this.DataSize = dataSize;
            this.DecimalPlaces = decimalPlaces;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Skrytý [persistentní položka]
        /// </summary>
        /// <value>Skrytý [persistentní položka]</value>
        [DataMember(Name="Hidden", EmitDefaultValue=false)]
        public bool? Hidden { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Typ [persistentní položka]
        /// </summary>
        /// <value>Typ [persistentní položka]</value>
        [DataMember(Name="ItemType", EmitDefaultValue=false)]
        public int? ItemType { get; set; }

        /// <summary>
        /// Uživatelská položka; ID objektu Definovatelná položka [persistentní položka]
        /// </summary>
        /// <value>Uživatelská položka; ID objektu Definovatelná položka [persistentní položka]</value>
        [DataMember(Name="UserFieldDef2_ID", EmitDefaultValue=false)]
        public string UserFieldDef2ID { get; set; }

        /// <summary>
        /// Skupina; ID objektu Skupina kategorizačních údajů [persistentní položka]
        /// </summary>
        /// <value>Skupina; ID objektu Skupina kategorizačních údajů [persistentní položka]</value>
        [DataMember(Name="CategoryItemGroup_ID", EmitDefaultValue=false)]
        public string CategoryItemGroupID { get; set; }

        /// <summary>
        /// Výraz [persistentní položka]
        /// </summary>
        /// <value>Výraz [persistentní položka]</value>
        [DataMember(Name="Expression", EmitDefaultValue=false)]
        public string Expression { get; set; }

        /// <summary>
        /// Jméno položky [persistentní položka]
        /// </summary>
        /// <value>Jméno položky [persistentní položka]</value>
        [DataMember(Name="FieldName", EmitDefaultValue=false)]
        public string FieldName { get; set; }

        /// <summary>
        /// Popis položky
        /// </summary>
        /// <value>Popis položky</value>
        [DataMember(Name="FieldLabel", EmitDefaultValue=false)]
        public string FieldLabel { get; set; }

        /// <summary>
        /// Popiska položky
        /// </summary>
        /// <value>Popiska položky</value>
        [DataMember(Name="FieldHint", EmitDefaultValue=false)]
        public string FieldHint { get; set; }

        /// <summary>
        /// Typ
        /// </summary>
        /// <value>Typ</value>
        [DataMember(Name="ItemTypeTXT", EmitDefaultValue=false)]
        public string ItemTypeTXT { get; set; }

        /// <summary>
        /// Datový typ
        /// </summary>
        /// <value>Datový typ</value>
        [DataMember(Name="DataTypeTXT", EmitDefaultValue=false)]
        public string DataTypeTXT { get; set; }

        /// <summary>
        /// Číslo položky
        /// </summary>
        /// <value>Číslo položky</value>
        [DataMember(Name="FieldCode", EmitDefaultValue=false)]
        public int? FieldCode { get; set; }

        /// <summary>
        /// Definice [persistentní položka]
        /// </summary>
        /// <value>Definice [persistentní položka]</value>
        [DataMember(Name="SQLQuery", EmitDefaultValue=false)]
        public string SQLQuery { get; set; }

        /// <summary>
        /// Datový typ [persistentní položka]
        /// </summary>
        /// <value>Datový typ [persistentní položka]</value>
        [DataMember(Name="DataType", EmitDefaultValue=false)]
        public int? DataType { get; set; }

        /// <summary>
        /// Velikost [persistentní položka]
        /// </summary>
        /// <value>Velikost [persistentní položka]</value>
        [DataMember(Name="DataSize", EmitDefaultValue=false)]
        public int? DataSize { get; set; }

        /// <summary>
        /// Počet des.míst [persistentní položka]
        /// </summary>
        /// <value>Počet des.míst [persistentní položka]</value>
        [DataMember(Name="DecimalPlaces", EmitDefaultValue=false)]
        public int? DecimalPlaces { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Categoryitem {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Hidden: ").Append(Hidden).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  ItemType: ").Append(ItemType).Append("\n");
            sb.Append("  UserFieldDef2ID: ").Append(UserFieldDef2ID).Append("\n");
            sb.Append("  CategoryItemGroupID: ").Append(CategoryItemGroupID).Append("\n");
            sb.Append("  Expression: ").Append(Expression).Append("\n");
            sb.Append("  FieldName: ").Append(FieldName).Append("\n");
            sb.Append("  FieldLabel: ").Append(FieldLabel).Append("\n");
            sb.Append("  FieldHint: ").Append(FieldHint).Append("\n");
            sb.Append("  ItemTypeTXT: ").Append(ItemTypeTXT).Append("\n");
            sb.Append("  DataTypeTXT: ").Append(DataTypeTXT).Append("\n");
            sb.Append("  FieldCode: ").Append(FieldCode).Append("\n");
            sb.Append("  SQLQuery: ").Append(SQLQuery).Append("\n");
            sb.Append("  DataType: ").Append(DataType).Append("\n");
            sb.Append("  DataSize: ").Append(DataSize).Append("\n");
            sb.Append("  DecimalPlaces: ").Append(DecimalPlaces).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Categoryitem);
        }

        /// <summary>
        /// Returns true if Categoryitem instances are equal
        /// </summary>
        /// <param name="input">Instance of Categoryitem to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Categoryitem input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Hidden == input.Hidden ||
                    (this.Hidden != null &&
                    this.Hidden.Equals(input.Hidden))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.ItemType == input.ItemType ||
                    (this.ItemType != null &&
                    this.ItemType.Equals(input.ItemType))
                ) && 
                (
                    this.UserFieldDef2ID == input.UserFieldDef2ID ||
                    (this.UserFieldDef2ID != null &&
                    this.UserFieldDef2ID.Equals(input.UserFieldDef2ID))
                ) && 
                (
                    this.CategoryItemGroupID == input.CategoryItemGroupID ||
                    (this.CategoryItemGroupID != null &&
                    this.CategoryItemGroupID.Equals(input.CategoryItemGroupID))
                ) && 
                (
                    this.Expression == input.Expression ||
                    (this.Expression != null &&
                    this.Expression.Equals(input.Expression))
                ) && 
                (
                    this.FieldName == input.FieldName ||
                    (this.FieldName != null &&
                    this.FieldName.Equals(input.FieldName))
                ) && 
                (
                    this.FieldLabel == input.FieldLabel ||
                    (this.FieldLabel != null &&
                    this.FieldLabel.Equals(input.FieldLabel))
                ) && 
                (
                    this.FieldHint == input.FieldHint ||
                    (this.FieldHint != null &&
                    this.FieldHint.Equals(input.FieldHint))
                ) && 
                (
                    this.ItemTypeTXT == input.ItemTypeTXT ||
                    (this.ItemTypeTXT != null &&
                    this.ItemTypeTXT.Equals(input.ItemTypeTXT))
                ) && 
                (
                    this.DataTypeTXT == input.DataTypeTXT ||
                    (this.DataTypeTXT != null &&
                    this.DataTypeTXT.Equals(input.DataTypeTXT))
                ) && 
                (
                    this.FieldCode == input.FieldCode ||
                    (this.FieldCode != null &&
                    this.FieldCode.Equals(input.FieldCode))
                ) && 
                (
                    this.SQLQuery == input.SQLQuery ||
                    (this.SQLQuery != null &&
                    this.SQLQuery.Equals(input.SQLQuery))
                ) && 
                (
                    this.DataType == input.DataType ||
                    (this.DataType != null &&
                    this.DataType.Equals(input.DataType))
                ) && 
                (
                    this.DataSize == input.DataSize ||
                    (this.DataSize != null &&
                    this.DataSize.Equals(input.DataSize))
                ) && 
                (
                    this.DecimalPlaces == input.DecimalPlaces ||
                    (this.DecimalPlaces != null &&
                    this.DecimalPlaces.Equals(input.DecimalPlaces))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Hidden != null)
                    hashCode = hashCode * 59 + this.Hidden.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.ItemType != null)
                    hashCode = hashCode * 59 + this.ItemType.GetHashCode();
                if (this.UserFieldDef2ID != null)
                    hashCode = hashCode * 59 + this.UserFieldDef2ID.GetHashCode();
                if (this.CategoryItemGroupID != null)
                    hashCode = hashCode * 59 + this.CategoryItemGroupID.GetHashCode();
                if (this.Expression != null)
                    hashCode = hashCode * 59 + this.Expression.GetHashCode();
                if (this.FieldName != null)
                    hashCode = hashCode * 59 + this.FieldName.GetHashCode();
                if (this.FieldLabel != null)
                    hashCode = hashCode * 59 + this.FieldLabel.GetHashCode();
                if (this.FieldHint != null)
                    hashCode = hashCode * 59 + this.FieldHint.GetHashCode();
                if (this.ItemTypeTXT != null)
                    hashCode = hashCode * 59 + this.ItemTypeTXT.GetHashCode();
                if (this.DataTypeTXT != null)
                    hashCode = hashCode * 59 + this.DataTypeTXT.GetHashCode();
                if (this.FieldCode != null)
                    hashCode = hashCode * 59 + this.FieldCode.GetHashCode();
                if (this.SQLQuery != null)
                    hashCode = hashCode * 59 + this.SQLQuery.GetHashCode();
                if (this.DataType != null)
                    hashCode = hashCode * 59 + this.DataType.GetHashCode();
                if (this.DataSize != null)
                    hashCode = hashCode * 59 + this.DataSize.GetHashCode();
                if (this.DecimalPlaces != null)
                    hashCode = hashCode * 59 + this.DecimalPlaces.GetHashCode();
                return hashCode;
            }
        }
    }

}
