/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Datachangeslog
    /// </summary>
    [DataContract]
    public partial class Datachangeslog :  IEquatable<Datachangeslog>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Datachangeslog" /> class.
        /// </summary>
        /// <param name="cLSIDText">Třída (textově).</param>
        public Datachangeslog(string cLSIDText = default(string))
        {
            this.CLSIDText = cLSIDText;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Hnízdo [persistentní položka]
        /// </summary>
        /// <value>Hnízdo [persistentní položka]</value>
        [DataMember(Name="Site", EmitDefaultValue=false)]
        public string Site { get; private set; }

        /// <summary>
        /// Klientský čas vzniku [persistentní položka]
        /// </summary>
        /// <value>Klientský čas vzniku [persistentní položka]</value>
        [DataMember(Name="ClientCreatedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ClientCreatedAtDATE { get; private set; }

        /// <summary>
        /// Centrální čas vzniku [persistentní položka]
        /// </summary>
        /// <value>Centrální čas vzniku [persistentní položka]</value>
        [DataMember(Name="ServerCreatedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ServerCreatedAtDATE { get; private set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; private set; }

        /// <summary>
        /// Třída [persistentní položka]
        /// </summary>
        /// <value>Třída [persistentní položka]</value>
        [DataMember(Name="CLSID", EmitDefaultValue=false)]
        public string CLSID { get; private set; }

        /// <summary>
        /// ID objektu [persistentní položka]
        /// </summary>
        /// <value>ID objektu [persistentní položka]</value>
        [DataMember(Name="Obj_ID", EmitDefaultValue=false)]
        public string ObjID { get; private set; }

        /// <summary>
        /// Data logu [persistentní položka]
        /// </summary>
        /// <value>Data logu [persistentní položka]</value>
        [DataMember(Name="LogData", EmitDefaultValue=false)]
        public byte[] LogData { get; private set; }

        /// <summary>
        /// Typ změny [persistentní položka]
        /// </summary>
        /// <value>Typ změny [persistentní položka]</value>
        [DataMember(Name="Status", EmitDefaultValue=false)]
        public int? Status { get; private set; }

        /// <summary>
        /// Název objektu [persistentní položka]
        /// </summary>
        /// <value>Název objektu [persistentní položka]</value>
        [DataMember(Name="ObjectName", EmitDefaultValue=false)]
        public string ObjectName { get; private set; }

        /// <summary>
        /// Formát dat logu [persistentní položka]
        /// </summary>
        /// <value>Formát dat logu [persistentní položka]</value>
        [DataMember(Name="LogDataFormat", EmitDefaultValue=false)]
        public int? LogDataFormat { get; private set; }

        /// <summary>
        /// Třída (textově)
        /// </summary>
        /// <value>Třída (textově)</value>
        [DataMember(Name="CLSIDText", EmitDefaultValue=false)]
        public string CLSIDText { get; set; }

        /// <summary>
        /// ID síť. karty [persistentní položka]
        /// </summary>
        /// <value>ID síť. karty [persistentní položka]</value>
        [DataMember(Name="NetworkCardID", EmitDefaultValue=false)]
        public string NetworkCardID { get; private set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Datachangeslog {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Site: ").Append(Site).Append("\n");
            sb.Append("  ClientCreatedAtDATE: ").Append(ClientCreatedAtDATE).Append("\n");
            sb.Append("  ServerCreatedAtDATE: ").Append(ServerCreatedAtDATE).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CLSID: ").Append(CLSID).Append("\n");
            sb.Append("  ObjID: ").Append(ObjID).Append("\n");
            sb.Append("  LogData: ").Append(LogData).Append("\n");
            sb.Append("  Status: ").Append(Status).Append("\n");
            sb.Append("  ObjectName: ").Append(ObjectName).Append("\n");
            sb.Append("  LogDataFormat: ").Append(LogDataFormat).Append("\n");
            sb.Append("  CLSIDText: ").Append(CLSIDText).Append("\n");
            sb.Append("  NetworkCardID: ").Append(NetworkCardID).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Datachangeslog);
        }

        /// <summary>
        /// Returns true if Datachangeslog instances are equal
        /// </summary>
        /// <param name="input">Instance of Datachangeslog to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Datachangeslog input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Site == input.Site ||
                    (this.Site != null &&
                    this.Site.Equals(input.Site))
                ) && 
                (
                    this.ClientCreatedAtDATE == input.ClientCreatedAtDATE ||
                    (this.ClientCreatedAtDATE != null &&
                    this.ClientCreatedAtDATE.Equals(input.ClientCreatedAtDATE))
                ) && 
                (
                    this.ServerCreatedAtDATE == input.ServerCreatedAtDATE ||
                    (this.ServerCreatedAtDATE != null &&
                    this.ServerCreatedAtDATE.Equals(input.ServerCreatedAtDATE))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CLSID == input.CLSID ||
                    (this.CLSID != null &&
                    this.CLSID.Equals(input.CLSID))
                ) && 
                (
                    this.ObjID == input.ObjID ||
                    (this.ObjID != null &&
                    this.ObjID.Equals(input.ObjID))
                ) && 
                (
                    this.LogData == input.LogData ||
                    (this.LogData != null &&
                    this.LogData.Equals(input.LogData))
                ) && 
                (
                    this.Status == input.Status ||
                    (this.Status != null &&
                    this.Status.Equals(input.Status))
                ) && 
                (
                    this.ObjectName == input.ObjectName ||
                    (this.ObjectName != null &&
                    this.ObjectName.Equals(input.ObjectName))
                ) && 
                (
                    this.LogDataFormat == input.LogDataFormat ||
                    (this.LogDataFormat != null &&
                    this.LogDataFormat.Equals(input.LogDataFormat))
                ) && 
                (
                    this.CLSIDText == input.CLSIDText ||
                    (this.CLSIDText != null &&
                    this.CLSIDText.Equals(input.CLSIDText))
                ) && 
                (
                    this.NetworkCardID == input.NetworkCardID ||
                    (this.NetworkCardID != null &&
                    this.NetworkCardID.Equals(input.NetworkCardID))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Site != null)
                    hashCode = hashCode * 59 + this.Site.GetHashCode();
                if (this.ClientCreatedAtDATE != null)
                    hashCode = hashCode * 59 + this.ClientCreatedAtDATE.GetHashCode();
                if (this.ServerCreatedAtDATE != null)
                    hashCode = hashCode * 59 + this.ServerCreatedAtDATE.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CLSID != null)
                    hashCode = hashCode * 59 + this.CLSID.GetHashCode();
                if (this.ObjID != null)
                    hashCode = hashCode * 59 + this.ObjID.GetHashCode();
                if (this.LogData != null)
                    hashCode = hashCode * 59 + this.LogData.GetHashCode();
                if (this.Status != null)
                    hashCode = hashCode * 59 + this.Status.GetHashCode();
                if (this.ObjectName != null)
                    hashCode = hashCode * 59 + this.ObjectName.GetHashCode();
                if (this.LogDataFormat != null)
                    hashCode = hashCode * 59 + this.LogDataFormat.GetHashCode();
                if (this.CLSIDText != null)
                    hashCode = hashCode * 59 + this.CLSIDText.GetHashCode();
                if (this.NetworkCardID != null)
                    hashCode = hashCode * 59 + this.NetworkCardID.GetHashCode();
                return hashCode;
            }
        }
    }

}
