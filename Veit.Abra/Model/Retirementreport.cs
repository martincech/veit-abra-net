/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Retirementreport
    /// </summary>
    [DataContract]
    public partial class Retirementreport :  IEquatable<Retirementreport>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Retirementreport" /> class.
        /// </summary>
        /// <param name="description">Popis [persistentní položka].</param>
        /// <param name="workingRelationID">Pracovní poměr; ID objektu Pracovní poměr [persistentní položka].</param>
        /// <param name="stateOfDocument">Stav zadání [persistentní položka].</param>
        /// <param name="stateOfSending">Stav zpracování [persistentní položka].</param>
        /// <param name="documentData">Data dokladu ELDP [persistentní položka].</param>
        /// <param name="calendarYear">Rok [persistentní položka].</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="corrective">Opravný [persistentní položka].</param>
        /// <param name="partOfYear">Za část roku [persistentní položka].</param>
        /// <param name="partDateFromDATE">Datum od [persistentní položka].</param>
        /// <param name="partDateToDATE">Datum do [persistentní položka].</param>
        /// <param name="employeeID">Zaměstnanec; ID objektu Zaměstnanec.</param>
        /// <param name="retirementReportDefID">Definice ELDP; ID objektu Definice ELDP.</param>
        /// <param name="communicationDocumentID">Dokument; ID objektu Dokument [persistentní položka].</param>
        /// <param name="partMonthFrom">Měsíc od.</param>
        /// <param name="partMonthTo">Měsíc do.</param>
        public Retirementreport(string description = default(string), string workingRelationID = default(string), int? stateOfDocument = default(int?), int? stateOfSending = default(int?), byte[] documentData = default(byte[]), int? calendarYear = default(int?), string createdByID = default(string), string correctedByID = default(string), bool? corrective = default(bool?), bool? partOfYear = default(bool?), DateTimeOffset? partDateFromDATE = default(DateTimeOffset?), DateTimeOffset? partDateToDATE = default(DateTimeOffset?), string employeeID = default(string), string retirementReportDefID = default(string), string communicationDocumentID = default(string), int? partMonthFrom = default(int?), int? partMonthTo = default(int?))
        {
            this.Description = description;
            this.WorkingRelationID = workingRelationID;
            this.StateOfDocument = stateOfDocument;
            this.StateOfSending = stateOfSending;
            this.DocumentData = documentData;
            this.CalendarYear = calendarYear;
            this.CreatedByID = createdByID;
            this.CorrectedByID = correctedByID;
            this.Corrective = corrective;
            this.PartOfYear = partOfYear;
            this.PartDateFromDATE = partDateFromDATE;
            this.PartDateToDATE = partDateToDATE;
            this.EmployeeID = employeeID;
            this.RetirementReportDefID = retirementReportDefID;
            this.CommunicationDocumentID = communicationDocumentID;
            this.PartMonthFrom = partMonthFrom;
            this.PartMonthTo = partMonthTo;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Popis [persistentní položka]
        /// </summary>
        /// <value>Popis [persistentní položka]</value>
        [DataMember(Name="Description", EmitDefaultValue=false)]
        public string Description { get; set; }

        /// <summary>
        /// Pracovní poměr; ID objektu Pracovní poměr [persistentní položka]
        /// </summary>
        /// <value>Pracovní poměr; ID objektu Pracovní poměr [persistentní položka]</value>
        [DataMember(Name="WorkingRelation_ID", EmitDefaultValue=false)]
        public string WorkingRelationID { get; set; }

        /// <summary>
        /// Stav zadání [persistentní položka]
        /// </summary>
        /// <value>Stav zadání [persistentní položka]</value>
        [DataMember(Name="StateOfDocument", EmitDefaultValue=false)]
        public int? StateOfDocument { get; set; }

        /// <summary>
        /// Stav zpracování [persistentní položka]
        /// </summary>
        /// <value>Stav zpracování [persistentní položka]</value>
        [DataMember(Name="StateOfSending", EmitDefaultValue=false)]
        public int? StateOfSending { get; set; }

        /// <summary>
        /// Data dokladu ELDP [persistentní položka]
        /// </summary>
        /// <value>Data dokladu ELDP [persistentní položka]</value>
        [DataMember(Name="DocumentData", EmitDefaultValue=false)]
        public byte[] DocumentData { get; set; }

        /// <summary>
        /// Rok [persistentní položka]
        /// </summary>
        /// <value>Rok [persistentní položka]</value>
        [DataMember(Name="CalendarYear", EmitDefaultValue=false)]
        public int? CalendarYear { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Opravný [persistentní položka]
        /// </summary>
        /// <value>Opravný [persistentní položka]</value>
        [DataMember(Name="Corrective", EmitDefaultValue=false)]
        public bool? Corrective { get; set; }

        /// <summary>
        /// Za část roku [persistentní položka]
        /// </summary>
        /// <value>Za část roku [persistentní položka]</value>
        [DataMember(Name="PartOfYear", EmitDefaultValue=false)]
        public bool? PartOfYear { get; set; }

        /// <summary>
        /// Datum od [persistentní položka]
        /// </summary>
        /// <value>Datum od [persistentní položka]</value>
        [DataMember(Name="PartDateFrom$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? PartDateFromDATE { get; set; }

        /// <summary>
        /// Datum do [persistentní položka]
        /// </summary>
        /// <value>Datum do [persistentní položka]</value>
        [DataMember(Name="PartDateTo$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? PartDateToDATE { get; set; }

        /// <summary>
        /// Stav zadání
        /// </summary>
        /// <value>Stav zadání</value>
        [DataMember(Name="StateOfDocument_Text", EmitDefaultValue=false)]
        public string StateOfDocumentText { get; private set; }

        /// <summary>
        /// Stav zpracování
        /// </summary>
        /// <value>Stav zpracování</value>
        [DataMember(Name="StateOfSending_Text", EmitDefaultValue=false)]
        public string StateOfSendingText { get; private set; }

        /// <summary>
        /// Zaměstnanec; ID objektu Zaměstnanec
        /// </summary>
        /// <value>Zaměstnanec; ID objektu Zaměstnanec</value>
        [DataMember(Name="Employee_ID", EmitDefaultValue=false)]
        public string EmployeeID { get; set; }

        /// <summary>
        /// Definice ELDP; ID objektu Definice ELDP
        /// </summary>
        /// <value>Definice ELDP; ID objektu Definice ELDP</value>
        [DataMember(Name="RetirementReportDef_ID", EmitDefaultValue=false)]
        public string RetirementReportDefID { get; set; }

        /// <summary>
        /// Dokument; ID objektu Dokument [persistentní položka]
        /// </summary>
        /// <value>Dokument; ID objektu Dokument [persistentní položka]</value>
        [DataMember(Name="CommunicationDocument_ID", EmitDefaultValue=false)]
        public string CommunicationDocumentID { get; set; }

        /// <summary>
        /// Příjmení a jméno
        /// </summary>
        /// <value>Příjmení a jméno</value>
        [DataMember(Name="EmployeeName", EmitDefaultValue=false)]
        public string EmployeeName { get; private set; }

        /// <summary>
        /// Osobní číslo
        /// </summary>
        /// <value>Osobní číslo</value>
        [DataMember(Name="PersonalNumber", EmitDefaultValue=false)]
        public string PersonalNumber { get; private set; }

        /// <summary>
        /// Měsíc od
        /// </summary>
        /// <value>Měsíc od</value>
        [DataMember(Name="PartMonthFrom", EmitDefaultValue=false)]
        public int? PartMonthFrom { get; set; }

        /// <summary>
        /// Měsíc do
        /// </summary>
        /// <value>Měsíc do</value>
        [DataMember(Name="PartMonthTo", EmitDefaultValue=false)]
        public int? PartMonthTo { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Retirementreport {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("  WorkingRelationID: ").Append(WorkingRelationID).Append("\n");
            sb.Append("  StateOfDocument: ").Append(StateOfDocument).Append("\n");
            sb.Append("  StateOfSending: ").Append(StateOfSending).Append("\n");
            sb.Append("  DocumentData: ").Append(DocumentData).Append("\n");
            sb.Append("  CalendarYear: ").Append(CalendarYear).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  Corrective: ").Append(Corrective).Append("\n");
            sb.Append("  PartOfYear: ").Append(PartOfYear).Append("\n");
            sb.Append("  PartDateFromDATE: ").Append(PartDateFromDATE).Append("\n");
            sb.Append("  PartDateToDATE: ").Append(PartDateToDATE).Append("\n");
            sb.Append("  StateOfDocumentText: ").Append(StateOfDocumentText).Append("\n");
            sb.Append("  StateOfSendingText: ").Append(StateOfSendingText).Append("\n");
            sb.Append("  EmployeeID: ").Append(EmployeeID).Append("\n");
            sb.Append("  RetirementReportDefID: ").Append(RetirementReportDefID).Append("\n");
            sb.Append("  CommunicationDocumentID: ").Append(CommunicationDocumentID).Append("\n");
            sb.Append("  EmployeeName: ").Append(EmployeeName).Append("\n");
            sb.Append("  PersonalNumber: ").Append(PersonalNumber).Append("\n");
            sb.Append("  PartMonthFrom: ").Append(PartMonthFrom).Append("\n");
            sb.Append("  PartMonthTo: ").Append(PartMonthTo).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Retirementreport);
        }

        /// <summary>
        /// Returns true if Retirementreport instances are equal
        /// </summary>
        /// <param name="input">Instance of Retirementreport to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Retirementreport input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Description == input.Description ||
                    (this.Description != null &&
                    this.Description.Equals(input.Description))
                ) && 
                (
                    this.WorkingRelationID == input.WorkingRelationID ||
                    (this.WorkingRelationID != null &&
                    this.WorkingRelationID.Equals(input.WorkingRelationID))
                ) && 
                (
                    this.StateOfDocument == input.StateOfDocument ||
                    (this.StateOfDocument != null &&
                    this.StateOfDocument.Equals(input.StateOfDocument))
                ) && 
                (
                    this.StateOfSending == input.StateOfSending ||
                    (this.StateOfSending != null &&
                    this.StateOfSending.Equals(input.StateOfSending))
                ) && 
                (
                    this.DocumentData == input.DocumentData ||
                    (this.DocumentData != null &&
                    this.DocumentData.Equals(input.DocumentData))
                ) && 
                (
                    this.CalendarYear == input.CalendarYear ||
                    (this.CalendarYear != null &&
                    this.CalendarYear.Equals(input.CalendarYear))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.Corrective == input.Corrective ||
                    (this.Corrective != null &&
                    this.Corrective.Equals(input.Corrective))
                ) && 
                (
                    this.PartOfYear == input.PartOfYear ||
                    (this.PartOfYear != null &&
                    this.PartOfYear.Equals(input.PartOfYear))
                ) && 
                (
                    this.PartDateFromDATE == input.PartDateFromDATE ||
                    (this.PartDateFromDATE != null &&
                    this.PartDateFromDATE.Equals(input.PartDateFromDATE))
                ) && 
                (
                    this.PartDateToDATE == input.PartDateToDATE ||
                    (this.PartDateToDATE != null &&
                    this.PartDateToDATE.Equals(input.PartDateToDATE))
                ) && 
                (
                    this.StateOfDocumentText == input.StateOfDocumentText ||
                    (this.StateOfDocumentText != null &&
                    this.StateOfDocumentText.Equals(input.StateOfDocumentText))
                ) && 
                (
                    this.StateOfSendingText == input.StateOfSendingText ||
                    (this.StateOfSendingText != null &&
                    this.StateOfSendingText.Equals(input.StateOfSendingText))
                ) && 
                (
                    this.EmployeeID == input.EmployeeID ||
                    (this.EmployeeID != null &&
                    this.EmployeeID.Equals(input.EmployeeID))
                ) && 
                (
                    this.RetirementReportDefID == input.RetirementReportDefID ||
                    (this.RetirementReportDefID != null &&
                    this.RetirementReportDefID.Equals(input.RetirementReportDefID))
                ) && 
                (
                    this.CommunicationDocumentID == input.CommunicationDocumentID ||
                    (this.CommunicationDocumentID != null &&
                    this.CommunicationDocumentID.Equals(input.CommunicationDocumentID))
                ) && 
                (
                    this.EmployeeName == input.EmployeeName ||
                    (this.EmployeeName != null &&
                    this.EmployeeName.Equals(input.EmployeeName))
                ) && 
                (
                    this.PersonalNumber == input.PersonalNumber ||
                    (this.PersonalNumber != null &&
                    this.PersonalNumber.Equals(input.PersonalNumber))
                ) && 
                (
                    this.PartMonthFrom == input.PartMonthFrom ||
                    (this.PartMonthFrom != null &&
                    this.PartMonthFrom.Equals(input.PartMonthFrom))
                ) && 
                (
                    this.PartMonthTo == input.PartMonthTo ||
                    (this.PartMonthTo != null &&
                    this.PartMonthTo.Equals(input.PartMonthTo))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Description != null)
                    hashCode = hashCode * 59 + this.Description.GetHashCode();
                if (this.WorkingRelationID != null)
                    hashCode = hashCode * 59 + this.WorkingRelationID.GetHashCode();
                if (this.StateOfDocument != null)
                    hashCode = hashCode * 59 + this.StateOfDocument.GetHashCode();
                if (this.StateOfSending != null)
                    hashCode = hashCode * 59 + this.StateOfSending.GetHashCode();
                if (this.DocumentData != null)
                    hashCode = hashCode * 59 + this.DocumentData.GetHashCode();
                if (this.CalendarYear != null)
                    hashCode = hashCode * 59 + this.CalendarYear.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.Corrective != null)
                    hashCode = hashCode * 59 + this.Corrective.GetHashCode();
                if (this.PartOfYear != null)
                    hashCode = hashCode * 59 + this.PartOfYear.GetHashCode();
                if (this.PartDateFromDATE != null)
                    hashCode = hashCode * 59 + this.PartDateFromDATE.GetHashCode();
                if (this.PartDateToDATE != null)
                    hashCode = hashCode * 59 + this.PartDateToDATE.GetHashCode();
                if (this.StateOfDocumentText != null)
                    hashCode = hashCode * 59 + this.StateOfDocumentText.GetHashCode();
                if (this.StateOfSendingText != null)
                    hashCode = hashCode * 59 + this.StateOfSendingText.GetHashCode();
                if (this.EmployeeID != null)
                    hashCode = hashCode * 59 + this.EmployeeID.GetHashCode();
                if (this.RetirementReportDefID != null)
                    hashCode = hashCode * 59 + this.RetirementReportDefID.GetHashCode();
                if (this.CommunicationDocumentID != null)
                    hashCode = hashCode * 59 + this.CommunicationDocumentID.GetHashCode();
                if (this.EmployeeName != null)
                    hashCode = hashCode * 59 + this.EmployeeName.GetHashCode();
                if (this.PersonalNumber != null)
                    hashCode = hashCode * 59 + this.PersonalNumber.GetHashCode();
                if (this.PartMonthFrom != null)
                    hashCode = hashCode * 59 + this.PartMonthFrom.GetHashCode();
                if (this.PartMonthTo != null)
                    hashCode = hashCode * 59 + this.PartMonthTo.GetHashCode();
                return hashCode;
            }
        }
    }

}
