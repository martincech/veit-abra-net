/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Pdmuser
    /// </summary>
    [DataContract]
    public partial class Pdmuser :  IEquatable<Pdmuser>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pdmuser" /> class.
        /// </summary>
        /// <param name="hidden">Skrytý [persistentní položka].</param>
        /// <param name="securityUserID">Uživatel systému; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="personID">Osoba; ID objektu Osoba [persistentní položka].</param>
        /// <param name="divisionID">Středisko; ID objektu Středisko [persistentní položka].</param>
        /// <param name="postProviderID">Poštovní poskytovatel; ID objektu Poskytovatel poštovních služeb [persistentní položka].</param>
        /// <param name="personName">Jméno osoby.</param>
        public Pdmuser(bool? hidden = default(bool?), string securityUserID = default(string), string personID = default(string), string divisionID = default(string), string postProviderID = default(string), string personName = default(string))
        {
            this.Hidden = hidden;
            this.SecurityUserID = securityUserID;
            this.PersonID = personID;
            this.DivisionID = divisionID;
            this.PostProviderID = postProviderID;
            this.PersonName = personName;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Skrytý [persistentní položka]
        /// </summary>
        /// <value>Skrytý [persistentní položka]</value>
        [DataMember(Name="Hidden", EmitDefaultValue=false)]
        public bool? Hidden { get; set; }

        /// <summary>
        /// Uživatel systému; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Uživatel systému; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="SecurityUser_ID", EmitDefaultValue=false)]
        public string SecurityUserID { get; set; }

        /// <summary>
        /// Osoba; ID objektu Osoba [persistentní položka]
        /// </summary>
        /// <value>Osoba; ID objektu Osoba [persistentní položka]</value>
        [DataMember(Name="Person_ID", EmitDefaultValue=false)]
        public string PersonID { get; set; }

        /// <summary>
        /// Středisko; ID objektu Středisko [persistentní položka]
        /// </summary>
        /// <value>Středisko; ID objektu Středisko [persistentní položka]</value>
        [DataMember(Name="Division_ID", EmitDefaultValue=false)]
        public string DivisionID { get; set; }

        /// <summary>
        /// Poštovní poskytovatel; ID objektu Poskytovatel poštovních služeb [persistentní položka]
        /// </summary>
        /// <value>Poštovní poskytovatel; ID objektu Poskytovatel poštovních služeb [persistentní položka]</value>
        [DataMember(Name="PostProvider_ID", EmitDefaultValue=false)]
        public string PostProviderID { get; set; }

        /// <summary>
        /// Jméno osoby
        /// </summary>
        /// <value>Jméno osoby</value>
        [DataMember(Name="PersonName", EmitDefaultValue=false)]
        public string PersonName { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Pdmuser {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Hidden: ").Append(Hidden).Append("\n");
            sb.Append("  SecurityUserID: ").Append(SecurityUserID).Append("\n");
            sb.Append("  PersonID: ").Append(PersonID).Append("\n");
            sb.Append("  DivisionID: ").Append(DivisionID).Append("\n");
            sb.Append("  PostProviderID: ").Append(PostProviderID).Append("\n");
            sb.Append("  PersonName: ").Append(PersonName).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Pdmuser);
        }

        /// <summary>
        /// Returns true if Pdmuser instances are equal
        /// </summary>
        /// <param name="input">Instance of Pdmuser to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Pdmuser input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Hidden == input.Hidden ||
                    (this.Hidden != null &&
                    this.Hidden.Equals(input.Hidden))
                ) && 
                (
                    this.SecurityUserID == input.SecurityUserID ||
                    (this.SecurityUserID != null &&
                    this.SecurityUserID.Equals(input.SecurityUserID))
                ) && 
                (
                    this.PersonID == input.PersonID ||
                    (this.PersonID != null &&
                    this.PersonID.Equals(input.PersonID))
                ) && 
                (
                    this.DivisionID == input.DivisionID ||
                    (this.DivisionID != null &&
                    this.DivisionID.Equals(input.DivisionID))
                ) && 
                (
                    this.PostProviderID == input.PostProviderID ||
                    (this.PostProviderID != null &&
                    this.PostProviderID.Equals(input.PostProviderID))
                ) && 
                (
                    this.PersonName == input.PersonName ||
                    (this.PersonName != null &&
                    this.PersonName.Equals(input.PersonName))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Hidden != null)
                    hashCode = hashCode * 59 + this.Hidden.GetHashCode();
                if (this.SecurityUserID != null)
                    hashCode = hashCode * 59 + this.SecurityUserID.GetHashCode();
                if (this.PersonID != null)
                    hashCode = hashCode * 59 + this.PersonID.GetHashCode();
                if (this.DivisionID != null)
                    hashCode = hashCode * 59 + this.DivisionID.GetHashCode();
                if (this.PostProviderID != null)
                    hashCode = hashCode * 59 + this.PostProviderID.GetHashCode();
                if (this.PersonName != null)
                    hashCode = hashCode * 59 + this.PersonName.GetHashCode();
                return hashCode;
            }
        }
    }

}
