/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Accaccrual
    /// </summary>
    [DataContract]
    public partial class Accaccrual :  IEquatable<Accaccrual>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Accaccrual" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Řádek časového rozlišení [nepersistentní položka].</param>
        /// <param name="sourceDocType">Typ zdrojového dokladu [persistentní položka].</param>
        /// <param name="sourceDocID">ID zdrojového účtovaného dokladu; ID objektu Dokument [persistentní položka].</param>
        /// <param name="sourceDocRowID">ID řádku zdrojového účtovaného dokladu [persistentní položka].</param>
        /// <param name="accrualsDateFromDATE">Datum časového rozlišení od [persistentní položka].</param>
        /// <param name="accrualsDateToDATE">Datum časového rozlišení do [persistentní položka].</param>
        /// <param name="periodCount">Počet období časového rozlišení [persistentní položka].</param>
        /// <param name="periodType">Typ periody časového rozlišení [persistentní položka].</param>
        /// <param name="userChange">Rozpady čas. rozlišení změněny uživatelem [persistentní položka].</param>
        /// <param name="totalRowLocalAmount">Celková částka (lok.) [persistentní položka].</param>
        /// <param name="totalRowAmountInCurrency">Celková částka [persistentní položka].</param>
        /// <param name="sourceDocumentAllRows">Platí pro celý zdr. doklad [persistentní položka].</param>
        /// <param name="sourceDocAccDateDATE">Datum zaúčtování [persistentní položka].</param>
        public Accaccrual(List<Accaccrualrow> rows = default(List<Accaccrualrow>), string sourceDocType = default(string), string sourceDocID = default(string), string sourceDocRowID = default(string), DateTimeOffset? accrualsDateFromDATE = default(DateTimeOffset?), DateTimeOffset? accrualsDateToDATE = default(DateTimeOffset?), int? periodCount = default(int?), int? periodType = default(int?), bool? userChange = default(bool?), double? totalRowLocalAmount = default(double?), double? totalRowAmountInCurrency = default(double?), bool? sourceDocumentAllRows = default(bool?), DateTimeOffset? sourceDocAccDateDATE = default(DateTimeOffset?))
        {
            this.Rows = rows;
            this.SourceDocType = sourceDocType;
            this.SourceDocID = sourceDocID;
            this.SourceDocRowID = sourceDocRowID;
            this.AccrualsDateFromDATE = accrualsDateFromDATE;
            this.AccrualsDateToDATE = accrualsDateToDATE;
            this.PeriodCount = periodCount;
            this.PeriodType = periodType;
            this.UserChange = userChange;
            this.TotalRowLocalAmount = totalRowLocalAmount;
            this.TotalRowAmountInCurrency = totalRowAmountInCurrency;
            this.SourceDocumentAllRows = sourceDocumentAllRows;
            this.SourceDocAccDateDATE = sourceDocAccDateDATE;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Řádek časového rozlišení [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Řádek časového rozlišení [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Accaccrualrow> Rows { get; set; }

        /// <summary>
        /// Typ zdrojového dokladu [persistentní položka]
        /// </summary>
        /// <value>Typ zdrojového dokladu [persistentní položka]</value>
        [DataMember(Name="SourceDocType", EmitDefaultValue=false)]
        public string SourceDocType { get; set; }

        /// <summary>
        /// ID zdrojového účtovaného dokladu; ID objektu Dokument [persistentní položka]
        /// </summary>
        /// <value>ID zdrojového účtovaného dokladu; ID objektu Dokument [persistentní položka]</value>
        [DataMember(Name="SourceDoc_ID", EmitDefaultValue=false)]
        public string SourceDocID { get; set; }

        /// <summary>
        /// ID řádku zdrojového účtovaného dokladu [persistentní položka]
        /// </summary>
        /// <value>ID řádku zdrojového účtovaného dokladu [persistentní položka]</value>
        [DataMember(Name="SourceDocRow_ID", EmitDefaultValue=false)]
        public string SourceDocRowID { get; set; }

        /// <summary>
        /// Datum časového rozlišení od [persistentní položka]
        /// </summary>
        /// <value>Datum časového rozlišení od [persistentní položka]</value>
        [DataMember(Name="AccrualsDateFrom$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? AccrualsDateFromDATE { get; set; }

        /// <summary>
        /// Datum časového rozlišení do [persistentní položka]
        /// </summary>
        /// <value>Datum časového rozlišení do [persistentní položka]</value>
        [DataMember(Name="AccrualsDateTo$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? AccrualsDateToDATE { get; set; }

        /// <summary>
        /// Počet období časového rozlišení [persistentní položka]
        /// </summary>
        /// <value>Počet období časového rozlišení [persistentní položka]</value>
        [DataMember(Name="PeriodCount", EmitDefaultValue=false)]
        public int? PeriodCount { get; set; }

        /// <summary>
        /// Typ periody časového rozlišení [persistentní položka]
        /// </summary>
        /// <value>Typ periody časového rozlišení [persistentní položka]</value>
        [DataMember(Name="PeriodType", EmitDefaultValue=false)]
        public int? PeriodType { get; set; }

        /// <summary>
        /// Rozpady čas. rozlišení změněny uživatelem [persistentní položka]
        /// </summary>
        /// <value>Rozpady čas. rozlišení změněny uživatelem [persistentní položka]</value>
        [DataMember(Name="UserChange", EmitDefaultValue=false)]
        public bool? UserChange { get; set; }

        /// <summary>
        /// Celková částka (lok.) [persistentní položka]
        /// </summary>
        /// <value>Celková částka (lok.) [persistentní položka]</value>
        [DataMember(Name="TotalRowLocalAmount", EmitDefaultValue=false)]
        public double? TotalRowLocalAmount { get; set; }

        /// <summary>
        /// Celková částka [persistentní položka]
        /// </summary>
        /// <value>Celková částka [persistentní položka]</value>
        [DataMember(Name="TotalRowAmountInCurrency", EmitDefaultValue=false)]
        public double? TotalRowAmountInCurrency { get; set; }

        /// <summary>
        /// Platí pro celý zdr. doklad [persistentní položka]
        /// </summary>
        /// <value>Platí pro celý zdr. doklad [persistentní položka]</value>
        [DataMember(Name="SourceDocumentAllRows", EmitDefaultValue=false)]
        public bool? SourceDocumentAllRows { get; set; }

        /// <summary>
        /// Datum zaúčtování [persistentní položka]
        /// </summary>
        /// <value>Datum zaúčtování [persistentní položka]</value>
        [DataMember(Name="SourceDocAccDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? SourceDocAccDateDATE { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Accaccrual {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  SourceDocType: ").Append(SourceDocType).Append("\n");
            sb.Append("  SourceDocID: ").Append(SourceDocID).Append("\n");
            sb.Append("  SourceDocRowID: ").Append(SourceDocRowID).Append("\n");
            sb.Append("  AccrualsDateFromDATE: ").Append(AccrualsDateFromDATE).Append("\n");
            sb.Append("  AccrualsDateToDATE: ").Append(AccrualsDateToDATE).Append("\n");
            sb.Append("  PeriodCount: ").Append(PeriodCount).Append("\n");
            sb.Append("  PeriodType: ").Append(PeriodType).Append("\n");
            sb.Append("  UserChange: ").Append(UserChange).Append("\n");
            sb.Append("  TotalRowLocalAmount: ").Append(TotalRowLocalAmount).Append("\n");
            sb.Append("  TotalRowAmountInCurrency: ").Append(TotalRowAmountInCurrency).Append("\n");
            sb.Append("  SourceDocumentAllRows: ").Append(SourceDocumentAllRows).Append("\n");
            sb.Append("  SourceDocAccDateDATE: ").Append(SourceDocAccDateDATE).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Accaccrual);
        }

        /// <summary>
        /// Returns true if Accaccrual instances are equal
        /// </summary>
        /// <param name="input">Instance of Accaccrual to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Accaccrual input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.SourceDocType == input.SourceDocType ||
                    (this.SourceDocType != null &&
                    this.SourceDocType.Equals(input.SourceDocType))
                ) && 
                (
                    this.SourceDocID == input.SourceDocID ||
                    (this.SourceDocID != null &&
                    this.SourceDocID.Equals(input.SourceDocID))
                ) && 
                (
                    this.SourceDocRowID == input.SourceDocRowID ||
                    (this.SourceDocRowID != null &&
                    this.SourceDocRowID.Equals(input.SourceDocRowID))
                ) && 
                (
                    this.AccrualsDateFromDATE == input.AccrualsDateFromDATE ||
                    (this.AccrualsDateFromDATE != null &&
                    this.AccrualsDateFromDATE.Equals(input.AccrualsDateFromDATE))
                ) && 
                (
                    this.AccrualsDateToDATE == input.AccrualsDateToDATE ||
                    (this.AccrualsDateToDATE != null &&
                    this.AccrualsDateToDATE.Equals(input.AccrualsDateToDATE))
                ) && 
                (
                    this.PeriodCount == input.PeriodCount ||
                    (this.PeriodCount != null &&
                    this.PeriodCount.Equals(input.PeriodCount))
                ) && 
                (
                    this.PeriodType == input.PeriodType ||
                    (this.PeriodType != null &&
                    this.PeriodType.Equals(input.PeriodType))
                ) && 
                (
                    this.UserChange == input.UserChange ||
                    (this.UserChange != null &&
                    this.UserChange.Equals(input.UserChange))
                ) && 
                (
                    this.TotalRowLocalAmount == input.TotalRowLocalAmount ||
                    (this.TotalRowLocalAmount != null &&
                    this.TotalRowLocalAmount.Equals(input.TotalRowLocalAmount))
                ) && 
                (
                    this.TotalRowAmountInCurrency == input.TotalRowAmountInCurrency ||
                    (this.TotalRowAmountInCurrency != null &&
                    this.TotalRowAmountInCurrency.Equals(input.TotalRowAmountInCurrency))
                ) && 
                (
                    this.SourceDocumentAllRows == input.SourceDocumentAllRows ||
                    (this.SourceDocumentAllRows != null &&
                    this.SourceDocumentAllRows.Equals(input.SourceDocumentAllRows))
                ) && 
                (
                    this.SourceDocAccDateDATE == input.SourceDocAccDateDATE ||
                    (this.SourceDocAccDateDATE != null &&
                    this.SourceDocAccDateDATE.Equals(input.SourceDocAccDateDATE))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.SourceDocType != null)
                    hashCode = hashCode * 59 + this.SourceDocType.GetHashCode();
                if (this.SourceDocID != null)
                    hashCode = hashCode * 59 + this.SourceDocID.GetHashCode();
                if (this.SourceDocRowID != null)
                    hashCode = hashCode * 59 + this.SourceDocRowID.GetHashCode();
                if (this.AccrualsDateFromDATE != null)
                    hashCode = hashCode * 59 + this.AccrualsDateFromDATE.GetHashCode();
                if (this.AccrualsDateToDATE != null)
                    hashCode = hashCode * 59 + this.AccrualsDateToDATE.GetHashCode();
                if (this.PeriodCount != null)
                    hashCode = hashCode * 59 + this.PeriodCount.GetHashCode();
                if (this.PeriodType != null)
                    hashCode = hashCode * 59 + this.PeriodType.GetHashCode();
                if (this.UserChange != null)
                    hashCode = hashCode * 59 + this.UserChange.GetHashCode();
                if (this.TotalRowLocalAmount != null)
                    hashCode = hashCode * 59 + this.TotalRowLocalAmount.GetHashCode();
                if (this.TotalRowAmountInCurrency != null)
                    hashCode = hashCode * 59 + this.TotalRowAmountInCurrency.GetHashCode();
                if (this.SourceDocumentAllRows != null)
                    hashCode = hashCode * 59 + this.SourceDocumentAllRows.GetHashCode();
                if (this.SourceDocAccDateDATE != null)
                    hashCode = hashCode * 59 + this.SourceDocAccDateDATE.GetHashCode();
                return hashCode;
            }
        }
    }

}
