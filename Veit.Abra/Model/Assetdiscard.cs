/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Assetdiscard
    /// </summary>
    [DataContract]
    public partial class Assetdiscard :  IEquatable<Assetdiscard>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Assetdiscard" /> class.
        /// </summary>
        /// <param name="docQueueID">Zdrojová řada; ID objektu Řada dokladů [persistentní položka].</param>
        /// <param name="periodID">Období; ID objektu Období [persistentní položka].</param>
        /// <param name="ordNumber">Pořadové číslo [persistentní položka].</param>
        /// <param name="docDateDATE">Datum dok. [persistentní položka].</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="newRelatedType">Typ relace.</param>
        /// <param name="newRelatedDocumentID">ID dokladu pro připojení.</param>
        /// <param name="accPresetDefID">Předkontace; ID objektu Účetní předkontace [persistentní položka].</param>
        /// <param name="description">Popis [persistentní položka].</param>
        /// <param name="accDateDATE">Datum účt. [persistentní položka].</param>
        /// <param name="accDocQueueID">Účetní řada; ID objektu Účetní řada dokladů [persistentní položka].</param>
        /// <param name="accountingType">Jak účtovat.</param>
        /// <param name="isAccounted">Účtováno.</param>
        /// <param name="dirty">Zakázané přepočítání.</param>
        /// <param name="assetCardType">Typ karty.</param>
        /// <param name="assetCardID">Karta majetku; ID objektu Karta majetku [persistentní položka].</param>
        /// <param name="taxBasePriceAmount">Daň.vstup.cena [persistentní položka].</param>
        /// <param name="accBasePriceAmount">Účet.vstup.cena [persistentní položka].</param>
        /// <param name="taxRemainderPriceAmount">Daň.zůst.cena [persistentní položka].</param>
        /// <param name="accRemainderPriceAmount">Účet.zůst.cena [persistentní položka].</param>
        /// <param name="divisionID">Středisko; ID objektu Středisko [persistentní položka].</param>
        /// <param name="busOrderID">Zakázka; ID objektu Zakázka [persistentní položka].</param>
        /// <param name="busTransactionID">Obch.případ; ID objektu Obchodní případ [persistentní položka].</param>
        /// <param name="busProjectID">Projekt; ID objektu Projekt [persistentní položka].</param>
        /// <param name="lastTaxDepreciationPerc">%daň.odpisu při vyř. [persistentní položka].</param>
        /// <param name="isFinal">úplné [persistentní položka].</param>
        /// <param name="isFinalAsText">úplné.</param>
        /// <param name="components">Prvky dokladu vyřazení majetku; kolekce BO Prvek na dokladu vyřazení majetku [nepersistentní položka].</param>
        public Assetdiscard(string docQueueID = default(string), string periodID = default(string), int? ordNumber = default(int?), DateTimeOffset? docDateDATE = default(DateTimeOffset?), string createdByID = default(string), string correctedByID = default(string), int? newRelatedType = default(int?), string newRelatedDocumentID = default(string), string accPresetDefID = default(string), string description = default(string), DateTimeOffset? accDateDATE = default(DateTimeOffset?), string accDocQueueID = default(string), int? accountingType = default(int?), bool? isAccounted = default(bool?), bool? dirty = default(bool?), int? assetCardType = default(int?), string assetCardID = default(string), double? taxBasePriceAmount = default(double?), double? accBasePriceAmount = default(double?), double? taxRemainderPriceAmount = default(double?), double? accRemainderPriceAmount = default(double?), string divisionID = default(string), string busOrderID = default(string), string busTransactionID = default(string), string busProjectID = default(string), double? lastTaxDepreciationPerc = default(double?), bool? isFinal = default(bool?), string isFinalAsText = default(string), List<Assetdiscardcomponent> components = default(List<Assetdiscardcomponent>))
        {
            this.DocQueueID = docQueueID;
            this.PeriodID = periodID;
            this.OrdNumber = ordNumber;
            this.DocDateDATE = docDateDATE;
            this.CreatedByID = createdByID;
            this.CorrectedByID = correctedByID;
            this.NewRelatedType = newRelatedType;
            this.NewRelatedDocumentID = newRelatedDocumentID;
            this.AccPresetDefID = accPresetDefID;
            this.Description = description;
            this.AccDateDATE = accDateDATE;
            this.AccDocQueueID = accDocQueueID;
            this.AccountingType = accountingType;
            this.IsAccounted = isAccounted;
            this.Dirty = dirty;
            this.AssetCardType = assetCardType;
            this.AssetCardID = assetCardID;
            this.TaxBasePriceAmount = taxBasePriceAmount;
            this.AccBasePriceAmount = accBasePriceAmount;
            this.TaxRemainderPriceAmount = taxRemainderPriceAmount;
            this.AccRemainderPriceAmount = accRemainderPriceAmount;
            this.DivisionID = divisionID;
            this.BusOrderID = busOrderID;
            this.BusTransactionID = busTransactionID;
            this.BusProjectID = busProjectID;
            this.LastTaxDepreciationPerc = lastTaxDepreciationPerc;
            this.IsFinal = isFinal;
            this.IsFinalAsText = isFinalAsText;
            this.Components = components;
        }
        
        /// <summary>
        /// Číslo dok.
        /// </summary>
        /// <value>Číslo dok.</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Zdrojová řada; ID objektu Řada dokladů [persistentní položka]
        /// </summary>
        /// <value>Zdrojová řada; ID objektu Řada dokladů [persistentní položka]</value>
        [DataMember(Name="DocQueue_ID", EmitDefaultValue=false)]
        public string DocQueueID { get; set; }

        /// <summary>
        /// Období; ID objektu Období [persistentní položka]
        /// </summary>
        /// <value>Období; ID objektu Období [persistentní položka]</value>
        [DataMember(Name="Period_ID", EmitDefaultValue=false)]
        public string PeriodID { get; set; }

        /// <summary>
        /// Pořadové číslo [persistentní položka]
        /// </summary>
        /// <value>Pořadové číslo [persistentní položka]</value>
        [DataMember(Name="OrdNumber", EmitDefaultValue=false)]
        public int? OrdNumber { get; set; }

        /// <summary>
        /// Datum dok. [persistentní položka]
        /// </summary>
        /// <value>Datum dok. [persistentní položka]</value>
        [DataMember(Name="DocDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DocDateDATE { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Typ relace
        /// </summary>
        /// <value>Typ relace</value>
        [DataMember(Name="NewRelatedType", EmitDefaultValue=false)]
        public int? NewRelatedType { get; set; }

        /// <summary>
        /// ID dokladu pro připojení
        /// </summary>
        /// <value>ID dokladu pro připojení</value>
        [DataMember(Name="NewRelatedDocument_ID", EmitDefaultValue=false)]
        public string NewRelatedDocumentID { get; set; }

        /// <summary>
        /// Předkontace; ID objektu Účetní předkontace [persistentní položka]
        /// </summary>
        /// <value>Předkontace; ID objektu Účetní předkontace [persistentní položka]</value>
        [DataMember(Name="AccPresetDef_ID", EmitDefaultValue=false)]
        public string AccPresetDefID { get; set; }

        /// <summary>
        /// Popis [persistentní položka]
        /// </summary>
        /// <value>Popis [persistentní položka]</value>
        [DataMember(Name="Description", EmitDefaultValue=false)]
        public string Description { get; set; }

        /// <summary>
        /// Datum účt. [persistentní položka]
        /// </summary>
        /// <value>Datum účt. [persistentní položka]</value>
        [DataMember(Name="AccDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? AccDateDATE { get; set; }

        /// <summary>
        /// Účetní řada; ID objektu Účetní řada dokladů [persistentní položka]
        /// </summary>
        /// <value>Účetní řada; ID objektu Účetní řada dokladů [persistentní položka]</value>
        [DataMember(Name="AccDocQueue_ID", EmitDefaultValue=false)]
        public string AccDocQueueID { get; set; }

        /// <summary>
        /// Jak účtovat
        /// </summary>
        /// <value>Jak účtovat</value>
        [DataMember(Name="AccountingType", EmitDefaultValue=false)]
        public int? AccountingType { get; set; }

        /// <summary>
        /// Účtováno
        /// </summary>
        /// <value>Účtováno</value>
        [DataMember(Name="IsAccounted", EmitDefaultValue=false)]
        public bool? IsAccounted { get; set; }

        /// <summary>
        /// Zakázané přepočítání
        /// </summary>
        /// <value>Zakázané přepočítání</value>
        [DataMember(Name="Dirty", EmitDefaultValue=false)]
        public bool? Dirty { get; set; }

        /// <summary>
        /// Typ karty
        /// </summary>
        /// <value>Typ karty</value>
        [DataMember(Name="AssetCard_Type", EmitDefaultValue=false)]
        public int? AssetCardType { get; set; }

        /// <summary>
        /// Karta majetku; ID objektu Karta majetku [persistentní položka]
        /// </summary>
        /// <value>Karta majetku; ID objektu Karta majetku [persistentní položka]</value>
        [DataMember(Name="AssetCard_ID", EmitDefaultValue=false)]
        public string AssetCardID { get; set; }

        /// <summary>
        /// Daň.vstup.cena [persistentní položka]
        /// </summary>
        /// <value>Daň.vstup.cena [persistentní položka]</value>
        [DataMember(Name="TaxBasePriceAmount", EmitDefaultValue=false)]
        public double? TaxBasePriceAmount { get; set; }

        /// <summary>
        /// Účet.vstup.cena [persistentní položka]
        /// </summary>
        /// <value>Účet.vstup.cena [persistentní položka]</value>
        [DataMember(Name="AccBasePriceAmount", EmitDefaultValue=false)]
        public double? AccBasePriceAmount { get; set; }

        /// <summary>
        /// Daň.zůst.cena [persistentní položka]
        /// </summary>
        /// <value>Daň.zůst.cena [persistentní položka]</value>
        [DataMember(Name="TaxRemainderPriceAmount", EmitDefaultValue=false)]
        public double? TaxRemainderPriceAmount { get; set; }

        /// <summary>
        /// Účet.zůst.cena [persistentní položka]
        /// </summary>
        /// <value>Účet.zůst.cena [persistentní položka]</value>
        [DataMember(Name="AccRemainderPriceAmount", EmitDefaultValue=false)]
        public double? AccRemainderPriceAmount { get; set; }

        /// <summary>
        /// Středisko; ID objektu Středisko [persistentní položka]
        /// </summary>
        /// <value>Středisko; ID objektu Středisko [persistentní položka]</value>
        [DataMember(Name="Division_ID", EmitDefaultValue=false)]
        public string DivisionID { get; set; }

        /// <summary>
        /// Zakázka; ID objektu Zakázka [persistentní položka]
        /// </summary>
        /// <value>Zakázka; ID objektu Zakázka [persistentní položka]</value>
        [DataMember(Name="BusOrder_ID", EmitDefaultValue=false)]
        public string BusOrderID { get; set; }

        /// <summary>
        /// Obch.případ; ID objektu Obchodní případ [persistentní položka]
        /// </summary>
        /// <value>Obch.případ; ID objektu Obchodní případ [persistentní položka]</value>
        [DataMember(Name="BusTransaction_ID", EmitDefaultValue=false)]
        public string BusTransactionID { get; set; }

        /// <summary>
        /// Projekt; ID objektu Projekt [persistentní položka]
        /// </summary>
        /// <value>Projekt; ID objektu Projekt [persistentní položka]</value>
        [DataMember(Name="BusProject_ID", EmitDefaultValue=false)]
        public string BusProjectID { get; set; }

        /// <summary>
        /// %daň.odpisu při vyř. [persistentní položka]
        /// </summary>
        /// <value>%daň.odpisu při vyř. [persistentní položka]</value>
        [DataMember(Name="LastTaxDepreciationPerc", EmitDefaultValue=false)]
        public double? LastTaxDepreciationPerc { get; set; }

        /// <summary>
        /// úplné [persistentní položka]
        /// </summary>
        /// <value>úplné [persistentní položka]</value>
        [DataMember(Name="IsFinal", EmitDefaultValue=false)]
        public bool? IsFinal { get; set; }

        /// <summary>
        /// úplné
        /// </summary>
        /// <value>úplné</value>
        [DataMember(Name="IsFinalAsText", EmitDefaultValue=false)]
        public string IsFinalAsText { get; set; }

        /// <summary>
        /// Prvky dokladu vyřazení majetku; kolekce BO Prvek na dokladu vyřazení majetku [nepersistentní položka]
        /// </summary>
        /// <value>Prvky dokladu vyřazení majetku; kolekce BO Prvek na dokladu vyřazení majetku [nepersistentní položka]</value>
        [DataMember(Name="Components", EmitDefaultValue=false)]
        public List<Assetdiscardcomponent> Components { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Assetdiscard {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  DocQueueID: ").Append(DocQueueID).Append("\n");
            sb.Append("  PeriodID: ").Append(PeriodID).Append("\n");
            sb.Append("  OrdNumber: ").Append(OrdNumber).Append("\n");
            sb.Append("  DocDateDATE: ").Append(DocDateDATE).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  NewRelatedType: ").Append(NewRelatedType).Append("\n");
            sb.Append("  NewRelatedDocumentID: ").Append(NewRelatedDocumentID).Append("\n");
            sb.Append("  AccPresetDefID: ").Append(AccPresetDefID).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("  AccDateDATE: ").Append(AccDateDATE).Append("\n");
            sb.Append("  AccDocQueueID: ").Append(AccDocQueueID).Append("\n");
            sb.Append("  AccountingType: ").Append(AccountingType).Append("\n");
            sb.Append("  IsAccounted: ").Append(IsAccounted).Append("\n");
            sb.Append("  Dirty: ").Append(Dirty).Append("\n");
            sb.Append("  AssetCardType: ").Append(AssetCardType).Append("\n");
            sb.Append("  AssetCardID: ").Append(AssetCardID).Append("\n");
            sb.Append("  TaxBasePriceAmount: ").Append(TaxBasePriceAmount).Append("\n");
            sb.Append("  AccBasePriceAmount: ").Append(AccBasePriceAmount).Append("\n");
            sb.Append("  TaxRemainderPriceAmount: ").Append(TaxRemainderPriceAmount).Append("\n");
            sb.Append("  AccRemainderPriceAmount: ").Append(AccRemainderPriceAmount).Append("\n");
            sb.Append("  DivisionID: ").Append(DivisionID).Append("\n");
            sb.Append("  BusOrderID: ").Append(BusOrderID).Append("\n");
            sb.Append("  BusTransactionID: ").Append(BusTransactionID).Append("\n");
            sb.Append("  BusProjectID: ").Append(BusProjectID).Append("\n");
            sb.Append("  LastTaxDepreciationPerc: ").Append(LastTaxDepreciationPerc).Append("\n");
            sb.Append("  IsFinal: ").Append(IsFinal).Append("\n");
            sb.Append("  IsFinalAsText: ").Append(IsFinalAsText).Append("\n");
            sb.Append("  Components: ").Append(Components).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Assetdiscard);
        }

        /// <summary>
        /// Returns true if Assetdiscard instances are equal
        /// </summary>
        /// <param name="input">Instance of Assetdiscard to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Assetdiscard input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.DocQueueID == input.DocQueueID ||
                    (this.DocQueueID != null &&
                    this.DocQueueID.Equals(input.DocQueueID))
                ) && 
                (
                    this.PeriodID == input.PeriodID ||
                    (this.PeriodID != null &&
                    this.PeriodID.Equals(input.PeriodID))
                ) && 
                (
                    this.OrdNumber == input.OrdNumber ||
                    (this.OrdNumber != null &&
                    this.OrdNumber.Equals(input.OrdNumber))
                ) && 
                (
                    this.DocDateDATE == input.DocDateDATE ||
                    (this.DocDateDATE != null &&
                    this.DocDateDATE.Equals(input.DocDateDATE))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.NewRelatedType == input.NewRelatedType ||
                    (this.NewRelatedType != null &&
                    this.NewRelatedType.Equals(input.NewRelatedType))
                ) && 
                (
                    this.NewRelatedDocumentID == input.NewRelatedDocumentID ||
                    (this.NewRelatedDocumentID != null &&
                    this.NewRelatedDocumentID.Equals(input.NewRelatedDocumentID))
                ) && 
                (
                    this.AccPresetDefID == input.AccPresetDefID ||
                    (this.AccPresetDefID != null &&
                    this.AccPresetDefID.Equals(input.AccPresetDefID))
                ) && 
                (
                    this.Description == input.Description ||
                    (this.Description != null &&
                    this.Description.Equals(input.Description))
                ) && 
                (
                    this.AccDateDATE == input.AccDateDATE ||
                    (this.AccDateDATE != null &&
                    this.AccDateDATE.Equals(input.AccDateDATE))
                ) && 
                (
                    this.AccDocQueueID == input.AccDocQueueID ||
                    (this.AccDocQueueID != null &&
                    this.AccDocQueueID.Equals(input.AccDocQueueID))
                ) && 
                (
                    this.AccountingType == input.AccountingType ||
                    (this.AccountingType != null &&
                    this.AccountingType.Equals(input.AccountingType))
                ) && 
                (
                    this.IsAccounted == input.IsAccounted ||
                    (this.IsAccounted != null &&
                    this.IsAccounted.Equals(input.IsAccounted))
                ) && 
                (
                    this.Dirty == input.Dirty ||
                    (this.Dirty != null &&
                    this.Dirty.Equals(input.Dirty))
                ) && 
                (
                    this.AssetCardType == input.AssetCardType ||
                    (this.AssetCardType != null &&
                    this.AssetCardType.Equals(input.AssetCardType))
                ) && 
                (
                    this.AssetCardID == input.AssetCardID ||
                    (this.AssetCardID != null &&
                    this.AssetCardID.Equals(input.AssetCardID))
                ) && 
                (
                    this.TaxBasePriceAmount == input.TaxBasePriceAmount ||
                    (this.TaxBasePriceAmount != null &&
                    this.TaxBasePriceAmount.Equals(input.TaxBasePriceAmount))
                ) && 
                (
                    this.AccBasePriceAmount == input.AccBasePriceAmount ||
                    (this.AccBasePriceAmount != null &&
                    this.AccBasePriceAmount.Equals(input.AccBasePriceAmount))
                ) && 
                (
                    this.TaxRemainderPriceAmount == input.TaxRemainderPriceAmount ||
                    (this.TaxRemainderPriceAmount != null &&
                    this.TaxRemainderPriceAmount.Equals(input.TaxRemainderPriceAmount))
                ) && 
                (
                    this.AccRemainderPriceAmount == input.AccRemainderPriceAmount ||
                    (this.AccRemainderPriceAmount != null &&
                    this.AccRemainderPriceAmount.Equals(input.AccRemainderPriceAmount))
                ) && 
                (
                    this.DivisionID == input.DivisionID ||
                    (this.DivisionID != null &&
                    this.DivisionID.Equals(input.DivisionID))
                ) && 
                (
                    this.BusOrderID == input.BusOrderID ||
                    (this.BusOrderID != null &&
                    this.BusOrderID.Equals(input.BusOrderID))
                ) && 
                (
                    this.BusTransactionID == input.BusTransactionID ||
                    (this.BusTransactionID != null &&
                    this.BusTransactionID.Equals(input.BusTransactionID))
                ) && 
                (
                    this.BusProjectID == input.BusProjectID ||
                    (this.BusProjectID != null &&
                    this.BusProjectID.Equals(input.BusProjectID))
                ) && 
                (
                    this.LastTaxDepreciationPerc == input.LastTaxDepreciationPerc ||
                    (this.LastTaxDepreciationPerc != null &&
                    this.LastTaxDepreciationPerc.Equals(input.LastTaxDepreciationPerc))
                ) && 
                (
                    this.IsFinal == input.IsFinal ||
                    (this.IsFinal != null &&
                    this.IsFinal.Equals(input.IsFinal))
                ) && 
                (
                    this.IsFinalAsText == input.IsFinalAsText ||
                    (this.IsFinalAsText != null &&
                    this.IsFinalAsText.Equals(input.IsFinalAsText))
                ) && 
                (
                    this.Components == input.Components ||
                    this.Components != null &&
                    this.Components.SequenceEqual(input.Components)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.DocQueueID != null)
                    hashCode = hashCode * 59 + this.DocQueueID.GetHashCode();
                if (this.PeriodID != null)
                    hashCode = hashCode * 59 + this.PeriodID.GetHashCode();
                if (this.OrdNumber != null)
                    hashCode = hashCode * 59 + this.OrdNumber.GetHashCode();
                if (this.DocDateDATE != null)
                    hashCode = hashCode * 59 + this.DocDateDATE.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.NewRelatedType != null)
                    hashCode = hashCode * 59 + this.NewRelatedType.GetHashCode();
                if (this.NewRelatedDocumentID != null)
                    hashCode = hashCode * 59 + this.NewRelatedDocumentID.GetHashCode();
                if (this.AccPresetDefID != null)
                    hashCode = hashCode * 59 + this.AccPresetDefID.GetHashCode();
                if (this.Description != null)
                    hashCode = hashCode * 59 + this.Description.GetHashCode();
                if (this.AccDateDATE != null)
                    hashCode = hashCode * 59 + this.AccDateDATE.GetHashCode();
                if (this.AccDocQueueID != null)
                    hashCode = hashCode * 59 + this.AccDocQueueID.GetHashCode();
                if (this.AccountingType != null)
                    hashCode = hashCode * 59 + this.AccountingType.GetHashCode();
                if (this.IsAccounted != null)
                    hashCode = hashCode * 59 + this.IsAccounted.GetHashCode();
                if (this.Dirty != null)
                    hashCode = hashCode * 59 + this.Dirty.GetHashCode();
                if (this.AssetCardType != null)
                    hashCode = hashCode * 59 + this.AssetCardType.GetHashCode();
                if (this.AssetCardID != null)
                    hashCode = hashCode * 59 + this.AssetCardID.GetHashCode();
                if (this.TaxBasePriceAmount != null)
                    hashCode = hashCode * 59 + this.TaxBasePriceAmount.GetHashCode();
                if (this.AccBasePriceAmount != null)
                    hashCode = hashCode * 59 + this.AccBasePriceAmount.GetHashCode();
                if (this.TaxRemainderPriceAmount != null)
                    hashCode = hashCode * 59 + this.TaxRemainderPriceAmount.GetHashCode();
                if (this.AccRemainderPriceAmount != null)
                    hashCode = hashCode * 59 + this.AccRemainderPriceAmount.GetHashCode();
                if (this.DivisionID != null)
                    hashCode = hashCode * 59 + this.DivisionID.GetHashCode();
                if (this.BusOrderID != null)
                    hashCode = hashCode * 59 + this.BusOrderID.GetHashCode();
                if (this.BusTransactionID != null)
                    hashCode = hashCode * 59 + this.BusTransactionID.GetHashCode();
                if (this.BusProjectID != null)
                    hashCode = hashCode * 59 + this.BusProjectID.GetHashCode();
                if (this.LastTaxDepreciationPerc != null)
                    hashCode = hashCode * 59 + this.LastTaxDepreciationPerc.GetHashCode();
                if (this.IsFinal != null)
                    hashCode = hashCode * 59 + this.IsFinal.GetHashCode();
                if (this.IsFinalAsText != null)
                    hashCode = hashCode * 59 + this.IsFinalAsText.GetHashCode();
                if (this.Components != null)
                    hashCode = hashCode * 59 + this.Components.GetHashCode();
                return hashCode;
            }
        }
    }

}
