/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Viescheckvatresponse
    /// </summary>
    [DataContract]
    public partial class Viescheckvatresponse :  IEquatable<Viescheckvatresponse>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Viescheckvatresponse" /> class.
        /// </summary>
        /// <param name="firmID">Firma; ID objektu Firma [persistentní položka].</param>
        /// <param name="vATIdentNumber">DIČ [persistentní položka].</param>
        /// <param name="vATIdentNumberStatus">Stav DIČ [persistentní položka].</param>
        /// <param name="vATIdentNumberStatusAsText">Stav DIČ - text.</param>
        /// <param name="requestDateDATE">Datum požadavku na ověření [persistentní položka].</param>
        /// <param name="responseDateTimeDATE">Datum a čas odpovědi [persistentní položka].</param>
        /// <param name="requestIdentifier">Identifikátor požadavku na ověření DIČ [persistentní položka].</param>
        /// <param name="traderName">Název subjektu v systému VIES [persistentní položka].</param>
        /// <param name="traderCompanyType">Typ subjektu [persistentní položka].</param>
        /// <param name="traderAddress">Kompletní adresa subjektu v systému VIES [persistentní položka].</param>
        /// <param name="traderStreet">Ulice subjektu [persistentní položka].</param>
        /// <param name="traderPostcode">PSČ subjektu [persistentní položka].</param>
        /// <param name="traderCity">Město subjektu [persistentní položka].</param>
        /// <param name="traderNameMatch">Shoda názvu subjektu [persistentní položka].</param>
        /// <param name="traderCompanyTypeMatch">Shoda typu subjektu [persistentní položka].</param>
        /// <param name="traderStreetMatch">Shoda ulice subjektu [persistentní položka].</param>
        /// <param name="traderPostcodeMatch">Shoda PSČ subjektu [persistentní položka].</param>
        /// <param name="traderCityMatch">Shoda města subjektu [persistentní položka].</param>
        /// <param name="checkMessage">Zpráva z provedeného ověření [persistentní položka].</param>
        public Viescheckvatresponse(string firmID = default(string), string vATIdentNumber = default(string), int? vATIdentNumberStatus = default(int?), string vATIdentNumberStatusAsText = default(string), DateTimeOffset? requestDateDATE = default(DateTimeOffset?), DateTimeOffset? responseDateTimeDATE = default(DateTimeOffset?), string requestIdentifier = default(string), string traderName = default(string), string traderCompanyType = default(string), string traderAddress = default(string), string traderStreet = default(string), string traderPostcode = default(string), string traderCity = default(string), int? traderNameMatch = default(int?), int? traderCompanyTypeMatch = default(int?), int? traderStreetMatch = default(int?), int? traderPostcodeMatch = default(int?), int? traderCityMatch = default(int?), string checkMessage = default(string))
        {
            this.FirmID = firmID;
            this.VATIdentNumber = vATIdentNumber;
            this.VATIdentNumberStatus = vATIdentNumberStatus;
            this.VATIdentNumberStatusAsText = vATIdentNumberStatusAsText;
            this.RequestDateDATE = requestDateDATE;
            this.ResponseDateTimeDATE = responseDateTimeDATE;
            this.RequestIdentifier = requestIdentifier;
            this.TraderName = traderName;
            this.TraderCompanyType = traderCompanyType;
            this.TraderAddress = traderAddress;
            this.TraderStreet = traderStreet;
            this.TraderPostcode = traderPostcode;
            this.TraderCity = traderCity;
            this.TraderNameMatch = traderNameMatch;
            this.TraderCompanyTypeMatch = traderCompanyTypeMatch;
            this.TraderStreetMatch = traderStreetMatch;
            this.TraderPostcodeMatch = traderPostcodeMatch;
            this.TraderCityMatch = traderCityMatch;
            this.CheckMessage = checkMessage;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Firma; ID objektu Firma [persistentní položka]
        /// </summary>
        /// <value>Firma; ID objektu Firma [persistentní položka]</value>
        [DataMember(Name="Firm_ID", EmitDefaultValue=false)]
        public string FirmID { get; set; }

        /// <summary>
        /// DIČ [persistentní položka]
        /// </summary>
        /// <value>DIČ [persistentní položka]</value>
        [DataMember(Name="VATIdentNumber", EmitDefaultValue=false)]
        public string VATIdentNumber { get; set; }

        /// <summary>
        /// Stav DIČ [persistentní položka]
        /// </summary>
        /// <value>Stav DIČ [persistentní položka]</value>
        [DataMember(Name="VATIdentNumberStatus", EmitDefaultValue=false)]
        public int? VATIdentNumberStatus { get; set; }

        /// <summary>
        /// Stav DIČ - text
        /// </summary>
        /// <value>Stav DIČ - text</value>
        [DataMember(Name="VATIdentNumberStatusAsText", EmitDefaultValue=false)]
        public string VATIdentNumberStatusAsText { get; set; }

        /// <summary>
        /// Datum požadavku na ověření [persistentní položka]
        /// </summary>
        /// <value>Datum požadavku na ověření [persistentní položka]</value>
        [DataMember(Name="RequestDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? RequestDateDATE { get; set; }

        /// <summary>
        /// Datum a čas odpovědi [persistentní položka]
        /// </summary>
        /// <value>Datum a čas odpovědi [persistentní položka]</value>
        [DataMember(Name="ResponseDateTime$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? ResponseDateTimeDATE { get; set; }

        /// <summary>
        /// Identifikátor požadavku na ověření DIČ [persistentní položka]
        /// </summary>
        /// <value>Identifikátor požadavku na ověření DIČ [persistentní položka]</value>
        [DataMember(Name="RequestIdentifier", EmitDefaultValue=false)]
        public string RequestIdentifier { get; set; }

        /// <summary>
        /// Název subjektu v systému VIES [persistentní položka]
        /// </summary>
        /// <value>Název subjektu v systému VIES [persistentní položka]</value>
        [DataMember(Name="TraderName", EmitDefaultValue=false)]
        public string TraderName { get; set; }

        /// <summary>
        /// Typ subjektu [persistentní položka]
        /// </summary>
        /// <value>Typ subjektu [persistentní položka]</value>
        [DataMember(Name="TraderCompanyType", EmitDefaultValue=false)]
        public string TraderCompanyType { get; set; }

        /// <summary>
        /// Kompletní adresa subjektu v systému VIES [persistentní položka]
        /// </summary>
        /// <value>Kompletní adresa subjektu v systému VIES [persistentní položka]</value>
        [DataMember(Name="TraderAddress", EmitDefaultValue=false)]
        public string TraderAddress { get; set; }

        /// <summary>
        /// Ulice subjektu [persistentní položka]
        /// </summary>
        /// <value>Ulice subjektu [persistentní položka]</value>
        [DataMember(Name="TraderStreet", EmitDefaultValue=false)]
        public string TraderStreet { get; set; }

        /// <summary>
        /// PSČ subjektu [persistentní položka]
        /// </summary>
        /// <value>PSČ subjektu [persistentní položka]</value>
        [DataMember(Name="TraderPostcode", EmitDefaultValue=false)]
        public string TraderPostcode { get; set; }

        /// <summary>
        /// Město subjektu [persistentní položka]
        /// </summary>
        /// <value>Město subjektu [persistentní položka]</value>
        [DataMember(Name="TraderCity", EmitDefaultValue=false)]
        public string TraderCity { get; set; }

        /// <summary>
        /// Shoda názvu subjektu [persistentní položka]
        /// </summary>
        /// <value>Shoda názvu subjektu [persistentní položka]</value>
        [DataMember(Name="TraderNameMatch", EmitDefaultValue=false)]
        public int? TraderNameMatch { get; set; }

        /// <summary>
        /// Shoda typu subjektu [persistentní položka]
        /// </summary>
        /// <value>Shoda typu subjektu [persistentní položka]</value>
        [DataMember(Name="TraderCompanyTypeMatch", EmitDefaultValue=false)]
        public int? TraderCompanyTypeMatch { get; set; }

        /// <summary>
        /// Shoda ulice subjektu [persistentní položka]
        /// </summary>
        /// <value>Shoda ulice subjektu [persistentní položka]</value>
        [DataMember(Name="TraderStreetMatch", EmitDefaultValue=false)]
        public int? TraderStreetMatch { get; set; }

        /// <summary>
        /// Shoda PSČ subjektu [persistentní položka]
        /// </summary>
        /// <value>Shoda PSČ subjektu [persistentní položka]</value>
        [DataMember(Name="TraderPostcodeMatch", EmitDefaultValue=false)]
        public int? TraderPostcodeMatch { get; set; }

        /// <summary>
        /// Shoda města subjektu [persistentní položka]
        /// </summary>
        /// <value>Shoda města subjektu [persistentní položka]</value>
        [DataMember(Name="TraderCityMatch", EmitDefaultValue=false)]
        public int? TraderCityMatch { get; set; }

        /// <summary>
        /// Zpráva z provedeného ověření [persistentní položka]
        /// </summary>
        /// <value>Zpráva z provedeného ověření [persistentní položka]</value>
        [DataMember(Name="CheckMessage", EmitDefaultValue=false)]
        public string CheckMessage { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Viescheckvatresponse {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  FirmID: ").Append(FirmID).Append("\n");
            sb.Append("  VATIdentNumber: ").Append(VATIdentNumber).Append("\n");
            sb.Append("  VATIdentNumberStatus: ").Append(VATIdentNumberStatus).Append("\n");
            sb.Append("  VATIdentNumberStatusAsText: ").Append(VATIdentNumberStatusAsText).Append("\n");
            sb.Append("  RequestDateDATE: ").Append(RequestDateDATE).Append("\n");
            sb.Append("  ResponseDateTimeDATE: ").Append(ResponseDateTimeDATE).Append("\n");
            sb.Append("  RequestIdentifier: ").Append(RequestIdentifier).Append("\n");
            sb.Append("  TraderName: ").Append(TraderName).Append("\n");
            sb.Append("  TraderCompanyType: ").Append(TraderCompanyType).Append("\n");
            sb.Append("  TraderAddress: ").Append(TraderAddress).Append("\n");
            sb.Append("  TraderStreet: ").Append(TraderStreet).Append("\n");
            sb.Append("  TraderPostcode: ").Append(TraderPostcode).Append("\n");
            sb.Append("  TraderCity: ").Append(TraderCity).Append("\n");
            sb.Append("  TraderNameMatch: ").Append(TraderNameMatch).Append("\n");
            sb.Append("  TraderCompanyTypeMatch: ").Append(TraderCompanyTypeMatch).Append("\n");
            sb.Append("  TraderStreetMatch: ").Append(TraderStreetMatch).Append("\n");
            sb.Append("  TraderPostcodeMatch: ").Append(TraderPostcodeMatch).Append("\n");
            sb.Append("  TraderCityMatch: ").Append(TraderCityMatch).Append("\n");
            sb.Append("  CheckMessage: ").Append(CheckMessage).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Viescheckvatresponse);
        }

        /// <summary>
        /// Returns true if Viescheckvatresponse instances are equal
        /// </summary>
        /// <param name="input">Instance of Viescheckvatresponse to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Viescheckvatresponse input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.FirmID == input.FirmID ||
                    (this.FirmID != null &&
                    this.FirmID.Equals(input.FirmID))
                ) && 
                (
                    this.VATIdentNumber == input.VATIdentNumber ||
                    (this.VATIdentNumber != null &&
                    this.VATIdentNumber.Equals(input.VATIdentNumber))
                ) && 
                (
                    this.VATIdentNumberStatus == input.VATIdentNumberStatus ||
                    (this.VATIdentNumberStatus != null &&
                    this.VATIdentNumberStatus.Equals(input.VATIdentNumberStatus))
                ) && 
                (
                    this.VATIdentNumberStatusAsText == input.VATIdentNumberStatusAsText ||
                    (this.VATIdentNumberStatusAsText != null &&
                    this.VATIdentNumberStatusAsText.Equals(input.VATIdentNumberStatusAsText))
                ) && 
                (
                    this.RequestDateDATE == input.RequestDateDATE ||
                    (this.RequestDateDATE != null &&
                    this.RequestDateDATE.Equals(input.RequestDateDATE))
                ) && 
                (
                    this.ResponseDateTimeDATE == input.ResponseDateTimeDATE ||
                    (this.ResponseDateTimeDATE != null &&
                    this.ResponseDateTimeDATE.Equals(input.ResponseDateTimeDATE))
                ) && 
                (
                    this.RequestIdentifier == input.RequestIdentifier ||
                    (this.RequestIdentifier != null &&
                    this.RequestIdentifier.Equals(input.RequestIdentifier))
                ) && 
                (
                    this.TraderName == input.TraderName ||
                    (this.TraderName != null &&
                    this.TraderName.Equals(input.TraderName))
                ) && 
                (
                    this.TraderCompanyType == input.TraderCompanyType ||
                    (this.TraderCompanyType != null &&
                    this.TraderCompanyType.Equals(input.TraderCompanyType))
                ) && 
                (
                    this.TraderAddress == input.TraderAddress ||
                    (this.TraderAddress != null &&
                    this.TraderAddress.Equals(input.TraderAddress))
                ) && 
                (
                    this.TraderStreet == input.TraderStreet ||
                    (this.TraderStreet != null &&
                    this.TraderStreet.Equals(input.TraderStreet))
                ) && 
                (
                    this.TraderPostcode == input.TraderPostcode ||
                    (this.TraderPostcode != null &&
                    this.TraderPostcode.Equals(input.TraderPostcode))
                ) && 
                (
                    this.TraderCity == input.TraderCity ||
                    (this.TraderCity != null &&
                    this.TraderCity.Equals(input.TraderCity))
                ) && 
                (
                    this.TraderNameMatch == input.TraderNameMatch ||
                    (this.TraderNameMatch != null &&
                    this.TraderNameMatch.Equals(input.TraderNameMatch))
                ) && 
                (
                    this.TraderCompanyTypeMatch == input.TraderCompanyTypeMatch ||
                    (this.TraderCompanyTypeMatch != null &&
                    this.TraderCompanyTypeMatch.Equals(input.TraderCompanyTypeMatch))
                ) && 
                (
                    this.TraderStreetMatch == input.TraderStreetMatch ||
                    (this.TraderStreetMatch != null &&
                    this.TraderStreetMatch.Equals(input.TraderStreetMatch))
                ) && 
                (
                    this.TraderPostcodeMatch == input.TraderPostcodeMatch ||
                    (this.TraderPostcodeMatch != null &&
                    this.TraderPostcodeMatch.Equals(input.TraderPostcodeMatch))
                ) && 
                (
                    this.TraderCityMatch == input.TraderCityMatch ||
                    (this.TraderCityMatch != null &&
                    this.TraderCityMatch.Equals(input.TraderCityMatch))
                ) && 
                (
                    this.CheckMessage == input.CheckMessage ||
                    (this.CheckMessage != null &&
                    this.CheckMessage.Equals(input.CheckMessage))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.FirmID != null)
                    hashCode = hashCode * 59 + this.FirmID.GetHashCode();
                if (this.VATIdentNumber != null)
                    hashCode = hashCode * 59 + this.VATIdentNumber.GetHashCode();
                if (this.VATIdentNumberStatus != null)
                    hashCode = hashCode * 59 + this.VATIdentNumberStatus.GetHashCode();
                if (this.VATIdentNumberStatusAsText != null)
                    hashCode = hashCode * 59 + this.VATIdentNumberStatusAsText.GetHashCode();
                if (this.RequestDateDATE != null)
                    hashCode = hashCode * 59 + this.RequestDateDATE.GetHashCode();
                if (this.ResponseDateTimeDATE != null)
                    hashCode = hashCode * 59 + this.ResponseDateTimeDATE.GetHashCode();
                if (this.RequestIdentifier != null)
                    hashCode = hashCode * 59 + this.RequestIdentifier.GetHashCode();
                if (this.TraderName != null)
                    hashCode = hashCode * 59 + this.TraderName.GetHashCode();
                if (this.TraderCompanyType != null)
                    hashCode = hashCode * 59 + this.TraderCompanyType.GetHashCode();
                if (this.TraderAddress != null)
                    hashCode = hashCode * 59 + this.TraderAddress.GetHashCode();
                if (this.TraderStreet != null)
                    hashCode = hashCode * 59 + this.TraderStreet.GetHashCode();
                if (this.TraderPostcode != null)
                    hashCode = hashCode * 59 + this.TraderPostcode.GetHashCode();
                if (this.TraderCity != null)
                    hashCode = hashCode * 59 + this.TraderCity.GetHashCode();
                if (this.TraderNameMatch != null)
                    hashCode = hashCode * 59 + this.TraderNameMatch.GetHashCode();
                if (this.TraderCompanyTypeMatch != null)
                    hashCode = hashCode * 59 + this.TraderCompanyTypeMatch.GetHashCode();
                if (this.TraderStreetMatch != null)
                    hashCode = hashCode * 59 + this.TraderStreetMatch.GetHashCode();
                if (this.TraderPostcodeMatch != null)
                    hashCode = hashCode * 59 + this.TraderPostcodeMatch.GetHashCode();
                if (this.TraderCityMatch != null)
                    hashCode = hashCode * 59 + this.TraderCityMatch.GetHashCode();
                if (this.CheckMessage != null)
                    hashCode = hashCode * 59 + this.CheckMessage.GetHashCode();
                return hashCode;
            }
        }
    }

}
