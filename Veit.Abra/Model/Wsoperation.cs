/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Wsoperation
    /// </summary>
    [DataContract]
    public partial class Wsoperation :  IEquatable<Wsoperation>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Wsoperation" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Operace pro webovou službu - parametr [nepersistentní položka].</param>
        /// <param name="hidden">Skrytý [persistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="methodType">Typ operace [persistentní položka].</param>
        /// <param name="sCRPackageName">Jméno balíčku [persistentní položka].</param>
        /// <param name="sCRLibraryName">Jméno knihovny [persistentní položka].</param>
        /// <param name="sCRMethodName">Jméno metody [persistentní položka].</param>
        /// <param name="sCRDescription">Identifikace skriptu.</param>
        public Wsoperation(List<Wsoperationrow> rows = default(List<Wsoperationrow>), bool? hidden = default(bool?), string name = default(string), int? methodType = default(int?), string sCRPackageName = default(string), string sCRLibraryName = default(string), string sCRMethodName = default(string), string sCRDescription = default(string))
        {
            this.Rows = rows;
            this.Hidden = hidden;
            this.Name = name;
            this.MethodType = methodType;
            this.SCRPackageName = sCRPackageName;
            this.SCRLibraryName = sCRLibraryName;
            this.SCRMethodName = sCRMethodName;
            this.SCRDescription = sCRDescription;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Operace pro webovou službu - parametr [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Operace pro webovou službu - parametr [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Wsoperationrow> Rows { get; set; }

        /// <summary>
        /// Skrytý [persistentní položka]
        /// </summary>
        /// <value>Skrytý [persistentní položka]</value>
        [DataMember(Name="Hidden", EmitDefaultValue=false)]
        public bool? Hidden { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Typ operace [persistentní položka]
        /// </summary>
        /// <value>Typ operace [persistentní položka]</value>
        [DataMember(Name="MethodType", EmitDefaultValue=false)]
        public int? MethodType { get; set; }

        /// <summary>
        /// Jméno balíčku [persistentní položka]
        /// </summary>
        /// <value>Jméno balíčku [persistentní položka]</value>
        [DataMember(Name="SCRPackageName", EmitDefaultValue=false)]
        public string SCRPackageName { get; set; }

        /// <summary>
        /// Jméno knihovny [persistentní položka]
        /// </summary>
        /// <value>Jméno knihovny [persistentní položka]</value>
        [DataMember(Name="SCRLibraryName", EmitDefaultValue=false)]
        public string SCRLibraryName { get; set; }

        /// <summary>
        /// Jméno metody [persistentní položka]
        /// </summary>
        /// <value>Jméno metody [persistentní položka]</value>
        [DataMember(Name="SCRMethodName", EmitDefaultValue=false)]
        public string SCRMethodName { get; set; }

        /// <summary>
        /// Identifikace skriptu
        /// </summary>
        /// <value>Identifikace skriptu</value>
        [DataMember(Name="SCRDescription", EmitDefaultValue=false)]
        public string SCRDescription { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Wsoperation {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  Hidden: ").Append(Hidden).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  MethodType: ").Append(MethodType).Append("\n");
            sb.Append("  SCRPackageName: ").Append(SCRPackageName).Append("\n");
            sb.Append("  SCRLibraryName: ").Append(SCRLibraryName).Append("\n");
            sb.Append("  SCRMethodName: ").Append(SCRMethodName).Append("\n");
            sb.Append("  SCRDescription: ").Append(SCRDescription).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Wsoperation);
        }

        /// <summary>
        /// Returns true if Wsoperation instances are equal
        /// </summary>
        /// <param name="input">Instance of Wsoperation to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Wsoperation input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.Hidden == input.Hidden ||
                    (this.Hidden != null &&
                    this.Hidden.Equals(input.Hidden))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.MethodType == input.MethodType ||
                    (this.MethodType != null &&
                    this.MethodType.Equals(input.MethodType))
                ) && 
                (
                    this.SCRPackageName == input.SCRPackageName ||
                    (this.SCRPackageName != null &&
                    this.SCRPackageName.Equals(input.SCRPackageName))
                ) && 
                (
                    this.SCRLibraryName == input.SCRLibraryName ||
                    (this.SCRLibraryName != null &&
                    this.SCRLibraryName.Equals(input.SCRLibraryName))
                ) && 
                (
                    this.SCRMethodName == input.SCRMethodName ||
                    (this.SCRMethodName != null &&
                    this.SCRMethodName.Equals(input.SCRMethodName))
                ) && 
                (
                    this.SCRDescription == input.SCRDescription ||
                    (this.SCRDescription != null &&
                    this.SCRDescription.Equals(input.SCRDescription))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.Hidden != null)
                    hashCode = hashCode * 59 + this.Hidden.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.MethodType != null)
                    hashCode = hashCode * 59 + this.MethodType.GetHashCode();
                if (this.SCRPackageName != null)
                    hashCode = hashCode * 59 + this.SCRPackageName.GetHashCode();
                if (this.SCRLibraryName != null)
                    hashCode = hashCode * 59 + this.SCRLibraryName.GetHashCode();
                if (this.SCRMethodName != null)
                    hashCode = hashCode * 59 + this.SCRMethodName.GetHashCode();
                if (this.SCRDescription != null)
                    hashCode = hashCode * 59 + this.SCRDescription.GetHashCode();
                return hashCode;
            }
        }
    }

}
