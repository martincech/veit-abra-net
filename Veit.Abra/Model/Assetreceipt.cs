/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Assetreceipt
    /// </summary>
    [DataContract]
    public partial class Assetreceipt :  IEquatable<Assetreceipt>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Assetreceipt" /> class.
        /// </summary>
        /// <param name="docQueueID">Zdrojová řada; ID objektu Řada dokladů [persistentní položka].</param>
        /// <param name="periodID">Období; ID objektu Období [persistentní položka].</param>
        /// <param name="ordNumber">Pořadové číslo [persistentní položka].</param>
        /// <param name="docDateDATE">Datum dok. [persistentní položka].</param>
        /// <param name="createdByID">Vytvořil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="correctedByID">Opravil; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="newRelatedType">Typ relace.</param>
        /// <param name="newRelatedDocumentID">ID dokladu pro připojení.</param>
        /// <param name="description">Popis [persistentní položka].</param>
        /// <param name="assetCardType">Typ karty [persistentní položka].</param>
        /// <param name="assetCardID">Karta majetku; ID objektu Karta majetku [persistentní položka].</param>
        /// <param name="divisionID">Středisko; ID objektu Středisko [persistentní položka].</param>
        /// <param name="busOrderID">Zakázka; ID objektu Zakázka [persistentní položka].</param>
        /// <param name="busTransactionID">Obch.případ; ID objektu Obchodní případ [persistentní položka].</param>
        /// <param name="busProjectID">Projekt; ID objektu Projekt [persistentní položka].</param>
        /// <param name="vATIncluded">Čerpat s DPH [persistentní položka].</param>
        /// <param name="amount">Částka [persistentní položka].</param>
        /// <param name="applyAmountInAssetCard">Promítnutí dokladu do karty.</param>
        public Assetreceipt(string docQueueID = default(string), string periodID = default(string), int? ordNumber = default(int?), DateTimeOffset? docDateDATE = default(DateTimeOffset?), string createdByID = default(string), string correctedByID = default(string), int? newRelatedType = default(int?), string newRelatedDocumentID = default(string), string description = default(string), int? assetCardType = default(int?), string assetCardID = default(string), string divisionID = default(string), string busOrderID = default(string), string busTransactionID = default(string), string busProjectID = default(string), bool? vATIncluded = default(bool?), double? amount = default(double?), int? applyAmountInAssetCard = default(int?))
        {
            this.DocQueueID = docQueueID;
            this.PeriodID = periodID;
            this.OrdNumber = ordNumber;
            this.DocDateDATE = docDateDATE;
            this.CreatedByID = createdByID;
            this.CorrectedByID = correctedByID;
            this.NewRelatedType = newRelatedType;
            this.NewRelatedDocumentID = newRelatedDocumentID;
            this.Description = description;
            this.AssetCardType = assetCardType;
            this.AssetCardID = assetCardID;
            this.DivisionID = divisionID;
            this.BusOrderID = busOrderID;
            this.BusTransactionID = busTransactionID;
            this.BusProjectID = busProjectID;
            this.VATIncluded = vATIncluded;
            this.Amount = amount;
            this.ApplyAmountInAssetCard = applyAmountInAssetCard;
        }
        
        /// <summary>
        /// Číslo dok.
        /// </summary>
        /// <value>Číslo dok.</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Zdrojová řada; ID objektu Řada dokladů [persistentní položka]
        /// </summary>
        /// <value>Zdrojová řada; ID objektu Řada dokladů [persistentní položka]</value>
        [DataMember(Name="DocQueue_ID", EmitDefaultValue=false)]
        public string DocQueueID { get; set; }

        /// <summary>
        /// Období; ID objektu Období [persistentní položka]
        /// </summary>
        /// <value>Období; ID objektu Období [persistentní položka]</value>
        [DataMember(Name="Period_ID", EmitDefaultValue=false)]
        public string PeriodID { get; set; }

        /// <summary>
        /// Pořadové číslo [persistentní položka]
        /// </summary>
        /// <value>Pořadové číslo [persistentní položka]</value>
        [DataMember(Name="OrdNumber", EmitDefaultValue=false)]
        public int? OrdNumber { get; set; }

        /// <summary>
        /// Datum dok. [persistentní položka]
        /// </summary>
        /// <value>Datum dok. [persistentní položka]</value>
        [DataMember(Name="DocDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DocDateDATE { get; set; }

        /// <summary>
        /// Vytvořil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Vytvořil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CreatedBy_ID", EmitDefaultValue=false)]
        public string CreatedByID { get; set; }

        /// <summary>
        /// Opravil; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Opravil; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CorrectedBy_ID", EmitDefaultValue=false)]
        public string CorrectedByID { get; set; }

        /// <summary>
        /// Typ relace
        /// </summary>
        /// <value>Typ relace</value>
        [DataMember(Name="NewRelatedType", EmitDefaultValue=false)]
        public int? NewRelatedType { get; set; }

        /// <summary>
        /// ID dokladu pro připojení
        /// </summary>
        /// <value>ID dokladu pro připojení</value>
        [DataMember(Name="NewRelatedDocument_ID", EmitDefaultValue=false)]
        public string NewRelatedDocumentID { get; set; }

        /// <summary>
        /// Popis [persistentní položka]
        /// </summary>
        /// <value>Popis [persistentní položka]</value>
        [DataMember(Name="Description", EmitDefaultValue=false)]
        public string Description { get; set; }

        /// <summary>
        /// Typ karty [persistentní položka]
        /// </summary>
        /// <value>Typ karty [persistentní položka]</value>
        [DataMember(Name="AssetCard_Type", EmitDefaultValue=false)]
        public int? AssetCardType { get; set; }

        /// <summary>
        /// Karta majetku; ID objektu Karta majetku [persistentní položka]
        /// </summary>
        /// <value>Karta majetku; ID objektu Karta majetku [persistentní položka]</value>
        [DataMember(Name="AssetCard_ID", EmitDefaultValue=false)]
        public string AssetCardID { get; set; }

        /// <summary>
        /// Středisko; ID objektu Středisko [persistentní položka]
        /// </summary>
        /// <value>Středisko; ID objektu Středisko [persistentní položka]</value>
        [DataMember(Name="Division_ID", EmitDefaultValue=false)]
        public string DivisionID { get; set; }

        /// <summary>
        /// Zakázka; ID objektu Zakázka [persistentní položka]
        /// </summary>
        /// <value>Zakázka; ID objektu Zakázka [persistentní položka]</value>
        [DataMember(Name="BusOrder_ID", EmitDefaultValue=false)]
        public string BusOrderID { get; set; }

        /// <summary>
        /// Obch.případ; ID objektu Obchodní případ [persistentní položka]
        /// </summary>
        /// <value>Obch.případ; ID objektu Obchodní případ [persistentní položka]</value>
        [DataMember(Name="BusTransaction_ID", EmitDefaultValue=false)]
        public string BusTransactionID { get; set; }

        /// <summary>
        /// Projekt; ID objektu Projekt [persistentní položka]
        /// </summary>
        /// <value>Projekt; ID objektu Projekt [persistentní položka]</value>
        [DataMember(Name="BusProject_ID", EmitDefaultValue=false)]
        public string BusProjectID { get; set; }

        /// <summary>
        /// Čerpat s DPH [persistentní položka]
        /// </summary>
        /// <value>Čerpat s DPH [persistentní položka]</value>
        [DataMember(Name="VATIncluded", EmitDefaultValue=false)]
        public bool? VATIncluded { get; set; }

        /// <summary>
        /// Částka [persistentní položka]
        /// </summary>
        /// <value>Částka [persistentní položka]</value>
        [DataMember(Name="Amount", EmitDefaultValue=false)]
        public double? Amount { get; set; }

        /// <summary>
        /// Promítnutí dokladu do karty
        /// </summary>
        /// <value>Promítnutí dokladu do karty</value>
        [DataMember(Name="ApplyAmountInAssetCard", EmitDefaultValue=false)]
        public int? ApplyAmountInAssetCard { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Assetreceipt {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  DocQueueID: ").Append(DocQueueID).Append("\n");
            sb.Append("  PeriodID: ").Append(PeriodID).Append("\n");
            sb.Append("  OrdNumber: ").Append(OrdNumber).Append("\n");
            sb.Append("  DocDateDATE: ").Append(DocDateDATE).Append("\n");
            sb.Append("  CreatedByID: ").Append(CreatedByID).Append("\n");
            sb.Append("  CorrectedByID: ").Append(CorrectedByID).Append("\n");
            sb.Append("  NewRelatedType: ").Append(NewRelatedType).Append("\n");
            sb.Append("  NewRelatedDocumentID: ").Append(NewRelatedDocumentID).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("  AssetCardType: ").Append(AssetCardType).Append("\n");
            sb.Append("  AssetCardID: ").Append(AssetCardID).Append("\n");
            sb.Append("  DivisionID: ").Append(DivisionID).Append("\n");
            sb.Append("  BusOrderID: ").Append(BusOrderID).Append("\n");
            sb.Append("  BusTransactionID: ").Append(BusTransactionID).Append("\n");
            sb.Append("  BusProjectID: ").Append(BusProjectID).Append("\n");
            sb.Append("  VATIncluded: ").Append(VATIncluded).Append("\n");
            sb.Append("  Amount: ").Append(Amount).Append("\n");
            sb.Append("  ApplyAmountInAssetCard: ").Append(ApplyAmountInAssetCard).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Assetreceipt);
        }

        /// <summary>
        /// Returns true if Assetreceipt instances are equal
        /// </summary>
        /// <param name="input">Instance of Assetreceipt to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Assetreceipt input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.DocQueueID == input.DocQueueID ||
                    (this.DocQueueID != null &&
                    this.DocQueueID.Equals(input.DocQueueID))
                ) && 
                (
                    this.PeriodID == input.PeriodID ||
                    (this.PeriodID != null &&
                    this.PeriodID.Equals(input.PeriodID))
                ) && 
                (
                    this.OrdNumber == input.OrdNumber ||
                    (this.OrdNumber != null &&
                    this.OrdNumber.Equals(input.OrdNumber))
                ) && 
                (
                    this.DocDateDATE == input.DocDateDATE ||
                    (this.DocDateDATE != null &&
                    this.DocDateDATE.Equals(input.DocDateDATE))
                ) && 
                (
                    this.CreatedByID == input.CreatedByID ||
                    (this.CreatedByID != null &&
                    this.CreatedByID.Equals(input.CreatedByID))
                ) && 
                (
                    this.CorrectedByID == input.CorrectedByID ||
                    (this.CorrectedByID != null &&
                    this.CorrectedByID.Equals(input.CorrectedByID))
                ) && 
                (
                    this.NewRelatedType == input.NewRelatedType ||
                    (this.NewRelatedType != null &&
                    this.NewRelatedType.Equals(input.NewRelatedType))
                ) && 
                (
                    this.NewRelatedDocumentID == input.NewRelatedDocumentID ||
                    (this.NewRelatedDocumentID != null &&
                    this.NewRelatedDocumentID.Equals(input.NewRelatedDocumentID))
                ) && 
                (
                    this.Description == input.Description ||
                    (this.Description != null &&
                    this.Description.Equals(input.Description))
                ) && 
                (
                    this.AssetCardType == input.AssetCardType ||
                    (this.AssetCardType != null &&
                    this.AssetCardType.Equals(input.AssetCardType))
                ) && 
                (
                    this.AssetCardID == input.AssetCardID ||
                    (this.AssetCardID != null &&
                    this.AssetCardID.Equals(input.AssetCardID))
                ) && 
                (
                    this.DivisionID == input.DivisionID ||
                    (this.DivisionID != null &&
                    this.DivisionID.Equals(input.DivisionID))
                ) && 
                (
                    this.BusOrderID == input.BusOrderID ||
                    (this.BusOrderID != null &&
                    this.BusOrderID.Equals(input.BusOrderID))
                ) && 
                (
                    this.BusTransactionID == input.BusTransactionID ||
                    (this.BusTransactionID != null &&
                    this.BusTransactionID.Equals(input.BusTransactionID))
                ) && 
                (
                    this.BusProjectID == input.BusProjectID ||
                    (this.BusProjectID != null &&
                    this.BusProjectID.Equals(input.BusProjectID))
                ) && 
                (
                    this.VATIncluded == input.VATIncluded ||
                    (this.VATIncluded != null &&
                    this.VATIncluded.Equals(input.VATIncluded))
                ) && 
                (
                    this.Amount == input.Amount ||
                    (this.Amount != null &&
                    this.Amount.Equals(input.Amount))
                ) && 
                (
                    this.ApplyAmountInAssetCard == input.ApplyAmountInAssetCard ||
                    (this.ApplyAmountInAssetCard != null &&
                    this.ApplyAmountInAssetCard.Equals(input.ApplyAmountInAssetCard))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.DocQueueID != null)
                    hashCode = hashCode * 59 + this.DocQueueID.GetHashCode();
                if (this.PeriodID != null)
                    hashCode = hashCode * 59 + this.PeriodID.GetHashCode();
                if (this.OrdNumber != null)
                    hashCode = hashCode * 59 + this.OrdNumber.GetHashCode();
                if (this.DocDateDATE != null)
                    hashCode = hashCode * 59 + this.DocDateDATE.GetHashCode();
                if (this.CreatedByID != null)
                    hashCode = hashCode * 59 + this.CreatedByID.GetHashCode();
                if (this.CorrectedByID != null)
                    hashCode = hashCode * 59 + this.CorrectedByID.GetHashCode();
                if (this.NewRelatedType != null)
                    hashCode = hashCode * 59 + this.NewRelatedType.GetHashCode();
                if (this.NewRelatedDocumentID != null)
                    hashCode = hashCode * 59 + this.NewRelatedDocumentID.GetHashCode();
                if (this.Description != null)
                    hashCode = hashCode * 59 + this.Description.GetHashCode();
                if (this.AssetCardType != null)
                    hashCode = hashCode * 59 + this.AssetCardType.GetHashCode();
                if (this.AssetCardID != null)
                    hashCode = hashCode * 59 + this.AssetCardID.GetHashCode();
                if (this.DivisionID != null)
                    hashCode = hashCode * 59 + this.DivisionID.GetHashCode();
                if (this.BusOrderID != null)
                    hashCode = hashCode * 59 + this.BusOrderID.GetHashCode();
                if (this.BusTransactionID != null)
                    hashCode = hashCode * 59 + this.BusTransactionID.GetHashCode();
                if (this.BusProjectID != null)
                    hashCode = hashCode * 59 + this.BusProjectID.GetHashCode();
                if (this.VATIncluded != null)
                    hashCode = hashCode * 59 + this.VATIncluded.GetHashCode();
                if (this.Amount != null)
                    hashCode = hashCode * 59 + this.Amount.GetHashCode();
                if (this.ApplyAmountInAssetCard != null)
                    hashCode = hashCode * 59 + this.ApplyAmountInAssetCard.GetHashCode();
                return hashCode;
            }
        }
    }

}
