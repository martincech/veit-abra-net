/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Approvingarea
    /// </summary>
    [DataContract]
    public partial class Approvingarea :  IEquatable<Approvingarea>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Approvingarea" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Oblast hodnocení - hodnotící kritérium [nepersistentní položka].</param>
        /// <param name="code">Kód [persistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="storeCards">Skladové karty; kolekce BO Oblast hodnocení - skladová karta [nepersistentní položka].</param>
        /// <param name="typeDefinition">Typ vymezení [persistentní položka].</param>
        /// <param name="storeMenus">Skladové menu; kolekce BO Oblast hodnocení - skladové menu [nepersistentní položka].</param>
        /// <param name="approvalFromDate">Schvalování od [persistentní položka].</param>
        public Approvingarea(List<Approvingareacriterion> rows = default(List<Approvingareacriterion>), string code = default(string), string name = default(string), List<Approvingareastorecard> storeCards = default(List<Approvingareastorecard>), int? typeDefinition = default(int?), List<Approvingareastoremenu> storeMenus = default(List<Approvingareastoremenu>), DateTime? approvalFromDate = default(DateTime?))
        {
            this.Rows = rows;
            this.Code = code;
            this.Name = name;
            this.StoreCards = storeCards;
            this.TypeDefinition = typeDefinition;
            this.StoreMenus = storeMenus;
            this.ApprovalFromDate = approvalFromDate;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Oblast hodnocení - hodnotící kritérium [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Oblast hodnocení - hodnotící kritérium [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Approvingareacriterion> Rows { get; set; }

        /// <summary>
        /// Kód [persistentní položka]
        /// </summary>
        /// <value>Kód [persistentní položka]</value>
        [DataMember(Name="Code", EmitDefaultValue=false)]
        public string Code { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Skladové karty; kolekce BO Oblast hodnocení - skladová karta [nepersistentní položka]
        /// </summary>
        /// <value>Skladové karty; kolekce BO Oblast hodnocení - skladová karta [nepersistentní položka]</value>
        [DataMember(Name="StoreCards", EmitDefaultValue=false)]
        public List<Approvingareastorecard> StoreCards { get; set; }

        /// <summary>
        /// Typ vymezení [persistentní položka]
        /// </summary>
        /// <value>Typ vymezení [persistentní položka]</value>
        [DataMember(Name="TypeDefinition", EmitDefaultValue=false)]
        public int? TypeDefinition { get; set; }

        /// <summary>
        /// Skladové menu; kolekce BO Oblast hodnocení - skladové menu [nepersistentní položka]
        /// </summary>
        /// <value>Skladové menu; kolekce BO Oblast hodnocení - skladové menu [nepersistentní položka]</value>
        [DataMember(Name="StoreMenus", EmitDefaultValue=false)]
        public List<Approvingareastoremenu> StoreMenus { get; set; }

        /// <summary>
        /// Schvalování od [persistentní položka]
        /// </summary>
        /// <value>Schvalování od [persistentní položka]</value>
        [DataMember(Name="ApprovalFromDate", EmitDefaultValue=false)]
        [JsonConverter(typeof(SwaggerDateConverter))]
        public DateTime? ApprovalFromDate { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Approvingarea {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  Code: ").Append(Code).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  StoreCards: ").Append(StoreCards).Append("\n");
            sb.Append("  TypeDefinition: ").Append(TypeDefinition).Append("\n");
            sb.Append("  StoreMenus: ").Append(StoreMenus).Append("\n");
            sb.Append("  ApprovalFromDate: ").Append(ApprovalFromDate).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Approvingarea);
        }

        /// <summary>
        /// Returns true if Approvingarea instances are equal
        /// </summary>
        /// <param name="input">Instance of Approvingarea to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Approvingarea input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.Code == input.Code ||
                    (this.Code != null &&
                    this.Code.Equals(input.Code))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.StoreCards == input.StoreCards ||
                    this.StoreCards != null &&
                    this.StoreCards.SequenceEqual(input.StoreCards)
                ) && 
                (
                    this.TypeDefinition == input.TypeDefinition ||
                    (this.TypeDefinition != null &&
                    this.TypeDefinition.Equals(input.TypeDefinition))
                ) && 
                (
                    this.StoreMenus == input.StoreMenus ||
                    this.StoreMenus != null &&
                    this.StoreMenus.SequenceEqual(input.StoreMenus)
                ) && 
                (
                    this.ApprovalFromDate == input.ApprovalFromDate ||
                    (this.ApprovalFromDate != null &&
                    this.ApprovalFromDate.Equals(input.ApprovalFromDate))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.Code != null)
                    hashCode = hashCode * 59 + this.Code.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.StoreCards != null)
                    hashCode = hashCode * 59 + this.StoreCards.GetHashCode();
                if (this.TypeDefinition != null)
                    hashCode = hashCode * 59 + this.TypeDefinition.GetHashCode();
                if (this.StoreMenus != null)
                    hashCode = hashCode * 59 + this.StoreMenus.GetHashCode();
                if (this.ApprovalFromDate != null)
                    hashCode = hashCode * 59 + this.ApprovalFromDate.GetHashCode();
                return hashCode;
            }
        }
    }

}
