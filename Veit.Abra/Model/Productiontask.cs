/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Productiontask
    /// </summary>
    [DataContract]
    public partial class Productiontask :  IEquatable<Productiontask>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Productiontask" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Výrobní úloha - řádek [nepersistentní položka].</param>
        /// <param name="storeCardID">Skl. karta výrobku; ID objektu Skladová karta [persistentní položka].</param>
        /// <param name="storeID">Sklad výrobku; ID objektu Sklad [persistentní položka].</param>
        /// <param name="plannedQuantity">Plánováno [persistentní položka].</param>
        /// <param name="deadLineDATE">Termín [persistentní položka].</param>
        /// <param name="quantity">Přijato výrobků [persistentní položka].</param>
        /// <param name="docQueueID">Řada; ID objektu Řada dokladů [persistentní položka].</param>
        /// <param name="periodID">Období; ID objektu Období [persistentní položka].</param>
        /// <param name="ordnumber">Číslo dokladu [persistentní položka].</param>
        /// <param name="text">Text [persistentní položka].</param>
        /// <param name="ownerType">Typ vlastníka [persistentní položka].</param>
        /// <param name="createdAtDATE">Vytvořeno [persistentní položka].</param>
        /// <param name="correctedAtDATE">Opraveno [persistentní položka].</param>
        public Productiontask(List<Productiontaskrow> rows = default(List<Productiontaskrow>), string storeCardID = default(string), string storeID = default(string), double? plannedQuantity = default(double?), DateTimeOffset? deadLineDATE = default(DateTimeOffset?), double? quantity = default(double?), string docQueueID = default(string), string periodID = default(string), int? ordnumber = default(int?), string text = default(string), int? ownerType = default(int?), DateTimeOffset? createdAtDATE = default(DateTimeOffset?), DateTimeOffset? correctedAtDATE = default(DateTimeOffset?))
        {
            this.Rows = rows;
            this.StoreCardID = storeCardID;
            this.StoreID = storeID;
            this.PlannedQuantity = plannedQuantity;
            this.DeadLineDATE = deadLineDATE;
            this.Quantity = quantity;
            this.DocQueueID = docQueueID;
            this.PeriodID = periodID;
            this.Ordnumber = ordnumber;
            this.Text = text;
            this.OwnerType = ownerType;
            this.CreatedAtDATE = createdAtDATE;
            this.CorrectedAtDATE = correctedAtDATE;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Výrobní úloha - řádek [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Výrobní úloha - řádek [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Productiontaskrow> Rows { get; set; }

        /// <summary>
        /// Skl. karta výrobku; ID objektu Skladová karta [persistentní položka]
        /// </summary>
        /// <value>Skl. karta výrobku; ID objektu Skladová karta [persistentní položka]</value>
        [DataMember(Name="StoreCard_ID", EmitDefaultValue=false)]
        public string StoreCardID { get; set; }

        /// <summary>
        /// Sklad výrobku; ID objektu Sklad [persistentní položka]
        /// </summary>
        /// <value>Sklad výrobku; ID objektu Sklad [persistentní položka]</value>
        [DataMember(Name="Store_ID", EmitDefaultValue=false)]
        public string StoreID { get; set; }

        /// <summary>
        /// Plánováno [persistentní položka]
        /// </summary>
        /// <value>Plánováno [persistentní položka]</value>
        [DataMember(Name="PlannedQuantity", EmitDefaultValue=false)]
        public double? PlannedQuantity { get; set; }

        /// <summary>
        /// Termín [persistentní položka]
        /// </summary>
        /// <value>Termín [persistentní položka]</value>
        [DataMember(Name="DeadLine$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DeadLineDATE { get; set; }

        /// <summary>
        /// Přijato výrobků [persistentní položka]
        /// </summary>
        /// <value>Přijato výrobků [persistentní položka]</value>
        [DataMember(Name="Quantity", EmitDefaultValue=false)]
        public double? Quantity { get; set; }

        /// <summary>
        /// Řada; ID objektu Řada dokladů [persistentní položka]
        /// </summary>
        /// <value>Řada; ID objektu Řada dokladů [persistentní položka]</value>
        [DataMember(Name="DocQueue_ID", EmitDefaultValue=false)]
        public string DocQueueID { get; set; }

        /// <summary>
        /// Období; ID objektu Období [persistentní položka]
        /// </summary>
        /// <value>Období; ID objektu Období [persistentní položka]</value>
        [DataMember(Name="Period_ID", EmitDefaultValue=false)]
        public string PeriodID { get; set; }

        /// <summary>
        /// Číslo dokladu [persistentní položka]
        /// </summary>
        /// <value>Číslo dokladu [persistentní položka]</value>
        [DataMember(Name="Ordnumber", EmitDefaultValue=false)]
        public int? Ordnumber { get; set; }

        /// <summary>
        /// Text [persistentní položka]
        /// </summary>
        /// <value>Text [persistentní položka]</value>
        [DataMember(Name="Text", EmitDefaultValue=false)]
        public string Text { get; set; }

        /// <summary>
        /// Typ vlastníka [persistentní položka]
        /// </summary>
        /// <value>Typ vlastníka [persistentní položka]</value>
        [DataMember(Name="OwnerType", EmitDefaultValue=false)]
        public int? OwnerType { get; set; }

        /// <summary>
        /// Vytvořeno [persistentní položka]
        /// </summary>
        /// <value>Vytvořeno [persistentní položka]</value>
        [DataMember(Name="CreatedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CreatedAtDATE { get; set; }

        /// <summary>
        /// Opraveno [persistentní položka]
        /// </summary>
        /// <value>Opraveno [persistentní položka]</value>
        [DataMember(Name="CorrectedAt$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CorrectedAtDATE { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Productiontask {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  StoreCardID: ").Append(StoreCardID).Append("\n");
            sb.Append("  StoreID: ").Append(StoreID).Append("\n");
            sb.Append("  PlannedQuantity: ").Append(PlannedQuantity).Append("\n");
            sb.Append("  DeadLineDATE: ").Append(DeadLineDATE).Append("\n");
            sb.Append("  Quantity: ").Append(Quantity).Append("\n");
            sb.Append("  DocQueueID: ").Append(DocQueueID).Append("\n");
            sb.Append("  PeriodID: ").Append(PeriodID).Append("\n");
            sb.Append("  Ordnumber: ").Append(Ordnumber).Append("\n");
            sb.Append("  Text: ").Append(Text).Append("\n");
            sb.Append("  OwnerType: ").Append(OwnerType).Append("\n");
            sb.Append("  CreatedAtDATE: ").Append(CreatedAtDATE).Append("\n");
            sb.Append("  CorrectedAtDATE: ").Append(CorrectedAtDATE).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Productiontask);
        }

        /// <summary>
        /// Returns true if Productiontask instances are equal
        /// </summary>
        /// <param name="input">Instance of Productiontask to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Productiontask input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.StoreCardID == input.StoreCardID ||
                    (this.StoreCardID != null &&
                    this.StoreCardID.Equals(input.StoreCardID))
                ) && 
                (
                    this.StoreID == input.StoreID ||
                    (this.StoreID != null &&
                    this.StoreID.Equals(input.StoreID))
                ) && 
                (
                    this.PlannedQuantity == input.PlannedQuantity ||
                    (this.PlannedQuantity != null &&
                    this.PlannedQuantity.Equals(input.PlannedQuantity))
                ) && 
                (
                    this.DeadLineDATE == input.DeadLineDATE ||
                    (this.DeadLineDATE != null &&
                    this.DeadLineDATE.Equals(input.DeadLineDATE))
                ) && 
                (
                    this.Quantity == input.Quantity ||
                    (this.Quantity != null &&
                    this.Quantity.Equals(input.Quantity))
                ) && 
                (
                    this.DocQueueID == input.DocQueueID ||
                    (this.DocQueueID != null &&
                    this.DocQueueID.Equals(input.DocQueueID))
                ) && 
                (
                    this.PeriodID == input.PeriodID ||
                    (this.PeriodID != null &&
                    this.PeriodID.Equals(input.PeriodID))
                ) && 
                (
                    this.Ordnumber == input.Ordnumber ||
                    (this.Ordnumber != null &&
                    this.Ordnumber.Equals(input.Ordnumber))
                ) && 
                (
                    this.Text == input.Text ||
                    (this.Text != null &&
                    this.Text.Equals(input.Text))
                ) && 
                (
                    this.OwnerType == input.OwnerType ||
                    (this.OwnerType != null &&
                    this.OwnerType.Equals(input.OwnerType))
                ) && 
                (
                    this.CreatedAtDATE == input.CreatedAtDATE ||
                    (this.CreatedAtDATE != null &&
                    this.CreatedAtDATE.Equals(input.CreatedAtDATE))
                ) && 
                (
                    this.CorrectedAtDATE == input.CorrectedAtDATE ||
                    (this.CorrectedAtDATE != null &&
                    this.CorrectedAtDATE.Equals(input.CorrectedAtDATE))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.StoreCardID != null)
                    hashCode = hashCode * 59 + this.StoreCardID.GetHashCode();
                if (this.StoreID != null)
                    hashCode = hashCode * 59 + this.StoreID.GetHashCode();
                if (this.PlannedQuantity != null)
                    hashCode = hashCode * 59 + this.PlannedQuantity.GetHashCode();
                if (this.DeadLineDATE != null)
                    hashCode = hashCode * 59 + this.DeadLineDATE.GetHashCode();
                if (this.Quantity != null)
                    hashCode = hashCode * 59 + this.Quantity.GetHashCode();
                if (this.DocQueueID != null)
                    hashCode = hashCode * 59 + this.DocQueueID.GetHashCode();
                if (this.PeriodID != null)
                    hashCode = hashCode * 59 + this.PeriodID.GetHashCode();
                if (this.Ordnumber != null)
                    hashCode = hashCode * 59 + this.Ordnumber.GetHashCode();
                if (this.Text != null)
                    hashCode = hashCode * 59 + this.Text.GetHashCode();
                if (this.OwnerType != null)
                    hashCode = hashCode * 59 + this.OwnerType.GetHashCode();
                if (this.CreatedAtDATE != null)
                    hashCode = hashCode * 59 + this.CreatedAtDATE.GetHashCode();
                if (this.CorrectedAtDATE != null)
                    hashCode = hashCode * 59 + this.CorrectedAtDATE.GetHashCode();
                return hashCode;
            }
        }
    }

}
