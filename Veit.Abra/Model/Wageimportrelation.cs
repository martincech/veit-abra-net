/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Wageimportrelation
    /// </summary>
    [DataContract]
    public partial class Wageimportrelation :  IEquatable<Wageimportrelation>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Wageimportrelation" /> class.
        /// </summary>
        /// <param name="posIndex">Pořadí [persistentní položka].</param>
        /// <param name="destinationField">Do položky [persistentní položka].</param>
        /// <param name="sourceField">Z položky [persistentní položka].</param>
        /// <param name="withExpression">Výrazem [persistentní položka].</param>
        /// <param name="expression">Výraz [persistentní položka].</param>
        public Wageimportrelation(int? posIndex = default(int?), int? destinationField = default(int?), string sourceField = default(string), bool? withExpression = default(bool?), string expression = default(string))
        {
            this.PosIndex = posIndex;
            this.DestinationField = destinationField;
            this.SourceField = sourceField;
            this.WithExpression = withExpression;
            this.Expression = expression;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Vlastník; ID objektu Definice převodů dat [persistentní položka]
        /// </summary>
        /// <value>Vlastník; ID objektu Definice převodů dat [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; private set; }

        /// <summary>
        /// Pořadí [persistentní položka]
        /// </summary>
        /// <value>Pořadí [persistentní položka]</value>
        [DataMember(Name="PosIndex", EmitDefaultValue=false)]
        public int? PosIndex { get; set; }

        /// <summary>
        /// Do položky [persistentní položka]
        /// </summary>
        /// <value>Do položky [persistentní položka]</value>
        [DataMember(Name="DestinationField", EmitDefaultValue=false)]
        public int? DestinationField { get; set; }

        /// <summary>
        /// Z položky [persistentní položka]
        /// </summary>
        /// <value>Z položky [persistentní položka]</value>
        [DataMember(Name="SourceField", EmitDefaultValue=false)]
        public string SourceField { get; set; }

        /// <summary>
        /// Výrazem [persistentní položka]
        /// </summary>
        /// <value>Výrazem [persistentní položka]</value>
        [DataMember(Name="WithExpression", EmitDefaultValue=false)]
        public bool? WithExpression { get; set; }

        /// <summary>
        /// Výraz [persistentní položka]
        /// </summary>
        /// <value>Výraz [persistentní položka]</value>
        [DataMember(Name="Expression", EmitDefaultValue=false)]
        public string Expression { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Wageimportrelation {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  PosIndex: ").Append(PosIndex).Append("\n");
            sb.Append("  DestinationField: ").Append(DestinationField).Append("\n");
            sb.Append("  SourceField: ").Append(SourceField).Append("\n");
            sb.Append("  WithExpression: ").Append(WithExpression).Append("\n");
            sb.Append("  Expression: ").Append(Expression).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Wageimportrelation);
        }

        /// <summary>
        /// Returns true if Wageimportrelation instances are equal
        /// </summary>
        /// <param name="input">Instance of Wageimportrelation to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Wageimportrelation input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.PosIndex == input.PosIndex ||
                    (this.PosIndex != null &&
                    this.PosIndex.Equals(input.PosIndex))
                ) && 
                (
                    this.DestinationField == input.DestinationField ||
                    (this.DestinationField != null &&
                    this.DestinationField.Equals(input.DestinationField))
                ) && 
                (
                    this.SourceField == input.SourceField ||
                    (this.SourceField != null &&
                    this.SourceField.Equals(input.SourceField))
                ) && 
                (
                    this.WithExpression == input.WithExpression ||
                    (this.WithExpression != null &&
                    this.WithExpression.Equals(input.WithExpression))
                ) && 
                (
                    this.Expression == input.Expression ||
                    (this.Expression != null &&
                    this.Expression.Equals(input.Expression))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.PosIndex != null)
                    hashCode = hashCode * 59 + this.PosIndex.GetHashCode();
                if (this.DestinationField != null)
                    hashCode = hashCode * 59 + this.DestinationField.GetHashCode();
                if (this.SourceField != null)
                    hashCode = hashCode * 59 + this.SourceField.GetHashCode();
                if (this.WithExpression != null)
                    hashCode = hashCode * 59 + this.WithExpression.GetHashCode();
                if (this.Expression != null)
                    hashCode = hashCode * 59 + this.Expression.GetHashCode();
                return hashCode;
            }
        }
    }

}
