/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Assetinvlistcard
    /// </summary>
    [DataContract]
    public partial class Assetinvlistcard :  IEquatable<Assetinvlistcard>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Assetinvlistcard" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Inventura majetku - prvek karty majetku [nepersistentní položka].</param>
        /// <param name="parentID">Odkaz na seznam; ID objektu Inventarizační seznam majetku [persistentní položka].</param>
        /// <param name="assetInvID">Odkaz na inventuru; ID objektu Inventura majetku [persistentní položka].</param>
        /// <param name="assetCardType">Typ majetku [persistentní položka].</param>
        /// <param name="assetCardID">Karta majetku; ID objektu Karta majetku [persistentní položka].</param>
        /// <param name="isCollection">Je kolekce [persistentní položka].</param>
        /// <param name="smallSubCardID">Karta DrM; ID objektu Položka drobného majetku [persistentní položka].</param>
        /// <param name="inventoryNr">Inventární číslo [persistentní položka].</param>
        /// <param name="name">Název [persistentní položka].</param>
        /// <param name="eAN">EAN [persistentní položka].</param>
        /// <param name="responsibleID">Odpovědná osoba; ID objektu Odpovědná osoba [persistentní položka].</param>
        /// <param name="assetLocationID">Umístění; ID objektu Umístění majetku [persistentní položka].</param>
        /// <param name="evidenceDivisionID">Evidenční středisko; ID objektu Středisko [persistentní položka].</param>
        /// <param name="expensesDivisionID">Nákladové středisko; ID objektu Středisko [persistentní položka].</param>
        /// <param name="quantity">Počet [persistentní položka].</param>
        public Assetinvlistcard(List<Assetinvlistcardcomp> rows = default(List<Assetinvlistcardcomp>), string parentID = default(string), string assetInvID = default(string), int? assetCardType = default(int?), string assetCardID = default(string), int? isCollection = default(int?), string smallSubCardID = default(string), string inventoryNr = default(string), string name = default(string), string eAN = default(string), string responsibleID = default(string), string assetLocationID = default(string), string evidenceDivisionID = default(string), string expensesDivisionID = default(string), int? quantity = default(int?))
        {
            this.Rows = rows;
            this.ParentID = parentID;
            this.AssetInvID = assetInvID;
            this.AssetCardType = assetCardType;
            this.AssetCardID = assetCardID;
            this.IsCollection = isCollection;
            this.SmallSubCardID = smallSubCardID;
            this.InventoryNr = inventoryNr;
            this.Name = name;
            this.EAN = eAN;
            this.ResponsibleID = responsibleID;
            this.AssetLocationID = assetLocationID;
            this.EvidenceDivisionID = evidenceDivisionID;
            this.ExpensesDivisionID = expensesDivisionID;
            this.Quantity = quantity;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Inventura majetku - prvek karty majetku [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Inventura majetku - prvek karty majetku [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Assetinvlistcardcomp> Rows { get; set; }

        /// <summary>
        /// Odkaz na seznam; ID objektu Inventarizační seznam majetku [persistentní položka]
        /// </summary>
        /// <value>Odkaz na seznam; ID objektu Inventarizační seznam majetku [persistentní položka]</value>
        [DataMember(Name="Parent_ID", EmitDefaultValue=false)]
        public string ParentID { get; set; }

        /// <summary>
        /// Odkaz na inventuru; ID objektu Inventura majetku [persistentní položka]
        /// </summary>
        /// <value>Odkaz na inventuru; ID objektu Inventura majetku [persistentní položka]</value>
        [DataMember(Name="AssetInv_ID", EmitDefaultValue=false)]
        public string AssetInvID { get; set; }

        /// <summary>
        /// Typ majetku [persistentní položka]
        /// </summary>
        /// <value>Typ majetku [persistentní položka]</value>
        [DataMember(Name="AssetCard_Type", EmitDefaultValue=false)]
        public int? AssetCardType { get; set; }

        /// <summary>
        /// Karta majetku; ID objektu Karta majetku [persistentní položka]
        /// </summary>
        /// <value>Karta majetku; ID objektu Karta majetku [persistentní položka]</value>
        [DataMember(Name="AssetCard_ID", EmitDefaultValue=false)]
        public string AssetCardID { get; set; }

        /// <summary>
        /// Je kolekce [persistentní položka]
        /// </summary>
        /// <value>Je kolekce [persistentní položka]</value>
        [DataMember(Name="IsCollection", EmitDefaultValue=false)]
        public int? IsCollection { get; set; }

        /// <summary>
        /// Karta DrM; ID objektu Položka drobného majetku [persistentní položka]
        /// </summary>
        /// <value>Karta DrM; ID objektu Položka drobného majetku [persistentní položka]</value>
        [DataMember(Name="SmallSubCard_ID", EmitDefaultValue=false)]
        public string SmallSubCardID { get; set; }

        /// <summary>
        /// Inventární číslo [persistentní položka]
        /// </summary>
        /// <value>Inventární číslo [persistentní položka]</value>
        [DataMember(Name="InventoryNr", EmitDefaultValue=false)]
        public string InventoryNr { get; set; }

        /// <summary>
        /// Název [persistentní položka]
        /// </summary>
        /// <value>Název [persistentní položka]</value>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// EAN [persistentní položka]
        /// </summary>
        /// <value>EAN [persistentní položka]</value>
        [DataMember(Name="EAN", EmitDefaultValue=false)]
        public string EAN { get; set; }

        /// <summary>
        /// Odpovědná osoba; ID objektu Odpovědná osoba [persistentní položka]
        /// </summary>
        /// <value>Odpovědná osoba; ID objektu Odpovědná osoba [persistentní položka]</value>
        [DataMember(Name="Responsible_ID", EmitDefaultValue=false)]
        public string ResponsibleID { get; set; }

        /// <summary>
        /// Umístění; ID objektu Umístění majetku [persistentní položka]
        /// </summary>
        /// <value>Umístění; ID objektu Umístění majetku [persistentní položka]</value>
        [DataMember(Name="AssetLocation_ID", EmitDefaultValue=false)]
        public string AssetLocationID { get; set; }

        /// <summary>
        /// Evidenční středisko; ID objektu Středisko [persistentní položka]
        /// </summary>
        /// <value>Evidenční středisko; ID objektu Středisko [persistentní položka]</value>
        [DataMember(Name="EvidenceDivision_ID", EmitDefaultValue=false)]
        public string EvidenceDivisionID { get; set; }

        /// <summary>
        /// Nákladové středisko; ID objektu Středisko [persistentní položka]
        /// </summary>
        /// <value>Nákladové středisko; ID objektu Středisko [persistentní položka]</value>
        [DataMember(Name="ExpensesDivision_ID", EmitDefaultValue=false)]
        public string ExpensesDivisionID { get; set; }

        /// <summary>
        /// Počet [persistentní položka]
        /// </summary>
        /// <value>Počet [persistentní položka]</value>
        [DataMember(Name="Quantity", EmitDefaultValue=false)]
        public int? Quantity { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Assetinvlistcard {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  ParentID: ").Append(ParentID).Append("\n");
            sb.Append("  AssetInvID: ").Append(AssetInvID).Append("\n");
            sb.Append("  AssetCardType: ").Append(AssetCardType).Append("\n");
            sb.Append("  AssetCardID: ").Append(AssetCardID).Append("\n");
            sb.Append("  IsCollection: ").Append(IsCollection).Append("\n");
            sb.Append("  SmallSubCardID: ").Append(SmallSubCardID).Append("\n");
            sb.Append("  InventoryNr: ").Append(InventoryNr).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  EAN: ").Append(EAN).Append("\n");
            sb.Append("  ResponsibleID: ").Append(ResponsibleID).Append("\n");
            sb.Append("  AssetLocationID: ").Append(AssetLocationID).Append("\n");
            sb.Append("  EvidenceDivisionID: ").Append(EvidenceDivisionID).Append("\n");
            sb.Append("  ExpensesDivisionID: ").Append(ExpensesDivisionID).Append("\n");
            sb.Append("  Quantity: ").Append(Quantity).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Assetinvlistcard);
        }

        /// <summary>
        /// Returns true if Assetinvlistcard instances are equal
        /// </summary>
        /// <param name="input">Instance of Assetinvlistcard to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Assetinvlistcard input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.ParentID == input.ParentID ||
                    (this.ParentID != null &&
                    this.ParentID.Equals(input.ParentID))
                ) && 
                (
                    this.AssetInvID == input.AssetInvID ||
                    (this.AssetInvID != null &&
                    this.AssetInvID.Equals(input.AssetInvID))
                ) && 
                (
                    this.AssetCardType == input.AssetCardType ||
                    (this.AssetCardType != null &&
                    this.AssetCardType.Equals(input.AssetCardType))
                ) && 
                (
                    this.AssetCardID == input.AssetCardID ||
                    (this.AssetCardID != null &&
                    this.AssetCardID.Equals(input.AssetCardID))
                ) && 
                (
                    this.IsCollection == input.IsCollection ||
                    (this.IsCollection != null &&
                    this.IsCollection.Equals(input.IsCollection))
                ) && 
                (
                    this.SmallSubCardID == input.SmallSubCardID ||
                    (this.SmallSubCardID != null &&
                    this.SmallSubCardID.Equals(input.SmallSubCardID))
                ) && 
                (
                    this.InventoryNr == input.InventoryNr ||
                    (this.InventoryNr != null &&
                    this.InventoryNr.Equals(input.InventoryNr))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.EAN == input.EAN ||
                    (this.EAN != null &&
                    this.EAN.Equals(input.EAN))
                ) && 
                (
                    this.ResponsibleID == input.ResponsibleID ||
                    (this.ResponsibleID != null &&
                    this.ResponsibleID.Equals(input.ResponsibleID))
                ) && 
                (
                    this.AssetLocationID == input.AssetLocationID ||
                    (this.AssetLocationID != null &&
                    this.AssetLocationID.Equals(input.AssetLocationID))
                ) && 
                (
                    this.EvidenceDivisionID == input.EvidenceDivisionID ||
                    (this.EvidenceDivisionID != null &&
                    this.EvidenceDivisionID.Equals(input.EvidenceDivisionID))
                ) && 
                (
                    this.ExpensesDivisionID == input.ExpensesDivisionID ||
                    (this.ExpensesDivisionID != null &&
                    this.ExpensesDivisionID.Equals(input.ExpensesDivisionID))
                ) && 
                (
                    this.Quantity == input.Quantity ||
                    (this.Quantity != null &&
                    this.Quantity.Equals(input.Quantity))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.ParentID != null)
                    hashCode = hashCode * 59 + this.ParentID.GetHashCode();
                if (this.AssetInvID != null)
                    hashCode = hashCode * 59 + this.AssetInvID.GetHashCode();
                if (this.AssetCardType != null)
                    hashCode = hashCode * 59 + this.AssetCardType.GetHashCode();
                if (this.AssetCardID != null)
                    hashCode = hashCode * 59 + this.AssetCardID.GetHashCode();
                if (this.IsCollection != null)
                    hashCode = hashCode * 59 + this.IsCollection.GetHashCode();
                if (this.SmallSubCardID != null)
                    hashCode = hashCode * 59 + this.SmallSubCardID.GetHashCode();
                if (this.InventoryNr != null)
                    hashCode = hashCode * 59 + this.InventoryNr.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.EAN != null)
                    hashCode = hashCode * 59 + this.EAN.GetHashCode();
                if (this.ResponsibleID != null)
                    hashCode = hashCode * 59 + this.ResponsibleID.GetHashCode();
                if (this.AssetLocationID != null)
                    hashCode = hashCode * 59 + this.AssetLocationID.GetHashCode();
                if (this.EvidenceDivisionID != null)
                    hashCode = hashCode * 59 + this.EvidenceDivisionID.GetHashCode();
                if (this.ExpensesDivisionID != null)
                    hashCode = hashCode * 59 + this.ExpensesDivisionID.GetHashCode();
                if (this.Quantity != null)
                    hashCode = hashCode * 59 + this.Quantity.GetHashCode();
                return hashCode;
            }
        }
    }

}
