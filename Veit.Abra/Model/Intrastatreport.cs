/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Intrastatreport
    /// </summary>
    [DataContract]
    public partial class Intrastatreport :  IEquatable<Intrastatreport>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Intrastatreport" /> class.
        /// </summary>
        /// <param name="rows">Řádky; kolekce BO Deklarace INTRASTAT [nepersistentní položka].</param>
        /// <param name="intrastatReportID">Opravované podání; ID objektu Podání INTRASTAT [persistentní položka].</param>
        /// <param name="envelopeid">Identifikátor PVS [persistentní položka].</param>
        /// <param name="docDateDATE">Datum a čas generování [persistentní položka].</param>
        /// <param name="dateFromDATE">Datum od [persistentní položka].</param>
        /// <param name="dateToDATE">Datum do [persistentní položka].</param>
        /// <param name="communicationDocumentID">Dokument; ID objektu Dokument [persistentní položka].</param>
        /// <param name="conditions">Podmínky [persistentní položka].</param>
        /// <param name="status">Status [persistentní položka].</param>
        /// <param name="origin">Původ [persistentní položka].</param>
        /// <param name="exportVersion">Verze exportu [persistentní položka].</param>
        public Intrastatreport(List<Intrastatreportdeclaration> rows = default(List<Intrastatreportdeclaration>), string intrastatReportID = default(string), string envelopeid = default(string), DateTimeOffset? docDateDATE = default(DateTimeOffset?), DateTimeOffset? dateFromDATE = default(DateTimeOffset?), DateTimeOffset? dateToDATE = default(DateTimeOffset?), string communicationDocumentID = default(string), byte[] conditions = default(byte[]), int? status = default(int?), int? origin = default(int?), int? exportVersion = default(int?))
        {
            this.Rows = rows;
            this.IntrastatReportID = intrastatReportID;
            this.Envelopeid = envelopeid;
            this.DocDateDATE = docDateDATE;
            this.DateFromDATE = dateFromDATE;
            this.DateToDATE = dateToDATE;
            this.CommunicationDocumentID = communicationDocumentID;
            this.Conditions = conditions;
            this.Status = status;
            this.Origin = origin;
            this.ExportVersion = exportVersion;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Řádky; kolekce BO Deklarace INTRASTAT [nepersistentní položka]
        /// </summary>
        /// <value>Řádky; kolekce BO Deklarace INTRASTAT [nepersistentní položka]</value>
        [DataMember(Name="Rows", EmitDefaultValue=false)]
        public List<Intrastatreportdeclaration> Rows { get; set; }

        /// <summary>
        /// Opravované podání; ID objektu Podání INTRASTAT [persistentní položka]
        /// </summary>
        /// <value>Opravované podání; ID objektu Podání INTRASTAT [persistentní položka]</value>
        [DataMember(Name="IntrastatReport_ID", EmitDefaultValue=false)]
        public string IntrastatReportID { get; set; }

        /// <summary>
        /// Identifikátor PVS [persistentní položka]
        /// </summary>
        /// <value>Identifikátor PVS [persistentní položka]</value>
        [DataMember(Name="Envelopeid", EmitDefaultValue=false)]
        public string Envelopeid { get; set; }

        /// <summary>
        /// Datum a čas generování [persistentní položka]
        /// </summary>
        /// <value>Datum a čas generování [persistentní položka]</value>
        [DataMember(Name="DocDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DocDateDATE { get; set; }

        /// <summary>
        /// Datum od [persistentní položka]
        /// </summary>
        /// <value>Datum od [persistentní položka]</value>
        [DataMember(Name="DateFrom$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DateFromDATE { get; set; }

        /// <summary>
        /// Datum do [persistentní položka]
        /// </summary>
        /// <value>Datum do [persistentní položka]</value>
        [DataMember(Name="DateTo$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? DateToDATE { get; set; }

        /// <summary>
        /// Dokument; ID objektu Dokument [persistentní položka]
        /// </summary>
        /// <value>Dokument; ID objektu Dokument [persistentní položka]</value>
        [DataMember(Name="CommunicationDocument_ID", EmitDefaultValue=false)]
        public string CommunicationDocumentID { get; set; }

        /// <summary>
        /// Podmínky [persistentní položka]
        /// </summary>
        /// <value>Podmínky [persistentní položka]</value>
        [DataMember(Name="Conditions", EmitDefaultValue=false)]
        public byte[] Conditions { get; set; }

        /// <summary>
        /// Status [persistentní položka]
        /// </summary>
        /// <value>Status [persistentní položka]</value>
        [DataMember(Name="Status", EmitDefaultValue=false)]
        public int? Status { get; set; }

        /// <summary>
        /// Podklady změněny
        /// </summary>
        /// <value>Podklady změněny</value>
        [DataMember(Name="IsChanged", EmitDefaultValue=false)]
        public bool? IsChanged { get; private set; }

        /// <summary>
        /// Původ [persistentní položka]
        /// </summary>
        /// <value>Původ [persistentní položka]</value>
        [DataMember(Name="Origin", EmitDefaultValue=false)]
        public int? Origin { get; set; }

        /// <summary>
        /// Různý od předchozího
        /// </summary>
        /// <value>Různý od předchozího</value>
        [DataMember(Name="DifferentFromPreviousReport", EmitDefaultValue=false)]
        public bool? DifferentFromPreviousReport { get; private set; }

        /// <summary>
        /// Obsahuje pouze mazací deklarace
        /// </summary>
        /// <value>Obsahuje pouze mazací deklarace</value>
        [DataMember(Name="IsOnlyDeleting", EmitDefaultValue=false)]
        public bool? IsOnlyDeleting { get; private set; }

        /// <summary>
        /// Verze exportu [persistentní položka]
        /// </summary>
        /// <value>Verze exportu [persistentní položka]</value>
        [DataMember(Name="ExportVersion", EmitDefaultValue=false)]
        public int? ExportVersion { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Intrastatreport {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  Rows: ").Append(Rows).Append("\n");
            sb.Append("  IntrastatReportID: ").Append(IntrastatReportID).Append("\n");
            sb.Append("  Envelopeid: ").Append(Envelopeid).Append("\n");
            sb.Append("  DocDateDATE: ").Append(DocDateDATE).Append("\n");
            sb.Append("  DateFromDATE: ").Append(DateFromDATE).Append("\n");
            sb.Append("  DateToDATE: ").Append(DateToDATE).Append("\n");
            sb.Append("  CommunicationDocumentID: ").Append(CommunicationDocumentID).Append("\n");
            sb.Append("  Conditions: ").Append(Conditions).Append("\n");
            sb.Append("  Status: ").Append(Status).Append("\n");
            sb.Append("  IsChanged: ").Append(IsChanged).Append("\n");
            sb.Append("  Origin: ").Append(Origin).Append("\n");
            sb.Append("  DifferentFromPreviousReport: ").Append(DifferentFromPreviousReport).Append("\n");
            sb.Append("  IsOnlyDeleting: ").Append(IsOnlyDeleting).Append("\n");
            sb.Append("  ExportVersion: ").Append(ExportVersion).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Intrastatreport);
        }

        /// <summary>
        /// Returns true if Intrastatreport instances are equal
        /// </summary>
        /// <param name="input">Instance of Intrastatreport to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Intrastatreport input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.Rows == input.Rows ||
                    this.Rows != null &&
                    this.Rows.SequenceEqual(input.Rows)
                ) && 
                (
                    this.IntrastatReportID == input.IntrastatReportID ||
                    (this.IntrastatReportID != null &&
                    this.IntrastatReportID.Equals(input.IntrastatReportID))
                ) && 
                (
                    this.Envelopeid == input.Envelopeid ||
                    (this.Envelopeid != null &&
                    this.Envelopeid.Equals(input.Envelopeid))
                ) && 
                (
                    this.DocDateDATE == input.DocDateDATE ||
                    (this.DocDateDATE != null &&
                    this.DocDateDATE.Equals(input.DocDateDATE))
                ) && 
                (
                    this.DateFromDATE == input.DateFromDATE ||
                    (this.DateFromDATE != null &&
                    this.DateFromDATE.Equals(input.DateFromDATE))
                ) && 
                (
                    this.DateToDATE == input.DateToDATE ||
                    (this.DateToDATE != null &&
                    this.DateToDATE.Equals(input.DateToDATE))
                ) && 
                (
                    this.CommunicationDocumentID == input.CommunicationDocumentID ||
                    (this.CommunicationDocumentID != null &&
                    this.CommunicationDocumentID.Equals(input.CommunicationDocumentID))
                ) && 
                (
                    this.Conditions == input.Conditions ||
                    (this.Conditions != null &&
                    this.Conditions.Equals(input.Conditions))
                ) && 
                (
                    this.Status == input.Status ||
                    (this.Status != null &&
                    this.Status.Equals(input.Status))
                ) && 
                (
                    this.IsChanged == input.IsChanged ||
                    (this.IsChanged != null &&
                    this.IsChanged.Equals(input.IsChanged))
                ) && 
                (
                    this.Origin == input.Origin ||
                    (this.Origin != null &&
                    this.Origin.Equals(input.Origin))
                ) && 
                (
                    this.DifferentFromPreviousReport == input.DifferentFromPreviousReport ||
                    (this.DifferentFromPreviousReport != null &&
                    this.DifferentFromPreviousReport.Equals(input.DifferentFromPreviousReport))
                ) && 
                (
                    this.IsOnlyDeleting == input.IsOnlyDeleting ||
                    (this.IsOnlyDeleting != null &&
                    this.IsOnlyDeleting.Equals(input.IsOnlyDeleting))
                ) && 
                (
                    this.ExportVersion == input.ExportVersion ||
                    (this.ExportVersion != null &&
                    this.ExportVersion.Equals(input.ExportVersion))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.Rows != null)
                    hashCode = hashCode * 59 + this.Rows.GetHashCode();
                if (this.IntrastatReportID != null)
                    hashCode = hashCode * 59 + this.IntrastatReportID.GetHashCode();
                if (this.Envelopeid != null)
                    hashCode = hashCode * 59 + this.Envelopeid.GetHashCode();
                if (this.DocDateDATE != null)
                    hashCode = hashCode * 59 + this.DocDateDATE.GetHashCode();
                if (this.DateFromDATE != null)
                    hashCode = hashCode * 59 + this.DateFromDATE.GetHashCode();
                if (this.DateToDATE != null)
                    hashCode = hashCode * 59 + this.DateToDATE.GetHashCode();
                if (this.CommunicationDocumentID != null)
                    hashCode = hashCode * 59 + this.CommunicationDocumentID.GetHashCode();
                if (this.Conditions != null)
                    hashCode = hashCode * 59 + this.Conditions.GetHashCode();
                if (this.Status != null)
                    hashCode = hashCode * 59 + this.Status.GetHashCode();
                if (this.IsChanged != null)
                    hashCode = hashCode * 59 + this.IsChanged.GetHashCode();
                if (this.Origin != null)
                    hashCode = hashCode * 59 + this.Origin.GetHashCode();
                if (this.DifferentFromPreviousReport != null)
                    hashCode = hashCode * 59 + this.DifferentFromPreviousReport.GetHashCode();
                if (this.IsOnlyDeleting != null)
                    hashCode = hashCode * 59 + this.IsOnlyDeleting.GetHashCode();
                if (this.ExportVersion != null)
                    hashCode = hashCode * 59 + this.ExportVersion.GetHashCode();
                return hashCode;
            }
        }
    }

}
