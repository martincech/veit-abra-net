/* 
 * ABRA Gen Web API (spojení veit)
 *
 * Webové API systému 18.03.17
 *
 * OpenAPI spec version: 18.03.17
 * Contact: abragen@abra.eu
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SwaggerDateConverter = Veit.Abra.Client.SwaggerDateConverter;

namespace Veit.Abra.Model
{
    /// <summary>
    /// Busorderfreal
    /// </summary>
    [DataContract]
    public partial class Busorderfreal :  IEquatable<Busorderfreal>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Busorderfreal" /> class.
        /// </summary>
        /// <param name="busObjectID">Nadřízený objekt; ID objektu Zakázka [persistentní položka].</param>
        /// <param name="evaluationDateDATE">Datum vyhodnocení [persistentní položka].</param>
        /// <param name="realCosts">Reálné náklady [persistentní položka].</param>
        /// <param name="realRevenues">Reálné výnosy [persistentní položka].</param>
        /// <param name="realCostsWithSubTree">Reálné náklady vč.podřízených [persistentní položka].</param>
        /// <param name="realRevenuesWithSubTree">Reálné výnosy vč.podřízených [persistentní položka].</param>
        /// <param name="realCostsFromSources">Reálné náklady zdrojů [persistentní položka].</param>
        /// <param name="realCostsFromSourcesWSubTree">Reálné náklady zdrojů vč.podřízených [persistentní položka].</param>
        /// <param name="calculatedByID">Bilanci spočítal; ID objektu Uživatel [persistentní položka].</param>
        /// <param name="calculatedWhenDATE">Datum posl.výpočtu [persistentní položka].</param>
        public Busorderfreal(string busObjectID = default(string), DateTimeOffset? evaluationDateDATE = default(DateTimeOffset?), double? realCosts = default(double?), double? realRevenues = default(double?), double? realCostsWithSubTree = default(double?), double? realRevenuesWithSubTree = default(double?), double? realCostsFromSources = default(double?), double? realCostsFromSourcesWSubTree = default(double?), string calculatedByID = default(string), DateTimeOffset? calculatedWhenDATE = default(DateTimeOffset?))
        {
            this.BusObjectID = busObjectID;
            this.EvaluationDateDATE = evaluationDateDATE;
            this.RealCosts = realCosts;
            this.RealRevenues = realRevenues;
            this.RealCostsWithSubTree = realCostsWithSubTree;
            this.RealRevenuesWithSubTree = realRevenuesWithSubTree;
            this.RealCostsFromSources = realCostsFromSources;
            this.RealCostsFromSourcesWSubTree = realCostsFromSourcesWSubTree;
            this.CalculatedByID = calculatedByID;
            this.CalculatedWhenDATE = calculatedWhenDATE;
        }
        
        /// <summary>
        /// Název
        /// </summary>
        /// <value>Název</value>
        [DataMember(Name="DisplayName", EmitDefaultValue=false)]
        public string DisplayName { get; private set; }

        /// <summary>
        /// Vlastní ID [persistentní položka]
        /// </summary>
        /// <value>Vlastní ID [persistentní položka]</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; private set; }

        /// <summary>
        /// ID třídy
        /// </summary>
        /// <value>ID třídy</value>
        [DataMember(Name="ClassID", EmitDefaultValue=false)]
        public string ClassID { get; private set; }

        /// <summary>
        /// Verze objektu [persistentní položka]
        /// </summary>
        /// <value>Verze objektu [persistentní položka]</value>
        [DataMember(Name="ObjVersion", EmitDefaultValue=false)]
        public int? ObjVersion { get; private set; }

        /// <summary>
        /// Nadřízený objekt; ID objektu Zakázka [persistentní položka]
        /// </summary>
        /// <value>Nadřízený objekt; ID objektu Zakázka [persistentní položka]</value>
        [DataMember(Name="BusObject_ID", EmitDefaultValue=false)]
        public string BusObjectID { get; set; }

        /// <summary>
        /// Datum vyhodnocení [persistentní položka]
        /// </summary>
        /// <value>Datum vyhodnocení [persistentní položka]</value>
        [DataMember(Name="EvaluationDate$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? EvaluationDateDATE { get; set; }

        /// <summary>
        /// Reálné náklady [persistentní položka]
        /// </summary>
        /// <value>Reálné náklady [persistentní položka]</value>
        [DataMember(Name="RealCosts", EmitDefaultValue=false)]
        public double? RealCosts { get; set; }

        /// <summary>
        /// Reálné výnosy [persistentní položka]
        /// </summary>
        /// <value>Reálné výnosy [persistentní položka]</value>
        [DataMember(Name="RealRevenues", EmitDefaultValue=false)]
        public double? RealRevenues { get; set; }

        /// <summary>
        /// Reálné náklady vč.podřízených [persistentní položka]
        /// </summary>
        /// <value>Reálné náklady vč.podřízených [persistentní položka]</value>
        [DataMember(Name="RealCostsWithSubTree", EmitDefaultValue=false)]
        public double? RealCostsWithSubTree { get; set; }

        /// <summary>
        /// Reálné výnosy vč.podřízených [persistentní položka]
        /// </summary>
        /// <value>Reálné výnosy vč.podřízených [persistentní položka]</value>
        [DataMember(Name="RealRevenuesWithSubTree", EmitDefaultValue=false)]
        public double? RealRevenuesWithSubTree { get; set; }

        /// <summary>
        /// Reálné náklady zdrojů [persistentní položka]
        /// </summary>
        /// <value>Reálné náklady zdrojů [persistentní položka]</value>
        [DataMember(Name="RealCostsFromSources", EmitDefaultValue=false)]
        public double? RealCostsFromSources { get; set; }

        /// <summary>
        /// Reálné náklady zdrojů vč.podřízených [persistentní položka]
        /// </summary>
        /// <value>Reálné náklady zdrojů vč.podřízených [persistentní položka]</value>
        [DataMember(Name="RealCostsFromSourcesWSubTree", EmitDefaultValue=false)]
        public double? RealCostsFromSourcesWSubTree { get; set; }

        /// <summary>
        /// Bilanci spočítal; ID objektu Uživatel [persistentní položka]
        /// </summary>
        /// <value>Bilanci spočítal; ID objektu Uživatel [persistentní položka]</value>
        [DataMember(Name="CalculatedBy_ID", EmitDefaultValue=false)]
        public string CalculatedByID { get; set; }

        /// <summary>
        /// Datum posl.výpočtu [persistentní položka]
        /// </summary>
        /// <value>Datum posl.výpočtu [persistentní položka]</value>
        [DataMember(Name="CalculatedWhen$DATE", EmitDefaultValue=false)]
        public DateTimeOffset? CalculatedWhenDATE { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Busorderfreal {\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  ClassID: ").Append(ClassID).Append("\n");
            sb.Append("  ObjVersion: ").Append(ObjVersion).Append("\n");
            sb.Append("  BusObjectID: ").Append(BusObjectID).Append("\n");
            sb.Append("  EvaluationDateDATE: ").Append(EvaluationDateDATE).Append("\n");
            sb.Append("  RealCosts: ").Append(RealCosts).Append("\n");
            sb.Append("  RealRevenues: ").Append(RealRevenues).Append("\n");
            sb.Append("  RealCostsWithSubTree: ").Append(RealCostsWithSubTree).Append("\n");
            sb.Append("  RealRevenuesWithSubTree: ").Append(RealRevenuesWithSubTree).Append("\n");
            sb.Append("  RealCostsFromSources: ").Append(RealCostsFromSources).Append("\n");
            sb.Append("  RealCostsFromSourcesWSubTree: ").Append(RealCostsFromSourcesWSubTree).Append("\n");
            sb.Append("  CalculatedByID: ").Append(CalculatedByID).Append("\n");
            sb.Append("  CalculatedWhenDATE: ").Append(CalculatedWhenDATE).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Busorderfreal);
        }

        /// <summary>
        /// Returns true if Busorderfreal instances are equal
        /// </summary>
        /// <param name="input">Instance of Busorderfreal to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Busorderfreal input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.DisplayName == input.DisplayName ||
                    (this.DisplayName != null &&
                    this.DisplayName.Equals(input.DisplayName))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.ClassID == input.ClassID ||
                    (this.ClassID != null &&
                    this.ClassID.Equals(input.ClassID))
                ) && 
                (
                    this.ObjVersion == input.ObjVersion ||
                    (this.ObjVersion != null &&
                    this.ObjVersion.Equals(input.ObjVersion))
                ) && 
                (
                    this.BusObjectID == input.BusObjectID ||
                    (this.BusObjectID != null &&
                    this.BusObjectID.Equals(input.BusObjectID))
                ) && 
                (
                    this.EvaluationDateDATE == input.EvaluationDateDATE ||
                    (this.EvaluationDateDATE != null &&
                    this.EvaluationDateDATE.Equals(input.EvaluationDateDATE))
                ) && 
                (
                    this.RealCosts == input.RealCosts ||
                    (this.RealCosts != null &&
                    this.RealCosts.Equals(input.RealCosts))
                ) && 
                (
                    this.RealRevenues == input.RealRevenues ||
                    (this.RealRevenues != null &&
                    this.RealRevenues.Equals(input.RealRevenues))
                ) && 
                (
                    this.RealCostsWithSubTree == input.RealCostsWithSubTree ||
                    (this.RealCostsWithSubTree != null &&
                    this.RealCostsWithSubTree.Equals(input.RealCostsWithSubTree))
                ) && 
                (
                    this.RealRevenuesWithSubTree == input.RealRevenuesWithSubTree ||
                    (this.RealRevenuesWithSubTree != null &&
                    this.RealRevenuesWithSubTree.Equals(input.RealRevenuesWithSubTree))
                ) && 
                (
                    this.RealCostsFromSources == input.RealCostsFromSources ||
                    (this.RealCostsFromSources != null &&
                    this.RealCostsFromSources.Equals(input.RealCostsFromSources))
                ) && 
                (
                    this.RealCostsFromSourcesWSubTree == input.RealCostsFromSourcesWSubTree ||
                    (this.RealCostsFromSourcesWSubTree != null &&
                    this.RealCostsFromSourcesWSubTree.Equals(input.RealCostsFromSourcesWSubTree))
                ) && 
                (
                    this.CalculatedByID == input.CalculatedByID ||
                    (this.CalculatedByID != null &&
                    this.CalculatedByID.Equals(input.CalculatedByID))
                ) && 
                (
                    this.CalculatedWhenDATE == input.CalculatedWhenDATE ||
                    (this.CalculatedWhenDATE != null &&
                    this.CalculatedWhenDATE.Equals(input.CalculatedWhenDATE))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.DisplayName != null)
                    hashCode = hashCode * 59 + this.DisplayName.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.ClassID != null)
                    hashCode = hashCode * 59 + this.ClassID.GetHashCode();
                if (this.ObjVersion != null)
                    hashCode = hashCode * 59 + this.ObjVersion.GetHashCode();
                if (this.BusObjectID != null)
                    hashCode = hashCode * 59 + this.BusObjectID.GetHashCode();
                if (this.EvaluationDateDATE != null)
                    hashCode = hashCode * 59 + this.EvaluationDateDATE.GetHashCode();
                if (this.RealCosts != null)
                    hashCode = hashCode * 59 + this.RealCosts.GetHashCode();
                if (this.RealRevenues != null)
                    hashCode = hashCode * 59 + this.RealRevenues.GetHashCode();
                if (this.RealCostsWithSubTree != null)
                    hashCode = hashCode * 59 + this.RealCostsWithSubTree.GetHashCode();
                if (this.RealRevenuesWithSubTree != null)
                    hashCode = hashCode * 59 + this.RealRevenuesWithSubTree.GetHashCode();
                if (this.RealCostsFromSources != null)
                    hashCode = hashCode * 59 + this.RealCostsFromSources.GetHashCode();
                if (this.RealCostsFromSourcesWSubTree != null)
                    hashCode = hashCode * 59 + this.RealCostsFromSourcesWSubTree.GetHashCode();
                if (this.CalculatedByID != null)
                    hashCode = hashCode * 59 + this.CalculatedByID.GetHashCode();
                if (this.CalculatedWhenDATE != null)
                    hashCode = hashCode * 59 + this.CalculatedWhenDATE.GetHashCode();
                return hashCode;
            }
        }
    }

}
